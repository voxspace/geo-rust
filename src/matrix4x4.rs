/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::vector4::Vector4;
use std::ops::{AddAssign, SubAssign, MulAssign, DivAssign, Index, IndexMut, Add, Neg, Sub, Mul, Div};
use crate::matrix3x3::Matrix3x3;
use crate::vector3::Vector3;

///
/// # 4-D matrix class.
///
/// This class is a row-major 4-D matrix class, which means each element of
/// the matrix is stored in order of (0,0), ... , (0,3), (1,0), ... , (3,3).
/// Also, this 4-D matrix is specialized for geometric transformations.
/// - tparam T - Type of the element.
///
#[derive(Clone, Copy)]
pub struct Matrix4x4<T: Float> {
    _elements: [T; 16],
}

/// Float-type 4x4 matrix.
pub type Matrix4x4F = Matrix4x4<f32>;

/// Double-type 4x4 matrix.
pub type Matrix4x4D = Matrix4x4<f64>;

impl<T: Float> Default for Matrix4x4<T> {
    /// Constructs identity matrix.
    fn default() -> Self {
        return Matrix4x4 {
            _elements: [T::one(), T::zero(), T::zero(), T::zero(),
                T::zero(), T::one(), T::zero(), T::zero(),
                T::zero(), T::zero(), T::one(), T::zero(),
                T::zero(), T::zero(), T::zero(), T::one()]
        };
    }
}

/// # Constructors
impl<T: Float> Matrix4x4<T> {
    /// Constructs constant value matrix.
    pub fn new_scalar(s: T) -> Matrix4x4<T> {
        return Matrix4x4 {
            _elements: [s, s, s, s,
                s, s, s, s,
                s, s, s, s,
                s, s, s, s]
        };
    }

    /// Constructs a matrix with input elements.
    /// \warning Ordering of the input elements is row-major.
    pub fn new(m00: T, m01: T, m02: T, m03: T,
               m10: T, m11: T, m12: T, m13: T,
               m20: T, m21: T, m22: T, m23: T,
               m30: T, m31: T, m32: T, m33: T) -> Matrix4x4<T> {
        return Matrix4x4 {
            _elements: [m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33]
        };
    }

    ///
    /// Constructs a matrix with given initializer list **lst**.
    ///
    /// This constructor will build a matrix with given initializer list **lst**
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::matrix4x4::Matrix4x4F;
    /// let arr = Matrix4x4F::new_slice(
    ///                             &[&[1.0, 2.0, 3.0, 4.0],
    ///                             &[5.0, 6.0, 7.0, 8.0],
    ///                             &[9.0, 10.0, 11.0, 12.0],
    ///                             &[13.0, 14.0, 15.0, 16.0]]);
    /// ```
    ///
    /// Note the initializer also has 2x2 structure.
    ///
    /// - parameter: lst Initializer list that should be copy to the new matrix.
    ///
    pub fn new_slice(lst: &[&[T]]) -> Matrix4x4<T> {
        return Matrix4x4 {
            _elements: [lst[0][0], lst[0][1], lst[0][2], lst[0][3],
                lst[1][0], lst[1][1], lst[1][2], lst[1][3],
                lst[2][0], lst[2][1], lst[2][2], lst[2][3],
                lst[3][0], lst[3][1], lst[3][2], lst[3][3]]
        };
    }

    /// Constructs a matrix with 3x3 matrix.
    /// This constructor initialize 3x3 part, and other parts are set to 0
    /// except (3,3) which is set to 1.
    pub fn new_mat(m33: Matrix3x3<T>) -> Matrix4x4<T> {
        return Matrix4x4 {
            _elements: [m33[0], m33[1], m33[2], T::zero(),
                m33[3], m33[4], m33[5], T::zero(),
                m33[6], m33[7], m33[8], T::zero(),
                T::zero(), T::zero(), T::zero(), T::one()]
        };
    }

    /// Constructs a matrix with input array.
    /// \warning Ordering of the input elements is row-major.
    pub fn new_array(arr: &[T; 16]) -> Matrix4x4<T> {
        return Matrix4x4 {
            _elements: *arr
        };
    }
}

/// # Basic setters
impl<T: Float> Matrix4x4<T> {
    /// Sets whole matrix with input scalar.
    pub fn set_scalar(&mut self, s: T) {
        self._elements[0] = s;
        self._elements[1] = s;
        self._elements[2] = s;
        self._elements[3] = s;

        self._elements[4] = s;
        self._elements[5] = s;
        self._elements[6] = s;
        self._elements[7] = s;

        self._elements[8] = s;
        self._elements[9] = s;
        self._elements[10] = s;
        self._elements[11] = s;

        self._elements[12] = s;
        self._elements[13] = s;
        self._elements[14] = s;
        self._elements[15] = s;
    }

    /// Sets this matrix with input elements.
    /// \warning Ordering of the input elements is row-major.
    pub fn set(&mut self, m00: T, m01: T, m02: T, m03: T,
               m10: T, m11: T, m12: T, m13: T,
               m20: T, m21: T, m22: T, m23: T,
               m30: T, m31: T, m32: T, m33: T) {
        self._elements[0] = m00;
        self._elements[1] = m01;
        self._elements[2] = m02;
        self._elements[3] = m03;

        self._elements[4] = m10;
        self._elements[5] = m11;
        self._elements[6] = m12;
        self._elements[7] = m13;

        self._elements[8] = m20;
        self._elements[9] = m21;
        self._elements[10] = m22;
        self._elements[11] = m23;

        self._elements[12] = m30;
        self._elements[13] = m31;
        self._elements[14] = m32;
        self._elements[15] = m33;
    }

    ///
    /// \brief Sets this matrix with given initializer list **lst**.
    ///
    /// This function will fill the matrix with given initializer list **lst**
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::matrix3x3::Matrix3x3F;
    /// let mut arr =  Matrix3x3F::default();
    /// arr.set_slice(&[
    ///     &[1.0, 2.0, 3.0],
    ///     &[9.0, 3.0, 4.0],
    ///     &[5.0, 6.0, 2.0]
    /// ]);
    /// ```
    ///
    /// Note the initializer also has 2x2 structure.
    ///
    /// - parameter: lst Initializer list that should be copy to the matrix.
    pub fn set_slice(&mut self, lst: &[&[T]]) {
        self._elements[0] = lst[0][0];
        self._elements[1] = lst[0][1];
        self._elements[2] = lst[0][2];
        self._elements[3] = lst[0][3];

        self._elements[4] = lst[1][0];
        self._elements[5] = lst[1][1];
        self._elements[6] = lst[1][2];
        self._elements[7] = lst[1][3];

        self._elements[8] = lst[2][0];
        self._elements[9] = lst[2][1];
        self._elements[10] = lst[2][2];
        self._elements[11] = lst[2][3];

        self._elements[12] = lst[3][0];
        self._elements[13] = lst[3][1];
        self._elements[14] = lst[3][2];
        self._elements[15] = lst[3][3];
    }

    /// Sets this matrix with input 3x3 matrix.
    /// This method copies 3x3 part only, and other parts are set to 0
    /// except (3,3) which will be set to 1.
    pub fn set_mat(&mut self, m33: &Matrix3x3<T>) {
        self.set(m33[0], m33[1], m33[2], T::zero(),
                 m33[3], m33[4], m33[5], T::zero(),
                 m33[6], m33[7], m33[8], T::zero(),
                 T::zero(), T::zero(), T::zero(), T::one());
    }

    /// Copies from input matrix.
    pub fn set_self(&mut self, m: &Matrix4x4<T>) {
        self._elements = m._elements;
    }

    /// Copies from input array.
    /// *warning* Ordering of the input elements is row-major.
    pub fn set_array(&mut self, arr: &[T; 16]) {
        self._elements = *arr;
    }

    /// Sets diagonal elements with input scalar.
    pub fn set_diagonal(&mut self, s: T) {
        self._elements[0] = s;
        self._elements[5] = s;
        self._elements[10] = s;
        self._elements[15] = s;
    }

    /// Sets off-diagonal elements with input scalar.
    pub fn set_off_diagonal(&mut self, s: T) {
        self._elements[1] = s;
        self._elements[2] = s;
        self._elements[3] = s;
        self._elements[4] = s;
        self._elements[6] = s;
        self._elements[7] = s;
        self._elements[8] = s;
        self._elements[9] = s;
        self._elements[11] = s;
        self._elements[12] = s;
        self._elements[13] = s;
        self._elements[14] = s;
    }

    /// Sets i-th row with input vector.
    pub fn set_row(&mut self, i: usize, row: &Vector4<T>) {
        self._elements[4 * i] = row.x;
        self._elements[4 * i + 1] = row.y;
        self._elements[4 * i + 2] = row.z;
        self._elements[4 * i + 3] = row.w;
    }

    /// Sets i-th column with input vector.
    pub fn set_column(&mut self, j: usize, col: &Vector4<T>) {
        self._elements[j] = col.x;
        self._elements[j + 4] = col.y;
        self._elements[j + 8] = col.z;
        self._elements[j + 12] = col.w;
    }
}

/// # Basic getters
impl<T: Float> Matrix4x4<T> {
    /// Returns true if this matrix is similar to the input matrix within the
    /// given tolerance.
    pub fn is_similar(&self, m: &Matrix4x4<T>, tol: Option<T>) -> bool {
        return
            T::abs(self._elements[0] - m._elements[0]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[1] - m._elements[1]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[2] - m._elements[2]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[3] - m._elements[3]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[4] - m._elements[4]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[5] - m._elements[5]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[6] - m._elements[6]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[7] - m._elements[7]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[8] - m._elements[8]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[9] - m._elements[9]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[10] - m._elements[10]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[11] - m._elements[11]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[12] - m._elements[12]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[13] - m._elements[13]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[14] - m._elements[14]) < tol.unwrap_or(T::epsilon())
                && T::abs(self._elements[15] - m._elements[15]) < tol.unwrap_or(T::epsilon());
    }

    /// Returns true if this matrix is a square matrix.
    pub fn is_square(&self) -> bool {
        return true;
    }

    /// Returns number of rows of this matrix.
    pub fn rows(&self) -> usize {
        return 4;
    }

    /// Returns number of columns of this matrix.
    pub fn cols(&self) -> usize {
        return 4;
    }

    /// Returns data pointer of this matrix.
    pub fn data_mut(&mut self) -> &mut [T] {
        return &mut self._elements;
    }

    /// Returns constant pointer of this matrix.
    pub fn data(&self) -> &[T] {
        return &self._elements;
    }

    /// Returns 3x3 part of this matrix.
    pub fn matrix3(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(
            self._elements[0], self._elements[1], self._elements[2],
            self._elements[4], self._elements[5], self._elements[6],
            self._elements[8], self._elements[9], self._elements[10]);
    }
}

/// # Binary operator methods - new instance = this instance (+) input
impl<T: Float> Matrix4x4<T> {
    /// Returns this matrix + input scalar.
    pub fn add_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0] + s,
                              self._elements[1] + s,
                              self._elements[2] + s,
                              self._elements[3] + s,
                              self._elements[4] + s,
                              self._elements[5] + s,
                              self._elements[6] + s,
                              self._elements[7] + s,
                              self._elements[8] + s,
                              self._elements[9] + s,
                              self._elements[10] + s,
                              self._elements[11] + s,
                              self._elements[12] + s,
                              self._elements[13] + s,
                              self._elements[14] + s,
                              self._elements[15] + s);
    }

    /// Returns this matrix + input matrix (element-wise).
    pub fn add_mat(&self, m: &Matrix4x4<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0] + m._elements[0],
                              self._elements[1] + m._elements[1],
                              self._elements[2] + m._elements[2],
                              self._elements[3] + m._elements[3],
                              self._elements[4] + m._elements[4],
                              self._elements[5] + m._elements[5],
                              self._elements[6] + m._elements[6],
                              self._elements[7] + m._elements[7],
                              self._elements[8] + m._elements[8],
                              self._elements[9] + m._elements[9],
                              self._elements[10] + m._elements[10],
                              self._elements[11] + m._elements[11],
                              self._elements[12] + m._elements[12],
                              self._elements[13] + m._elements[13],
                              self._elements[14] + m._elements[14],
                              self._elements[15] + m._elements[15]);
    }

    /// Returns this matrix - input scalar.
    pub fn sub_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0] - s, self._elements[1] - s, self._elements[2] - s, self._elements[3] - s,
                              self._elements[4] - s, self._elements[5] - s, self._elements[6] - s, self._elements[7] - s,
                              self._elements[8] - s, self._elements[9] - s, self._elements[10] - s, self._elements[11] - s,
                              self._elements[12] - s, self._elements[13] - s, self._elements[14] - s, self._elements[15] - s);
    }

    /// Returns this matrix - input matrix (element-wise).
    pub fn sub_mat(&self, m: &Matrix4x4<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0] - m._elements[0],
                              self._elements[1] - m._elements[1],
                              self._elements[2] - m._elements[2],
                              self._elements[3] - m._elements[3],
                              self._elements[4] - m._elements[4],
                              self._elements[5] - m._elements[5],
                              self._elements[6] - m._elements[6],
                              self._elements[7] - m._elements[7],
                              self._elements[8] - m._elements[8],
                              self._elements[9] - m._elements[9],
                              self._elements[10] - m._elements[10],
                              self._elements[11] - m._elements[11],
                              self._elements[12] - m._elements[12],
                              self._elements[13] - m._elements[13],
                              self._elements[14] - m._elements[14],
                              self._elements[15] - m._elements[15]);
    }

    /// Returns this matrix * input scalar.
    pub fn mul_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0] * s, self._elements[1] * s, self._elements[2] * s, self._elements[3] * s,
                              self._elements[4] * s, self._elements[5] * s, self._elements[6] * s, self._elements[7] * s,
                              self._elements[8] * s, self._elements[9] * s, self._elements[10] * s, self._elements[11] * s,
                              self._elements[12] * s, self._elements[13] * s, self._elements[14] * s, self._elements[15] * s);
    }

    /// Returns this matrix * input vector.
    pub fn mul_vec(&self, v: &Vector4<T>) -> Vector4<T> {
        return Vector4::new(self._elements[0] * v.x + self._elements[1] * v.y + self._elements[2] * v.z + self._elements[3] * v.w,
                            self._elements[4] * v.x + self._elements[5] * v.y + self._elements[6] * v.z + self._elements[7] * v.w,
                            self._elements[8] * v.x + self._elements[9] * v.y + self._elements[10] * v.z + self._elements[11] * v.w,
                            self._elements[12] * v.x + self._elements[13] * v.y + self._elements[14] * v.z + self._elements[15] * v.w);
    }

    /// Returns this matrix * input matrix.
    pub fn mul_mat(&self, m: &Matrix4x4<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(
            self._elements[0] * m._elements[0] + self._elements[1] * m._elements[4] + self._elements[2] * m._elements[8] + self._elements[3] * m._elements[12],
            self._elements[0] * m._elements[1] + self._elements[1] * m._elements[5] + self._elements[2] * m._elements[9] + self._elements[3] * m._elements[13],
            self._elements[0] * m._elements[2] + self._elements[1] * m._elements[6] + self._elements[2] * m._elements[10] + self._elements[3] * m._elements[14],
            self._elements[0] * m._elements[3] + self._elements[1] * m._elements[7] + self._elements[2] * m._elements[11] + self._elements[3] * m._elements[15],
            self._elements[4] * m._elements[0] + self._elements[5] * m._elements[4] + self._elements[6] * m._elements[8] + self._elements[7] * m._elements[12],
            self._elements[4] * m._elements[1] + self._elements[5] * m._elements[5] + self._elements[6] * m._elements[9] + self._elements[7] * m._elements[13],
            self._elements[4] * m._elements[2] + self._elements[5] * m._elements[6] + self._elements[6] * m._elements[10] + self._elements[7] * m._elements[14],
            self._elements[4] * m._elements[3] + self._elements[5] * m._elements[7] + self._elements[6] * m._elements[11] + self._elements[7] * m._elements[15],
            self._elements[8] * m._elements[0] + self._elements[9] * m._elements[4] + self._elements[10] * m._elements[8] + self._elements[11] * m._elements[12],
            self._elements[8] * m._elements[1] + self._elements[9] * m._elements[5] + self._elements[10] * m._elements[9] + self._elements[11] * m._elements[13],
            self._elements[8] * m._elements[2] + self._elements[9] * m._elements[6] + self._elements[10] * m._elements[10] + self._elements[11] * m._elements[14],
            self._elements[8] * m._elements[3] + self._elements[9] * m._elements[7] + self._elements[10] * m._elements[11] + self._elements[11] * m._elements[15],
            self._elements[12] * m._elements[0] + self._elements[13] * m._elements[4] + self._elements[14] * m._elements[8] + self._elements[15] * m._elements[12],
            self._elements[12] * m._elements[1] + self._elements[13] * m._elements[5] + self._elements[14] * m._elements[9] + self._elements[15] * m._elements[13],
            self._elements[12] * m._elements[2] + self._elements[13] * m._elements[6] + self._elements[14] * m._elements[10] + self._elements[15] * m._elements[14],
            self._elements[12] * m._elements[3] + self._elements[13] * m._elements[7] + self._elements[14] * m._elements[11] + self._elements[15] * m._elements[15]);
    }

    /// Returns this matrix / input scalar.
    pub fn div_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0] / s, self._elements[1] / s, self._elements[2] / s, self._elements[3] / s,
                              self._elements[4] / s, self._elements[5] / s, self._elements[6] / s, self._elements[7] / s,
                              self._elements[8] / s, self._elements[9] / s, self._elements[10] / s, self._elements[11] / s,
                              self._elements[12] / s, self._elements[13] / s, self._elements[14] / s, self._elements[15] / s);
    }
}

/// # Binary operator methods - new instance = input (+) this instance
impl<T: Float> Matrix4x4<T> {
    /// Returns input scalar + this matrix.
    pub fn radd_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(s + self._elements[0], s + self._elements[1], s + self._elements[2], s + self._elements[3],
                              s + self._elements[4], s + self._elements[5], s + self._elements[6], s + self._elements[7],
                              s + self._elements[8], s + self._elements[9], s + self._elements[10], s + self._elements[11],
                              s + self._elements[12], s + self._elements[13], s + self._elements[14], s + self._elements[15]);
    }

    /// Returns input matrix + this matrix (element-wise).
    pub fn radd_mat(&self, m: &Matrix4x4<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(m._elements[0] + self._elements[0], m._elements[1] + self._elements[1], m._elements[2] + self._elements[2], m._elements[3] + self._elements[3],
                              m._elements[4] + self._elements[4], m._elements[5] + self._elements[5], m._elements[6] + self._elements[6], m._elements[7] + self._elements[7],
                              m._elements[8] + self._elements[8], m._elements[9] + self._elements[9], m._elements[10] + self._elements[10], m._elements[11] + self._elements[11],
                              m._elements[12] + self._elements[12], m._elements[13] + self._elements[13], m._elements[14] + self._elements[14], m._elements[15] + self._elements[15]);
    }

    /// Returns input scalar - this matrix.
    pub fn rsub_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(s - self._elements[0], s - self._elements[1], s - self._elements[2], s - self._elements[3],
                              s - self._elements[4], s - self._elements[5], s - self._elements[6], s - self._elements[7],
                              s - self._elements[8], s - self._elements[9], s - self._elements[10], s - self._elements[11],
                              s - self._elements[12], s - self._elements[13], s - self._elements[14], s - self._elements[15]);
    }

    /// Returns input matrix - this matrix (element-wise).
    pub fn rsub_mat(&self, m: &Matrix4x4<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(m._elements[0] - self._elements[0], m._elements[1] - self._elements[1], m._elements[2] - self._elements[2], m._elements[3] - self._elements[3],
                              m._elements[4] - self._elements[4], m._elements[5] - self._elements[5], m._elements[6] - self._elements[6], m._elements[7] - self._elements[7],
                              m._elements[8] - self._elements[8], m._elements[9] - self._elements[9], m._elements[10] - self._elements[10], m._elements[11] - self._elements[11],
                              m._elements[12] - self._elements[12], m._elements[13] - self._elements[13], m._elements[14] - self._elements[14], m._elements[15] - self._elements[15]);
    }

    /// Returns input scalar * this matrix.
    pub fn rmul_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(s * self._elements[0], s * self._elements[1], s * self._elements[2], s * self._elements[3],
                              s * self._elements[4], s * self._elements[5], s * self._elements[6], s * self._elements[7],
                              s * self._elements[8], s * self._elements[9], s * self._elements[10], s * self._elements[11],
                              s * self._elements[12], s * self._elements[13], s * self._elements[14], s * self._elements[15]);
    }

    /// Returns input matrix * this matrix.
    pub fn rmul_mat(&self, m: &Matrix4x4<T>) -> Matrix4x4<T> {
        return m.mul_mat(self);
    }

    /// Returns input scalar / this matrix.
    pub fn rdiv_scalar(&self, s: T) -> Matrix4x4<T> {
        return Matrix4x4::new(s / self._elements[0], s / self._elements[1], s / self._elements[2], s / self._elements[3],
                              s / self._elements[4], s / self._elements[5], s / self._elements[6], s / self._elements[7],
                              s / self._elements[8], s / self._elements[9], s / self._elements[10], s / self._elements[11],
                              s / self._elements[12], s / self._elements[13], s / self._elements[14], s / self._elements[15]);
    }
}

/// # Augmented operator methods - this instance (+)= input
impl<T: Float> Matrix4x4<T> {
    /// Adds input scalar to this matrix.
    pub fn iadd_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] + s;
        self._elements[1] = self._elements[1] + s;
        self._elements[2] = self._elements[2] + s;
        self._elements[3] = self._elements[3] + s;

        self._elements[4] = self._elements[4] + s;
        self._elements[5] = self._elements[5] + s;
        self._elements[6] = self._elements[6] + s;
        self._elements[7] = self._elements[7] + s;

        self._elements[8] = self._elements[8] + s;
        self._elements[9] = self._elements[9] + s;
        self._elements[10] = self._elements[10] + s;
        self._elements[11] = self._elements[11] + s;

        self._elements[12] = self._elements[12] + s;
        self._elements[13] = self._elements[13] + s;
        self._elements[14] = self._elements[14] + s;
        self._elements[15] = self._elements[15] + s;
    }

    /// Adds input matrix to this matrix (element-wise).
    pub fn iadd_mat(&mut self, m: &Matrix4x4<T>) {
        self._elements[0] = self._elements[0] + m._elements[0];
        self._elements[1] = self._elements[1] + m._elements[1];
        self._elements[2] = self._elements[2] + m._elements[2];
        self._elements[3] = self._elements[3] + m._elements[3];

        self._elements[4] = self._elements[4] + m._elements[4];
        self._elements[5] = self._elements[5] + m._elements[5];
        self._elements[6] = self._elements[6] + m._elements[6];
        self._elements[7] = self._elements[7] + m._elements[7];

        self._elements[8] = self._elements[8] + m._elements[8];
        self._elements[9] = self._elements[9] + m._elements[9];
        self._elements[10] = self._elements[10] + m._elements[10];
        self._elements[11] = self._elements[11] + m._elements[11];

        self._elements[12] = self._elements[12] + m._elements[12];
        self._elements[13] = self._elements[13] + m._elements[13];
        self._elements[14] = self._elements[14] + m._elements[14];
        self._elements[15] = self._elements[15] + m._elements[15];
    }

    /// Subtracts input scalar from this matrix.
    pub fn isub_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] - s;
        self._elements[1] = self._elements[1] - s;
        self._elements[2] = self._elements[2] - s;
        self._elements[3] = self._elements[3] - s;

        self._elements[4] = self._elements[4] - s;
        self._elements[5] = self._elements[5] - s;
        self._elements[6] = self._elements[6] - s;
        self._elements[7] = self._elements[7] - s;

        self._elements[8] = self._elements[8] - s;
        self._elements[9] = self._elements[9] - s;
        self._elements[10] = self._elements[10] - s;
        self._elements[11] = self._elements[11] - s;

        self._elements[12] = self._elements[12] - s;
        self._elements[13] = self._elements[13] - s;
        self._elements[14] = self._elements[14] - s;
        self._elements[15] = self._elements[15] - s;
    }

    /// Subtracts input matrix from this matrix (element-wise).
    pub fn isub_mat(&mut self, m: &Matrix4x4<T>) {
        self._elements[0] = self._elements[0] - m._elements[0];
        self._elements[1] = self._elements[1] - m._elements[1];
        self._elements[2] = self._elements[2] - m._elements[2];
        self._elements[3] = self._elements[3] - m._elements[3];

        self._elements[4] = self._elements[4] - m._elements[4];
        self._elements[5] = self._elements[5] - m._elements[5];
        self._elements[6] = self._elements[6] - m._elements[6];
        self._elements[7] = self._elements[7] - m._elements[7];

        self._elements[8] = self._elements[8] - m._elements[8];
        self._elements[9] = self._elements[9] - m._elements[9];
        self._elements[10] = self._elements[10] - m._elements[10];
        self._elements[11] = self._elements[11] - m._elements[11];

        self._elements[12] = self._elements[12] - m._elements[12];
        self._elements[13] = self._elements[13] - m._elements[13];
        self._elements[14] = self._elements[14] - m._elements[14];
        self._elements[15] = self._elements[15] - m._elements[15];
    }

    /// Multiplies input scalar to this matrix.
    pub fn imul_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] * s;
        self._elements[1] = self._elements[1] * s;
        self._elements[2] = self._elements[2] * s;
        self._elements[3] = self._elements[3] * s;

        self._elements[4] = self._elements[4] * s;
        self._elements[5] = self._elements[5] * s;
        self._elements[6] = self._elements[6] * s;
        self._elements[7] = self._elements[7] * s;

        self._elements[8] = self._elements[8] * s;
        self._elements[9] = self._elements[9] * s;
        self._elements[10] = self._elements[10] * s;
        self._elements[11] = self._elements[11] * s;

        self._elements[12] = self._elements[12] * s;
        self._elements[13] = self._elements[13] * s;
        self._elements[14] = self._elements[14] * s;
        self._elements[15] = self._elements[15] * s;
    }

    /// Multiplies input matrix to this matrix.
    pub fn imul_mat(&mut self, m: &Matrix4x4<T>) {
        self.set_self(&self.mul_mat(m));
    }

    /// Divides this matrix with input scalar.
    pub fn idiv_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] / s;
        self._elements[1] = self._elements[1] / s;
        self._elements[2] = self._elements[2] / s;
        self._elements[3] = self._elements[3] / s;

        self._elements[4] = self._elements[4] / s;
        self._elements[5] = self._elements[5] / s;
        self._elements[6] = self._elements[6] / s;
        self._elements[7] = self._elements[7] / s;

        self._elements[8] = self._elements[8] / s;
        self._elements[9] = self._elements[9] / s;
        self._elements[10] = self._elements[10] / s;
        self._elements[11] = self._elements[11] / s;

        self._elements[12] = self._elements[12] / s;
        self._elements[13] = self._elements[13] / s;
        self._elements[14] = self._elements[14] / s;
        self._elements[15] = self._elements[15] / s;
    }
}

/// # Modifiers
impl<T: Float> Matrix4x4<T> {
    /// Transposes this matrix.
    pub fn transpose(&mut self) {
        let temp = self._elements[1];
        self._elements[1] = self._elements[4];
        self._elements[4] = temp;

        let temp = self._elements[2];
        self._elements[2] = self._elements[8];
        self._elements[8] = temp;

        let temp = self._elements[3];
        self._elements[3] = self._elements[12];
        self._elements[12] = temp;

        let temp = self._elements[6];
        self._elements[6] = self._elements[9];
        self._elements[9] = temp;

        let temp = self._elements[7];
        self._elements[7] = self._elements[13];
        self._elements[13] = temp;

        let temp = self._elements[11];
        self._elements[11] = self._elements[14];
        self._elements[14] = temp;
    }

    /// Inverts this matrix.
    pub fn invert(&mut self) {
        let d = self.determinant();
        let mut m = Matrix4x4::default();
        m._elements[0] = self._elements[5] * self._elements[10] * self._elements[15] + self._elements[6] * self._elements[11] * self._elements[13] + self._elements[7] * self._elements[9] * self._elements[14] - self._elements[5] * self._elements[11] * self._elements[14] - self._elements[6] * self._elements[9] * self._elements[15] - self._elements[7] * self._elements[10] * self._elements[13];
        m._elements[1] = self._elements[1] * self._elements[11] * self._elements[14] + self._elements[2] * self._elements[9] * self._elements[15] + self._elements[3] * self._elements[10] * self._elements[13] - self._elements[1] * self._elements[10] * self._elements[15] - self._elements[2] * self._elements[11] * self._elements[13] - self._elements[3] * self._elements[9] * self._elements[14];
        m._elements[2] = self._elements[1] * self._elements[6] * self._elements[15] + self._elements[2] * self._elements[7] * self._elements[13] + self._elements[3] * self._elements[5] * self._elements[14] - self._elements[1] * self._elements[7] * self._elements[14] - self._elements[2] * self._elements[5] * self._elements[15] - self._elements[3] * self._elements[6] * self._elements[13];
        m._elements[3] = self._elements[1] * self._elements[7] * self._elements[10] + self._elements[2] * self._elements[5] * self._elements[11] + self._elements[3] * self._elements[6] * self._elements[9] - self._elements[1] * self._elements[6] * self._elements[11] - self._elements[2] * self._elements[7] * self._elements[9] - self._elements[3] * self._elements[5] * self._elements[10];
        m._elements[4] = self._elements[4] * self._elements[11] * self._elements[14] + self._elements[6] * self._elements[8] * self._elements[15] + self._elements[7] * self._elements[10] * self._elements[12] - self._elements[4] * self._elements[10] * self._elements[15] - self._elements[6] * self._elements[11] * self._elements[12] - self._elements[7] * self._elements[8] * self._elements[14];
        m._elements[5] = self._elements[0] * self._elements[10] * self._elements[15] + self._elements[2] * self._elements[11] * self._elements[12] + self._elements[3] * self._elements[8] * self._elements[14] - self._elements[0] * self._elements[11] * self._elements[14] - self._elements[2] * self._elements[8] * self._elements[15] - self._elements[3] * self._elements[10] * self._elements[12];
        m._elements[6] = self._elements[0] * self._elements[7] * self._elements[14] + self._elements[2] * self._elements[4] * self._elements[15] + self._elements[3] * self._elements[6] * self._elements[12] - self._elements[0] * self._elements[6] * self._elements[15] - self._elements[2] * self._elements[7] * self._elements[12] - self._elements[3] * self._elements[4] * self._elements[14];
        m._elements[7] = self._elements[0] * self._elements[6] * self._elements[11] + self._elements[2] * self._elements[7] * self._elements[8] + self._elements[3] * self._elements[4] * self._elements[10] - self._elements[0] * self._elements[7] * self._elements[10] - self._elements[2] * self._elements[4] * self._elements[11] - self._elements[3] * self._elements[6] * self._elements[8];
        m._elements[8] = self._elements[4] * self._elements[9] * self._elements[15] + self._elements[5] * self._elements[11] * self._elements[12] + self._elements[7] * self._elements[8] * self._elements[13] - self._elements[4] * self._elements[11] * self._elements[13] - self._elements[5] * self._elements[8] * self._elements[15] - self._elements[7] * self._elements[9] * self._elements[12];
        m._elements[9] = self._elements[0] * self._elements[11] * self._elements[13] + self._elements[1] * self._elements[8] * self._elements[15] + self._elements[3] * self._elements[9] * self._elements[12] - self._elements[0] * self._elements[9] * self._elements[15] - self._elements[1] * self._elements[11] * self._elements[12] - self._elements[3] * self._elements[8] * self._elements[13];
        m._elements[10] = self._elements[0] * self._elements[5] * self._elements[15] + self._elements[1] * self._elements[7] * self._elements[12] + self._elements[3] * self._elements[4] * self._elements[13] - self._elements[0] * self._elements[7] * self._elements[13] - self._elements[1] * self._elements[4] * self._elements[15] - self._elements[3] * self._elements[5] * self._elements[12];
        m._elements[11] = self._elements[0] * self._elements[7] * self._elements[9] + self._elements[1] * self._elements[4] * self._elements[11] + self._elements[3] * self._elements[5] * self._elements[8] - self._elements[0] * self._elements[5] * self._elements[11] - self._elements[1] * self._elements[7] * self._elements[8] - self._elements[3] * self._elements[4] * self._elements[9];
        m._elements[12] = self._elements[4] * self._elements[10] * self._elements[13] + self._elements[5] * self._elements[8] * self._elements[14] + self._elements[6] * self._elements[9] * self._elements[12] - self._elements[4] * self._elements[9] * self._elements[14] - self._elements[5] * self._elements[10] * self._elements[12] - self._elements[6] * self._elements[8] * self._elements[13];
        m._elements[13] = self._elements[0] * self._elements[9] * self._elements[14] + self._elements[1] * self._elements[10] * self._elements[12] + self._elements[2] * self._elements[8] * self._elements[13] - self._elements[0] * self._elements[10] * self._elements[13] - self._elements[1] * self._elements[8] * self._elements[14] - self._elements[2] * self._elements[9] * self._elements[12];
        m._elements[14] = self._elements[0] * self._elements[6] * self._elements[13] + self._elements[1] * self._elements[4] * self._elements[14] + self._elements[2] * self._elements[5] * self._elements[12] - self._elements[0] * self._elements[5] * self._elements[14] - self._elements[1] * self._elements[6] * self._elements[12] - self._elements[2] * self._elements[4] * self._elements[13];
        m._elements[15] = self._elements[0] * self._elements[5] * self._elements[10] + self._elements[1] * self._elements[6] * self._elements[8] + self._elements[2] * self._elements[4] * self._elements[9] - self._elements[0] * self._elements[6] * self._elements[9] - self._elements[1] * self._elements[4] * self._elements[10] - self._elements[2] * self._elements[5] * self._elements[8];
        m.idiv_scalar(d);

        self.set_self(&m);
    }
}

/// # Complex getters
impl<T: Float> Matrix4x4<T> {
    /// Returns sum of all elements.
    pub fn sum(&self) -> T {
        let mut s = T::zero();
        for i in 0..16 {
            s = s + self._elements[i];
        }
        return s;
    }

    /// Returns average of all elements.
    pub fn avg(&self) -> T {
        return self.sum() / T::from(16.0).unwrap();
    }

    /// Returns minimum among all elements.
    pub fn min(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = T::min(m, self._elements[i]);
        }
        return m;
    }

    /// Returns maximum among all elements.
    pub fn max(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = T::max(m, self._elements[i]);
        }
        return m;
    }

    /// Returns absolute minimum among all elements.
    pub fn absmin(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = crate::math_utils::absmin(m, self._elements[i]);
        }
        return m;
    }

    /// Returns absolute maximum among all elements.
    pub fn absmax(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = crate::math_utils::absmax(m, self._elements[i]);
        }
        return m;
    }

    /// Returns sum of all diagonal elements.
    pub fn trace(&self) -> T {
        return self._elements[0] + self._elements[5] + self._elements[10] + self._elements[15];
    }

    /// Returns determinant of this matrix.
    pub fn determinant(&self) -> T {
        return self._elements[0] * self._elements[5] * self._elements[10] * self._elements[15] + self._elements[0] * self._elements[6] * self._elements[11] * self._elements[13] + self._elements[0] * self._elements[7] * self._elements[9] * self._elements[14]
            + self._elements[1] * self._elements[4] * self._elements[11] * self._elements[14] + self._elements[1] * self._elements[6] * self._elements[8] * self._elements[15] + self._elements[1] * self._elements[7] * self._elements[10] * self._elements[12]
            + self._elements[2] * self._elements[4] * self._elements[9] * self._elements[15] + self._elements[2] * self._elements[5] * self._elements[11] * self._elements[12] + self._elements[2] * self._elements[7] * self._elements[8] * self._elements[13]
            + self._elements[3] * self._elements[4] * self._elements[10] * self._elements[13] + self._elements[3] * self._elements[5] * self._elements[8] * self._elements[14] + self._elements[3] * self._elements[6] * self._elements[9] * self._elements[12]
            - self._elements[0] * self._elements[5] * self._elements[11] * self._elements[14] - self._elements[0] * self._elements[6] * self._elements[9] * self._elements[15] - self._elements[0] * self._elements[7] * self._elements[10] * self._elements[13]
            - self._elements[1] * self._elements[4] * self._elements[10] * self._elements[15] - self._elements[1] * self._elements[6] * self._elements[11] * self._elements[12] - self._elements[1] * self._elements[7] * self._elements[8] * self._elements[14]
            - self._elements[2] * self._elements[4] * self._elements[11] * self._elements[13] - self._elements[2] * self._elements[5] * self._elements[8] * self._elements[15] - self._elements[2] * self._elements[7] * self._elements[9] * self._elements[12]
            - self._elements[3] * self._elements[4] * self._elements[9] * self._elements[14] - self._elements[3] * self._elements[5] * self._elements[10] * self._elements[12] - self._elements[3] * self._elements[6] * self._elements[8] * self._elements[13];
    }

    /// Returns diagonal part of this matrix.
    pub fn diagonal(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0], T::zero(), T::zero(), T::zero(),
                              T::zero(), self._elements[5], T::zero(), T::zero(),
                              T::zero(), T::zero(), self._elements[10], T::zero(),
                              T::zero(), T::zero(), T::zero(), self._elements[15]);
    }

    /// Returns off-diagonal part of this matrix.
    pub fn off_diagonal(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(T::zero(), self._elements[1], self._elements[2], self._elements[3],
                              self._elements[4], T::zero(), self._elements[6], self._elements[7],
                              self._elements[8], self._elements[9], T::zero(), self._elements[11],
                              self._elements[12], self._elements[13], self._elements[14], T::zero());
    }

    /// Returns strictly lower triangle part of this matrix.
    pub fn strict_lower_tri(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(T::zero(), T::zero(), T::zero(), T::zero(),
                              self._elements[4], T::zero(), T::zero(), T::zero(),
                              self._elements[8], self._elements[9], T::zero(), T::zero(),
                              self._elements[12], self._elements[13], self._elements[14], T::zero());
    }

    /// Returns strictly upper triangle part of this matrix.
    pub fn strict_upper_tri(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(T::zero(), self._elements[1], self._elements[2], self._elements[3],
                              T::zero(), T::zero(), self._elements[6], self._elements[7],
                              T::zero(), T::zero(), T::zero(), self._elements[11],
                              T::zero(), T::zero(), T::zero(), T::zero());
    }

    /// Returns lower triangle part of this matrix (including the diagonal).
    pub fn lower_tri(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0], T::zero(), T::zero(), T::zero(),
                              self._elements[4], self._elements[5], T::zero(), T::zero(),
                              self._elements[8], self._elements[9], self._elements[10], T::zero(),
                              self._elements[12], self._elements[13], self._elements[14], self._elements[15]);
    }

    /// Returns upper triangle part of this matrix (including the diagonal).
    pub fn upper_tri(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0], self._elements[1], self._elements[2], self._elements[3],
                              T::zero(), self._elements[5], self._elements[6], self._elements[7],
                              T::zero(), T::zero(), self._elements[10], self._elements[11],
                              T::zero(), T::zero(), T::zero(), self._elements[15]);
    }

    /// Returns transposed matrix.
    pub fn transposed(&self) -> Matrix4x4<T> {
        return Matrix4x4::new(self._elements[0], self._elements[4], self._elements[8], self._elements[12],
                              self._elements[1], self._elements[5], self._elements[9], self._elements[13],
                              self._elements[2], self._elements[6], self._elements[10], self._elements[14],
                              self._elements[3], self._elements[7], self._elements[11], self._elements[15]);
    }

    /// Returns inverse matrix.
    pub fn inverse(&self) -> Matrix4x4<T> {
        let mut m = *self;
        m.invert();
        return m;
    }
}

/// # Setter operators

/// Addition assignment with input scalar.
impl<T: Float> AddAssign<T> for Matrix4x4<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Addition assignment with input matrix (element-wise).
impl<T: Float> AddAssign for Matrix4x4<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_mat(&rhs);
    }
}

/// Subtraction assignment with input scalar.
impl<T: Float> SubAssign<T> for Matrix4x4<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Subtraction assignment with input matrix (element-wise).
impl<T: Float> SubAssign for Matrix4x4<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_mat(&rhs);
    }
}

/// Multiplication assignment with input scalar.
impl<T: Float> MulAssign<T> for Matrix4x4<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Multiplication assignment with input matrix.
impl<T: Float> MulAssign for Matrix4x4<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_mat(&rhs);
    }
}

/// Division assignment with input scalar.
impl<T: Float> DivAssign<T> for Matrix4x4<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// # Getter operators
/// Returns constant reference of i-th element.
impl<T: Float> Index<usize> for Matrix4x4<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return &self._elements[index];
    }
}

/// Returns reference of i-th element.
impl<T: Float> IndexMut<usize> for Matrix4x4<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._elements[index];
    }
}

/// Returns constant reference of i-th element.
impl<T: Float> Index<(usize, usize)> for Matrix4x4<T> {
    type Output = T;
    fn index(&self, index: (usize, usize)) -> &Self::Output {
        return &self._elements[4 * index.0 + index.1];
    }
}

/// Returns reference of i-th element.
impl<T: Float> IndexMut<(usize, usize)> for Matrix4x4<T> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        return &mut self._elements[4 * index.0 + index.1];
    }
}

/// Returns true if is equal to m.
impl<T: Float> PartialEq for Matrix4x4<T> {
    fn eq(&self, m: &Self) -> bool {
        return self._elements[0] == m._elements[0] && self._elements[1] == m._elements[1] && self._elements[2] == m._elements[2] &&
            self._elements[3] == m._elements[3] && self._elements[4] == m._elements[4] && self._elements[5] == m._elements[5] &&
            self._elements[6] == m._elements[6] && self._elements[7] == m._elements[7] && self._elements[8] == m._elements[8] &&
            self._elements[9] == m._elements[9] && self._elements[10] == m._elements[10] && self._elements[11] == m._elements[11] &&
            self._elements[12] == m._elements[12] && self._elements[13] == m._elements[13] && self._elements[14] == m._elements[14] &&
            self._elements[15] == m._elements[15];
    }
}

impl<T: Float> Eq for Matrix4x4<T> {}

/// # Helpers
impl<T: Float> Matrix4x4<T> {
    /// Sets all matrix entries to zero.
    pub fn make_zero() -> Matrix4x4<T> {
        return Matrix4x4::new(T::zero(), T::zero(), T::zero(), T::zero(),
                              T::zero(), T::zero(), T::zero(), T::zero(),
                              T::zero(), T::zero(), T::zero(), T::zero(),
                              T::zero(), T::zero(), T::zero(), T::zero());
    }

    /// Makes all diagonal elements to 1, and other elements to 0.
    pub fn make_identity() -> Matrix4x4<T> {
        return Matrix4x4::new(T::one(), T::zero(), T::zero(), T::zero(),
                              T::zero(), T::one(), T::zero(), T::zero(),
                              T::zero(), T::zero(), T::one(), T::zero(),
                              T::zero(), T::zero(), T::zero(), T::one());
    }

    /// Makes scale matrix.
    pub fn make_scale_matrix_scalar(sx: T, sy: T, sz: T) -> Matrix4x4<T> {
        return Matrix4x4::new(sx, T::zero(), T::zero(), T::zero(),
                              T::zero(), sy, T::zero(), T::zero(),
                              T::zero(), T::zero(), sz, T::zero(),
                              T::zero(), T::zero(), T::zero(), T::one());
    }

    /// Makes scale matrix.
    pub fn make_scale_matrix_vec(s: Vector3<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(s.x, T::zero(), T::zero(), T::zero(),
                              T::zero(), s.y, T::zero(), T::zero(),
                              T::zero(), T::zero(), s.z, T::zero(),
                              T::zero(), T::zero(), T::zero(), T::one());
    }

    /// Makes rotation matrix.
    /// *warning* Input angle should be radian.
    pub fn make_rotation_matrix(axis: Vector3<T>, rad: T) -> Matrix4x4<T> {
        return Matrix4x4::new_mat(Matrix3x3::make_rotation_matrix(axis, rad));
    }

    /// Makes translation matrix.
    pub fn make_translation_matrix(t: Vector3<T>) -> Matrix4x4<T> {
        return Matrix4x4::new(
            T::one(), T::zero(), T::zero(), t.x,
            T::zero(), T::one(), T::zero(), t.y,
            T::zero(), T::zero(), T::one(), t.z,
            T::zero(), T::zero(), T::zero(), T::one());
    }
}


impl<T: Float> Neg for Matrix4x4<T> {
    type Output = Matrix4x4<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Matrix4x4::new(-self._elements[0], -self._elements[1], -self._elements[2], -self._elements[3],
                              -self._elements[4], -self._elements[5], -self._elements[6], -self._elements[7],
                              -self._elements[8], -self._elements[9], -self._elements[10], -self._elements[11],
                              -self._elements[12], -self._elements[13], -self._elements[14], -self._elements[15]);
    }
}

/// Returns a + b', where every element of matrix b' is b.
impl<T: Float> Add<T> for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Returns a + b (element-size).
impl<T: Float> Add for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn add(self, rhs: Self) -> Self::Output {
        return self.add_mat(&rhs);
    }
}

/// Returns a - b', where every element of matrix b' is b.
impl<T: Float> Sub<T> for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Returns a - b (element-size).
impl<T: Float> Sub for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_mat(&rhs);
    }
}

/// Returns a * b', where every element of matrix b' is b.
impl<T: Float> Mul<T> for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Returns a * b.
impl<T: Float> Mul<Vector4<T>> for Matrix4x4<T> {
    type Output = Vector4<T>;

    fn mul(self, rhs: Vector4<T>) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Returns a * b.
impl<T: Float> Mul for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_mat(&rhs);
    }
}

/// Returns a' / b, where every element of matrix a' is a.
impl<T: Float> Div<T> for Matrix4x4<T> {
    type Output = Matrix4x4<T>;

    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod matrix4x4 {
    use crate::vector3::Vector3D;
    use crate::vector4::*;
    use crate::matrix4x4::Matrix4x4D;

    #[test]
    fn constructors() {
        let mat = Matrix4x4D::default();
        for i in 0..4 {
            for j in 0..4 {
                if i == j {
                    assert_eq!(1.0, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        let mat2 = Matrix4x4D::new_scalar(3.1);
        for i in 0..16 {
            assert_eq!(3.1, mat2[i]);
        }

        let mat3 = Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                   5.0, 6.0, 7.0, 8.0,
                                   9.0, 10.0, 11.0, 12.0,
                                   13.0, 14.0, 15.0, 16.0);
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat3[i]);
        }

        let mat4 = Matrix4x4D::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0],
                &[13.0, 14.0, 15.0, 16.0]]);
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat4[i]);
        }

        let mat5 = mat4.clone();
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat5[i]);
        }

        let arr = [1.0, 2.0, 3.0, 4.0,
            5.0, 6.0, 7.0, 8.0,
            9.0, 10.0, 11.0, 12.0,
            13.0, 14.0, 15.0, 16.0];
        let mat6 = Matrix4x4D::new_array(&arr);
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat6[i]);
        }
    }

    #[test]
    fn set_methods() {
        let mut mat = Matrix4x4D::default();

        mat.set_scalar(3.1);
        for i in 0..16 {
            assert_eq!(3.1, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set(1.0, 2.0, 3.0, 4.0,
                5.0, 6.0, 7.0, 8.0,
                9.0, 10.0, 11.0, 12.0,
                13.0, 14.0, 15.0, 16.0);
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0],
            &[5.0, 6.0, 7.0, 8.0],
            &[9.0, 10.0, 11.0, 12.0],
            &[13.0, 14.0, 15.0, 16.0]]);
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_self(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                      5.0, 6.0, 7.0, 8.0,
                                      9.0, 10.0, 11.0, 12.0,
                                      13.0, 14.0, 15.0, 16.0));
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        let arr =
            [1.0, 2.0, 3.0, 4.0,
                5.0, 6.0, 7.0, 8.0,
                9.0, 10.0, 11.0, 12.0,
                13.0, 14.0, 15.0, 16.0];
        mat.set_array(&arr);
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_diagonal(3.1);
        for i in 0..4 {
            for j in 0..4 {
                if i == j {
                    assert_eq!(3.1, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat.set_scalar(0.0);
        mat.set_off_diagonal(4.2);
        for i in 0..4 {
            for j in 0..4 {
                if i != j {
                    assert_eq!(4.2, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat.set_scalar(0.0);
        mat.set_row(0, &Vector4D::new(1.0, 2.0, 3.0, 4.0));
        mat.set_row(1, &Vector4D::new(5.0, 6.0, 7.0, 8.0));
        mat.set_row(2, &Vector4D::new(9.0, 10.0, 11.0, 12.0));
        mat.set_row(3, &Vector4D::new(13.0, 14.0, 15.0, 16.0));
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_column(0, &Vector4D::new(1.0, 5.0, 9.0, 13.0));
        mat.set_column(1, &Vector4D::new(2.0, 6.0, 10.0, 14.0));
        mat.set_column(2, &Vector4D::new(3.0, 7.0, 11.0, 15.0));
        mat.set_column(3, &Vector4D::new(4.0, 8.0, 12.0, 16.0));
        for i in 0..16 {
            assert_eq!((i + 1) as f64, mat[i]);
        }
    }

    #[test]
    fn basic_getters() {
        let mat = Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                  5.0, 6.0, 7.0, 8.0,
                                  9.0, 10.0, 11.0, 12.0,
                                  13.0, 14.0, 15.0, 16.0);
        let mat2 = Matrix4x4D::new(1.01, 2.01, 2.99, 4.0,
                                   4.99, 6.001, 7.0003, 8.0,
                                   8.99, 10.01, 11.0, 11.99,
                                   13.01, 14.001, 14.999, 16.0);

        assert_eq!(mat.is_similar(&mat2, Some(0.02)), true);
        assert_eq!(mat.is_similar(&mat2, Some(0.001)), false);

        assert_eq!(mat.is_square(), true);

        assert_eq!(4, mat.rows());
        assert_eq!(4, mat.cols());
    }

    #[test]
    fn binary_operators() {
        let mat = Matrix4x4D::new(-16.0, 15.0, -14.0, 13.0,
                                  -12.0, 11.0, -10.0, 9.0,
                                  -8.0, 7.0, -6.0, 5.0,
                                  -6.0, 3.0, -2.0, 1.0);

        let mut mat2 = mat.add_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-14.0, 17.0, -12.0, 15.0],
                &[-10.0, 13.0, -8.0, 11.0],
                &[-6.0, 9.0, -4.0, 7.0],
                &[-4.0, 5.0, 0.0, 3.0]]), None), true);

        mat2 = mat.add_mat(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                            5.0, 6.0, 7.0, 8.0,
                                            9.0, 10.0, 11.0, 12.0,
                                            13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-15.0, 17.0, -11.0, 17.0],
                &[-7.0, 17.0, -3.0, 17.0],
                &[1.0, 17.0, 5.0, 17.0],
                &[7.0, 17.0, 13.0, 17.0]]), None), true);

        mat2 = mat.sub_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-18.0, 13.0, -16.0, 11.0],
                &[-14.0, 9.0, -12.0, 7.0],
                &[-10.0, 5.0, -8.0, 3.0],
                &[-8.0, 1.0, -4.0, -1.0]]), None), true);

        mat2 = mat.sub_mat(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                            5.0, 6.0, 7.0, 8.0,
                                            9.0, 10.0, 11.0, 12.0,
                                            13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-17.0, 13.0, -17.0, 9.0],
                &[-17.0, 5.0, -17.0, 1.0],
                &[-17.0, -3.0, -17.0, -7.0],
                &[-19.0, -11.0, -17.0, -15.0]]), None), true);

        mat2 = mat.mul_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-32.0, 30.0, -28.0, 26.0],
                &[-24.0, 22.0, -20.0, 18.0],
                &[-16.0, 14.0, -12.0, 10.0],
                &[-12.0, 6.0, -4.0, 2.0]]), None), true);

        let vec = mat.mul_vec(&Vector4D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(vec.is_similar(&Vector4D::new(24.0, 16.0, 8.0, -2.0), None), true);

        mat2 = mat.mul_mat(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                            5.0, 6.0, 7.0, 8.0,
                                            9.0, 10.0, 11.0, 12.0,
                                            13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[102.0, 100.0, 98.0, 96.0],
                &[70.0, 68.0, 66.0, 64.0],
                &[38.0, 36.0, 34.0, 32.0],
                &[4.0, 0.0, -4.0, -8.0]]), None), true);

        mat2 = mat.div_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-8.0, 15.0 / 2.0, -7.0, 13.0 / 2.0],
                &[-6.0, 11.0 / 2.0, -5.0, 9.0 / 2.0],
                &[-4.0, 7.0 / 2.0, -3.0, 5.0 / 2.0],
                &[-3.0, 3.0 / 2.0, -1.0, 1.0 / 2.0]]), None), true);


        mat2 = mat.radd_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-14.0, 17.0, -12.0, 15.0],
                &[-10.0, 13.0, -8.0, 11.0],
                &[-6.0, 9.0, -4.0, 7.0],
                &[-4.0, 5.0, 0.0, 3.0]]), None), true);

        mat2 = mat.radd_mat(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                             5.0, 6.0, 7.0, 8.0,
                                             9.0, 10.0, 11.0, 12.0,
                                             13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-15.0, 17.0, -11.0, 17.0],
                &[-7.0, 17.0, -3.0, 17.0],
                &[1.0, 17.0, 5.0, 17.0],
                &[7.0, 17.0, 13.0, 17.0]]), None), true);

        mat2 = mat.rsub_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[18.0, -13.0, 16.0, -11.0],
                &[14.0, -9.0, 12.0, -7.0],
                &[10.0, -5.0, 8.0, -3.0],
                &[8.0, -1.0, 4.0, 1.0]]), None), true);

        mat2 = mat.rsub_mat(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                             5.0, 6.0, 7.0, 8.0,
                                             9.0, 10.0, 11.0, 12.0,
                                             13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[17.0, -13.0, 17.0, -9.0],
                &[17.0, -5.0, 17.0, -1.0],
                &[17.0, 3.0, 17.0, 7.0],
                &[19.0, 11.0, 17.0, 15.0]]), None), true);

        mat2 = mat.rmul_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-32.0, 30.0, -28.0, 26.0],
                &[-24.0, 22.0, -20.0, 18.0],
                &[-16.0, 14.0, -12.0, 10.0],
                &[-12.0, 6.0, -4.0, 2.0]]), None), true);

        mat2 = mat.rmul_mat(&Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                                             5.0, 6.0, 7.0, 8.0,
                                             9.0, 10.0, 11.0, 12.0,
                                             13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-88.0, 70.0, -60.0, 50.0],
                &[-256.0, 214.0, -188.0, 162.0],
                &[-424.0, 358.0, -316.0, 274.0],
                &[-592.0, 502.0, -444.0, 386.0]]), None), true);

        mat2 = mat.rdiv_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-1.0 / 8.0, 2.0 / 15.0, -1.0 / 7.0, 2.0 / 13.0],
                &[-1.0 / 6.0, 2.0 / 11.0, -1.0 / 5.0, 2.0 / 9.0],
                &[-1.0 / 4.0, 2.0 / 7.0, -1.0 / 3.0, 2.0 / 5.0],
                &[-1.0 / 3.0, 2.0 / 3.0, -1.0, 2.0]]), None), true);
    }

    #[test]
    fn augmented_operators() {
        let mut mat = Matrix4x4D::new(
            -16.0, 15.0, -14.0, 13.0,
            -12.0, 11.0, -10.0, 9.0,
            -8.0, 7.0, -6.0, 5.0,
            -6.0, 3.0, -2.0, 1.0);

        mat.iadd_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-14.0, 17.0, -12.0, 15.0],
                &[-10.0, 13.0, -8.0, 11.0],
                &[-6.0, 9.0, -4.0, 7.0],
                &[-4.0, 5.0, 0.0, 3.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat.iadd_mat(
            &Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                             5.0, 6.0, 7.0, 8.0,
                             9.0, 10.0, 11.0, 12.0,
                             13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-15.0, 17.0, -11.0, 17.0],
                &[-7.0, 17.0, -3.0, 17.0],
                &[1.0, 17.0, 5.0, 17.0],
                &[7.0, 17.0, 13.0, 17.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat.isub_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-18.0, 13.0, -16.0, 11.0],
                &[-14.0, 9.0, -12.0, 7.0],
                &[-10.0, 5.0, -8.0, 3.0],
                &[-8.0, 1.0, -4.0, -1.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat.isub_mat(&
            Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                            5.0, 6.0, 7.0, 8.0,
                            9.0, 10.0, 11.0, 12.0,
                            13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-17.0, 13.0, -17.0, 9.0],
                &[-17.0, 5.0, -17.0, 1.0],
                &[-17.0, -3.0, -17.0, -7.0],
                &[-19.0, -11.0, -17.0, -15.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat.imul_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-32.0, 30.0, -28.0, 26.0],
                &[-24.0, 22.0, -20.0, 18.0],
                &[-16.0, 14.0, -12.0, 10.0],
                &[-12.0, 6.0, -4.0, 2.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat.imul_mat(&
            Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                            5.0, 6.0, 7.0, 8.0,
                            9.0, 10.0, 11.0, 12.0,
                            13.0, 14.0, 15.0, 16.0));
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[102.0, 100.0, 98.0, 96.0],
                &[70.0, 68.0, 66.0, 64.0],
                &[38.0, 36.0, 34.0, 32.0],
                &[4.0, 0.0, -4.0, -8.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat.idiv_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-8.0, 15.0 / 2.0, -7.0, 13.0 / 2.0],
                &[-6.0, 11.0 / 2.0, -5.0, 9.0 / 2.0],
                &[-4.0, 7.0 / 2.0, -3.0, 5.0 / 2.0],
                &[-3.0, 3.0 / 2.0, -1.0, 1.0 / 2.0]]), None), true);
    }

    #[test]
    fn modifiers() {
        let mut mat = Matrix4x4D::new(
            -16.0, 15.0, -14.0, 13.0,
            -12.0, 11.0, -10.0, 9.0,
            -8.0, 7.0, -6.0, 5.0,
            -6.0, 3.0, -2.0, 1.0);

        mat.transpose();
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-16.0, -12.0, -8.0, -6.0],
                &[15.0, 11.0, 7.0, 3.0],
                &[-14.0, -10.0, -6.0, -2.0],
                &[13.0, 9.0, 5.0, 1.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 6.0,
                -6.0, 3.0, -2.0, 2.0);
        mat.invert();
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-1.0 / 2.0, 1.0 / 2.0, 1.0 / 2.0, -1.0 / 2.0],
                &[-5.0 / 2.0, 5.0 / 2.0, 2.0, -1.0],
                &[-5.0 / 4.0, 1.0 / 4.0, 5.0 / 2.0, -1.0 / 2.0],
                &[1.0, -2.0, 1.0, 0.0]]), None), true);
    }

    #[test]
    fn complex_getters() {
        let mut mat = Matrix4x4D::new(
            -16.0, 15.0, -14.0, 13.0,
            -12.0, 11.0, -10.0, 9.0,
            -8.0, 7.0, -6.0, 5.0,
            -4.0, 3.0, -2.0, 1.0);

        assert_eq!(-8.0, mat.sum());

        assert_eq!(-0.5, mat.avg());

        assert_eq!(-16.0, mat.min());

        assert_eq!(15.0, mat.max());

        assert_eq!(1.0, mat.absmin());

        assert_eq!(-16.0, mat.absmax());

        assert_eq!(-10.0, mat.trace());

        assert_eq!(0.0, mat.determinant());

        let mut mat2 = mat.diagonal();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(
                -16.0, 0.0, 0.0, 0.0,
                0.0, 11.0, 0.0, 0.0,
                0.0, 0.0, -6.0, 0.0,
                0.0, 0.0, 0.0, 1.0), None), true);

        mat2 = mat.off_diagonal();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(
                0.0, 15.0, -14.0, 13.0,
                -12.0, 0.0, -10.0, 9.0,
                -8.0, 7.0, 0.0, 5.0,
                -4.0, 3.0, -2.0, 0.0), None), true);

        mat2 = mat.strict_lower_tri();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(0.0, 0.0, 0.0, 0.0,
                             -12.0, 0.0, 0.0, 0.0,
                             -8.0, 7.0, 0.0, 0.0,
                             -4.0, 3.0, -2.0, 0.0), None), true);

        mat2 = mat.strict_upper_tri();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(0.0, 15.0, -14.0, 13.0,
                             0.0, 0.0, -10.0, 9.0,
                             0.0, 0.0, 0.0, 5.0,
                             0.0, 0.0, 0.0, 0.0), None), true);

        mat2 = mat.lower_tri();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(-16.0, 0.0, 0.0, 0.0,
                             -12.0, 11.0, 0.0, 0.0,
                             -8.0, 7.0, -6.0, 0.0,
                             -4.0, 3.0, -2.0, 1.0), None), true);

        mat2 = mat.upper_tri();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(-16.0, 15.0, -14.0, 13.0,
                             0.0, 11.0, -10.0, 9.0,
                             0.0, 0.0, -6.0, 5.0,
                             0.0, 0.0, 0.0, 1.0), None), true);

        mat2 = mat.transposed();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-16.0, -12.0, -8.0, -4.0],
                &[15.0, 11.0, 7.0, 3.0],
                &[-14.0, -10.0, -6.0, -2.0],
                &[13.0, 9.0, 5.0, 1.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 6.0,
                -6.0, 3.0, -2.0, 2.0);
        mat2 = mat.inverse();
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new_slice(&[&[-1.0 / 2.0, 1.0 / 2.0, 1.0 / 2.0, -1.0 / 2.0],
                &[-5.0 / 2.0, 5.0 / 2.0, 2.0, -1.0],
                &[-5.0 / 4.0, 1.0 / 4.0, 5.0 / 2.0, -1.0 / 2.0],
                &[1.0, -2.0, 1.0, 0.0]]), None), true);
    }

    #[test]
    fn setter_operator_overloadings() {
        let mut mat = Matrix4x4D::new(-16.0, 15.0, -14.0, 13.0,
                                      -12.0, 11.0, -10.0, 9.0,
                                      -8.0, 7.0, -6.0, 5.0,
                                      -6.0, 3.0, -2.0, 1.0);

        let mat2 = mat;
        assert_eq!(mat2.is_similar(
            &Matrix4x4D::new(
                -16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0), None), true);

        mat += 2.0;
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-14.0, 17.0, -12.0, 15.0],
                &[-10.0, 13.0, -8.0, 11.0],
                &[-6.0, 9.0, -4.0, 7.0],
                &[-4.0, 5.0, 0.0, 3.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat += Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                               5.0, 6.0, 7.0, 8.0,
                               9.0, 10.0, 11.0, 12.0,
                               13.0, 14.0, 15.0, 16.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-15.0, 17.0, -11.0, 17.0],
                &[-7.0, 17.0, -3.0, 17.0],
                &[1.0, 17.0, 5.0, 17.0],
                &[7.0, 17.0, 13.0, 17.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat -= 2.0;
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-18.0, 13.0, -16.0, 11.0],
                &[-14.0, 9.0, -12.0, 7.0],
                &[-10.0, 5.0, -8.0, 3.0],
                &[-8.0, 1.0, -4.0, -1.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat -= Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                               5.0, 6.0, 7.0, 8.0,
                               9.0, 10.0, 11.0, 12.0,
                               13.0, 14.0, 15.0, 16.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-17.0, 13.0, -17.0, 9.0],
                &[-17.0, 5.0, -17.0, 1.0],
                &[-17.0, -3.0, -17.0, -7.0],
                &[-19.0, -11.0, -17.0, -15.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat *= 2.0;
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-32.0, 30.0, -28.0, 26.0],
                &[-24.0, 22.0, -20.0, 18.0],
                &[-16.0, 14.0, -12.0, 10.0],
                &[-12.0, 6.0, -4.0, 2.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat *= Matrix4x4D::new(1.0, 2.0, 3.0, 4.0,
                               5.0, 6.0, 7.0, 8.0,
                               9.0, 10.0, 11.0, 12.0,
                               13.0, 14.0, 15.0, 16.0);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[102.0, 100.0, 98.0, 96.0],
                &[70.0, 68.0, 66.0, 64.0],
                &[38.0, 36.0, 34.0, 32.0],
                &[4.0, 0.0, -4.0, -8.0]]), None), true);

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -6.0, 3.0, -2.0, 1.0);
        mat /= 2.0;
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new_slice(&[&[-8.0, 15.0 / 2.0, -7.0, 13.0 / 2.0],
                &[-6.0, 11.0 / 2.0, -5.0, 9.0 / 2.0],
                &[-4.0, 7.0 / 2.0, -3.0, 5.0 / 2.0],
                &[-3.0, 3.0 / 2.0, -1.0, 1.0 / 2.0]]), None), true);
    }

    #[test]
    fn getter_operator_overloadings() {
        let mut mat = Matrix4x4D::new(
            -16.0, 15.0, -14.0, 13.0,
            -12.0, 11.0, -10.0, 9.0,
            -8.0, 7.0, -6.0, 5.0,
            -4.0, 3.0, -2.0, 1.0);

        let mut sign = -1.0;
        for i in 0..16 {
            assert_eq!(sign * (16.0 - i as f64), mat[i]);
            sign *= -1.0;

            mat[i] *= -1.0;
        }

        sign = 1.0;
        for i in 0..16 {
            assert_eq!(sign * (16.0 - i as f64), mat[i]);
            sign *= -1.0;
        }

        sign = 1.0;
        for i in 0..4 {
            for j in 0..4 {
                let ans = sign * (16.0 - (4.0 * i as f64 + j as f64));
                assert_eq!(ans, mat[(i, j)]);
                sign *= -1.0;
            }
        }

        mat.set(-16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -4.0, 3.0, -2.0, 1.0);
        assert_eq!(
            mat == Matrix4x4D::new(
                -16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -4.0, 3.0, -2.0, 1.0), true);

        mat.set(16.0, -15.0, 14.0, -13.0,
                12.0, -11.0, 10.0, -9.0,
                -8.0, 7.0, -6.0, 5.0,
                -4.0, 3.0, -2.0, 1.0);
        assert_eq!(
            mat != Matrix4x4D::new(
                -16.0, 15.0, -14.0, 13.0,
                -12.0, 11.0, -10.0, 9.0,
                -8.0, 7.0, -6.0, 5.0,
                -4.0, 3.0, -2.0, 1.0), true);
    }

    #[test]
    fn helpers() {
        let mut mat = Matrix4x4D::make_zero();
        for i in 0..16 {
            assert_eq!(0.0, mat[i]);
        }

        mat = Matrix4x4D::make_identity();
        for i in 0..4 {
            for j in 0..4 {
                if i == j {
                    assert_eq!(1.0, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat = Matrix4x4D::make_scale_matrix_scalar(3.0, -4.0, 2.4);
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new(3.0, 0.0, 0.0, 0.0,
                             0.0, -4.0, 0.0, 0.0,
                             0.0, 0.0, 2.4, 0.0,
                             0.0, 0.0, 0.0, 1.0), None), true);

        mat = Matrix4x4D::make_scale_matrix_vec(Vector3D::new(-2.0, 5.0, 3.5));
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new(-2.0, 0.0, 0.0, 0.0,
                             0.0, 5.0, 0.0, 0.0,
                             0.0, 0.0, 3.5, 0.0,
                             0.0, 0.0, 0.0, 1.0), None), true);

        mat = Matrix4x4D::make_rotation_matrix(
            Vector3D::new(-1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0),
            -74.0 / 180.0 * crate::constants::K_PI_D);
        assert_eq!(mat.is_similar(&Matrix4x4D::new(
            0.36, 0.48, -0.8, 0.0,
            -0.8, 0.60, 0.0, 0.0,
            0.48, 0.64, 0.6, 0.0,
            0.0, 0.0, 0.0, 1.0), Some(0.05)), true);

        mat = Matrix4x4D::make_translation_matrix(Vector3D::new(-2.0, 5.0, 3.5));
        assert_eq!(mat.is_similar(
            &Matrix4x4D::new(1.0, 0.0, 0.0, -2.0,
                             0.0, 1.0, 0.0, 5.0,
                             0.0, 0.0, 1.0, 3.5,
                             0.0, 0.0, 0.0, 1.0), None), true);
    }
}