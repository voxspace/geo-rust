/*
 * // Copyright (c) 3031 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::fdm_linear_system3::*;
use crate::vector_n::VectorN;
use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;
use crate::matrix_csr::MatrixCsr;
use crate::blas::Blas;

/// # 3-D finite difference-type linear system solver using Jacobi method.
pub struct FdmJacobiSolver3 {
    _max_number_of_iterations: u32,
    _last_number_of_iterations: u32,
    _residual_check_interval: u32,
    _tolerance: f64,
    _last_residual: f64,

    // Uncompressed vectors
    _x_temp: FdmVector3,
    _residual: FdmVector3,

    // Compressed vectors
    _x_temp_comp: VectorN,
    _residual_comp: VectorN,
}

impl FdmJacobiSolver3 {
    /// Constructs the solver with given parameters.
    pub fn new(max_number_of_iterations: u32,
               residual_check_interval: u32, tolerance: f64) -> FdmJacobiSolver3 {
        return FdmJacobiSolver3 {
            _max_number_of_iterations: max_number_of_iterations,
            _last_number_of_iterations: 0,
            _residual_check_interval: residual_check_interval,
            _tolerance: tolerance,
            _last_residual: crate::constants::K_MAX_D,
            _x_temp: Default::default(),
            _residual: Default::default(),
            _x_temp_comp: Default::default(),
            _residual_comp: Default::default(),
        };
    }

    /// Returns the max number of Jacobi iterations.
    pub fn max_number_of_iterations(&self) -> u32 {
        return self._max_number_of_iterations;
    }

    /// Returns the last number of Jacobi iterations the solver made.
    pub fn last_number_of_iterations(&self) -> u32 {
        return self._last_number_of_iterations;
    }

    /// Returns the max residual tolerance for the Jacobi method.
    pub fn tolerance(&self) -> f64 {
        return self._tolerance;
    }

    /// Returns the last residual after the Jacobi iterations.
    pub fn last_residual(&self) -> f64 {
        return self._last_residual;
    }

    /// Performs single Jacobi relaxation step.
    pub fn relax(a: &FdmMatrix3, b: &FdmVector3, x: &mut FdmVector3, x_temp: &mut FdmVector3) {
        let size = a.size();

        a.for_each_index(|i, j, k| {
            let r =
                (if i > 0 { a[(i - 1, j, k)].right * x[(i - 1, j, k)] } else { 0.0 }) +
                    (if i + 1 < size.x { a[(i, j, k)].right * x[(i + 1, j, k)] } else { 0.0 }) +
                    (if j > 0 { a[(i, j - 1, k)].up * x[(i, j - 1, k)] } else { 0.0 }) +
                    (if j + 1 < size.y { a[(i, j, k)].up * x[(i, j + 1, k)] } else { 0.0 }) +
                    (if k > 0 { a[(i, j, k - 1)].front * x[(i, j, k - 1)] } else { 0.0 }) +
                    (if k + 1 < size.z { a[(i, j, k)].front * x[(i, j, k + 1)] } else { 0.0 });

            x_temp[(i, j, k)] = (b[(i, j, k)] - r) / a[(i, j, k)].center;
        });
    }

    /// Performs single Jacobi relaxation step for compressed sys.
    pub fn relax_compressed(a: &MatrixCsr, b: &VectorN, x: &mut VectorN, x_temp: &mut VectorN) {
        let rp = a.row_pointers_data();
        let ci = a.column_indices_data();
        let nnz = a.non_zero_data();

        b.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut r = 0.0;
            let mut diag = 1.0;
            for jj in row_begin..row_end {
                let j = ci[jj];

                if i == j {
                    diag = nnz[jj];
                } else {
                    r += nnz[jj] * x[j];
                }
            }

            x_temp[i] = (b[i] - r) / diag;
        });
    }
}

impl<'a> FdmLinearSystemSolver3<'a> for FdmJacobiSolver3 {
    fn solve(&mut self, system: &mut FdmLinearSystem3) -> bool {
        self.clear_compressed_vectors();

        self._x_temp.resize_with_packed_size(&system.x.size(), None);
        self._residual.resize_with_packed_size(&system.x.size(), None);

        self._last_number_of_iterations = self._max_number_of_iterations;

        for iter in 0..self._max_number_of_iterations {
            FdmJacobiSolver3::relax(&system.a, &system.b, &mut system.x, &mut self._x_temp);

            self._x_temp.swap(&mut system.x);

            if iter != 0 && iter % self._residual_check_interval == 0 {
                FdmBlas3::residual(&system.a, &system.x, &system.b, &mut self._residual);

                if FdmBlas3::l2norm(&self._residual) < self._tolerance {
                    self._last_number_of_iterations = iter + 1;
                    break;
                }
            }
        }

        FdmBlas3::residual(&system.a, &system.x, &system.b, &mut self._residual);
        self._last_residual = FdmBlas3::l2norm(&self._residual);

        return self._last_residual < self._tolerance;
    }

    fn solve_compressed(&mut self, system: &mut FdmCompressedLinearSystem3) -> bool {
        self.clear_uncompressed_vectors();

        self._x_temp_comp.resize(system.x.size(), None);
        self._residual_comp.resize(system.x.size(), None);

        self._last_number_of_iterations = self._max_number_of_iterations;

        for iter in 0..self._max_number_of_iterations {
            FdmJacobiSolver3::relax_compressed(&system.a, &system.b, &mut system.x, &mut self._x_temp_comp);

            self._x_temp_comp.swap(&mut system.x);

            if iter != 0 && iter % self._residual_check_interval == 0 {
                FdmCompressedBlas3::residual(&system.a, &system.x, &system.b,
                                             &mut self._residual_comp);

                if FdmCompressedBlas3::l2norm(&self._residual_comp) < self._tolerance {
                    self._last_number_of_iterations = iter + 1;
                    break;
                }
            }
        }

        FdmCompressedBlas3::residual(&system.a, &system.x, &system.b,
                                     &mut self._residual_comp);
        self._last_residual = FdmCompressedBlas3::l2norm(&self._residual_comp);

        return self._last_residual < self._tolerance;
    }
}

impl FdmJacobiSolver3 {
    fn clear_uncompressed_vectors(&mut self) {
        self._x_temp_comp.clear();
        self._residual_comp.clear();
    }

    fn clear_compressed_vectors(&mut self) {
        self._x_temp.clear();
        self._residual.clear();
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod fdm_jacobi_solver3 {
    use crate::fdm_linear_system3::{FdmLinearSystem3, FdmCompressedLinearSystem3};
    use crate::usize3::USize3;
    use crate::fdm_jacobi_solver3::FdmJacobiSolver3;
    use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;

    #[test]
    fn solve() {
        let mut system = FdmLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_linear_system(
            &mut system, &USize3::new(3, 3, 3));

        let mut solver = FdmJacobiSolver3::new(100, 10, 1e-9);
        solver.solve(&mut system);

        assert_eq!(solver.tolerance() > solver.last_residual(), true);
    }

    #[test]
    fn solve_compressed() {
        let mut system = FdmCompressedLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_compressed_linear_system(
            &mut system, &USize3::new(3, 3, 3));

        let mut solver = FdmJacobiSolver3::new(100, 10, 1e-9);
        solver.solve_compressed(&mut system);

        assert_eq!(solver.tolerance() > solver.last_residual(), true);
    }
}