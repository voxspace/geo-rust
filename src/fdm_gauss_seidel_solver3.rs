/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::fdm_linear_system3::*;
use crate::vector_n::VectorN;
use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;
use crate::matrix_csr::MatrixCsr;
use crate::blas::Blas;

/// # 3-D finite difference-type linear system solver using Gauss-Seidel method.
pub struct FdmGaussSeidelSolver3 {
    _max_number_of_iterations: u32,
    _last_number_of_iterations: u32,
    _residual_check_interval: u32,
    _tolerance: f64,
    _last_residual: f64,
    _sor_factor: f64,
    _use_red_black_ordering: bool,

    // Uncompressed vectors
    _residual: FdmVector3,

    // Compressed vectors
    _residual_comp: VectorN,
}

impl FdmGaussSeidelSolver3 {
    /// Constructs the solver with given parameters.
    pub fn new(max_number_of_iterations: u32,
               residual_check_interval: u32, tolerance: f64,
               sor_factor: Option<f64>,
               use_red_black_ordering: Option<bool>) -> FdmGaussSeidelSolver3 {
        return FdmGaussSeidelSolver3 {
            _max_number_of_iterations: max_number_of_iterations,
            _last_number_of_iterations: 0,
            _residual_check_interval: residual_check_interval,
            _tolerance: tolerance,
            _last_residual: crate::constants::K_MAX_D,
            _sor_factor: sor_factor.unwrap_or(1.0),
            _use_red_black_ordering: use_red_black_ordering.unwrap_or(false),
            _residual: Default::default(),
            _residual_comp: Default::default(),
        };
    }

    /// Returns the max number of Gauss-Seidel iterations.
    pub fn max_number_of_iterations(&self) -> u32 {
        return self._max_number_of_iterations;
    }

    /// Returns the last number of Gauss-Seidel iterations the solver made.
    pub fn last_number_of_iterations(&self) -> u32 {
        return self._last_number_of_iterations;
    }

    /// Returns the max residual tolerance for the Gauss-Seidel method.
    pub fn tolerance(&self) -> f64 {
        return self._tolerance;
    }

    /// Returns the last residual after the Gauss-Seidel iterations.
    pub fn last_residual(&self) -> f64 {
        return self._last_residual;
    }

    /// Returns the SOR (Successive Over Relaxation) factor.
    pub fn sor_factor(&self) -> f64 {
        return self._sor_factor;
    }

    /// Returns true if red-black ordering is enabled.
    pub fn use_red_black_ordering(&self) -> bool {
        return self._use_red_black_ordering;
    }

    /// Performs single natural Gauss-Seidel relaxation step.
    pub fn relax(a: &FdmMatrix3, b: &FdmVector3,
                 sor_factor: f64, x: &mut FdmVector3) {
        let size = a.size();

        a.for_each_index(|i, j, k| {
            let r =
                (if i > 0 { a[(i - 1, j, k)].right * x[(i - 1, j, k)] } else { 0.0 }) +
                    (if i + 1 < size.x { a[(i, j, k)].right * x[(i + 1, j, k)] } else { 0.0 }) +
                    (if j > 0 { a[(i, j - 1, k)].up * x[(i, j - 1, k)] } else { 0.0 }) +
                    (if j + 1 < size.y { a[(i, j, k)].up * x[(i, j + 1, k)] } else { 0.0 }) +
                    (if k > 0 { a[(i, j, k - 1)].front * x[(i, j, k - 1)] } else { 0.0 }) +
                    (if k + 1 < size.z { a[(i, j, k)].front * x[(i, j, k + 1)] } else { 0.0 });

            x[(i, j, k)] = (1.0 - sor_factor) * x[(i, j, k)] +
                sor_factor * (b[(i, j, k)] - r) / a[(i, j, k)].center;
        });
    }

    /// \brief Performs single natural Gauss-Seidel relaxation step for compressed sys.
    pub fn relax_compressed(a: &MatrixCsr, b: &VectorN, sor_factor: f64, x: &mut VectorN) {
        let rp = a.row_pointers_data();
        let ci = a.column_indices_data();
        let nnz = a.non_zero_data();

        b.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut r = 0.0;
            let mut diag = 1.0;
            for jj in row_begin..row_end {
                let j = ci[jj];

                if i == j {
                    diag = nnz[jj];
                } else {
                    r += nnz[jj] * x[j];
                }
            }

            x[i] = (1.0 - sor_factor) * x[i] + sor_factor * (b[i] - r) / diag;
        });
    }

    /// Performs single Red-Black Gauss-Seidel relaxation step.
    pub fn relax_red_black(a: &FdmMatrix3, b: &FdmVector3,
                           sor_factor: f64, x: &mut FdmVector3) {
        let size = a.size();

        // Red update
        for k in 0..size.z {
            for j in 0..size.y {
                for i in ((j + k) % 2..size.x).step_by(2) {
                    let r =
                        (if i > 0 { a[(i - 1, j, k)].right * x[(i - 1, j, k)] } else { 0.0 }) +
                            (if i + 1 < size.x { a[(i, j, k)].right * x[(i + 1, j, k)] } else { 0.0 }) +
                            (if j > 0 { a[(i, j - 1, k)].up * x[(i, j - 1, k)] } else { 0.0 }) +
                            (if j + 1 < size.y { a[(i, j, k)].up * x[(i, j + 1, k)] } else { 0.0 }) +
                            (if k > 0 { a[(i, j, k - 1)].front * x[(i, j, k - 1)] } else { 0.0 }) +
                            (if k + 1 < size.z { a[(i, j, k)].front * x[(i, j, k + 1)] } else { 0.0 });

                    x[(i, j, k)] =
                        (1.0 - sor_factor) * x[(i, j, k)] +
                            sor_factor * (b[(i, j, k)] - r) / a[(i, j, k)].center;
                }
            }
        }

        // Black update
        for k in 0..size.z {
            for j in 0..size.y {
                for i in ((1 - (j + k) % 2)..size.x).step_by(2) {
                    let r =
                        (if i > 0 { a[(i - 1, j, k)].right * x[(i - 1, j, k)] } else { 0.0 }) +
                            (if i + 1 < size.x { a[(i, j, k)].right * x[(i + 1, j, k)] } else { 0.0 }) +
                            (if j > 0 { a[(i, j - 1, k)].up * x[(i, j - 1, k)] } else { 0.0 }) +
                            (if j + 1 < size.y { a[(i, j, k)].up * x[(i, j + 1, k)] } else { 0.0 }) +
                            (if k > 0 { a[(i, j, k - 1)].front * x[(i, j, k - 1)] } else { 0.0 }) +
                            (if k + 1 < size.z { a[(i, j, k)].front * x[(i, j, k + 1)] } else { 0.0 });

                    x[(i, j, k)] =
                        (1.0 - sor_factor) * x[(i, j, k)] +
                            sor_factor * (b[(i, j, k)] - r) / a[(i, j, k)].center;
                }
            }
        }
    }
}

impl<'a> FdmLinearSystemSolver3<'a> for FdmGaussSeidelSolver3 {
    fn solve(&mut self, system: &mut FdmLinearSystem3) -> bool {
        self.clear_compressed_vectors();

        self._residual.resize_with_packed_size(&system.x.size(), None);

        self._last_number_of_iterations = self._max_number_of_iterations;

        for iter in 0..self._max_number_of_iterations {
            if self._use_red_black_ordering {
                FdmGaussSeidelSolver3::relax_red_black(&system.a, &system.b, self._sor_factor, &mut system.x);
            } else {
                FdmGaussSeidelSolver3::relax(&system.a, &system.b, self._sor_factor, &mut system.x);
            }

            if iter != 0 && iter % self._residual_check_interval == 0 {
                FdmBlas3::residual(&system.a, &system.x, &system.b, &mut self._residual);

                if FdmBlas3::l2norm(&self._residual) < self._tolerance {
                    self._last_number_of_iterations = iter + 1;
                    break;
                }
            }
        }

        FdmBlas3::residual(&system.a, &system.x, &system.b, &mut self._residual);
        self._last_residual = FdmBlas3::l2norm(&self._residual);

        return self._last_residual < self._tolerance;
    }

    fn solve_compressed(&mut self, system: &mut FdmCompressedLinearSystem3) -> bool {
        self.clear_uncompressed_vectors();

        self._residual_comp.resize(system.x.size(), None);

        self._last_number_of_iterations = self._max_number_of_iterations;

        for iter in 0..self._max_number_of_iterations {
            FdmGaussSeidelSolver3::relax_compressed(&system.a, &system.b, self._sor_factor, &mut system.x);

            if iter != 0 && iter % self._residual_check_interval == 0 {
                FdmCompressedBlas3::residual(&system.a, &system.x, &system.b,
                                             &mut self._residual_comp);

                if FdmCompressedBlas3::l2norm(&self._residual_comp) < self._tolerance {
                    self._last_number_of_iterations = iter + 1;
                    break;
                }
            }
        }

        FdmCompressedBlas3::residual(&system.a, &system.x, &system.b,
                                     &mut self._residual_comp);
        self._last_residual = FdmCompressedBlas3::l2norm(&self._residual_comp);

        return self._last_residual < self._tolerance;
    }
}

impl FdmGaussSeidelSolver3 {
    fn clear_uncompressed_vectors(&mut self) {
        self._residual.clear();
    }

    fn clear_compressed_vectors(&mut self) {
        self._residual_comp.clear();
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod fdm_gauss_seidel_solver3 {
    use crate::fdm_linear_system3::*;
    use crate::fdm_gauss_seidel_solver3::FdmGaussSeidelSolver3;
    use crate::usize3::USize3;
    use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;
    use crate::blas::Blas;

    #[test]
    fn solve_low_res() {
        let mut system = FdmLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_linear_system(
            &mut system, &USize3::new(3, 3, 3));

        let mut solver = FdmGaussSeidelSolver3::new(
            100, 10, 1e-9,
            None, None);
        solver.solve(&mut system);

        assert_eq!(solver.tolerance() > solver.last_residual(), true);
    }

    #[test]
    fn solve() {
        let mut system = FdmLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_linear_system(
            &mut system, &USize3::new(32, 32, 32));

        let mut buffer = system.x.clone();
        FdmBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
        let norm0 = FdmBlas3::l2norm(&buffer);

        let mut solver = FdmGaussSeidelSolver3::new(
            100, 10, 1e-9,
            None, None);
        solver.solve(&mut system);

        FdmBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
        let norm1 = FdmBlas3::l2norm(&buffer);

        assert_eq!(norm1 < norm0, true);
    }

    #[test]
    fn relax() {
        let mut system = FdmLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_linear_system(
            &mut system, &USize3::new(32, 32, 32));

        let mut buffer = system.x.clone();
        FdmBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
        let mut norm0 = FdmBlas3::l2norm(&buffer);

        for _ in 0..200 {
            FdmGaussSeidelSolver3::relax(&system.a, &system.b, 1.0, &mut system.x);

            FdmBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
            let norm = FdmBlas3::l2norm(&buffer);
            assert_eq!(norm < norm0, true);

            norm0 = norm;
        }
    }

    #[test]
    fn relax_red_black() {
        let mut system = FdmLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_linear_system(
            &mut system, &USize3::new(32, 32, 32));

        let mut buffer = system.x.clone();
        FdmBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
        let mut norm0 = FdmBlas3::l2norm(&buffer);

        for i in 0..200 {
            FdmGaussSeidelSolver3::relax_red_black(&system.a, &system.b, 1.0,
                                                   &mut system.x);

            FdmBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
            let norm = FdmBlas3::l2norm(&buffer);
            if i > 0 {
                assert_eq!(norm < norm0, true);
            }

            norm0 = norm;
        }
    }

    #[test]
    fn solve_compressed_res() {
        let mut system = FdmCompressedLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_compressed_linear_system(
            &mut system, &USize3::new(3, 3, 3));

        let mut solver = FdmGaussSeidelSolver3::new(
            100, 10, 1e-9,
            None, None);
        solver.solve_compressed(&mut system);

        assert_eq!(solver.tolerance() > solver.last_residual(), true);
    }

    #[test]
    fn solve_compressed() {
        let mut system = FdmCompressedLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_compressed_linear_system(
            &mut system, &USize3::new(32, 32, 32));

        let mut buffer = system.x.clone();
        FdmCompressedBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
        let norm0 = FdmCompressedBlas3::l2norm(&buffer);

        let mut solver = FdmGaussSeidelSolver3::new(
            100, 10, 1e-9,
            None, None);
        solver.solve_compressed(&mut system);

        FdmCompressedBlas3::residual(&system.a, &system.x, &system.b, &mut buffer);
        let norm1 = FdmCompressedBlas3::l2norm(&buffer);

        assert_eq!(norm1 < norm0, true);
    }
}