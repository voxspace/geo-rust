/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::ops::{Index, IndexMut, AddAssign, SubAssign, MulAssign, DivAssign, Add, Sub, Mul, Div};
use std::fmt::{Debug, Formatter, Result};
use crate::usize2::USize2;

///
/// # 3-D size class.
///
/// This class defines simple 3-D size data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy, Default)]
pub struct USize3 {
    /// X (or the first) component of the size.
    pub x: usize,

    /// Y (or the second) component of the size.
    pub y: usize,

    /// Z (or the third) component of the size.
    pub z: usize,
}

/// # Constructors
impl USize3 {
    /// Constructs size with given parameters **x_**, **y_**, and **z_**.
    pub fn new(x_: usize, y_: usize, z_: usize) -> USize3 {
        return USize3 {
            x: x_,
            y: y_,
            z: z_,
        };
    }

    /// Constructs size with a 2-D size and a scalar.
    pub fn new_vec(v: &USize2, z_: usize) -> USize3 {
        return USize3 {
            x: v.x,
            y: v.y,
            z: z_,
        };
    }

    /// Constructs size with initializer list.
    pub fn new_slice(lst: &[usize]) -> USize3 {
        return USize3 {
            x: lst[0],
            y: lst[1],
            z: lst[2],
        };
    }
}

/// # Basic setters
impl USize3 {
    /// Set all x, y, and z components to **s**.
    pub fn set_scalar(&mut self, s: usize) {
        self.x = s;
        self.y = s;
        self.z = s;
    }

    /// Set x, y, and z components with given parameters.
    pub fn set(&mut self, x: usize, y: usize, z: usize) {
        self.x = x;
        self.y = y;
        self.z = z;
    }

    /// Set x, y, and z components with given **pt.x**, **pt.y**, and **z**.
    pub fn set_vec(&mut self, pt: &USize2, z: usize) {
        self.x = pt.x;
        self.y = pt.y;
        self.z = z;
    }

    /// Set x, y, and z components with given initializer list.
    pub fn set_slice(&mut self, lst: &[usize]) {
        self.x = lst[0];
        self.y = lst[1];
        self.z = lst[2];
    }

    /// Set x, y, and z with other size **v**.
    pub fn set_self(&mut self, v: &USize3) {
        self.x = v.x;
        self.y = v.y;
        self.z = v.z;
    }

    /// Set all x, y, and z to zero.
    pub fn set_zero(&mut self) {
        self.x = 0;
        self.y = 0;
        self.z = 0;
    }
}

/// # Binary operations: new instance = self (+) v
impl USize3 {
    /// Computes self + (v, v, v).
    pub fn add_scalar(&self, v: usize) -> USize3 {
        return USize3::new(self.x + v, self.y + v, self.z + v);
    }

    /// Computes self + (v.x, v.y, v.z).
    pub fn add_vec(&self, v: &USize3) -> USize3 {
        return USize3::new(self.x + v.x, self.y + v.y, self.z + v.z);
    }

    /// Computes self - (v, v, v).
    pub fn sub_scalar(&self, v: usize) -> USize3 {
        return USize3::new(self.x - v, self.y - v, self.z - v);
    }

    /// Computes self - (v.x, v.y, v.z).
    pub fn sub_vec(&self, v: &USize3) -> USize3 {
        return USize3::new(self.x - v.x, self.y - v.y, self.z - v.z);
    }

    /// Computes self * (v, v, v).
    pub fn mul_scalar(&self, v: usize) -> USize3 {
        return USize3::new(self.x * v, self.y * v, self.z * v);
    }

    /// Computes self * (v.x, v.y, v.z).
    pub fn mul_vec(&self, v: &USize3) -> USize3 {
        return USize3::new(self.x * v.x, self.y * v.y, self.z * v.z);
    }

    /// Computes self / (v, v, v).
    pub fn div_scalar(&self, v: usize) -> USize3 {
        return USize3::new(self.x / v, self.y / v, self.z / v);
    }

    /// Computes self / (v.x, v.y, v.z).
    pub fn div_vec(&self, v: &USize3) -> USize3 {
        return USize3::new(self.x / v.x, self.y / v.y, self.z / v.z);
    }
}

/// # Binary operations: new instance = v (+) self
impl USize3 {
    /// Computes (v, v, v) - self.
    pub fn rsub_scalar(&self, v: usize) -> USize3 {
        return USize3::new(v - self.x, v - self.y, v - self.z);
    }

    /// Computes (v.x, v.y, v.z) - self.
    pub fn rsub_vec(&self, v: &USize3) -> USize3 {
        return USize3::new(v.x - self.x, v.y - self.y, v.z - self.z);
    }

    /// Computes (v, v, v) / self.
    pub fn rdiv_scalar(&self, v: usize) -> USize3 {
        return USize3::new(v / self.x, v / self.y, v / self.z);
    }

    /// Computes (v.x, v.y, v.z) / self.
    pub fn rdiv_vec(&self, v: &USize3) -> USize3 {
        return USize3::new(v.x / self.x, v.y / self.y, v.z / self.z);
    }
}

/// # Augmented operators: self (+)= v
impl USize3 {
    /// Computes self += (v, v, v).
    pub fn iadd_scalar(&mut self, v: usize) {
        self.x = self.x + v;
        self.y = self.y + v;
        self.z = self.z + v;
    }

    /// Computes self += (v.x, v.y, v.z).
    pub fn iadd_vec(&mut self, v: &USize3) {
        self.x = self.x + v.x;
        self.y = self.y + v.y;
        self.z = self.z + v.z;
    }

    /// Computes self -= (v, v, v).
    pub fn isub_scalar(&mut self, v: usize) {
        self.x = self.x - v;
        self.y = self.y - v;
        self.z = self.z - v;
    }

    /// Computes self -= (v.x, v.y, v.z).
    pub fn isub_vec(&mut self, v: &USize3) {
        self.x = self.x - v.x;
        self.y = self.y - v.y;
        self.z = self.z - v.z;
    }

    /// Computes self *= (v, v, v).
    pub fn imul_scalar(&mut self, v: usize) {
        self.x = self.x * v;
        self.y = self.y * v;
        self.z = self.z * v;
    }

    /// Computes self *= (v.x, v.y, v.z).
    pub fn imul_vec(&mut self, v: &USize3) {
        self.x = self.x * v.x;
        self.y = self.y * v.y;
        self.z = self.z * v.z;
    }

    /// Computes self /= (v, v, v).
    pub fn idiv_scalar(&mut self, v: usize) {
        self.x = self.x / v;
        self.y = self.y / v;
        self.z = self.z / v;
    }

    /// Computes self /= (v.x, v.y, v.z).
    pub fn idiv_vec(&mut self, v: &USize3) {
        self.x = self.x / v.x;
        self.y = self.y / v.y;
        self.z = self.z / v.z;
    }
}

/// # Basic getters
impl USize3 {
    /// Returns const reference to the **i** -th element of the size.
    pub fn at(&self, i: usize) -> &usize {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the size.
    pub fn at_mut(&mut self, i: usize) -> &mut usize {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y + z).
    pub fn sum(&self) -> usize {
        return self.x + self.y + self.z;
    }

    /// Returns the minimum value among x, y, and z.
    pub fn min(&self) -> usize {
        return usize::min(usize::min(self.x, self.y), self.z);
    }

    /// Returns the maximum value among x, y, and z.
    pub fn max(&self) -> usize {
        return usize::max(usize::max(self.x, self.y), self.z);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        return match self.x > self.y {
            true => match self.x > self.z {
                true => 0,
                false => 2
            }
            false => match self.y > self.z {
                true => 1,
                false => 2
            }
        };
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        return match self.x < self.y {
            true => match self.x < self.z {
                true => 0,
                false => 2
            }
            false => match self.y < self.z {
                true => 1,
                false => 2
            }
        };
    }

    /// Returns true if **other** is the same as self size.
    pub fn is_equal(&self, other: &USize3) -> bool {
        return self.x == other.x && self.y == other.y && self.z == other.z;
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the size.
impl Index<usize> for USize3 {
    type Output = usize;
    fn index(&self, index: usize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the size.
impl IndexMut<usize> for USize3 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v, v)
impl AddAssign<usize> for USize3 {
    fn add_assign(&mut self, rhs: usize) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y, v.z)
impl AddAssign for USize3 {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v, v)
impl SubAssign<usize> for USize3 {
    fn sub_assign(&mut self, rhs: usize) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y, v.z)
impl SubAssign for USize3 {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v, v)
impl MulAssign<usize> for USize3 {
    fn mul_assign(&mut self, rhs: usize) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y, v.z)
impl MulAssign for USize3 {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v, v)
impl DivAssign<usize> for USize3 {
    fn div_assign(&mut self, rhs: usize) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y, v.z)
impl DivAssign for USize3 {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self size.
impl PartialEq for USize3 {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl Eq for USize3 {}

/// Computes (a, a, a) + (b.x, b.y, b.z).
impl Add<usize> for USize3 {
    type Output = USize3;
    fn add(self, rhs: usize) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) + (b.x, b.y, b.z).
impl Add for USize3 {
    type Output = USize3;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) - (b, b, b).
impl Sub<usize> for USize3 {
    type Output = USize3;
    fn sub(self, rhs: usize) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) - (b.x, b.y, b.z).
impl Sub for USize3 {
    type Output = USize3;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) * (b, b, b).
impl Mul<usize> for USize3 {
    type Output = USize3;
    fn mul(self, rhs: usize) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) * (b.x, b.y, b.z).
impl Mul for USize3 {
    type Output = USize3;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) / (b, b, b).
impl Div<usize> for USize3 {
    type Output = USize3;
    fn div(self, rhs: usize) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) / (b.x, b.y, b.z).
impl Div for USize3 {
    type Output = USize3;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl Debug for USize3 {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::usize3::USize3;
    /// let vec = USize3::new(10, 20, 30);
    /// assert_eq!(format!("{:?}", vec), "(10, 20, 30)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10,
    ///     20,
    ///     30,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .field(&self.z)
            .finish()
    }
}

/// Returns element-wise min size.
pub fn min(a: &USize3, b: &USize3) -> USize3 {
    return USize3::new(usize::min(a.x, b.x),
                       usize::min(a.y, b.y),
                       usize::min(a.z, b.z));
}

/// Returns element-wise max size.
pub fn max(a: &USize3, b: &USize3) -> USize3 {
    return USize3::new(usize::max(a.x, b.x),
                       usize::max(a.y, b.y),
                       usize::max(a.z, b.z));
}

/// Returns element-wise clamped size.
pub fn clamp(v: &USize3, low: &USize3, high: &USize3) -> USize3 {
    return USize3::new(usize::clamp(v.x, low.x, high.x),
                       usize::clamp(v.y, low.y, high.y),
                       usize::clamp(v.z, low.z, high.z));
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod usize3 {
    #[test]
    fn constructors() {}
}