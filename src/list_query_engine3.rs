/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::bounding_box3::BoundingBox3D;
use crate::ray3::Ray3D;
use crate::nearest_neighbor_query_engine3::*;
use crate::intersection_query_engine3::*;
use crate::vector3::Vector3D;

/// # list-based 3-D intersection/nearest-neighbor query engine.
pub struct ListQueryEngine3<T: Clone> {
    _items: Vec<T>,
}

impl<T: Clone> ListQueryEngine3<T> {
    pub fn new() -> ListQueryEngine3<T> {
        return ListQueryEngine3 {
            _items: vec![]
        };
    }

    /// Adds an item to the container.
    pub fn add(&mut self, item: T) {
        self._items.push(item);
    }

    /// Adds items to the container.
    pub fn add_vec(&mut self, items: Vec<T>) {
        self._items.reserve(items.len());
        for val in items {
            self._items.push(val);
        }
    }
}

impl<T: Clone> IntersectionQueryEngine3<T> for ListQueryEngine3<T> {
    fn intersects_aabb<Callback>(&self, aabb: &BoundingBox3D, test_func: &mut Callback) -> bool
        where Callback: BoxIntersectionTestFunc3<T> {
        for item in &self._items {
            if test_func(item, aabb) {
                return true;
            }
        }

        return false;
    }

    fn intersects_ray<Callback>(&self, ray: &Ray3D, test_func: &mut Callback) -> bool
        where Callback: RayIntersectionTestFunc3<T> {
        for item in &self._items {
            if test_func(item, ray) {
                return true;
            }
        }

        return false;
    }

    fn for_each_intersecting_item_aabb<Callback, Visitor>(&self, aabb: &BoundingBox3D,
                                                          test_func: &mut Callback, visitor_func: &mut Visitor)
        where Callback: BoxIntersectionTestFunc3<T>,
              Visitor: IntersectionVisitorFunc3<T> {
        for item in &self._items {
            if test_func(item, aabb) {
                visitor_func(item);
            }
        }
    }

    fn for_each_intersecting_item_ray<Callback, Visitor>(&self, ray: &Ray3D,
                                                         test_func: &mut Callback, visitor_func: &mut Visitor)
        where Callback: RayIntersectionTestFunc3<T>,
              Visitor: IntersectionVisitorFunc3<T> {
        for item in &self._items {
            if test_func(item, ray) {
                visitor_func(item);
            }
        }
    }

    fn closest_intersection<Callback>(&self, ray: &Ray3D, test_func: &mut Callback) -> ClosestIntersectionQueryResult3<T>
        where Callback: GetRayIntersectionFunc3<T> {
        let mut best: ClosestIntersectionQueryResult3<T> = ClosestIntersectionQueryResult3::new();
        for item in &self._items {
            let dist = test_func(item, ray);
            if dist < best.distance {
                best.distance = dist;
                best.item = Some(item.clone());
            }
        }

        return best;
    }
}

impl<T: Clone> NearestNeighborQueryEngine3<T> for ListQueryEngine3<T> {
    fn nearest<Callback>(&self, pt: &Vector3D, distance_func: &mut Callback) -> NearestNeighborQueryResult3<T>
        where Callback: NearestNeighborDistanceFunc3<T> {
        let mut best = NearestNeighborQueryResult3::new();
        for item in &self._items {
            let dist = distance_func(item, pt);
            if dist < best.distance {
                best.item = Some(item.clone());
                best.distance = dist;
            }
        }

        return best;
    }
}