/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::matrix_mxn::MatrixMxN;
use crate::vector_n::VectorN;
use crate::matrix::Matrix;
use crate::vector::Vector;

///
/// # Singular value decomposition (SVD).
///
/// This function decompose the input matrix \p a to \p u * \p w * \p v^T.
///
/// \tparam T Real-value type.
///
/// \param a The input matrix to decompose.
/// \param u Left-most output matrix.
/// \param w The vector of singular values.
/// \param v Right-most output matrix.
///
pub fn svd_dynamic(a: &MatrixMxN, u: &mut MatrixMxN, w: &mut VectorN, v: &mut MatrixMxN) {
    todo!()
}

///
/// # Singular value decomposition (SVD).
///
/// This function decompose the input matrix \p a to \p u * \p w * \p v^T.
///
/// \tparam T Real-value type.
///
/// \param a The input matrix to decompose.
/// \param u Left-most output matrix.
/// \param w The vector of singular values.
/// \param v Right-most output matrix.
///
pub fn svd_static<const M: usize, const N: usize, const TOTAL: usize>(
    a: &Matrix<M, N, TOTAL>, u: &mut Matrix<M, N, TOTAL>, w: &mut Vector<N>, v: &mut Matrix<M, N, TOTAL>) {
    todo!()
}