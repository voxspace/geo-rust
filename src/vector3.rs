/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use std::ops::*;
use std::fmt::{Debug, Formatter, Result, Display};
use crate::vector2::Vector2;

///
/// # 3-D vector class.
///
/// This class defines simple 3-D vector data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy)]
pub struct Vector3<T: Float> {
    /// X (or the first) component of the vector.
    pub x: T,

    /// Y (or the second) component of the vector.
    pub y: T,

    /// Z (or the third) component of the vector.
    pub z: T,
}

/// Float-type 3D vector.
pub type Vector3F = Vector3<f32>;
/// Double-type 3D vector.
pub type Vector3D = Vector3<f64>;

impl<T: Float> Default for Vector3<T> {
    /// Constructs default vector (0, 0, 0).
    fn default() -> Self {
        return Vector3 {
            x: T::zero(),
            y: T::zero(),
            z: T::zero(),
        };
    }
}

/// # Constructors
impl<T: Float> Vector3<T> {
    /// Constructs vector with given parameters **x_**, **y_**, and **z_**.
    pub fn new(x_: T, y_: T, z_: T) -> Vector3<T> {
        return Vector3 {
            x: x_,
            y: y_,
            z: z_,
        };
    }

    /// Constructs vector with a 2-D vector and a scalar.
    pub fn new_vec(v: &Vector2<T>, z_: T) -> Vector3<T> {
        return Vector3 {
            x: v.x,
            y: v.y,
            z: z_,
        };
    }

    /// Constructs vector with initializer list.
    pub fn new_slice(lst: &[T]) -> Vector3<T> {
        return Vector3 {
            x: lst[0],
            y: lst[1],
            z: lst[2],
        };
    }
}

/// # Basic setters
impl<T: Float> Vector3<T> {
    /// Set all x, y, and z components to **s**.
    pub fn set_scalar(&mut self, s: T) {
        self.x = s;
        self.y = s;
        self.z = s;
    }

    /// Set x, y, and z components with given parameters.
    pub fn set(&mut self, x: T, y: T, z: T) {
        self.x = x;
        self.y = y;
        self.z = z;
    }

    /// Set x, y, and z components with given **pt.x**, **pt.y**, and **z**.
    pub fn set_vec(&mut self, pt: &Vector2<T>, z: T) {
        self.x = pt.x;
        self.y = pt.y;
        self.z = z;
    }

    /// Set x, y, and z components with given initializer list.
    pub fn set_slice(&mut self, lst: &[T]) {
        self.x = lst[0];
        self.y = lst[1];
        self.z = lst[2];
    }

    /// Set x, y, and z with other vector **v**.
    pub fn set_self(&mut self, v: &Vector3<T>) {
        self.x = v.x;
        self.y = v.y;
        self.z = v.z;
    }

    /// Set all x, y, and z to zero.
    pub fn set_zero(&mut self) {
        self.x = T::zero();
        self.y = T::zero();
        self.z = T::zero();
    }

    /// Normalizes self vector.
    pub fn normalize(&mut self) {
        let l = self.length();
        self.x = self.x / l;
        self.y = self.y / l;
        self.z = self.z / l;
    }
}

/// # Binary operations: new instance = self (+) v
impl<T: Float> Vector3<T> {
    /// Computes self + (v, v, v).
    pub fn add_scalar(&self, v: T) -> Vector3<T> {
        return Vector3::new(self.x + v, self.y + v, self.z + v);
    }

    /// Computes self + (v.x, v.y, v.z).
    pub fn add_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(self.x + v.x, self.y + v.y, self.z + v.z);
    }

    /// Computes self - (v, v, v).
    pub fn sub_scalar(&self, v: T) -> Vector3<T> {
        return Vector3::new(self.x - v, self.y - v, self.z - v);
    }

    /// Computes self - (v.x, v.y, v.z).
    pub fn sub_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(self.x - v.x, self.y - v.y, self.z - v.z);
    }

    /// Computes self * (v, v, v).
    pub fn mul_scalar(&self, v: T) -> Vector3<T> {
        return Vector3::new(self.x * v, self.y * v, self.z * v);
    }

    /// Computes self * (v.x, v.y, v.z).
    pub fn mul_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(self.x * v.x, self.y * v.y, self.z * v.z);
    }

    /// Computes self / (v, v, v).
    pub fn div_scalar(&self, v: T) -> Vector3<T> {
        return Vector3::new(self.x / v, self.y / v, self.z / v);
    }

    /// Computes self / (v.x, v.y, v.z).
    pub fn div_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(self.x / v.x, self.y / v.y, self.z / v.z);
    }

    /// Computes dot product.
    pub fn dot(&self, v: &Vector3<T>) -> T {
        return self.x * v.x + self.y * v.y + self.z * v.z;
    }

    /// Computes cross product.
    pub fn cross(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(self.y * v.z - v.y * self.z,
                            self.z * v.x - v.z * self.x,
                            self.x * v.y - v.x * self.y);
    }
}

/// # Binary operations: new instance = v (+) self
impl<T: Float> Vector3<T> {
    /// Computes (v, v, v) - self.
    pub fn rsub_scalar(&self, v: T) -> Vector3<T> {
        return Vector3::new(v - self.x, v - self.y, v - self.z);
    }

    /// Computes (v.x, v.y, v.z) - self.
    pub fn rsub_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(v.x - self.x, v.y - self.y, v.z - self.z);
    }

    /// Computes (v, v, v) / self.
    pub fn rdiv_scalar(&self, v: T) -> Vector3<T> {
        return Vector3::new(v / self.x, v / self.y, v / self.z);
    }

    /// Computes (v.x, v.y, v.z) / self.
    pub fn rdiv_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(v.x / self.x, v.y / self.y, v.z / self.z);
    }

    /// Computes **v** cross self.
    pub fn rcross(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(v.y * self.z - self.y * v.z,
                            v.z * self.x - self.z * v.x,
                            v.x * self.y - self.x * v.y);
    }
}

/// # Augmented operators: self (+)= v
impl<T: Float> Vector3<T> {
    /// Computes self += (v, v, v).
    pub fn iadd_scalar(&mut self, v: T) {
        self.x = self.x + v;
        self.y = self.y + v;
        self.z = self.z + v;
    }

    /// Computes self += (v.x, v.y, v.z).
    pub fn iadd_vec(&mut self, v: &Vector3<T>) {
        self.x = self.x + v.x;
        self.y = self.y + v.y;
        self.z = self.z + v.z;
    }

    /// Computes self -= (v, v, v).
    pub fn isub_scalar(&mut self, v: T) {
        self.x = self.x - v;
        self.y = self.y - v;
        self.z = self.z - v;
    }

    /// Computes self -= (v.x, v.y, v.z).
    pub fn isub_vec(&mut self, v: &Vector3<T>) {
        self.x = self.x - v.x;
        self.y = self.y - v.y;
        self.z = self.z - v.z;
    }

    /// Computes self *= (v, v, v).
    pub fn imul_scalar(&mut self, v: T) {
        self.x = self.x * v;
        self.y = self.y * v;
        self.z = self.z * v;
    }

    /// Computes self *= (v.x, v.y, v.z).
    pub fn imul_vec(&mut self, v: &Vector3<T>) {
        self.x = self.x * v.x;
        self.y = self.y * v.y;
        self.z = self.z * v.z;
    }

    /// Computes self /= (v, v, v).
    pub fn idiv_scalar(&mut self, v: T) {
        self.x = self.x / v;
        self.y = self.y / v;
        self.z = self.z / v;
    }

    /// Computes self /= (v.x, v.y, v.z).
    pub fn idiv_vec(&mut self, v: &Vector3<T>) {
        self.x = self.x / v.x;
        self.y = self.y / v.y;
        self.z = self.z / v.z;
    }
}

/// # Basic getters
impl<T: Float> Vector3<T> {
    /// Returns const reference to the **i** -th element of the vector.
    pub fn at(&self, i: usize) -> &T {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the vector.
    pub fn at_mut(&mut self, i: usize) -> &mut T {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y + z).
    pub fn sum(&self) -> T {
        return self.x + self.y + self.z;
    }

    pub fn avg(&self) -> T {
        return (self.x + self.y + self.z) / T::from(3.0).unwrap();
    }

    /// Returns the minimum value among x, y, and z.
    pub fn min(&self) -> T {
        return T::min(T::min(self.x, self.y), self.z);
    }

    /// Returns the maximum value among x, y, and z.
    pub fn max(&self) -> T {
        return T::max(T::max(self.x, self.y), self.z);
    }

    /// Returns the absolute minimum value among x, y, and z.
    pub fn absmin(&self) -> T {
        return crate::math_utils::absmin(crate::math_utils::absmin(self.x, self.y), self.z);
    }

    /// Returns the absolute maximum value among x, y, and z.
    pub fn absmax(&self) -> T {
        return crate::math_utils::absmax(crate::math_utils::absmax(self.x, self.y), self.z);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        return match T::abs(self.x) > T::abs(self.y) {
            true => match T::abs(self.x) > T::abs(self.z) {
                true => 0,
                false => 2
            }
            false => match T::abs(self.y) > T::abs(self.z) {
                true => 1,
                false => 2
            }
        };
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        return match T::abs(self.x) < T::abs(self.y) {
            true => match T::abs(self.x) < T::abs(self.z) {
                true => 0,
                false => 2
            }
            false => match T::abs(self.y) < T::abs(self.z) {
                true => 1,
                false => 2
            }
        };
    }

    /// Returns normalized vector.
    pub fn normalized(&self) -> Vector3<T> {
        let l = self.length();
        return Vector3::new(self.x / l, self.y / l, self.z / l);
    }

    /// Returns the length of the vector.
    pub fn length(&self) -> T {
        return T::sqrt(self.x * self.x + self.y * self.y + self.z * self.z);
    }

    /// Returns the squared length of the vector.
    pub fn length_squared(&self) -> T {
        return self.x * self.x + self.y * self.y + self.z * self.z;
    }

    /// Returns the distance to the other vector.
    pub fn distance_to(&self, other: &Vector3<T>) -> T {
        return self.sub_vec(other).length();
    }

    /// Returns the squared distance to the other vector.
    pub fn distance_squared_to(&self, other: &Vector3<T>) -> T {
        return self.sub_vec(other).length_squared();
    }

    /// Returns the reflection vector to the surface with given surface normal.
    pub fn reflected(&self, normal: &Vector3<T>) -> Vector3<T> {
        // self - 2(self.n)n
        return self.sub_vec(&normal.mul_scalar(T::from(2.0).unwrap() * self.dot(normal)));
    }

    /// Returns the projected vector to the surface with given surface normal.
    pub fn projected(&self, normal: &Vector3<T>) -> Vector3<T> {
        // self - self.n n
        return self.sub_vec(&normal.mul_scalar(self.dot(normal)));
    }

    /// Returns the tangential vector for self vector.
    pub fn tangential(&self) -> (Vector3<T>, Vector3<T>) {
        let a = match T::abs(self.y) > T::zero() || T::abs(self.z) > T::zero() {
            true => Vector3::new(T::one(), T::zero(), T::zero()),
            false => Vector3::new(T::zero(), T::one(), T::zero())
        }.cross(self).normalized();
        let b = self.cross(&a);
        return (a, b);
    }

    /// Returns true if **other** is the same as self vector.
    pub fn is_equal(&self, other: &Vector3<T>) -> bool {
        return self.x == other.x && self.y == other.y && self.z == other.z;
    }

    /// Returns true if **other** is similar to self vector.
    pub fn is_similar(&self, other: &Vector3<T>, epsilon: Option<T>) -> bool {
        return (T::abs(self.x - other.x) < epsilon.unwrap_or(T::epsilon())) &&
            (T::abs(self.y - other.y) < epsilon.unwrap_or(T::epsilon())) &&
            (T::abs(self.z - other.z) < epsilon.unwrap_or(T::epsilon()));
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the vector.
impl<T: Float> Index<usize> for Vector3<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the vector.
impl<T: Float> IndexMut<usize> for Vector3<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v, v)
impl<T: Float> AddAssign<T> for Vector3<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y, v.z)
impl<T: Float> AddAssign for Vector3<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v, v)
impl<T: Float> SubAssign<T> for Vector3<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y, v.z)
impl<T: Float> SubAssign for Vector3<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v, v)
impl<T: Float> MulAssign<T> for Vector3<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y, v.z)
impl<T: Float> MulAssign for Vector3<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v, v)
impl<T: Float> DivAssign<T> for Vector3<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y, v.z)
impl<T: Float> DivAssign for Vector3<T> {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self vector.
impl<T: Float> PartialEq for Vector3<T> {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl<T: Float> Eq for Vector3<T> {}

impl<T: Float> Neg for Vector3<T> {
    type Output = Vector3<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Vector3::new(-self.x, -self.y, -self.z);
    }
}

/// Computes (a, a, a) + (b.x, b.y, b.z).
impl<T: Float> Add<T> for Vector3<T> {
    type Output = Vector3<T>;
    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) + (b.x, b.y, b.z).
impl<T: Float> Add for Vector3<T> {
    type Output = Vector3<T>;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

impl<T: Float> Add<&Vector3<T>> for Vector3<T> {
    type Output = Vector3<T>;

    fn add(self, rhs: &Vector3<T>) -> Self::Output {
        return self.add_vec(rhs);
    }
}

/// Computes (a.x, a.y, a.z) - (b, b, b).
impl<T: Float> Sub<T> for Vector3<T> {
    type Output = Vector3<T>;
    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) - (b.x, b.y, b.z).
impl<T: Float> Sub for Vector3<T> {
    type Output = Vector3<T>;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

impl<T: Float> Sub<&Vector3<T>> for &Vector3<T> {
    type Output = Vector3<T>;

    fn sub(self, rhs: &Vector3<T>) -> Self::Output {
        return self.sub_vec(rhs);
    }
}

/// Computes (a.x, a.y, a.z) * (b, b, b).
impl<T: Float> Mul<T> for Vector3<T> {
    type Output = Vector3<T>;
    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) * (b.x, b.y, b.z).
impl<T: Float> Mul for Vector3<T> {
    type Output = Vector3<T>;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) / (b, b, b).
impl<T: Float> Div<T> for Vector3<T> {
    type Output = Vector3<T>;
    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) / (b.x, b.y, b.z).
impl<T: Float> Div for Vector3<T> {
    type Output = Vector3<T>;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl<T: Float + Debug> Debug for Vector3<T> {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::vector3::Vector3F;
    /// let vec = Vector3F::new(10.0, 20.0, 30.0);
    /// assert_eq!(format!("{:?}", vec), "(10.0, 20.0, 30.0)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10.0,
    ///     20.0,
    ///     30.0,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .field(&self.z)
            .finish()
    }
}

impl<T: Float + Display> Display for Vector3<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{} {} {}", self.x, self.y, self.z)
    }
}

/// Returns element-wise min vector.
pub fn min<T: Float>(a: &Vector3<T>, b: &Vector3<T>) -> Vector3<T> {
    return Vector3::new(T::min(a.x, b.x),
                        T::min(a.y, b.y),
                        T::min(a.z, b.z));
}

/// Returns element-wise max vector.
pub fn max<T: Float>(a: &Vector3<T>, b: &Vector3<T>) -> Vector3<T> {
    return Vector3::new(T::max(a.x, b.x),
                        T::max(a.y, b.y),
                        T::max(a.z, b.z));
}

/// Returns element-wise clamped vector.
pub fn clamp<T: Float>(v: &Vector3<T>, low: &Vector3<T>, high: &Vector3<T>) -> Vector3<T> {
    return Vector3::new(crate::math_utils::clamp(v.x, low.x, high.x),
                        crate::math_utils::clamp(v.y, low.y, high.y),
                        crate::math_utils::clamp(v.z, low.z, high.z));
}

/// Returns element-wise ceiled vector.
pub fn ceil<T: Float>(a: &Vector3<T>) -> Vector3<T> {
    return Vector3::new((a.x).ceil(), (a.y).ceil(), (a.z).ceil());
}

/// Returns element-wise floored vector.
pub fn floor<T: Float>(a: &Vector3<T>) -> Vector3<T> {
    return Vector3::new((a.x).floor(), (a.y).floor(), (a.z).floor());
}

/// Computes monotonic Catmull-Rom interpolation.
pub fn monotonic_catmull_rom<T: Float>(v0: &Vector3<T>, v1: &Vector3<T>,
                                       v2: &Vector3<T>, v3: &Vector3<T>, f: T) -> Vector3<T> {
    let two = T::from(2.0).unwrap();
    let three = T::from(3.0).unwrap();

    let mut d1 = (v2 - v0) / two;
    let mut d2 = (v3 - v1) / two;
    let d_diff = v2 - v1;

    if T::abs(d_diff.x) < T::epsilon() ||
        crate::math_utils::sign(d_diff.x) != crate::math_utils::sign(d1.x) ||
        crate::math_utils::sign(d_diff.x) != crate::math_utils::sign(d2.x) {
        d1.x = T::zero();
        d2.x = T::zero();
    }

    if T::abs(d_diff.y) < T::epsilon() ||
        crate::math_utils::sign(d_diff.y) != crate::math_utils::sign(d1.y) ||
        crate::math_utils::sign(d_diff.y) != crate::math_utils::sign(d2.y) {
        d1.y = T::zero();
        d2.y = T::zero();
    }

    if T::abs(d_diff.z) < T::epsilon() ||
        crate::math_utils::sign(d_diff.z) != crate::math_utils::sign(d1.z) ||
        crate::math_utils::sign(d_diff.z) != crate::math_utils::sign(d2.z) {
        d1.z = T::zero();
        d2.z = T::zero();
    }

    let a3 = d1 + d2 - d_diff * two;
    let a2 = d_diff * three - d1 * two - d2;
    let a1 = d1;
    let a0 = v1;

    return a3 * crate::math_utils::cubic(f) + a2 * crate::math_utils::square(f) + a1 * f + a0;
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod vector3 {
    use crate::vector3::*;
    use crate::assert_delta;
    use crate::vector2::Vector2F;

    #[test]
    fn constructors() {
        {
            let vec = Vector3F::default();
            assert_eq!(0.0, vec.x);
            assert_eq!(0.0, vec.y);
            assert_eq!(0.0, vec.z);
        }
        {
            let vec2 = Vector3F::new(5.0, 3.0, 8.0);
            assert_eq!(5.0, vec2.x);
            assert_eq!(3.0, vec2.y);
            assert_eq!(8.0, vec2.z);
        }
        {
            let vec3 = Vector2F::new(4.0, 7.0);
            let vec4 = Vector3F::new_vec(&vec3, 9.0);
            assert_eq!(4.0, vec4.x);
            assert_eq!(7.0, vec4.y);
            assert_eq!(9.0, vec4.z);
        }
        {
            let vec5 = Vector3F::new_slice(&[7.0, 6.0, 1.0]);
            assert_eq!(7.0, vec5.x);
            assert_eq!(6.0, vec5.y);
            assert_eq!(1.0, vec5.z);
        }
    }

    #[test]
    fn set_methods() {
        {
            let mut vec = Vector3F::default();
            vec.set(4.0, 2.0, 8.0);
            assert_eq!(4.0, vec.x);
            assert_eq!(2.0, vec.y);
            assert_eq!(8.0, vec.z);
        }
        {
            let mut vec = Vector3F::default();
            vec.set_vec(&Vector2F::new(1.0, 3.0), 10.0);
            assert_eq!(1.0, vec.x);
            assert_eq!(3.0, vec.y);
            assert_eq!(10.0, vec.z);
        }
        {
            let mut vec = Vector3F::default();
            let lst = [0.0, 5.0, 6.0];
            vec.set_slice(&lst);
            assert_eq!(0.0, vec.x);
            assert_eq!(5.0, vec.y);
            assert_eq!(6.0, vec.z);
        }
        {
            let mut vec = Vector3F::default();
            vec.set_self(&Vector3F::new(9.0, 8.0, 2.0));
            assert_eq!(9.0, vec.x);
            assert_eq!(8.0, vec.y);
            assert_eq!(2.0, vec.z);
        }
    }

    #[test]
    fn basic_setter_methods() {
        {
            let mut vec = Vector3F::new(3.0, 9.0, 4.0);
            vec.set_zero();
            assert_eq!(0.0, vec.x);
            assert_eq!(0.0, vec.y);
            assert_eq!(0.0, vec.z);
        }
        {
            let mut vec = Vector3F::default();
            vec.set(4.0, 2.0, 8.0);
            vec.normalize();
            let len = vec.x * vec.x + vec.y * vec.y + vec.z * vec.z;
            assert_eq!((len - 1.0).abs() < 1e-6, true);
        }
    }

    #[test]
    fn binary_operator_methods() {
        let mut vec = Vector3F::new(3.0, 9.0, 4.0);
        vec = vec.add_scalar(4.0);
        assert_eq!(7.0, vec.x);
        assert_eq!(13.0, vec.y);
        assert_eq!(8.0, vec.z);

        vec = vec.add_vec(&Vector3F::new(-2.0, 1.0, 5.0));
        assert_eq!(5.0, vec.x);
        assert_eq!(14.0, vec.y);
        assert_eq!(13.0, vec.z);

        vec = vec.sub_scalar(8.0);
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(5.0, vec.z);

        vec = vec.sub_vec(&Vector3F::new(-5.0, 3.0, 12.0));
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(-7.0, vec.z);

        vec = vec.mul_scalar(2.0);
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(-14.0, vec.z);

        vec = vec.mul_vec(&Vector3F::new(3.0, -2.0, 0.5));
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);
        assert_eq!(-7.0, vec.z);

        vec = vec.div_scalar(4.0);
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);
        assert_eq!(-1.75, vec.z);

        vec = vec.div_vec(&Vector3F::new(3.0, -1.0, 0.25));
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(-7.0, vec.z);

        let d = vec.dot(&Vector3F::new(4.0, 2.0, 1.0));
        assert_eq!(3.0, d);

        let c = vec.cross(&Vector3F::new(5.0, -7.0, 2.0));
        assert_eq!(-43.0, c.x);
        assert_eq!(-37.0, c.y);
        assert_eq!(-22.0, c.z);
    }

    #[test]
    fn binary_inverse_operator_methods() {
        let mut vec = Vector3F::new(5.0, 14.0, 13.0);
        vec = vec.rsub_scalar(8.0);
        assert_eq!(3.0, vec.x);
        assert_eq!(-6.0, vec.y);
        assert_eq!(-5.0, vec.z);

        vec = vec.rsub_vec(&Vector3F::new(-5.0, 3.0, -1.0));
        assert_eq!(-8.0, vec.x);
        assert_eq!(9.0, vec.y);
        assert_eq!(4.0, vec.z);

        vec = Vector3F::new(-12.0, -9.0, 8.0);
        vec = vec.rdiv_scalar(36.0);
        assert_eq!(-3.0, vec.x);
        assert_eq!(-4.0, vec.y);
        assert_eq!(4.5, vec.z);

        vec = vec.rdiv_vec(&Vector3F::new(3.0, -16.0, 18.0));
        assert_eq!(-1.0, vec.x);
        assert_eq!(4.0, vec.y);
        assert_eq!(4.0, vec.z);

        let c = vec.rcross(&Vector3F::new(5.0, -7.0, 3.0));
        assert_eq!(-40.0, c.x);
        assert_eq!(-23.0, c.y);
        assert_eq!(13.0, c.z);
    }

    #[test]
    fn augmented_operator_methods() {
        let mut vec = Vector3F::new(3.0, 9.0, 4.0);
        vec.iadd_scalar(4.0);
        assert_eq!(7.0, vec.x);
        assert_eq!(13.0, vec.y);
        assert_eq!(8.0, vec.z);

        vec.iadd_vec(&Vector3F::new(-2.0, 1.0, 5.0));
        assert_eq!(5.0, vec.x);
        assert_eq!(14.0, vec.y);
        assert_eq!(13.0, vec.z);

        vec.isub_scalar(8.0);
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(5.0, vec.z);

        vec.isub_vec(&Vector3F::new(-5.0, 3.0, 12.0));
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(-7.0, vec.z);

        vec.imul_scalar(2.0);
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(-14.0, vec.z);

        vec.imul_vec(&Vector3F::new(3.0, -2.0, 0.5));
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);
        assert_eq!(-7.0, vec.z);

        vec.idiv_scalar(4.0);
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);
        assert_eq!(-1.75, vec.z);

        vec.idiv_vec(&Vector3F::new(3.0, -1.0, 0.25));
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(-7.0, vec.z);
    }

    #[test]
    fn at_methods() {
        {
            let vec = Vector3F::new(8.0, 9.0, 1.0);
            assert_eq!(8.0, *vec.at(0));
            assert_eq!(9.0, *vec.at(1));
            assert_eq!(1.0, *vec.at(2));
        }
        {
            let mut vec = Vector3F::new(8.0, 9.0, 1.0);
            *vec.at_mut(0) = 7.0;
            *vec.at_mut(1) = 6.0;
            *vec.at_mut(2) = 5.0;
            assert_eq!(7.0, vec.x);
            assert_eq!(6.0, vec.y);
            assert_eq!(5.0, vec.z);
        }
    }

    #[test]
    fn basic_getter_methods() {
        let vec = Vector3F::new(3.0, 7.0, -1.0);
        let mut vec2 = Vector3F::new(-3.0, -7.0, 1.0);

        let sum = vec.sum();
        assert_eq!(9.0, sum);

        let avg = vec.avg();
        assert_eq!(3.0, avg);

        let min = vec.min();
        assert_eq!(-1.0, min);

        let max = vec.max();
        assert_eq!(7.0, max);

        let absmin = vec2.absmin();
        assert_eq!(1.0, absmin);

        let absmax = vec2.absmax();
        assert_eq!(-7.0, absmax);

        let daxis = vec.dominant_axis();
        assert_eq!(1, daxis);

        let saxis = vec.subminant_axis();
        assert_eq!(2, saxis);

        let eps = 1e-6;
        vec2 = vec.normalized();
        let mut len_sqr = vec2.x * vec2.x + vec2.y * vec2.y + vec2.z * vec2.z;
        assert_eq!(len_sqr - 1.0 < eps, true);

        vec2.imul_scalar(2.0);
        let len = vec2.length();
        assert_eq!(len - 2.0 < eps, true);

        len_sqr = vec2.length_squared();
        assert_eq!(len_sqr - 4.0 < eps, true);
    }

    #[test]
    fn bracket_operators() {
        let mut vec = Vector3F::new(8.0, 9.0, 1.0);
        assert_eq!(8.0, vec[0]);
        assert_eq!(9.0, vec[1]);
        assert_eq!(1.0, vec[2]);

        vec[0] = 7.0;
        vec[1] = 6.0;
        vec[2] = 5.0;
        assert_eq!(7.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(5.0, vec.z);
    }

    #[test]
    fn assignment_operators() {
        let vec = Vector3F::new(5.0, 1.0, 0.0);
        let vec2 = vec;
        assert_eq!(5.0, vec2.x);
        assert_eq!(1.0, vec2.y);
        assert_eq!(0.0, vec2.z);
    }

    #[test]
    fn augmented_operators() {
        let mut vec = Vector3F::new(3.0, 9.0, -2.0);
        vec += 4.0;
        assert_eq!(7.0, vec.x);
        assert_eq!(13.0, vec.y);
        assert_eq!(2.0, vec.z);

        vec += Vector3F::new(-2.0, 1.0, 5.0);
        assert_eq!(5.0, vec.x);
        assert_eq!(14.0, vec.y);
        assert_eq!(7.0, vec.z);

        vec -= 8.0;
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(-1.0, vec.z);

        vec -= Vector3F::new(-5.0, 3.0, -6.0);
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(5.0, vec.z);

        vec *= 2.0;
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(10.0, vec.z);

        vec *= Vector3F::new(3.0, -2.0, 0.4);
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);
        assert_eq!(4.0, vec.z);

        vec /= 4.0;
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);
        assert_eq!(1.0, vec.z);

        vec /= Vector3F::new(3.0, -1.0, 2.0);
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(0.5, vec.z);
    }

    #[test]
    fn equal_operators() {
        let vec2 = Vector3F::new(3.0, 7.0, 4.0);
        let vec3 = Vector3F::new(3.0, 5.0, 4.0);
        let vec4 = Vector3F::new(5.0, 1.0, 2.0);
        let vec = vec2;
        assert_eq!(vec == vec2, true);
        assert_eq!(vec == vec3, false);
        assert_eq!(vec != vec2, false);
        assert_eq!(vec != vec3, true);
        assert_eq!(vec != vec4, true);
    }

    #[test]
    fn min_max_functions() {
        let vec = Vector3F::new(5.0, 1.0, 0.0);
        let vec2 = Vector3F::new(3.0, 3.0, 3.0);
        let min_vector = min(&vec, &vec2);
        let max_vector = max(&vec, &vec2);
        assert_eq!(min_vector == Vector3F::new(3.0, 1.0, 0.0), true);
        assert_eq!(max_vector == Vector3F::new(5.0, 3.0, 3.0), true);
    }

    #[test]
    fn clamp_function() {
        let vec = Vector3F::new(2.0, 4.0, 1.0);
        let low = Vector3F::new(3.0, -1.0, 0.0);
        let high = Vector3F::new(5.0, 2.0, 3.0);
        let clamped_vec = clamp(&vec, &low, &high);
        assert_eq!(clamped_vec == Vector3F::new(3.0, 2.0, 1.0), true);
    }

    #[test]
    fn ceil_floor_functions() {
        let vec = Vector3F::new(2.2, 4.7, -0.2);
        let ceil_vec = ceil(&vec);
        assert_eq!(ceil_vec == Vector3F::new(3.0, 5.0, 0.0), true);

        let floor_vec = floor(&vec);
        assert_eq!(floor_vec == Vector3F::new(2.0, 4.0, -1.0), true);
    }

    #[test]
    fn binary_operators() {
        let mut vec = Vector3F::new(3.0, 9.0, 4.0);
        vec = vec + 4.0;
        assert_eq!(7.0, vec.x);
        assert_eq!(13.0, vec.y);
        assert_eq!(8.0, vec.z);

        vec = vec + Vector3F::new(-2.0, 1.0, 5.0);
        assert_eq!(5.0, vec.x);
        assert_eq!(14.0, vec.y);
        assert_eq!(13.0, vec.z);

        vec = vec - 8.0;
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(5.0, vec.z);

        vec = vec - Vector3F::new(-5.0, 3.0, 12.0);
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(-7.0, vec.z);

        vec = vec * 2.0;
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);
        assert_eq!(-14.0, vec.z);

        vec = vec * Vector3F::new(3.0, -2.0, 0.5);
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);
        assert_eq!(-7.0, vec.z);

        vec = vec / 4.0;
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);
        assert_eq!(-1.75, vec.z);

        vec = vec / Vector3F::new(3.0, -1.0, 0.25);
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
        assert_eq!(-7.0, vec.z);

        let v = Vector3D::new(2.0, 1.0, 3.0).normalized();
        let normal = Vector3D::new(1.0, 1.0, 1.0).normalized();

        let reflected = v.reflected(&normal);
        let reflected_answer = Vector3D::new(-2.0, -3.0, -1.0).normalized();
        assert_delta!(reflected.distance_to(&reflected_answer), 0.0, 1e-9);

        let projected = v.projected(&normal);
        assert_delta!(projected.dot(&normal), 0.0, 1e-9);

        let tangential = normal.tangential();
        assert_delta!((tangential.0).dot(&normal), 0.0, 1e-9);
        assert_delta!((tangential.1).dot(&normal), 0.0, 1e-9);
    }
}