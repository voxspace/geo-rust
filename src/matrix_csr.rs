/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::usize2::USize2;
use crate::vector_expression::VectorExpression;
use crate::matrix_expression::MatrixExpression;
use std::ops::*;
use std::cmp::Ordering::{Less, Greater};
use rayon::prelude::*;
use crate::operators::*;
use crate::vector_n::VectorN;

///
/// \brief Vector expression for CSR matrix-vector multiplication.
///
/// This vector expression represents a CSR matrix-vector operation that
/// takes one CSR input matrix expression and one vector expression.
///
/// \tparam T   Element value type.
/// \tparam VE  Vector expression.
///
pub struct MatrixCsrVectorMul<'a, VE: VectorExpression> {
    _m: &'a MatrixCsr,
    _v: &'a VE,
}

impl<'a, VE: VectorExpression> MatrixCsrVectorMul<'a, VE> {
    pub fn new(m: &'a MatrixCsr, v: &'a VE) -> MatrixCsrVectorMul<'a, VE> {
        debug_assert!(m.cols() == v.size());
        return MatrixCsrVectorMul {
            _m: m,
            _v: v,
        };
    }
}

impl<'a, VE: VectorExpression> VectorExpression for MatrixCsrVectorMul<'a, VE> {
    fn size(&self) -> usize {
        return self._m.rows();
    }

    fn eval(&self, i: usize) -> f64 {
        let rp = self._m.row_pointers_data();
        let ci = self._m.column_indices_data();
        let nnz = self._m.non_zero_data();

        let col_begin = rp[i];
        let col_end = rp[i + 1];

        let mut sum = 0.0;

        for jj in col_begin..col_end {
            let j = ci[jj];
            sum += nnz[jj] * self._v.eval(j);
        }

        return sum;
    }
}

///
/// \brief Matrix expression for CSR matrix-matrix multiplication.
///
/// This matrix expression represents a CSR matrix-matrix operation that
/// takes one CSR input matrix expression and one (probably dense) matrix
/// expression.
///
/// \tparam T   Element value type.
/// \tparam ME  Matrix expression.
///
pub struct MatrixCsrMatrixMul<'a, ME: MatrixExpression> {
    _m1: &'a MatrixCsr,
    _m2: &'a ME,
    _nnz: &'a [f64],
    _rp: &'a [usize],
    _ci: &'a [usize],
}

impl<'a, ME: MatrixExpression> MatrixCsrMatrixMul<'a, ME> {
    pub fn new(m1: &'a MatrixCsr, m2: &'a ME) -> MatrixCsrMatrixMul<'a, ME> {
        return MatrixCsrMatrixMul {
            _m1: m1,
            _m2: m2,
            _nnz: m1.non_zero_data(),
            _rp: m1.row_pointers_data(),
            _ci: m1.column_indices_data(),
        };
    }
}

impl<'a, ME: MatrixExpression> MatrixExpression for MatrixCsrMatrixMul<'a, ME> {
    fn size(&self) -> USize2 {
        return USize2::new(self.rows(), self.cols());
    }

    fn rows(&self) -> usize {
        return self._m1.rows();
    }

    fn cols(&self) -> usize {
        return self._m2.cols();
    }

    fn eval(&self, i: usize, j: usize) -> f64 {
        let col_begin = self._rp[i];
        let col_end = self._rp[i + 1];

        let mut sum = 0.0;
        for kk in col_begin..col_end {
            let k = self._ci[kk];
            sum += self._nnz[kk] * self._m2.eval(k, j);
        }

        return sum;
    }
}

//--------------------------------------------------------------------------------------------------
pub struct Element {
    pub i: usize,
    pub j: usize,
    pub value: f64,
}

impl Default for Element {
    fn default() -> Self {
        return Element {
            i: 0,
            j: 0,
            value: 0.0,
        };
    }
}

impl Element {
    pub fn new(i: usize, j: usize, value: f64) -> Element {
        return Element {
            i,
            j,
            value,
        };
    }
}

///
/// \brief Compressed Sparse Row (CSR) matrix class.
///
/// This class defines Compressed Sparse Row (CSR) matrix using arrays of
/// non-zero elements, row pointers, and column indices.
///
/// \see http://www.netlib.org/utk/people/JackDongarra/etemplates/node373.html
///
/// \tparam T Type of the element.
///
#[derive(Clone)]
pub struct MatrixCsr {
    _size: USize2,
    _non_zeros: Vec<f64>,
    _row_pointers: Vec<usize>,
    _column_indices: Vec<usize>,
}

impl MatrixExpression for MatrixCsr {
    fn size(&self) -> USize2 {
        return self._size;
    }

    fn rows(&self) -> usize {
        return self._size.x;
    }

    fn cols(&self) -> usize {
        return self._size.y;
    }

    fn eval(&self, x: usize, y: usize) -> f64 {
        let nz_index = self.has_element(x, y);
        return if nz_index == crate::constants::K_MAX_SIZE {
            0.0
        } else {
            self._non_zeros[nz_index]
        };
    }
}

impl Default for MatrixCsr {
    /// Constructs an empty matrix.
    fn default() -> Self {
        let mut mat = MatrixCsr {
            _size: USize2::default(),
            _non_zeros: vec![],
            _row_pointers: vec![],
            _column_indices: vec![],
        };
        mat.clear();
        return mat;
    }
}

/// # Constructors
impl MatrixCsr {
    ///
    /// \brief Compresses given initializer list \p lst into a sparse matrix.
    ///
    /// This constructor will build a matrix with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// MatrixCsr<float> mat = {
    ///     {1.f, 0.f, 0.f, 3.f},
    ///     {0.f, 3.f, 5.f, 1.f},
    ///     {4.f, 0.f, 1.f, 5.f}
    /// };
    /// \endcode
    ///
    /// Note the initializer has 4x3 structure which will create 4x3 matrix.
    /// During the process, zero elements (less than \p epsilon) will not be
    /// stored.
    ///
    /// \param lst Initializer list that should be copy to the new matrix.
    ///
    pub fn new_slice(lst: &[&[f64]], epsilon: Option<f64>) -> MatrixCsr {
        let mut mat = MatrixCsr {
            _size: USize2::default(),
            _non_zeros: vec![],
            _row_pointers: vec![],
            _column_indices: vec![],
        };
        mat.compress_lst(lst, epsilon);
        return mat;
    }

    ///
    /// \brief Compresses input (dense) matrix expression into a sparse matrix.
    ///
    /// This function sets this sparse matrix with dense input matrix.
    /// During the process, zero elements (less than \p epsilon) will not be
    /// stored.
    ///
    pub fn new_expression<E: MatrixExpression>(other: &E, epsilon: Option<f64>) -> MatrixCsr {
        let mut mat = MatrixCsr {
            _size: USize2::default(),
            _non_zeros: vec![],
            _row_pointers: vec![],
            _column_indices: vec![],
        };
        mat.compress(other, epsilon);
        return mat;
    }
}

/// # Basic setters
impl MatrixCsr {
    /// Clears the matrix and make it zero-dimensional.
    pub fn clear(&mut self) {
        self._size = USize2::default();
        self._non_zeros.clear();
        self._row_pointers.clear();
        self._column_indices.clear();
        self._row_pointers.push(0);
    }

    /// Sets whole matrix with input scalar.
    pub fn set_scalar(&mut self, s: f64) {
        self._non_zeros.fill(s);
    }

    /// Copy from given sparse matrix.
    pub fn set_self(&mut self, other: &MatrixCsr) {
        self._size = other._size.clone();
        self._non_zeros = other._non_zeros.clone();
        self._row_pointers = other._row_pointers.clone();
        self._column_indices = other._column_indices.clone();
    }

    /// Reserves memory space of this matrix.
    pub fn reserve(&mut self, rows: usize, cols: usize, num_non_zeros: usize) {
        self._size = USize2::new(rows, cols);
        self._row_pointers.resize(self._size.x + 1, 0);
        self._non_zeros.resize(num_non_zeros, 0.0);
        self._column_indices.resize(num_non_zeros, 0);
    }

    ///
    /// \brief Compresses given initializer list \p lst into a sparse matrix.
    ///
    /// This function will fill the matrix with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// MatrixCsr<float> mat;
    /// mat.compress({
    ///     {1.f, 0.f, 0.f, 3.f},
    ///     {0.f, 3.f, 5.f, 1.f},
    ///     {4.f, 0.f, 1.f, 5.f}
    /// });
    /// \endcode
    ///
    /// Note the initializer has 4x3 structure which will resize to 4x3 matrix.
    /// During the process, zero elements (less than \p epsilon) will not be
    /// stored.
    ///
    /// \param lst Initializer list that should be copy to the new matrix.
    ///
    pub fn compress_lst(&mut self, lst: &[&[f64]], epsilon: Option<f64>) {
        let num_rows = lst.len();
        let num_cols = if num_rows > 0 { lst[0].len() } else { 0 };

        self._size = USize2::new(num_rows, num_cols);
        self._non_zeros.clear();
        self._row_pointers.clear();
        self._column_indices.clear();

        for i in 0..num_rows {
            debug_assert!(num_cols == lst[i].len());
            self._row_pointers.push(self._non_zeros.len());

            for j in 0..num_cols {
                if f64::abs(lst[i][j]) > epsilon.unwrap_or(f64::EPSILON) {
                    self._non_zeros.push(lst[i][j]);
                    self._column_indices.push(j);
                }
            }
        }

        self._row_pointers.push(self._non_zeros.len());
    }

    ///
    /// \brief Compresses input (dense) matrix expression into a sparse matrix.
    ///
    /// This function sets this sparse matrix with dense input matrix.
    /// During the process, zero elements (less than \p epsilon) will not be
    /// stored.
    ///
    pub fn compress<E: MatrixExpression>(&mut self, other: &E, epsilon: Option<f64>) {
        let num_rows = other.rows();
        let num_cols = other.cols();

        self._size = USize2::new(num_rows, num_cols);
        self._non_zeros.clear();
        self._column_indices.clear();

        for i in 0..num_rows {
            self._row_pointers.push(self._non_zeros.len());

            for j in 0..num_cols {
                let val = other.eval(i, j);
                if f64::abs(val) > epsilon.unwrap_or(f64::EPSILON) {
                    self._non_zeros.push(val);
                    self._column_indices.push(j);
                }
            }
        }

        self._row_pointers.push(self._non_zeros.len());
    }

    /// Adds non-zero element to (i, j).
    pub fn add_element_with_location(&mut self, i: usize, j: usize, value: f64) {
        self.add_element(&Element::new(i, j, value));
    }

    /// Adds non-zero element.
    pub fn add_element(&mut self, element: &Element) {
        let num_rows_to_add = element.i as isize - self._size.x as isize + 1;
        if num_rows_to_add > 0 {
            for _ in 0..num_rows_to_add {
                self.add_row(&vec![], &vec![]);
            }
        }

        self._size.y = usize::max(self._size.y, element.j + 1);

        let row_begin = self._row_pointers[element.i];
        let row_end = self._row_pointers[element.i + 1];

        let offset = self._column_indices[row_begin..row_end].partition_point(|&val| {
            return val < element.j;
        }) + row_begin;

        self._column_indices.insert(offset, element.j);
        self._non_zeros.insert(offset, element.value);

        for i in element.i + 1..self._row_pointers.len() {
            self._row_pointers[i] += 1;
        }
    }

    ///
    /// Adds a row to the sparse matrix.
    ///
    /// \param non_zeros - Array of non-zero elements for the row.
    /// \param column_indices - Array of column indices for the row.
    ///
    pub fn add_row(&mut self, non_zeros: &Vec<f64>, column_indices: &Vec<usize>) {
        debug_assert!(non_zeros.len() == column_indices.len());

        self._size.x += 1;

        // TODO: Implement zip iterator
        let mut zipped: Vec<(f64, usize)> = Vec::new();
        for i in 0..non_zeros.len() {
            zipped.push((non_zeros[i], column_indices[i]));
            self._size.y = usize::max(self._size.y, column_indices[i] + 1);
        }
        zipped.sort_by(|a, b| {
            match a.1 < b.1 {
                true => Less,
                false => Greater
            }
        });
        for i in 0..zipped.len() {
            self._non_zeros.push(zipped[i].0);
            self._column_indices.push(zipped[i].1);
        }

        self._row_pointers.push(self._non_zeros.len());
    }

    /// Sets non-zero element to (i, j).
    pub fn set_element_with_location(&mut self, i: usize, j: usize, value: f64) {
        self.set_element(&Element::new(i, j, value));
    }

    /// Sets non-zero element.
    pub fn set_element(&mut self, element: &Element) {
        let nz_index = self.has_element(element.i, element.j);
        if nz_index == crate::constants::K_MAX_SIZE {
            self.add_element(element);
        } else {
            self._non_zeros[nz_index] = element.value;
        }
    }
}

/// # Basic getters
impl MatrixCsr {
    pub fn is_equal(&self, other: &MatrixCsr) -> bool {
        if self._size != other._size {
            return false;
        }

        if self._non_zeros.len() != other._non_zeros.len() {
            return false;
        }

        for i in 0..self._non_zeros.len() {
            if self._non_zeros[i] != other._non_zeros[i] {
                return false;
            }
            if self._column_indices[i] != other._column_indices[i] {
                return false;
            }
        }

        for i in 0..self._row_pointers.len() {
            if self._row_pointers[i] != other._row_pointers[i] {
                return false;
            }
        }

        return true;
    }

    /// Returns true if this matrix is similar to the input matrix within the
    /// given tolerance.
    pub fn is_similar(&self, other: &MatrixCsr, tol: Option<f64>) -> bool {
        if self._size != other._size {
            return false;
        }

        if self._non_zeros.len() != other._non_zeros.len() {
            return false;
        }

        for i in 0..self._non_zeros.len() {
            if f64::abs(self._non_zeros[i] - other._non_zeros[i]) > tol.unwrap_or(f64::EPSILON) {
                return false;
            }
            if self._column_indices[i] != other._column_indices[i] {
                return false;
            }
        }

        for i in 0..self._row_pointers.len() {
            if self._row_pointers[i] != other._row_pointers[i] {
                return false;
            }
        }

        return true;
    }

    /// Returns true if this matrix is a square matrix.
    pub fn is_square(&self) -> bool {
        return self.rows() == self.cols();
    }

    /// Returns the number of non-zero elements.
    pub fn number_of_non_zeros(&self) -> usize {
        return self._non_zeros.len();
    }

    /// Returns i-th non-zero element.
    pub fn non_zero(&self, i: usize) -> &f64 {
        return &self._non_zeros[i];
    }

    /// Returns i-th non-zero element.
    pub fn non_zero_mut(&mut self, i: usize) -> &mut f64 {
        return &mut self._non_zeros[i];
    }

    /// Returns i-th row pointer.
    pub fn row_pointer(&self, i: usize) -> &usize {
        return &self._row_pointers[i];
    }

    /// Returns i-th column index.
    pub fn column_index(&self, i: usize) -> &usize {
        return &self._column_indices[i];
    }

    /// Returns pointer of the non-zero elements data.
    pub fn non_zero_data_mut(&mut self) -> &mut [f64] {
        return &mut self._non_zeros;
    }

    /// Returns constant pointer of the non-zero elements data.
    pub fn non_zero_data(&self) -> &[f64] {
        return &self._non_zeros;
    }

    /// Returns constant pointer of the row pointers data.
    pub fn row_pointers_data(&self) -> &[usize] {
        return &self._row_pointers;
    }

    /// Returns constant pointer of the column indices data.
    pub fn column_indices_data(&self) -> &[usize] {
        return &self._column_indices;
    }
}

/// # Binary operator methods - new instance = this instance (+) input
impl MatrixCsr {
    /// Returns this matrix + input scalar.
    pub fn add_scalar(&self, s: f64) -> MatrixCsr {
        let mut ret = self.clone();
        (&mut ret._non_zeros).into_par_iter().for_each(|val| {
            *val += s;
        });
        return ret;
    }

    /// Returns this matrix + input matrix (element-wise).
    pub fn add_mat(&self, m: &MatrixCsr) -> MatrixCsr {
        return self.binary_op(m, Plus::new());
    }

    /// Returns this matrix - input scalar.
    pub fn sub_scalar(&self, s: f64) -> MatrixCsr {
        let mut ret = self.clone();
        (&mut ret._non_zeros).into_par_iter().for_each(|val| {
            *val -= s;
        });
        return ret;
    }

    /// Returns this matrix - input matrix (element-wise).
    pub fn sub_mat(&self, m: &MatrixCsr) -> MatrixCsr {
        return self.binary_op(m, Minus::new());
    }

    /// Returns this matrix * input scalar.
    pub fn mul_scalar(&self, s: f64) -> MatrixCsr {
        let mut ret = self.clone();
        (&mut ret._non_zeros).into_par_iter().for_each(|val| {
            *val *= s;
        });
        return ret;
    }

    /// Returns this matrix * input vector.
    pub fn mul_vec<'a, VE: VectorExpression>(&'a self, v: &'a VE) -> MatrixCsrVectorMul<VE> {
        return MatrixCsrVectorMul::new(self, v);
    }

    /// Returns this matrix * input matrix.
    pub fn mul_mat<'a, ME: MatrixExpression>(&'a self, m: &'a ME) -> MatrixCsrMatrixMul<ME> {
        return MatrixCsrMatrixMul::new(self, m);
    }

    /// Returns this matrix / input scalar.
    pub fn div_scalar(&self, s: f64) -> MatrixCsr {
        let mut ret = self.clone();
        (&mut ret._non_zeros).into_par_iter().for_each(|val| {
            *val /= s;
        });
        return ret;
    }
}

/// # Binary operator methods - new instance = input (+) this instance
impl MatrixCsr {
    /// Returns input scalar + this matrix.
    pub fn radd_scalar(&self, s: f64) -> MatrixCsr {
        return self.add_scalar(s);
    }

    /// Returns input matrix + this matrix (element-wise).
    pub fn radd_mat(&self, m: &MatrixCsr) -> MatrixCsr {
        return self.add_mat(m);
    }

    /// Returns input scalar - this matrix.
    pub fn rsub_scalar(&self, s: f64) -> MatrixCsr {
        let mut ret = self.clone();
        (&mut ret._non_zeros).into_par_iter().for_each(|val| {
            *val = s - *val;
        });
        return ret;
    }

    /// Returns input matrix - this matrix (element-wise).
    pub fn rsub_mat(&self, m: &MatrixCsr) -> MatrixCsr {
        return m.sub_mat(self);
    }

    /// Returns input scalar * this matrix.
    pub fn rmul_scalar(&self, s: f64) -> MatrixCsr {
        return self.mul_scalar(s);
    }

    /// Returns input matrix / this scalar.
    pub fn rdiv_scalar(&self, s: f64) -> MatrixCsr {
        let mut ret = self.clone();
        (&mut ret._non_zeros).into_par_iter().for_each(|val| {
            *val = s / *val;
        });
        return ret;
    }
}

/// # Augmented operator methods - this instance (+)= input
impl MatrixCsr {
    /// Adds input scalar to this matrix.
    pub fn iadd_scalar(&mut self, s: f64) {
        (&mut self._non_zeros).into_par_iter().for_each(|val| {
            *val += s;
        });
    }

    /// Adds input matrix to this matrix (element-wise).
    pub fn iadd_mat(&mut self, m: &MatrixCsr) {
        self.set_self(&self.add_mat(m));
    }

    /// Subtracts input scalar from this matrix.
    pub fn isub_scalar(&mut self, s: f64) {
        (&mut self._non_zeros).into_par_iter().for_each(|val| {
            *val -= s;
        });
    }

    /// Subtracts input matrix from this matrix (element-wise).
    pub fn isub_mat(&mut self, m: &MatrixCsr) {
        self.set_self(&self.sub_mat(m));
    }

    /// Multiplies input scalar to this matrix.
    pub fn imul_scalar(&mut self, s: f64) {
        (&mut self._non_zeros).into_par_iter().for_each(|val| {
            *val *= s;
        });
    }

    /// Multiplies input matrix to this matrix.
    pub fn imul_mat<ME: MatrixExpression>(&mut self, m: &ME) {
        let result = MatrixCsr::new_expression(&self.mul_mat(m), None);
        *self = result;
    }

    /// Divides this matrix with input scalar.
    pub fn idiv_scalar(&mut self, s: f64) {
        (&mut self._non_zeros).into_par_iter().for_each(|val| {
            *val /= s;
        });
    }
}

/// # Complex getters
impl MatrixCsr {
    /// Returns sum of all elements.
    pub fn sum(&self) -> f64 {
        return self._non_zeros.par_iter().sum();
    }

    /// Returns average of all elements.
    pub fn avg(&self) -> f64 {
        return self.sum() / self.number_of_non_zeros() as f64;
    }

    /// Returns minimum among all elements.
    pub fn min(&self) -> f64 {
        return self._non_zeros.par_iter().cloned().reduce(|| { f64::MAX }, |a, b| {
            return f64::min(a, b);
        });
    }

    /// Returns maximum among all elements.
    pub fn max(&self) -> f64 {
        return self._non_zeros.par_iter().cloned().reduce(|| { f64::MIN }, |a, b| {
            return f64::max(a, b);
        });
    }

    /// Returns absolute minimum among all elements.
    pub fn absmin(&self) -> f64 {
        return self._non_zeros.par_iter().cloned().reduce(|| { f64::MAX }, |a, b| {
            return crate::math_utils::absmin(a, b);
        });
    }

    /// Returns absolute maximum among all elements.
    pub fn absmax(&self) -> f64 {
        return self._non_zeros.par_iter().cloned().reduce(|| { 0.0 }, |a, b| {
            return crate::math_utils::absmax(a, b);
        });
    }

    /// Returns sum of all diagonal elements.
    /// \warning Should be a square matrix.
    pub fn trace(&self) -> f64 {
        //todo parallelism
        let mut result = 0.0;
        for i in 0..self.rows() {
            result += self[(i, i)];
        }
        return result;
    }
}

/// # Builders
impl MatrixCsr {
    /// Makes a m x m matrix with all diagonal elements to 1, and other elements
    /// to 0.
    pub fn make_identity(m: usize) -> MatrixCsr {
        let mut ret = MatrixCsr::default();
        ret._size = USize2::new(m, m);
        ret._non_zeros.resize(m, 1.0);
        ret._column_indices.resize(m, 0);
        ret._column_indices = (0..ret._column_indices.len()).collect();
        ret._row_pointers.resize(m + 1, 0);
        ret._row_pointers = (0..ret._row_pointers.len()).collect();
        return ret;
    }
}

impl MatrixCsr {
    fn has_element(&self, i: usize, j: usize) -> usize {
        if i >= self._size.x || j >= self._size.y {
            return crate::constants::K_MAX_SIZE;
        }

        let row_begin = self._row_pointers[i];
        let row_end = self._row_pointers[i + 1];

        let iter = self._column_indices[row_begin..row_end].binary_search(&j);

        return if iter.is_ok() {
            iter.ok().unwrap() + row_begin
        } else {
            crate::constants::K_MAX_SIZE
        };
    }

    fn binary_op<Op: BinaryOp>(&self, m: &MatrixCsr, op: Op) -> MatrixCsr {
        debug_assert!(self._size == m._size);

        let mut ret = MatrixCsr::default();

        for i in 0..self._size.x {
            let mut col: Vec<usize> = Vec::new();
            let mut nnz: Vec<f64> = Vec::new();

            let mut col_iter_a = self._row_pointers[i];
            let mut col_iter_b = m._row_pointers[i];
            let col_end_a = self._row_pointers[i + 1];
            let col_end_b = m._row_pointers[i + 1];
            let mut nnz_iter_a = self._row_pointers[i];
            let mut nnz_iter_b = m._row_pointers[i];

            while col_iter_a != col_end_a || col_iter_b != col_end_b {
                if col_iter_b == col_end_b || self._column_indices[col_iter_a] < m._column_indices[col_iter_b] {
                    col.push(self._column_indices[col_iter_a]);
                    nnz.push(op.eval(self._non_zeros[nnz_iter_a], 0.0));
                    col_iter_a += 1;
                    nnz_iter_a += 1;
                } else if col_iter_a == col_end_a || self._column_indices[col_iter_a] > m._column_indices[col_iter_b] {
                    col.push(m._column_indices[col_iter_b]);
                    nnz.push(op.eval(0.0, m._non_zeros[nnz_iter_b]));
                    col_iter_b += 1;
                    nnz_iter_b += 1;
                } else {
                    debug_assert!(self._column_indices[col_iter_a] == m._column_indices[col_iter_b]);
                    col.push(m._column_indices[col_iter_b]);
                    nnz.push(op.eval(self._non_zeros[nnz_iter_a], m._non_zeros[nnz_iter_b]));
                    col_iter_a += 1;
                    nnz_iter_a += 1;
                    col_iter_b += 1;
                    nnz_iter_b += 1;
                }
            }

            ret.add_row(&nnz, &col);
        }

        return ret;
    }
}

impl AddAssign<f64> for MatrixCsr {
    fn add_assign(&mut self, rhs: f64) {
        self.iadd_scalar(rhs);
    }
}

impl AddAssign<&MatrixCsr> for MatrixCsr {
    fn add_assign(&mut self, rhs: &MatrixCsr) {
        self.iadd_mat(rhs);
    }
}

impl SubAssign<f64> for MatrixCsr {
    fn sub_assign(&mut self, rhs: f64) {
        self.isub_scalar(rhs);
    }
}


impl SubAssign<&MatrixCsr> for MatrixCsr {
    fn sub_assign(&mut self, rhs: &MatrixCsr) {
        self.isub_mat(rhs);
    }
}

impl MulAssign<f64> for MatrixCsr {
    fn mul_assign(&mut self, rhs: f64) {
        self.imul_scalar(rhs);
    }
}

impl MulAssign<&MatrixCsr> for MatrixCsr {
    fn mul_assign(&mut self, rhs: &MatrixCsr) {
        self.imul_mat(rhs);
    }
}

impl DivAssign<f64> for MatrixCsr {
    fn div_assign(&mut self, rhs: f64) {
        self.idiv_scalar(rhs);
    }
}

impl Index<(usize, usize)> for MatrixCsr {
    type Output = f64;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        let nz_index = self.has_element(index.0, index.1);
        return if nz_index == crate::constants::K_MAX_SIZE {
            &0.0
        } else {
            &self._non_zeros[nz_index]
        };
    }
}

impl PartialEq for MatrixCsr {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

//--------------------------------------------------------------------------------------------------
/// Note lazy operator anymore
impl Add<f64> for MatrixCsr {
    type Output = MatrixCsr;

    fn add(self, rhs: f64) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

impl Add<MatrixCsr> for MatrixCsr {
    type Output = MatrixCsr;

    fn add(self, rhs: MatrixCsr) -> Self::Output {
        return self.add_mat(&rhs);
    }
}

impl Sub<f64> for MatrixCsr {
    type Output = MatrixCsr;

    fn sub(self, rhs: f64) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

impl Sub<MatrixCsr> for MatrixCsr {
    type Output = MatrixCsr;

    fn sub(self, rhs: MatrixCsr) -> Self::Output {
        return self.sub_mat(&rhs);
    }
}

impl Mul<f64> for MatrixCsr {
    type Output = MatrixCsr;

    fn mul(self, rhs: f64) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

impl Mul<VectorN> for MatrixCsr {
    type Output = VectorN;

    fn mul(self, rhs: VectorN) -> Self::Output {
        return VectorN::new_expression(&self.mul_vec(&rhs));
    }
}

impl Mul<MatrixCsr> for MatrixCsr {
    type Output = MatrixCsr;

    fn mul(self, rhs: MatrixCsr) -> Self::Output {
        return MatrixCsr::new_expression(&self.mul_mat(&rhs), None);
    }
}

impl Div<f64> for MatrixCsr {
    type Output = MatrixCsr;

    fn div(self, rhs: f64) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod matrix_csr {
    use crate::matrix_csr::MatrixCsr;
    use crate::matrix_expression::MatrixExpression;
    use crate::matrix_mxn::MatrixMxN;
    use crate::vector_n::VectorN;

    #[test]
    fn constructors() {
        let empty_mat = MatrixCsr::default();

        assert_eq!(0, empty_mat.rows());
        assert_eq!(0, empty_mat.cols());
        assert_eq!(0, empty_mat.number_of_non_zeros());
        assert_eq!(0, empty_mat.non_zero_data().len());
        assert_eq!(1, empty_mat.row_pointers_data().len());
        assert_eq!(0, empty_mat.column_indices_data().len());

        let mat_init_lst = MatrixCsr::new_slice(&[
            &[1.0, 0.0, 0.0, -3.0], &[0.0, 3.0, -5.0, 1.0], &[-4.0, 0.0, 1.0, 5.0]], None);
        assert_eq!(3, mat_init_lst.rows());
        assert_eq!(4, mat_init_lst.cols());
        assert_eq!(8, mat_init_lst.number_of_non_zeros());

        let iter_init_lst = mat_init_lst.non_zero_data();
        assert_eq!(1.0, iter_init_lst[0]);
        assert_eq!(-3.0, iter_init_lst[1]);
        assert_eq!(3.0, iter_init_lst[2]);
        assert_eq!(-5.0, iter_init_lst[3]);
        assert_eq!(1.0, iter_init_lst[4]);
        assert_eq!(-4.0, iter_init_lst[5]);
        assert_eq!(1.0, iter_init_lst[6]);
        assert_eq!(5.0, iter_init_lst[7]);

        let mat_dense = MatrixCsr::new_slice(&[
            &[1.0, 0.0, 0.0, -3.0], &[0.0, 3.0, -5.0, 1.0], &[-4.0, 0.0, 1.0, 5.0]], None);
        let mat_sparse = MatrixCsr::new_expression(&mat_dense, Some(0.02));
        assert_eq!(3, mat_sparse.rows());
        assert_eq!(4, mat_sparse.cols());
        assert_eq!(8, mat_sparse.number_of_non_zeros());

        let iter_sparse = mat_sparse.non_zero_data();
        assert_eq!(1.0, iter_sparse[0]);
        assert_eq!(-3.0, iter_sparse[1]);
        assert_eq!(3.0, iter_sparse[2]);
        assert_eq!(-5.0, iter_sparse[3]);
        assert_eq!(1.0, iter_sparse[4]);
        assert_eq!(-4.0, iter_sparse[5]);
        assert_eq!(1.0, iter_sparse[6]);
        assert_eq!(5.0, iter_sparse[7]);

        let mat_copied = mat_sparse.clone();
        assert_eq!(3, mat_copied.rows());
        assert_eq!(4, mat_copied.cols());
        assert_eq!(8, mat_copied.number_of_non_zeros());

        let iter_copied = mat_copied.non_zero_data();
        assert_eq!(1.0, iter_copied[0]);
        assert_eq!(-3.0, iter_copied[1]);
        assert_eq!(3.0, iter_copied[2]);
        assert_eq!(-5.0, iter_copied[3]);
        assert_eq!(1.0, iter_copied[4]);
        assert_eq!(-4.0, iter_copied[5]);
        assert_eq!(1.0, iter_copied[6]);
        assert_eq!(5.0, iter_copied[7]);

        let mat_moved = mat_copied;
        assert_eq!(3, mat_moved.rows());
        assert_eq!(4, mat_moved.cols());
        assert_eq!(8, mat_moved.number_of_non_zeros());

        let iter_moved = mat_moved.non_zero_data();
        assert_eq!(1.0, iter_moved[0]);
        assert_eq!(-3.0, iter_moved[1]);
        assert_eq!(3.0, iter_moved[2]);
        assert_eq!(-5.0, iter_moved[3]);
        assert_eq!(1.0, iter_moved[4]);
        assert_eq!(-4.0, iter_moved[5]);
        assert_eq!(1.0, iter_moved[6]);
        assert_eq!(5.0, iter_moved[7]);
    }

    #[test]
    fn basic_setters() {
        // Compress initializer list
        let mut mat_init_lst = MatrixCsr::default();
        mat_init_lst.compress_lst(&[
            &[1.0, 0.01, 0.0, -3.0],
            &[0.01, 3.0, -5.0, 1.0],
            &[-4.0, 0.01, 1.0, 5.0]], Some(0.02));
        assert_eq!(3, mat_init_lst.rows());
        assert_eq!(4, mat_init_lst.cols());
        assert_eq!(8, mat_init_lst.number_of_non_zeros());

        let iter_init_lst = mat_init_lst.non_zero_data();
        assert_eq!(1.0, iter_init_lst[0]);
        assert_eq!(-3.0, iter_init_lst[1]);
        assert_eq!(3.0, iter_init_lst[2]);
        assert_eq!(-5.0, iter_init_lst[3]);
        assert_eq!(1.0, iter_init_lst[4]);
        assert_eq!(-4.0, iter_init_lst[5]);
        assert_eq!(1.0, iter_init_lst[6]);
        assert_eq!(5.0, iter_init_lst[7]);

        // Set scalar
        mat_init_lst.set_scalar(42.0);
        let mut iter_init_lst = mat_init_lst.non_zero_data();
        for i in 0..8 {
            assert_eq!(42.0, iter_init_lst[i]);
        }

        // Compress dense matrix
        let mat_dense = MatrixMxN::new_slice(&[
            &[1.0, 0.01, 0.0, -3.0],
            &[0.01, 3.0, -5.0, 1.0],
            &[-4.0, 0.01, 1.0, 5.0]]);
        let mut mat_sparse = MatrixCsr::default();
        mat_sparse.compress(&mat_dense, Some(0.02));
        assert_eq!(3, mat_sparse.rows());
        assert_eq!(4, mat_sparse.cols());
        assert_eq!(8, mat_sparse.number_of_non_zeros());

        let iter_sparse = mat_sparse.non_zero_data();
        assert_eq!(1.0, iter_sparse[0]);
        assert_eq!(-3.0, iter_sparse[1]);
        assert_eq!(3.0, iter_sparse[2]);
        assert_eq!(-5.0, iter_sparse[3]);
        assert_eq!(1.0, iter_sparse[4]);
        assert_eq!(-4.0, iter_sparse[5]);
        assert_eq!(1.0, iter_sparse[6]);
        assert_eq!(5.0, iter_sparse[7]);

        // Set other CSR mat
        mat_init_lst.set_self(&mat_sparse);
        iter_init_lst = mat_init_lst.non_zero_data();

        assert_eq!(1.0, iter_init_lst[0]);
        for i in 0..8 {
            assert_eq!(iter_sparse[i], iter_init_lst[i]);
        }

        // Add/set element
        let mut mat_add_elem = MatrixCsr::default();
        mat_add_elem.add_element_with_location(0, 0, 1.0);
        mat_add_elem.set_element_with_location(0, 3, -3.0);
        mat_add_elem.add_element_with_location(1, 1, 3.0);
        mat_add_elem.set_element_with_location(1, 2, -5.0);
        mat_add_elem.add_element_with_location(1, 3, 1.0);
        mat_add_elem.set_element_with_location(2, 0, -4.0);
        mat_add_elem.add_element_with_location(2, 2, 1.0);
        mat_add_elem.set_element_with_location(2, 3, 5.0);

        assert_eq!(3, mat_add_elem.rows());
        assert_eq!(4, mat_add_elem.cols());
        assert_eq!(8, mat_add_elem.number_of_non_zeros());

        let iter_add_elem = mat_add_elem.non_zero_data();
        assert_eq!(1.0, iter_add_elem[0]);
        assert_eq!(-3.0, iter_add_elem[1]);
        assert_eq!(3.0, iter_add_elem[2]);
        assert_eq!(-5.0, iter_add_elem[3]);
        assert_eq!(1.0, iter_add_elem[4]);
        assert_eq!(-4.0, iter_add_elem[5]);
        assert_eq!(1.0, iter_add_elem[6]);
        assert_eq!(5.0, iter_add_elem[7]);

        mat_add_elem.set_element_with_location(1, 3, 7.0);
        let iter_add_elem = mat_add_elem.non_zero_data();
        assert_eq!(7.0, iter_add_elem[4]);

        // Add element in random order
        let mut mat_add_elem_random = MatrixCsr::default();
        mat_add_elem_random.add_element_with_location(2, 2, 1.0);
        mat_add_elem_random.add_element_with_location(0, 3, -3.0);
        mat_add_elem_random.add_element_with_location(2, 0, -4.0);
        mat_add_elem_random.add_element_with_location(1, 1, 3.0);
        mat_add_elem_random.add_element_with_location(2, 3, 5.0);
        mat_add_elem_random.add_element_with_location(1, 3, 1.0);
        mat_add_elem_random.add_element_with_location(1, 2, -5.0);
        mat_add_elem_random.add_element_with_location(0, 0, 1.0);

        assert_eq!(3, mat_add_elem_random.rows());
        assert_eq!(4, mat_add_elem_random.cols());
        assert_eq!(8, mat_add_elem_random.number_of_non_zeros());

        let iter_add_elem_random = mat_add_elem_random.non_zero_data();
        assert_eq!(1.0, iter_add_elem_random[0]);
        assert_eq!(-3.0, iter_add_elem_random[1]);
        assert_eq!(3.0, iter_add_elem_random[2]);
        assert_eq!(-5.0, iter_add_elem_random[3]);
        assert_eq!(1.0, iter_add_elem_random[4]);
        assert_eq!(-4.0, iter_add_elem_random[5]);
        assert_eq!(1.0, iter_add_elem_random[6]);
        assert_eq!(5.0, iter_add_elem_random[7]);

        // Add row
        let mut mat_add_row = MatrixCsr::default();
        mat_add_row.add_row(&vec![1.0, -3.0], &vec![0, 3]);
        mat_add_row.add_row(&vec![3.0, -5.0, 1.0], &vec![1, 2, 3]);
        mat_add_row.add_row(&vec![-4.0, 1.0, 5.0], &vec![0, 2, 3]);

        assert_eq!(3, mat_add_row.rows());
        assert_eq!(4, mat_add_row.cols());
        assert_eq!(8, mat_add_row.number_of_non_zeros());

        let iter_add_row = mat_add_row.non_zero_data();
        assert_eq!(1.0, iter_add_row[0]);
        assert_eq!(-3.0, iter_add_row[1]);
        assert_eq!(3.0, iter_add_row[2]);
        assert_eq!(-5.0, iter_add_row[3]);
        assert_eq!(1.0, iter_add_row[4]);
        assert_eq!(-4.0, iter_add_row[5]);
        assert_eq!(1.0, iter_add_row[6]);
        assert_eq!(5.0, iter_add_row[7]);

        // Clear
        mat_add_row.clear();
        assert_eq!(0, mat_add_row.rows());
        assert_eq!(0, mat_add_row.cols());
        assert_eq!(0, mat_add_row.number_of_non_zeros());
        assert_eq!(1, mat_add_row.row_pointers_data().len());
        assert_eq!(0, mat_add_row.row_pointers_data()[0]);
    }

    #[test]
    fn binary_operator_methods() {
        let mat_a = MatrixCsr::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]], None);
        let add_result1 = mat_a.add_scalar(3.5);
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, *add_result1.non_zero(i));
        }

        let mat_c = MatrixCsr::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]], None);
        let add_result2 = mat_a.add_mat(&mat_c);
        let add_ans1 = MatrixCsr::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]], None);
        assert_eq!(add_ans1.is_equal(&add_result2), true);

        let mat_d = MatrixCsr::new_slice(&[&[3.0, 0.0, 2.0], &[0.0, 2.0, 0.0]], None);
        let add_result3 = mat_a.add_mat(&mat_d);
        let add_ans2 = MatrixCsr::new_slice(&[&[4.0, 2.0, 5.0], &[4.0, 7.0, 6.0]], None);
        assert_eq!(add_ans2.is_equal(&add_result3), true);

        let mat_e = MatrixCsr::new_slice(&[&[3.0, 0.0, 2.0], &[0.0, 0.0, 0.0]], None);
        let add_result4 = mat_a.add_mat(&mat_e);
        let add_ans3 = MatrixCsr::new_slice(&[&[4.0, 2.0, 5.0], &[4.0, 5.0, 6.0]], None);
        assert_eq!(add_ans3.is_equal(&add_result4), true);

        let sub_result1 = mat_a.sub_scalar(1.5);
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, *sub_result1.non_zero(i));
        }

        let sub_result2 = mat_a.sub_mat(&mat_c);
        let ans2 = MatrixCsr::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]], None);
        assert_eq!(ans2.is_similar(&sub_result2, None), true);

        let mat_b = mat_a.mul_scalar(2.0);
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), *mat_b.non_zero(i));
        }

        let vec_a = VectorN::new_slice(&[-1.0, 9.0, 8.0]);
        let vec_b = VectorN::new_expression(&mat_a.mul_vec(&vec_a));
        let ans_v = VectorN::new_slice(&[41.0, 89.0]);
        assert_eq!(ans_v.is_similar(&vec_b, None), true);

        let mat_f = MatrixCsr::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]], None);
        let mat_g = MatrixCsr::new_expression(&mat_a.mul_mat(&mat_f), None);
        let ans3 = MatrixCsr::new_slice(&[&[13.0, 41.0], &[34.0, 89.0]], None);
        assert_eq!(ans3.is_equal(&mat_g), true);

        let mat_h = mat_a.div_scalar(2.0);
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, *mat_h.non_zero(i));
        }

        let mat_i = mat_a.radd_scalar(3.5);
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, *mat_i.non_zero(i));
        }

        let mat_j = mat_a.rsub_scalar(1.5);
        for i in 0..6 {
            assert_eq!(0.5 - i as f64, *mat_j.non_zero(i));
        }

        let mat_k = MatrixCsr::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]], None);
        let mat_l = mat_a.rsub_mat(&mat_k);
        let ans4 = MatrixCsr::new_slice(&[&[2.0, -3.0, -1.0], &[5.0, -3.0, 2.0]], None);
        assert_eq!(ans4.is_equal(&mat_l), true);

        let mat_m = mat_a.rmul_scalar(2.0);
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), *mat_m.non_zero(i));
        }

        let mat_p = mat_a.rdiv_scalar(2.0);
        for i in 0..6 {
            assert_eq!(2.0 / (i as f64 + 1.0), *mat_p.non_zero(i));
        }
    }

    #[test]
    fn augmented_methods() {
        let mat_a = MatrixCsr::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]], None);
        let mat_b = MatrixCsr::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]], None);

        let mut mat = mat_a.clone();
        mat.iadd_scalar(3.5);
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, *mat.non_zero(i));
        }

        mat = mat_a.clone();
        mat.iadd_mat(&mat_b);
        let mut ans = MatrixCsr::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]], None);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat.isub_scalar(1.5);
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, *mat.non_zero(i));
        }

        mat = mat_a.clone();
        mat.isub_mat(&mat_b);
        ans = MatrixCsr::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]], None);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat.imul_scalar(2.0);
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), *mat.non_zero(i));
        }

        let mut mat_a2 = MatrixCsr::default();
        let mut mat_c2 = MatrixCsr::default();
        let mut cnt = 0;
        for i in 0..5 {
            for j in 0..5 {
                mat_a2.set_element_with_location(i, j, cnt as f64 + 1.0);
                mat_c2.set_element_with_location(i, j, 25.0 - cnt as f64);
                cnt += 1;
            }
        }
        mat_a2.imul_mat(&mat_c2);

        let ans2 = MatrixCsr::new_slice(&[
            &[175.0, 160.0, 145.0, 130.0, 115.0],
            &[550.0, 510.0, 470.0, 430.0, 390.0],
            &[925.0, 860.0, 795.0, 730.0, 665.0],
            &[1300.0, 1210.0, 1120.0, 1030.0, 940.0],
            &[1675.0, 1560.0, 1445.0, 1330.0, 1215.0]], None);
        assert_eq!(ans2.is_similar(&mat_a2, None), true);

        mat = mat_a.clone();
        mat.idiv_scalar(2.0);
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, *mat.non_zero(i));
        }
    }

    #[test]
    fn complex_getters() {
        let mat_a = MatrixCsr::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]], None);

        assert_eq!(21.0, mat_a.sum());
        assert_eq!(21.0 / 6.0, mat_a.avg());

        let mat_b = MatrixCsr::new_slice(&[&[3.0, -1.0, 2.0], &[-9.0, 2.0, 8.0]], None);
        assert_eq!(-9.0, mat_b.min());
        assert_eq!(8.0, mat_b.max());
        assert_eq!(-1.0, mat_b.absmin());
        assert_eq!(-9.0, mat_b.absmax());

        let mat_c = MatrixCsr::new_slice(&[
            &[3.0, -1.0, 2.0, 4.0, 5.0],
            &[-9.0, 2.0, 8.0, -1.0, 2.0],
            &[4.0, 3.0, 6.0, 7.0, -5.0],
            &[-2.0, 6.0, 7.0, 1.0, 0.0],
            &[4.0, 2.0, 3.0, 3.0, -9.0]], None);
        assert_eq!(3.0, mat_c.trace());
    }

    #[test]
    fn setter_operators() {
        let mat_a = MatrixCsr::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]], None);
        let mat_b = MatrixCsr::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]], None);

        let mut mat = mat_a.clone();
        mat += 3.5;
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, *mat.non_zero(i));
        }

        mat = mat_a.clone();
        mat += &mat_b;
        let mut ans = MatrixCsr::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]], None);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat -= 1.5;
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, *mat.non_zero(i));
        }

        mat = mat_a.clone();
        mat -= &mat_b;
        ans = MatrixCsr::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]], None);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat *= 2.0;
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), *mat.non_zero(i));
        }

        let mut mat_a2 = MatrixCsr::default();
        let mut mat_c2 = MatrixCsr::default();
        let mut cnt = 0;
        for i in 0..5 {
            for j in 0..5 {
                mat_a2.set_element_with_location(i, j, cnt as f64 + 1.0);
                mat_c2.set_element_with_location(i, j, 25.0 - cnt as f64);
                cnt += 1;
            }
        }
        mat_a2 *= &mat_c2;

        let ans2 = MatrixCsr::new_slice(&[
            &[175.0, 160.0, 145.0, 130.0, 115.0],
            &[550.0, 510.0, 470.0, 430.0, 390.0],
            &[925.0, 860.0, 795.0, 730.0, 665.0],
            &[1300.0, 1210.0, 1120.0, 1030.0, 940.0],
            &[1675.0, 1560.0, 1445.0, 1330.0, 1215.0]], None);
        assert_eq!(ans2.is_equal(&mat_a2), true);

        mat = mat_a.clone();
        mat /= 2.0;
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, *mat.non_zero(i));
        }
    }

    #[test]
    fn getter_operators() {
        let mat_dense = MatrixCsr::new_slice(&[
            &[1.0, 0.0, 0.0, -3.0], &[0.0, 3.0, -5.0, 1.0], &[-4.0, 0.0, 1.0, 5.0]], None);
        let mat_sparse = MatrixCsr::new_expression(&mat_dense, Some(0.02));
        for i in 0..3 {
            for j in 0..4 {
                assert_eq!(mat_dense[(i, j)], mat_sparse[(i, j)]);
            }
        }
    }

    #[test]
    fn builders() {
        let mat_iden = MatrixCsr::make_identity(5);
        for i in 0..5 {
            for j in 0..5 {
                if i == j {
                    assert_eq!(1.0, mat_iden[(i, j)]);
                } else {
                    assert_eq!(0.0, mat_iden[(i, j)]);
                }
            }
        }
    }

    #[test]
    fn operator_overloadings() {
        let mat_a = MatrixCsr::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]], None);
        let add_result1 = mat_a.clone() + 3.5;
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, *add_result1.non_zero(i));
        }

        let mat_c = MatrixCsr::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]], None);
        let add_result2 = mat_a.clone() + mat_c.clone();
        let add_ans1 = MatrixCsr::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]], None);
        assert_eq!(add_ans1.is_equal(&add_result2), true);

        let mat_d = MatrixCsr::new_slice(&[&[3.0, 0.0, 2.0], &[0.0, 2.0, 0.0]], None);
        let add_result3 = mat_a.clone() + mat_d;
        let add_ans2 = MatrixCsr::new_slice(&[&[4.0, 2.0, 5.0], &[4.0, 7.0, 6.0]], None);
        assert_eq!(add_ans2.is_equal(&add_result3), true);

        let mat_e = MatrixCsr::new_slice(&[&[3.0, 0.0, 2.0], &[0.0, 0.0, 0.0]], None);
        let add_result4 = mat_a.clone() + mat_e;
        let add_ans3 = MatrixCsr::new_slice(&[&[4.0, 2.0, 5.0], &[4.0, 5.0, 6.0]], None);
        assert_eq!(add_ans3.is_equal(&add_result4), true);

        let sub_result1 = mat_a.clone() - 1.5;
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, *sub_result1.non_zero(i));
        }

        let sub_result2 = mat_a.clone() - mat_c.clone();
        let ans2 = MatrixCsr::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]], None);
        assert_eq!(ans2.is_similar(&sub_result2, None), true);

        let mat_b = mat_a.clone() * 2.0;
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), *mat_b.non_zero(i));
        }

        let vec_a = VectorN::new_slice(&[-1.0, 9.0, 8.0]);
        let vec_b = mat_a.clone() * vec_a;
        let ans_v = VectorN::new_slice(&[41.0, 89.0]);
        assert_eq!(ans_v.is_equal(&vec_b), true);

        let mat_f = MatrixCsr::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]], None);
        let mat_g = mat_a.clone() * mat_f;
        let ans3 = MatrixCsr::new_slice(&[&[13.0, 41.0], &[34.0, 89.0]], None);
        assert_eq!(ans3.is_equal(&mat_g), true);

        let mat_h = mat_a.clone() / 2.0;
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, *mat_h.non_zero(i));
        }

        // let matI = 3.5 + mat_a;
        // for i in 0..6 {
        //     EXPECT_EQ(i + 4.5, matI.nonZero(i));
        // }
        //
        // let matJ = 1.5 - mat_a;
        // for i in 0..6 {
        //     EXPECT_EQ(0.5 - i, matJ.nonZero(i));
        // }
        //
        // let matM = 2.0 * mat_a;
        // for i in 0..6 {
        //     EXPECT_EQ(2.0 * (i + 1.0), matM.nonZero(i));
        // }
        //
        // let matP = 2.0 / mat_a;
        // for i in 0..6 {
        //     EXPECT_EQ(2.0 / (i + 1.0), matP.nonZero(i));
        // }
    }
}