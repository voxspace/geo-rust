/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */


use crate::point_generator2::PointGenerator2;
use crate::vector2::Vector2D;
use crate::bounding_box2::BoundingBox2D;
use std::sync::{RwLock, Arc};

///
/// # 2-D regular-grid point generator.
///
pub struct GridPointGenerator2 {}

impl PointGenerator2 for GridPointGenerator2 {
    fn for_each_point<Callback>(&self, bounding_box: &BoundingBox2D, spacing: f64, callback: &mut Callback)
        where Callback: FnMut(&Vector2D) -> bool {
        let mut position = Vector2D::default();
        let box_width = bounding_box.width();
        let box_height = bounding_box.height();

        let mut should_quit = false;
        let mut j = 0;
        while j as f64 * spacing <= box_height && !should_quit {
            position.y = j as f64 * spacing + bounding_box.lower_corner.y;

            let mut i = 0;
            while i as f64 * spacing <= box_width && !should_quit {
                position.x = i as f64 * spacing + bounding_box.lower_corner.x;
                if !callback(&position) {
                    should_quit = true;
                    break;
                }
                i += 1;
            }
            j += 1;
        }
    }
}

/// Shared pointer type for the GridPointGenerator2.
pub type GridPointGenerator2Ptr = Arc<RwLock<GridPointGenerator2>>;