/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use std::ops::*;
use std::fmt::{Debug, Formatter, Result, Display};

///
/// # 2-D vector class.
///
/// This class defines simple 2-D vector data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy)]
pub struct Vector2<T: Float> {
    /// X (or the first) component of the vector.
    pub x: T,

    /// Y (or the second) component of the vector.
    pub y: T,

}

/// Float-type 2D vector.
pub type Vector2F = Vector2<f32>;
/// Double-type 2D vector.
pub type Vector2D = Vector2<f64>;

impl<T: Float> Default for Vector2<T> {
    /// Constructs default vector (0, 0).
    fn default() -> Self {
        return Vector2 {
            x: T::zero(),
            y: T::zero(),
        };
    }
}

/// # Constructors
impl<T: Float> Vector2<T> {
    /// Constructs vector with given parameters **x_** and **y_**.
    pub fn new(x_: T, y_: T) -> Vector2<T> {
        return Vector2 {
            x: x_,
            y: y_,
        };
    }

    /// Constructs vector with initializer list.
    pub fn new_slice(lst: &[T]) -> Vector2<T> {
        return Vector2 {
            x: lst[0],
            y: lst[1],
        };
    }
}

/// # Basic setters
impl<T: Float> Vector2<T> {
    /// Set both x and y components to **s**.
    pub fn set_scalar(&mut self, s: T) {
        self.x = s;
        self.y = s;
    }

    /// Set x and y components with given parameters.
    pub fn set(&mut self, x: T, y: T) {
        self.x = x;
        self.y = y;
    }

    /// Set x and y components with given initializer list.
    pub fn set_slice(&mut self, lst: &[T]) {
        self.x = lst[0];
        self.y = lst[1];
    }

    /// Set x and y with other vector **pt**.
    pub fn set_self(&mut self, pt: &Vector2<T>) {
        self.x = pt.x;
        self.y = pt.y;
    }

    /// Set both x and y to zero.
    pub fn set_zero(&mut self) {
        self.x = T::zero();
        self.y = T::zero();
    }

    /// Normalizes self vector.
    pub fn normalize(&mut self) {
        let l = self.length();
        self.x = T::div(self.x, l);
        self.y = T::div(self.y, l);
    }
}

/// # Binary operations: new instance = self (+) v
impl<T: Float> Vector2<T> {
    /// Computes self + (v, v).
    pub fn add_scalar(&self, v: T) -> Vector2<T> {
        return Vector2::new(self.x + v, self.y + v);
    }

    /// Computes self + (v.x, v.y).
    pub fn add_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(self.x + v.x, self.y + v.y);
    }

    /// Computes self - (v, v).
    pub fn sub_scalar(&self, v: T) -> Vector2<T> {
        return Vector2::new(self.x - v, self.y - v);
    }

    /// Computes self - (v.x, v.y).
    pub fn sub_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(self.x - v.x, self.y - v.y);
    }

    /// Computes self * (v, v).
    pub fn mul_scalar(&self, v: T) -> Vector2<T> {
        return Vector2::new(self.x * v, self.y * v);
    }

    /// Computes self * (v.x, v.y).
    pub fn mul_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(self.x * v.x, self.y * v.y);
    }

    /// Computes self / (v, v).
    pub fn div_scalar(&self, v: T) -> Vector2<T> {
        return Vector2::new(self.x / v, self.y / v);
    }

    /// Computes self / (v.x, v.y).
    pub fn div_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(self.x / v.x, self.y / v.y);
    }

    /// Computes dot product.
    pub fn dot(&self, v: &Vector2<T>) -> T {
        return self.x * v.x + self.y * v.y;
    }

    /// Computes cross product.
    pub fn cross(&self, v: &Vector2<T>) -> T {
        return self.x * v.y - v.x * self.y;
    }
}

/// # Binary operations: new instance = v (+) self
impl<T: Float> Vector2<T> {
    /// Computes (v, v) - self.
    pub fn rsub_scalar(&self, v: T) -> Vector2<T> {
        return Vector2::new(v - self.x, v - self.y);
    }

    /// Computes (v.x, v.y) - self.
    pub fn rsub_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(v.x - self.x, v.y - self.y);
    }

    /// Computes (v, v) / self.
    pub fn rdiv_scalar(&self, v: T) -> Vector2<T> {
        return Vector2::new(v / self.x, v / self.y);
    }

    /// Computes (v.x, v.y) / self.
    pub fn rdiv_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(v.x / self.x, v.y / self.y);
    }

    /// Computes **v** cross self.
    pub fn rcross(&self, v: &Vector2<T>) -> T {
        return v.x * self.y - self.x * v.y;
    }
}

/// # Augmented operations: self (+)= v
impl<T: Float> Vector2<T> {
    /// Computes self += (v, v).
    pub fn iadd_scalar(&mut self, v: T) {
        self.x = T::add(self.x, v);
        self.y = T::add(self.y, v);
    }

    /// Computes self += (v.x, v.y).
    pub fn iadd_vec(&mut self, v: &Vector2<T>) {
        self.x = T::add(self.x, v.x);
        self.y = T::add(self.y, v.y);
    }

    /// Computes self -= (v, v).
    pub fn isub_scalar(&mut self, v: T) {
        self.x = T::sub(self.x, v);
        self.y = T::sub(self.y, v);
    }

    /// Computes self -= (v.x, v.y).
    pub fn isub_vec(&mut self, v: &Vector2<T>) {
        self.x = T::sub(self.x, v.x);
        self.y = T::sub(self.y, v.y);
    }

    /// Computes self *= (v, v).
    pub fn imul_scalar(&mut self, v: T) {
        self.x = T::mul(self.x, v);
        self.y = T::mul(self.y, v);
    }

    /// Computes self *= (v.x, v.y).
    pub fn imul_vec(&mut self, v: &Vector2<T>) {
        self.x = T::mul(self.x, v.x);
        self.y = T::mul(self.y, v.y);
    }

    /// Computes self /= (v, v).
    pub fn idiv_scalar(&mut self, v: T) {
        self.x = T::div(self.x, v);
        self.y = T::div(self.y, v);
    }

    /// Computes self /= (v.x, v.y).
    pub fn idiv_vec(&mut self, v: &Vector2<T>) {
        self.x = T::div(self.x, v.x);
        self.y = T::div(self.y, v.y);
    }
}

/// # Basic getters
impl<T: Float> Vector2<T> {
    /// Returns const reference to the **i** -th element of the vector.
    pub fn at(&self, i: usize) -> &T {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the vector.
    pub fn at_mut(&mut self, i: usize) -> &mut T {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y).
    pub fn sum(&self) -> T {
        return self.x + self.y;
    }

    /// Returns the average of all the components.
    pub fn avg(&self) -> T {
        return (self.x + self.y) / T::from(2.0).unwrap();
    }

    /// Returns the minimum value among x and y.
    pub fn min(&self) -> T {
        return self.x.min(self.y);
    }

    /// Returns the maximum value among x and y.
    pub fn max(&self) -> T {
        return self.x.max(self.y);
    }

    /// Returns the absolute minimum value among x and y.
    pub fn absmin(&self) -> T {
        return crate::math_utils::absmin(self.x, self.y);
    }

    /// Returns the absolute maximum value among x and y.
    pub fn absmax(&self) -> T {
        return crate::math_utils::absmax(self.x, self.y);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        match self.x.abs() > self.y.abs() {
            true => 0,
            false => 1
        }
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        match self.x.abs() < self.y.abs() {
            true => 0,
            false => 1
        }
    }

    /// Returns normalized vector.
    pub fn normalized(&self) -> Vector2<T> {
        let l = self.length();
        return Vector2::new(self.x / l, self.y / l);
    }

    /// Returns the length of the vector.
    pub fn length(&self) -> T {
        return T::sqrt(self.x * self.x + self.y * self.y);
    }

    /// Returns the squared length of the vector.
    pub fn length_squared(&self) -> T {
        return self.x * self.x + self.y * self.y;
    }

    /// Returns the distance to the other vector.
    pub fn distance_to(&self, other: &Vector2<T>) -> T {
        return self.sub_vec(other).length();
    }

    /// Returns the squared distance to the other vector.
    pub fn distance_squared_to(&self, other: &Vector2<T>) -> T {
        return self.sub_vec(other).length_squared();
    }

    /// Returns the reflection vector to the surface with given surface normal.
    pub fn reflected(&self, normal: &Vector2<T>) -> Vector2<T> {
        // self - 2(self.n)n
        return self.sub_vec(&normal.mul_scalar(T::mul(T::from(2.0).unwrap(), self.dot(normal))));
    }

    /// Returns the projected vector to the surface with given surface normal.
    pub fn projected(&self, normal: &Vector2<T>) -> Vector2<T> {
        // self - self.n n
        return self.sub_vec(&normal.mul_scalar(self.dot(normal)));
    }

    /// Returns the tangential vector for self vector.
    pub fn tangential(&self) -> Vector2<T> {
        // Rotate 90 degrees
        return Vector2::new(-self.y, self.x);
    }

    /// Returns true if **other** is the same as self vector.
    pub fn is_equal(&self, other: &Vector2<T>) -> bool {
        return self.x == other.x && self.y == other.y;
    }

    /// Returns true if **other** is similar to self vector.
    pub fn is_similar(&self, other: &Vector2<T>, epsilon: Option<T>) -> bool {
        return ((self.x - other.x).abs() < epsilon.unwrap_or(T::epsilon())) &&
            ((self.y - other.y).abs() < epsilon.unwrap_or(T::epsilon()));
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the vector.
impl<T: Float> Index<usize> for Vector2<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the vector.
impl<T: Float> IndexMut<usize> for Vector2<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v)
impl<T: Float> AddAssign<T> for Vector2<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y)
impl<T: Float> AddAssign for Vector2<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v)
impl<T: Float> SubAssign<T> for Vector2<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y)
impl<T: Float> SubAssign for Vector2<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v)
impl<T: Float> MulAssign<T> for Vector2<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y)
impl<T: Float> MulAssign for Vector2<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v)
impl<T: Float> DivAssign<T> for Vector2<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y)
impl<T: Float> DivAssign for Vector2<T> {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self vector.
impl<T: Float> PartialEq for Vector2<T> {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl<T: Float> Eq for Vector2<T> {}

impl<T: Float> Neg for Vector2<T> {
    type Output = Vector2<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Vector2::new(-self.x, -self.y);
    }
}

/// Computes (a, a) + (b.x, b.y).
impl<T: Float> Add<T> for Vector2<T> {
    type Output = Vector2<T>;
    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y) + (b.x, b.y).
impl<T: Float> Add for Vector2<T> {
    type Output = Vector2<T>;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

impl<T: Float> Add<&Vector2<T>> for Vector2<T> {
    type Output = Vector2<T>;

    fn add(self, rhs: &Vector2<T>) -> Self::Output {
        return self.add_vec(rhs);
    }
}

impl<T: Float> Add<&Vector2<T>> for &Vector2<T> {
    type Output = Vector2<T>;

    fn add(self, rhs: &Vector2<T>) -> Self::Output {
        return self.add_vec(rhs);
    }
}

/// Computes (a.x, a.y) - (b, b).
impl<T: Float> Sub<T> for Vector2<T> {
    type Output = Vector2<T>;
    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y) - (b.x, b.y).
impl<T: Float> Sub for Vector2<T> {
    type Output = Vector2<T>;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

impl<T: Float> Sub<&Vector2<T>> for &Vector2<T> {
    type Output = Vector2<T>;

    fn sub(self, rhs: &Vector2<T>) -> Self::Output {
        return self.sub_vec(rhs);
    }
}

/// Computes (a.x, a.y) * (b, b).
impl<T: Float> Mul<T> for Vector2<T> {
    type Output = Vector2<T>;
    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

impl<T: Float> Mul<T> for &Vector2<T> {
    type Output = Vector2<T>;

    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y) * (b.x, b.y).
impl<T: Float> Mul for Vector2<T> {
    type Output = Vector2<T>;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y) / (b, b).
impl<T: Float> Div<T> for Vector2<T> {
    type Output = Vector2<T>;
    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y) / (b.x, b.y).
impl<T: Float> Div for Vector2<T> {
    type Output = Vector2<T>;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl<T: Float + Debug> Debug for Vector2<T> {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::vector2::Vector2F;
    /// let vec = Vector2F::new(10.0, 20.0);
    /// assert_eq!(format!("{:?}", vec), "(10.0, 20.0)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10.0,
    ///     20.0,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .finish()
    }
}

impl<T: Float + Display> Display for Vector2<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{} {}", self.x, self.y)
    }
}

/// # utility
/// Returns element-wise min vector: (min(a.x, b.x), min(a.y, b.y)).
pub fn min<T: Float>(a: &Vector2<T>, b: &Vector2<T>) -> Vector2<T> {
    return Vector2::new(T::min(a.x, b.x), T::min(a.y, b.y));
}

/// Returns element-wise max vector: (max(a.x, b.x), max(a.y, b.y)).
pub fn max<T: Float>(a: &Vector2<T>, b: &Vector2<T>) -> Vector2<T> {
    return Vector2::new(T::max(a.x, b.x), T::max(a.y, b.y));
}

/// Returns element-wise clamped vector.
pub fn clamp<T: Float>(v: &Vector2<T>, low: &Vector2<T>, high: &Vector2<T>) -> Vector2<T> {
    return Vector2::new(crate::math_utils::clamp(v.x, low.x, high.x),
                        crate::math_utils::clamp(v.y, low.y, high.y));
}

/// Returns element-wise ceiled vector.
pub fn ceil<T: Float>(a: &Vector2<T>) -> Vector2<T> {
    return Vector2::new((a.x).ceil(), (a.y).ceil());
}

/// Returns element-wise floored vector.
pub fn floor<T: Float>(a: &Vector2<T>) -> Vector2<T> {
    return Vector2::new((a.x).floor(), (a.y).floor());
}

/// Computes monotonic Catmull-Rom interpolation.
pub fn monotonic_catmull_rom<T: Float>(v0: &Vector2<T>, v1: &Vector2<T>,
                                       v2: &Vector2<T>, v3: &Vector2<T>, f: T) -> Vector2<T> {
    let two = T::from(2.0).unwrap();
    let three = T::from(3.0).unwrap();

    let mut d1 = (v2 - v0) / two;
    let mut d2 = (v3 - v1) / two;
    let d_diff = v2 - v1;

    if d_diff.x.abs() < T::epsilon() ||
        crate::math_utils::sign(d_diff.x) != crate::math_utils::sign(d1.x) ||
        crate::math_utils::sign(d_diff.x) != crate::math_utils::sign(d2.x) {
        d1.x = T::zero();
        d2.x = T::zero();
    }

    if d_diff.y.abs() < T::epsilon() ||
        crate::math_utils::sign(d_diff.y) != crate::math_utils::sign(d1.y) ||
        crate::math_utils::sign(d_diff.y) != crate::math_utils::sign(d2.y) {
        d1.y = T::zero();
        d2.y = T::zero();
    }

    let a3 = d1 + d2 - d_diff * two;
    let a2 = d_diff * three - d1 * two - d2;
    let a1 = d1;
    let a0 = v1;

    return a3 * crate::math_utils::cubic(f) + a2 * crate::math_utils::square(f) + a1 * f + a0;
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod vector2 {
    use crate::vector2::*;
    use crate::assert_delta;

    #[test]
    fn constructors() {
        {
            let vec = Vector2F::default();
            assert_eq!(0.0, vec.x);
            assert_eq!(0.0, vec.y);
        }
        {
            let vec2 = Vector2F::new(5.0, 3.0);
            assert_eq!(5.0, vec2.x);
            assert_eq!(3.0, vec2.y);
        }
        {
            let vec5 = Vector2F::new_slice(&[7.0, 6.0]);
            assert_eq!(7.0, vec5.x);
            assert_eq!(6.0, vec5.y);
        }
    }

    #[test]
    fn set_methods() {
        {
            let mut vec = Vector2F::default();
            vec.set(4.0, 2.0);
            assert_eq!(4.0, vec.x);
            assert_eq!(2.0, vec.y);
        }
        {
            let mut vec = Vector2F::default();
            let lst = [0.0, 5.0];
            vec.set_slice(&lst);
            assert_eq!(0.0, vec.x);
            assert_eq!(5.0, vec.y);
        }
        {
            let mut vec = Vector2F::default();
            vec.set_self(&Vector2F::new(9.0, 8.0));
            assert_eq!(9.0, vec.x);
            assert_eq!(8.0, vec.y);
        }
    }

    #[test]
    fn basic_setter_methods() {
        {
            let mut vec = Vector2F::new(3.0, 9.0);
            vec.set_zero();
            assert_eq!(0.0, vec.x);
            assert_eq!(0.0, vec.y);
        }
        {
            let mut vec = Vector2F::default();
            vec.set(4.0, 2.0);
            vec.normalize();
            let len = vec.x * vec.x + vec.y * vec.y;
            assert_delta!(len, 1.0, 1e-6);
        }
    }

    #[test]
    fn binary_operator_methods() {
        let mut vec = Vector2F::new(3.0, 9.0);
        vec = vec.add_scalar(4.0);
        assert_eq!(7.0, vec.x);
        assert_eq!(13.0, vec.y);

        vec = vec.add_vec(&Vector2F::new(-2.0, 1.0));
        assert_eq!(5.0, vec.x);
        assert_eq!(14.0, vec.y);

        vec = vec.sub_scalar(8.0);
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec = vec.sub_vec(&Vector2F::new(-5.0, 3.0));
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);

        vec = vec.mul_scalar(2.0);
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec = vec.mul_vec(&Vector2F::new(3.0, -2.0));
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);

        vec = vec.div_scalar(4.0);
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);

        vec = vec.div_vec(&Vector2F::new(3.0, -1.0));
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);

        let d = vec.dot(&Vector2F::new(4.0, 2.0));
        assert_eq!(d, 10.0);

        let c = vec.cross(&Vector2F::new(5.0, -7.0));
        assert_eq!(c, -22.0);
    }

    #[test]
    fn binary_inverse_operator_methods() {
        let mut vec = Vector2F::new(3.0, 9.0);
        vec = vec.rsub_scalar(8.0);
        assert_eq!(5.0, vec.x);
        assert_eq!(-1.0, vec.y);

        vec = vec.rsub_vec(&Vector2F::new(-5.0, 3.0));
        assert_eq!(-10.0, vec.x);
        assert_eq!(4.0, vec.y);

        vec = Vector2F::new(-4.0, -3.0);
        vec = vec.rdiv_scalar(12.0);
        assert_eq!(-3.0, vec.x);
        assert_eq!(vec.y, -4.0);

        vec = vec.rdiv_vec(&Vector2F::new(3.0, -16.0));
        assert_eq!(-1.0, vec.x);
        assert_eq!(4.0, vec.y);

        let c = vec.rcross(&Vector2F::new(5.0, -7.0));
        assert_eq!(c, 13.0);
    }

    #[test]
    fn augmented_operator_methods() {
        let mut vec = Vector2F::new(3.0, 9.0);
        vec.iadd_scalar(4.0);
        assert_eq!(7.0, vec.x);
        assert_eq!(vec.y, 13.0);

        vec.iadd_vec(&Vector2F::new(-2.0, 1.0));
        assert_eq!(5.0, vec.x);
        assert_eq!(vec.y, 14.0);

        vec.isub_scalar(8.0);
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec.isub_vec(&Vector2F::new(-5.0, 3.0));
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);

        vec.imul_scalar(2.0);
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec.imul_vec(&Vector2F::new(3.0, -2.0));
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);

        vec.idiv_scalar(4.0);
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);

        vec.idiv_vec(&Vector2F::new(3.0, -1.0));
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
    }

    #[test]
    fn at_method() {
        {
            let vec = Vector2F::new(8.0, 9.0);
            assert_eq!(*vec.at(0), 8.0);
            assert_eq!(*vec.at(1), 9.0);
        }
        {
            let mut a = Vector2F::default();
            *a.at_mut(0) = 10.0_f32;
            *a.at_mut(1) = 20.0_f32;
            assert_eq!(*a.at(0), 10.0);
            assert_eq!(*a.at(1), 20.0);
        }
    }

    #[test]
    fn basic_getter_methods() {
        {
            let vec = Vector2F::new(3.0, 7.0);
            let sum = vec.sum();
            assert_eq!(sum, 10.0);
        }
        {
            let vec = Vector2F::new(3.0, 7.0);
            let avg = vec.avg();
            assert_eq!(avg, 5.0);
        }
        {
            let vec = Vector2F::new(3.0, 7.0);
            let min = vec.min();
            assert_eq!(min, 3.0);
        }
        {
            let vec = Vector2F::new(3.0, 7.0);
            let max = vec.max();
            assert_eq!(max, 7.0);
        }
        {
            let vec = Vector2F::new(-3.0, -7.0);
            let absmin = vec.absmin();
            assert_eq!(absmin, -3.0);
        }
        {
            let vec = Vector2F::new(-3.0, -7.0);
            let absmax = vec.absmax();
            assert_eq!(absmax, -7.0);
        }
        {
            let vec = Vector2F::new(3.0, 7.0);
            let dominant_axis = vec.dominant_axis();
            assert_eq!(dominant_axis, 1);
        }
        {
            let vec = Vector2F::new(3.0, 7.0);
            let subminant_axis = vec.subminant_axis();
            assert_eq!(subminant_axis, 0);
        }
        {
            let eps = 1e-6_f32;
            let vec = Vector2F::new(3.0, 7.0);
            let vec2 = vec.normalized();
            let len_sqr = vec2.x * vec2.x + vec2.y * vec2.y;
            assert_delta!(len_sqr, 1.0, eps);
        }
        {
            let eps = 1e-6_f32;
            let vec = Vector2F::new(3.0, 7.0);
            let mut vec2 = vec.normalized();
            vec2.imul_scalar(2.0);
            let len = vec2.length();
            assert_delta!(len, 2.0, eps);
        }
        {
            let eps = 1e-6_f32;
            let vec = Vector2F::new(3.0, 7.0);
            let mut vec2 = vec.normalized();
            vec2.imul_scalar(2.0);
            let len = vec2.length_squared();
            assert_delta!(len, 4.0, eps);
        }
    }

    #[test]
    fn bracket_operator() {
        {
            let vec = Vector2F::new(8.0, 9.0);
            assert_eq!(vec[0], 8.0);
            assert_eq!(vec[1], 9.0);
        }
        {
            let mut vec = Vector2F::new(8.0, 9.0);
            vec[0] = 7.0;
            vec[1] = 6.0;
            assert_eq!(7.0, vec.x);
            assert_eq!(6.0, vec.y);
        }
    }

    #[test]
    fn assignment_operator() {
        let vec = Vector2F::new(5.0, 1.0);
        let vec2 = vec;
        assert_eq!(5.0, vec2.x);
        assert_eq!(vec2.y, 1.0);
    }

    #[test]
    fn augmented_operators() {
        let mut vec = Vector2F::new(3.0, 9.0);
        vec += 4.0;
        assert_eq!(7.0, vec.x);
        assert_eq!(vec.y, 13.0);

        vec += Vector2F::new(-2.0, 1.0);
        assert_eq!(5.0, vec.x);
        assert_eq!(vec.y, 14.0);

        vec -= 8.0;
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec -= Vector2F::new(-5.0, 3.0);
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);

        vec *= 2.0;
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec *= Vector2F::new(3.0, -2.0);
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);

        vec /= 4.0;
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);

        vec /= Vector2F::new(3.0, -1.0);
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
    }

    #[test]
    fn equal_operator() {
        let vec2 = Vector2F::new(3.0, 7.0);
        let vec3 = Vector2F::new(3.0, 5.0);
        let vec4 = Vector2F::new(5.0, 1.0);
        let vec = vec2;
        assert_eq!(vec == vec2, true);
        assert_eq!(vec == vec3, false);
        assert_eq!(vec != vec2, false);
        assert_eq!(vec != vec3, true);
        assert_eq!(vec != vec4, true);
    }

    #[test]
    fn min_max_function() {
        {
            let vec = Vector2F::new(5.0, 1.0);
            let vec2 = Vector2F::new(3.0, 3.0);
            let min_vector = min(&vec, &vec2);
            assert_eq!(Vector2F::new(3.0, 1.0), min_vector);
        }
        {
            let vec = Vector2F::new(5.0, 1.0);
            let vec2 = Vector2F::new(3.0, 3.0);
            let max_vector = max(&vec, &vec2);
            assert_eq!(Vector2F::new(5.0, 3.0), max_vector);
        }
    }

    #[test]
    fn clamp_function() {
        let vec = Vector2F::new(2.0, 4.0);
        let low = Vector2F::new(3.0, -1.0);
        let high = Vector2F::new(5.0, 2.0);
        let clamped_vec = clamp(&vec, &low, &high);
        assert_eq!(Vector2F::new(3.0, 2.0), clamped_vec);
    }

    #[test]
    fn ceil_floor_function() {
        {
            let vec = Vector2F::new(2.2, 4.7);
            let ceil_vec = ceil(&vec);
            assert_eq!(Vector2F::new(3.0, 5.0), ceil_vec);
        }
        {
            let vec = Vector2F::new(2.2, 4.7);
            let floor_vec = floor(&vec);
            assert_eq!(Vector2F::new(2.0, 4.0), floor_vec);
        }
    }

    #[test]
    fn binary_operators() {
        let mut vec = Vector2F::new(3.0, 9.0);
        vec = vec + 4.0;
        assert_eq!(7.0, vec.x);
        assert_eq!(vec.y, 13.0);

        vec = vec + Vector2F::new(-2.0, 1.0);
        assert_eq!(5.0, vec.x);
        assert_eq!(vec.y, 14.0);

        vec = vec - 8.0;
        assert_eq!(-3.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec = vec - Vector2F::new(-5.0, 3.0);
        assert_eq!(2.0, vec.x);
        assert_eq!(3.0, vec.y);

        vec = vec * 2.0;
        assert_eq!(4.0, vec.x);
        assert_eq!(6.0, vec.y);

        vec = vec * Vector2F::new(3.0, -2.0);
        assert_eq!(12.0, vec.x);
        assert_eq!(-12.0, vec.y);

        vec = vec / 4.0;
        assert_eq!(3.0, vec.x);
        assert_eq!(-3.0, vec.y);

        vec = vec / Vector2F::new(3.0, -1.0);
        assert_eq!(1.0, vec.x);
        assert_eq!(3.0, vec.y);
        {
            let v = Vector2D::new(2.0, 1.0).normalized();
            let normal = Vector2D::new(1.0, 1.0).normalized();

            let reflected = v.reflected(&normal);
            let reflected_answer = Vector2D::new(-1.0, -2.0).normalized();
            assert_delta!(reflected.distance_to(&reflected_answer), 0.0, 1e-9);
        }
        {
            let v = Vector2D::new(2.0, 1.0).normalized();
            let normal = Vector2D::new(1.0, 1.0).normalized();
            let projected = v.projected(&normal);
            assert_delta!(projected.dot(&normal), 0.0, 1e-9);
        }
        {
            let normal = Vector2D::new(1.0, 1.0).normalized();
            let tangential = normal.tangential();
            assert_delta!(tangential.dot(&normal), 0.0, 1e-9);
        }
    }
}