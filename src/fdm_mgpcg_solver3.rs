/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::fdm_mg_linear_system3::FdmMgLinearSystem3;
use crate::mg::{MgParameters, mg_vcycle};
use crate::fdm_linear_system3::*;
use crate::cg::{PrecondTypeProtocol, pcg};
use crate::blas::Blas;
use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;
use crate::fdm_mg_solver3::FdmMgSolver3;
use log::info;

#[derive(Default)]
struct Preconditioner<'a> {
    system: Option<&'a FdmMgLinearSystem3>,
    mg_params: MgParameters<FdmBlas3>,
}

impl<'a> Preconditioner<'a> {
    fn build(&mut self, system: &'a FdmMgLinearSystem3, mg_params: MgParameters<FdmBlas3>) {
        self.system = Some(system);
        self.mg_params = mg_params;
    }
}

impl<'a> PrecondTypeProtocol<FdmBlas3> for Preconditioner<'a> {
    fn solve(&mut self, b: &<FdmBlas3 as Blas>::VectorType, x: &mut <FdmBlas3 as Blas>::VectorType) {
        // Copy dimension
        let mut mg_x = self.system.as_ref().unwrap().x.clone();
        let mut mg_b = self.system.as_ref().unwrap().x.clone();
        let mut mg_buffer = self.system.as_ref().unwrap().x.clone();

        // Copy input to the top
        mg_x.levels.first_mut().unwrap().set_self(x);
        mg_b.levels.first_mut().unwrap().set_self(b);

        mg_vcycle(&self.system.as_ref().unwrap().a, self.mg_params.clone(),
                  &mut mg_x, &mut mg_b, &mut mg_buffer);

        // Copy result to the output
        x.set_self(mg_x.levels.first().unwrap());
    }
}

///
/// # 3-D finite difference-type linear system solver using Multigrid Preconditioned conjugate gradient (MGPCG).
///
/// \see McAdams, Aleka, Eftychios Sifakis, and Joseph Teran.
///      "A parallel multigrid Poisson solver for fluids simulation on large
///      grids." Proceedings of the 3010 ACM SIGGRAPH/Eurographics Symposium on
///      Computer Animation. Eurographics Association, 3010.
///
pub struct FdmMgpcgSolver3 {
    _max_number_of_iterations: u32,
    _last_number_of_iterations: u32,
    _tolerance: f64,
    _last_residual_norm: f64,

    _r: FdmVector3,
    _d: FdmVector3,
    _q: FdmVector3,
    _s: FdmVector3,

    _base: FdmMgSolver3,
}

impl FdmMgpcgSolver3 {
    ///
    /// Constructs the solver with given parameters.
    ///
    /// \param number_of_cg_iter - Number of CG iterations.
    /// \param max_number_of_levels - Number of maximum MG levels.
    /// \param number_of_restriction_iter - Number of restriction iterations.
    /// \param number_of_correction_iter - Number of correction iterations.
    /// \param number_of_coarsest_iter - Number of iterations at the coarsest grid.
    /// \param number_of_final_iter - Number of final iterations.
    /// \param max_tolerance - Number of max residual tolerance.
    pub fn new(number_of_cg_iter: u32, max_number_of_levels: usize,
               number_of_restriction_iter: Option<u32>,
               number_of_correction_iter: Option<u32>,
               number_of_coarsest_iter: Option<u32>,
               number_of_final_iter: Option<u32>,
               max_tolerance: Option<f64>, sor_factor: Option<f64>,
               use_red_black_ordering: Option<bool>) -> FdmMgpcgSolver3 {
        return FdmMgpcgSolver3 {
            _max_number_of_iterations: number_of_cg_iter,
            _last_number_of_iterations: 0,
            _tolerance: max_tolerance.unwrap_or(1e-9),
            _last_residual_norm: crate::constants::K_MAX_D,
            _r: Default::default(),
            _d: Default::default(),
            _q: Default::default(),
            _s: Default::default(),
            _base: FdmMgSolver3::new(max_number_of_levels, number_of_restriction_iter,
                                     number_of_correction_iter, number_of_coarsest_iter,
                                     number_of_final_iter, max_tolerance, sor_factor,
                                     use_red_black_ordering),
        };
    }

    /// Returns the max number of Jacobi iterations.
    pub fn max_number_of_iterations(&self) -> u32 {
        return self._max_number_of_iterations;
    }

    /// Returns the last number of Jacobi iterations the solver made.
    pub fn last_number_of_iterations(&self) -> u32 {
        return self._last_number_of_iterations;
    }

    /// Returns the max residual tolerance for the Jacobi method.
    pub fn tolerance(&self) -> f64 {
        return self._tolerance;
    }

    /// Returns the last residual after the Jacobi iterations.
    pub fn last_residual(&self) -> f64 {
        return self._last_residual_norm;
    }
}

impl<'a> FdmLinearSystemSolver3<'a> for FdmMgpcgSolver3 {
    fn solve(&mut self, _system: &mut FdmLinearSystem3) -> bool {
        unimplemented!();
    }
}

impl FdmMgpcgSolver3 {
    /// Solves Multigrid linear system.
    pub fn solve(&mut self, system: &mut FdmMgLinearSystem3) -> bool {
        let size = system.a.levels.first().unwrap().size();
        self._r.resize_with_packed_size(&size, None);
        self._d.resize_with_packed_size(&size, None);
        self._q.resize_with_packed_size(&size, None);
        self._s.resize_with_packed_size(&size, None);

        system.x.levels.first_mut().unwrap().set_scalar(0.0);
        self._r.set_scalar(0.0);
        self._d.set_scalar(0.0);
        self._q.set_scalar(0.0);
        self._s.set_scalar(0.0);

        let mut x = FdmVector3::new_with_packed_size(&system.x.levels.first_mut().unwrap().size(), Some(0.0));
        {
            let mut _precond: Preconditioner = Default::default();
            _precond.build(system, self._base.params().clone());

            pcg::<FdmBlas3, Preconditioner>(system.a.levels.first().unwrap(),
                                            system.b.levels.first().unwrap(),
                                            self._max_number_of_iterations, self._tolerance, &mut _precond,
                                            &mut x, &mut self._r, &mut self._d, &mut self._q, &mut self._s,
                                            &mut self._last_number_of_iterations, &mut self._last_residual_norm);
        }
        *system.x.levels.first_mut().unwrap() = x;

        info!("Residual after solving MGPCG: {} Number of MGPCG iterations: {}",
              self._last_residual_norm, self._last_number_of_iterations);

        return self._last_residual_norm <= self._tolerance ||
            self._last_number_of_iterations < self._max_number_of_iterations;
    }
}