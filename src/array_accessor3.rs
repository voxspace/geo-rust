/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use crate::usize3::USize3;
use std::ops::{Index, IndexMut};
use std::mem::swap;

///
/// \brief 3-D array accessor class.
///
/// This class represents 3-D array accessor. Array accessor provides array-like
/// data read/write functions, but does not handle memory management. Thus, it
/// is more like a random access iterator, but with multi-dimension support.
/// Similar to Array<T, 3>, this class interprets a linear array as a 3-D array
/// using i-major indexing.
///
/// \see Array<T, 3>
///
/// \tparam T - Array value type.
///
pub struct ArrayAccessor3<'a, T: ZeroInit> {
    _size: USize3,
    _data: Option<&'a mut [T]>,
}

impl<'a, T: ZeroInit> Default for ArrayAccessor3<'a, T> {
    /// Constructs empty 3-D array accessor.
    fn default() -> Self {
        return ArrayAccessor3 {
            _size: USize3::default(),
            _data: None,
        };
    }
}

impl<'a, T: ZeroInit> ArrayAccessor3<'a, T> {
    /// Constructs an array accessor that wraps given array.
    /// \param size Size of the 3-D array.
    /// \param data Raw array pointer.
    pub fn new_with_packed_size(size: &USize3, data: &'a mut [T]) -> ArrayAccessor3<'a, T> {
        let mut accessor = ArrayAccessor3::default();
        accessor.reset_with_packed_size(size, data);
        return accessor;
    }

    /// Constructs an array accessor that wraps given array.
    /// \param width Width of the 3-D array.
    /// \param height Height of the 3-D array.
    /// \param depth Depth of the 3-D array.
    /// \param data Raw array pointer.
    pub fn new_with_size(width: usize, height: usize, depth: usize, data: &'a mut [T]) -> ArrayAccessor3<'a, T> {
        let mut accessor = ArrayAccessor3::default();
        accessor.reset_with_size(width, height, depth, data);
        return accessor;
    }

    /// Replaces the content with given \p other array accessor.
    pub fn set(&mut self, other: &'a mut ArrayAccessor3<T>) {
        self.reset_with_packed_size(&other._size, other._data.as_mut().unwrap());
    }

    /// Resets the array.
    pub fn reset_with_packed_size(&mut self, size: &USize3, data: &'a mut [T]) {
        self._size = *size;
        self._data = Some(data);
    }

    /// Resets the array.
    pub fn reset_with_size(&mut self, width: usize, height: usize, depth: usize, data: &'a mut [T]) {
        self.reset_with_packed_size(&USize3::new(width, height, depth), data);
    }

    /// Returns the reference to the i-th element.
    pub fn at_index_mut(&mut self, i: usize) -> &mut T {
        debug_assert!(i < self._size.x * self._size.y * self._size.z);
        return &mut self._data.as_mut().unwrap()[i];
    }

    /// Returns the const reference to the i-th element.
    pub fn at_index(&self, i: usize) -> &T {
        debug_assert!(i < self._size.x * self._size.y * self._size.z);
        return &self._data.as_ref().unwrap()[i];
    }

    /// Returns the reference to the element at (pt.x, pt.y, pt.z).
    pub fn at_with_packed_size_mut(&mut self, pt: &USize3) -> &mut T {
        return self.at_with_size_mut(pt.x, pt.y, pt.z);
    }

    /// Returns the const reference to the element at (pt.x, pt.y, pt.z).
    pub fn at_with_packed_size(&self, pt: &USize3) -> &T {
        return self.at_with_size(pt.x, pt.y, pt.z);
    }

    /// Returns the reference to the element at (i, j, k).
    pub fn at_with_size_mut(&mut self, i: usize, j: usize, k: usize) -> &mut T {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return &mut self._data.as_mut().unwrap()[i + self._size.x * (j + self._size.y * k)];
    }

    /// Returns the const reference to the element at (i, j, k).
    pub fn at_with_size(&self, i: usize, j: usize, k: usize) -> &T {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return &self._data.as_ref().unwrap()[i + self._size.x * (j + self._size.y * k)];
    }

    /// Returns the size of the array.
    pub fn size(&self) -> USize3 {
        return self._size;
    }

    /// Returns the width of the array.
    pub fn width(&self) -> usize {
        return self._size.x;
    }

    /// Returns the height of the array.
    pub fn height(&self) -> usize {
        return self._size.y;
    }

    /// Returns the depth of the array.
    pub fn depth(&self) -> usize {
        return self._size.z;
    }

    /// Returns the raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return &self._data.as_ref().unwrap();
    }

    /// Swaps the content of with \p other array accessor.
    pub fn swap(&mut self, other: &mut ArrayAccessor3<'a, T>) {
        swap(&mut other._data, &mut self._data);
        swap(&mut other._size, &mut self._size);
    }

    /// Returns the linear index of the given 3-D coordinate (pt.x, pt.y, pt.z).
    pub fn index_with_packed_size(&self, pt: &USize3) -> usize {
        debug_assert!(pt.x < self._size.x && pt.y < self._size.y && pt.z < self._size.z);
        return pt.x + self._size.x * (pt.y + self._size.y * pt.z);
    }

    /// Returns the linear index of the given =3-D coordinate (i, j, k).
    pub fn index_with_size(&self, i: usize, j: usize, k: usize) -> usize {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return i + self._size.x * (j + self._size.y * k);
    }
}

impl<'a, T: ZeroInit> ArrayAccessor3<'a, T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// for (size_t k = 0; k < acc.depth(); ++k) {
    ///     for (size_t j = 0; j < acc.height(); ++j) {
    ///         for (size_t i = 0; i < acc.width(); ++i) {
    ///             func(i, j, k);
    ///         }
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// acc.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, mut func: Callback) {
        for k in 0..self._size.z {
            for j in 0..self._size.y {
                for i in 0..self._size.x {
                    func(self.at_with_size(i, j, k));
                }
            }
        }
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes three parameters which are
    /// the (i, j, j) indices of the array. The order of execution will be the
    /// same as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// for (size_t k = 0; k < acc.depth(); ++k) {
    ///     for (size_t j = 0; j < acc.height(); ++j) {
    ///         for (size_t i = 0; i < acc.width(); ++i) {
    ///             func(i, j, k);
    ///         }
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// acc.for_each_index([&](size_t i, size_t j, size_t k) {
    ///     acc(i, j, k) = 4.f * i + 7.f * j + 3.f * k + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize, usize)>(&self, mut func: Callback) {
        for k in 0..self._size.z {
            for j in 0..self._size.y {
                for i in 0..self._size.x {
                    func(i, j, k);
                }
            }
        }
    }
}

impl<'a, T: ZeroInit> Index<usize> for ArrayAccessor3<'a, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data.as_ref().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> IndexMut<usize> for ArrayAccessor3<'a, T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._data.as_mut().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> Index<(usize, usize, usize)> for ArrayAccessor3<'a, T> {
    type Output = T;

    fn index(&self, index: (usize, usize, usize)) -> &Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y && index.2 < self._size.z);
        return &self._data.as_ref().unwrap()[index.0 + self._size.x * (index.1 + self._size.y * index.2)];
    }
}

impl<'a, T: ZeroInit> IndexMut<(usize, usize, usize)> for ArrayAccessor3<'a, T> {
    fn index_mut(&mut self, index: (usize, usize, usize)) -> &mut Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y && index.2 < self._size.z);
        return &mut self._data.as_mut().unwrap()[index.0 + self._size.x * (index.1 + self._size.y * index.2)];
    }
}

impl<'a, T: ZeroInit> Index<USize3> for ArrayAccessor3<'a, T> {
    type Output = T;

    fn index(&self, index: USize3) -> &Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y && index.z < self._size.z);
        return &self._data.as_ref().unwrap()[index.x + self._size.x * (index.y + self._size.y * index.z)];
    }
}

impl<'a, T: ZeroInit> IndexMut<USize3> for ArrayAccessor3<'a, T> {
    fn index_mut(&mut self, index: USize3) -> &mut Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y && index.z < self._size.z);
        return &mut self._data.as_mut().unwrap()[index.x + self._size.x * (index.y + self._size.y * index.z)];
    }
}

//--------------------------------------------------------------------------------------------------
///
/// \brief 3-D read-only array accessor class.
///
/// This class represents 3-D read-only array accessor. Array accessor provides
/// array-like data read/write functions, but does not handle memory management.
/// Thus, it is more like a random access iterator, but with multi-dimension
/// support.Similar to Array<T, 3>, this class interprets a linear array as a
/// 3-D array using i-major indexing.
///
/// \see Array<T, 3>
///
pub struct ConstArrayAccessor3<'a, T: ZeroInit> {
    _size: USize3,
    _data: Option<&'a [T]>,
}

impl<'a, T: ZeroInit> Default for ConstArrayAccessor3<'a, T> {
    /// Constructs empty 3-D read-only array accessor.
    fn default() -> Self {
        return ConstArrayAccessor3 {
            _size: USize3::default(),
            _data: None,
        };
    }
}

impl<'a, T: ZeroInit> ConstArrayAccessor3<'a, T> {
    /// Constructs a read-only array accessor that wraps given array.
    /// \param size Size of the 3-D array.
    /// \param data Raw array pointer.
    pub fn new_with_packed_size(size: &USize3, data: &'a [T]) -> ConstArrayAccessor3<'a, T> {
        let mut accessor = ConstArrayAccessor3::default();
        accessor._size = *size;
        accessor._data = Some(data);
        return accessor;
    }


    /// Constructs a read-only array accessor that wraps given array.
    /// \param width Width of the 3-D array.
    /// \param height Height of the 3-D array.
    /// \param depth Depth of the 3-D array.
    /// \param data Raw array pointer.
    pub fn new_with_size(width: usize, height: usize, depth: usize, data: &'a [T]) -> ConstArrayAccessor3<'a, T> {
        let mut accessor = ConstArrayAccessor3::default();
        accessor._size = USize3::new(width, height, depth);
        accessor._data = Some(data);
        return accessor;
    }

    /// Returns the const reference to the i-th element.
    pub fn at_index(&self, i: usize) -> &T {
        debug_assert!(i < self._size.x * self._size.y * self._size.z);
        return &self._data.as_ref().unwrap()[i];
    }

    /// Returns the const reference to the element at (pt.x, pt.y, pt.z).
    pub fn at_with_packed_size(&self, pt: &USize3) -> &T {
        return self.at_with_size(pt.x, pt.y, pt.z);
    }

    /// Returns the const reference to the element at (i, j, k).
    pub fn at_with_size(&self, i: usize, j: usize, k: usize) -> &T {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return &self._data.as_ref().unwrap()[i + self._size.x * (j + self._size.y * k)];
    }

    /// Returns the size of the array.
    pub fn size(&self) -> USize3 {
        return self._size;
    }

    /// Returns the width of the array.
    pub fn width(&self) -> usize {
        return self._size.x;
    }

    /// Returns the height of the array.
    pub fn height(&self) -> usize {
        return self._size.y;
    }

    /// Returns the depth of the array.
    pub fn depth(&self) -> usize {
        return self._size.z;
    }

    /// Returns the raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return self._data.as_ref().unwrap();
    }

    /// Returns the linear index of the given 3-D coordinate (pt.x, pt.y, pt.z).
    pub fn index_with_packed_size(&self, pt: &USize3) -> usize {
        debug_assert!(pt.x < self._size.x && pt.y < self._size.y && pt.z < self._size.z);
        return pt.x + self._size.x * (pt.y + self._size.y * pt.z);
    }

    /// Returns the linear index of the given =3-D coordinate (i, j, k).
    pub fn index_with_size(&self, i: usize, j: usize, k: usize) -> usize {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return i + self._size.x * (j + self._size.y * k);
    }
}

impl<'a, T: ZeroInit> ConstArrayAccessor3<'a, T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ConstArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// for (size_t k = 0; k < acc.depth(); ++k) {
    ///     for (size_t j = 0; j < acc.height(); ++j) {
    ///         for (size_t i = 0; i < acc.width(); ++i) {
    ///             func(i, j, k);
    ///         }
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ConstArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// acc.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, mut func: Callback) {
        for k in 0..self._size.z {
            for j in 0..self._size.y {
                for i in 0..self._size.x {
                    func(self.at_with_size(i, j, k));
                }
            }
        }
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes three parameters which are
    /// the (i, j, j) indices of the array. The order of execution will be the
    /// same as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ConstArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// for (size_t k = 0; k < acc.depth(); ++k) {
    ///     for (size_t j = 0; j < acc.height(); ++j) {
    ///         for (size_t i = 0; i < acc.width(); ++i) {
    ///             func(i, j, k);
    ///         }
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    /// ConstArrayAccessor<int, 3> acc(2, 3, 2, data);
    /// acc.for_each_index([&](size_t i, size_t j, size_t k) {
    ///     acc(i, j, k) = 4.f * i + 7.f * j + 3.f * k + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize, usize)>(&self, mut func: Callback) {
        for k in 0..self._size.z {
            for j in 0..self._size.y {
                for i in 0..self._size.x {
                    func(i, j, k);
                }
            }
        }
    }
}

impl<'a, T: ZeroInit> Index<usize> for ConstArrayAccessor3<'a, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data.as_ref().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> Index<(usize, usize, usize)> for ConstArrayAccessor3<'a, T> {
    type Output = T;

    fn index(&self, index: (usize, usize, usize)) -> &Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y && index.2 < self._size.z);
        return &self._data.as_ref().unwrap()[index.0 + self._size.x * (index.1 + self._size.y * index.2)];
    }
}

impl<'a, T: ZeroInit> Index<USize3> for ConstArrayAccessor3<'a, T> {
    type Output = T;

    fn index(&self, index: USize3) -> &Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y && index.z < self._size.z);
        return &self._data.as_ref().unwrap()[index.x + self._size.x * (index.y + self._size.y * index.z)];
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod array_accessor3 {
    use crate::array_accessor3::ArrayAccessor3;
    use crate::array3::Array3;
    use crate::usize3::USize3;

    #[test]
    fn constructors() {
        let mut data = [0.0; 60];
        for i in 0..60 {
            data[i] = i as f64;
        }

        let acc = ArrayAccessor3::new_with_packed_size(&USize3::new(5, 4, 3), &mut data);

        assert_eq!(5, acc.size().x);
        assert_eq!(4, acc.size().y);
        assert_eq!(3, acc.size().z);
    }

    #[test]
    fn iterators() {
        let mut arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);
        let acc = arr1.accessor();

        let mut cnt = 1.0;
        for elem in acc.data() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }
    }

    #[test]
    fn for_each() {
        let mut arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);
        let acc = arr1.accessor();

        let mut i = 0;
        acc.for_each(|val| {
            assert_eq!(acc[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let mut arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);
        let acc = arr1.accessor();

        acc.for_each_index(|i, j, k| {
            let idx = i + (4 * (j + 3 * k)) + 1;
            assert_eq!(idx as f64, acc[(i, j, k)]);
        });
    }
}

//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod const_array_accessor3 {
    use crate::array_accessor3::ConstArrayAccessor3;
    use crate::array3::Array3;
    use crate::usize3::USize3;

    #[test]
    fn constructors() {
        let mut data = [0.0; 60];
        for i in 0..60 {
            data[i] = i as f64;
        }

        let acc = ConstArrayAccessor3::new_with_packed_size(&USize3::new(5, 4, 3), &mut data);

        assert_eq!(5, acc.size().x);
        assert_eq!(4, acc.size().y);
        assert_eq!(3, acc.size().z);
    }

    #[test]
    fn iterators() {
        let arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);
        let acc = arr1.const_accessor();

        let mut cnt = 1.0;
        for elem in acc.data() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);
        let acc = arr1.const_accessor();

        let mut i = 0;
        acc.for_each(|val| {
            assert_eq!(acc[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);
        let acc = arr1.const_accessor();

        acc.for_each_index(|i, j, k| {
            let idx = i + (4 * (j + 3 * k)) + 1;
            assert_eq!(idx as f64, acc[(i, j, k)]);
        });
    }
}