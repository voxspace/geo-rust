/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::point_generator2::PointGenerator2;
use crate::vector2::Vector2D;
use crate::bounding_box2::BoundingBox2D;
use std::sync::{RwLock, Arc};

///
/// # Right triangle point generator.
///
pub struct TrianglePointGenerator {}

impl PointGenerator2 for TrianglePointGenerator {
    ///
    /// # Invokes \p callback function for each right triangle points
    /// inside \p boundingBox.
    ///
    /// This function iterates every right triangle points inside \p boundingBox
    /// where \p spacing is the size of the right triangle structure.
    ///
    fn for_each_point<Callback>(&self, bounding_box: &BoundingBox2D, spacing: f64, callback: &mut Callback)
        where Callback: FnMut(&Vector2D) -> bool {
        let half_spacing = spacing / 2.0;
        let y_spacing = spacing * f64::sqrt(3.0) / 2.0;
        let box_width = bounding_box.width();
        let box_height = bounding_box.height();

        let mut position = Vector2D::default();
        let mut has_offset = false;
        let mut should_quit = false;
        let mut j: usize = 0;
        while j as f64 * y_spacing <= box_height && !should_quit {
            position.y = j as f64 * y_spacing + bounding_box.lower_corner.y;

            let offset = match has_offset {
                true => half_spacing,
                false => 0.0
            };

            let mut i: usize = 0;
            while i as f64 * spacing + offset <= box_width && !should_quit {
                position.x = i as f64 * spacing + offset + bounding_box.lower_corner.x;
                if !callback(&position) {
                    should_quit = true;
                    break;
                }
                i += 1;
            }

            has_offset = !has_offset;
            j += 1;
        }
    }
}

pub type TrianglePointGeneratorPtr = Arc<RwLock<TrianglePointGenerator>>;