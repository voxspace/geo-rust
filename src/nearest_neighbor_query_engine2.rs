/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;

/// Nearest neighbor query result.
pub struct NearestNeighborQueryResult2<T> {
    pub item: Option<T>,
    pub distance: f64,
}

impl<T> NearestNeighborQueryResult2<T> {
    pub fn new() -> NearestNeighborQueryResult2<T> {
        return NearestNeighborQueryResult2 {
            item: None,
            distance: f64::MAX,
        };
    }
}

/// Nearest neighbor distance measure function.
pub trait NearestNeighborDistanceFunc2<T>: FnMut(&T, &Vector2D) -> f64 {}

impl<T, Super: FnMut(&T, &Vector2D) -> f64> NearestNeighborDistanceFunc2<T> for Super {}

/// Abstract base class for 2-D nearest neighbor query engine.
pub trait NearestNeighborQueryEngine2<T> {
    /// Returns the nearest neighbor for given point and distance measure function.
    fn nearest<Callback>(&self, pt: &Vector2D,
                         distance_func: &mut Callback) -> NearestNeighborQueryResult2<T>
        where Callback: NearestNeighborDistanceFunc2<T>;
}