/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::vector3::Vector3D;
use num::traits::Pow;

//MARK:- computeDragForce
pub fn compute_drag_force2(drag_coefficient: f64,
                           radius: f64,
                           velocity: &Vector2D) -> Vector2D {
    // Stoke's drag force assuming our Reynolds number is very low.
    // http://en.wikipedia.org/wiki/Drag_(physics)#Very_low_Reynolds_numbers:_Stokes.27_drag
    return *velocity * -6.0 * crate::constants::K_PI_D * drag_coefficient * radius;
}

pub fn compute_drag_force3(drag_coefficient: f64,
                           radius: f64,
                           velocity: &Vector3D) -> Vector3D {
    // Stoke's drag force assuming our Reynolds number is very low.
    // http://en.wikipedia.org/wiki/Drag_(physics)#Very_low_Reynolds_numbers:_Stokes.27_drag
    return *velocity * -6.0 * crate::constants::K_PI_D * drag_coefficient * radius;
}

//MARK:- projectAndApplyFriction
pub fn project_and_apply_friction2(vel: &Vector2D,
                                   normal: &Vector2D,
                                   friction_coefficient: f64) -> Vector2D {
    let mut velt = vel.projected(normal);
    if velt.length_squared() > 0.0 {
        let veln = f64::max(-vel.dot(&normal), 0.0);
        velt *= f64::max(1.0 - friction_coefficient * veln / velt.length(), 0.0)
    }

    return velt;
}

pub fn project_and_apply_friction3(vel: &Vector3D,
                                   normal: &Vector3D,
                                   friction_coefficient: f64) -> Vector3D {
    let mut velt = vel.projected(normal);
    if velt.length_squared() > 0.0 {
        let veln = f64::max(-vel.dot(normal), 0.0);
        velt *= f64::max(1.0 - friction_coefficient * veln / velt.length(), 0.0)
    }

    return velt;
}

//MARK:- compute_pressure_from_eos
pub fn compute_pressure_from_eos(density: f64,
                                 target_density: f64,
                                 eos_scale: f64,
                                 eos_exponent: f64,
                                 negative_pressure_scale: f64) -> f64 {
    // See Murnaghan-Tait equation of state from
    // https://en.wikipedia.org/wiki/Tait_equation
    let mut p = eos_scale / eos_exponent * (f64::pow(density / target_density, eos_exponent) - 1.0);

    // Negative pressure scaling
    if p < 0.0 {
        p *= negative_pressure_scale
    }

    return p;
}