/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use std::ops::{Index, IndexMut, AddAssign, SubAssign, MulAssign, DivAssign, Add, Neg, Sub, Mul, Div};
use std::fmt::{Debug, Formatter, Result};

///
/// # 2-D point class.
///
/// This class defines simple 2-D point data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy)]
pub struct Point2<T: Float> {
    /// X (or the first) component of the point.
    pub x: T,

    /// Y (or the second) component of the point.
    pub y: T,
}

/// Float-type 2D point.
pub type Point2F = Point2<f32>;
/// Double-type 2D point.
pub type Point2D = Point2<f64>;

impl<T: Float> Default for Point2<T> {
    /// Constructs default point (0, 0).
    fn default() -> Self {
        return Point2 {
            x: T::zero(),
            y: T::zero(),
        };
    }
}

/// # Constructors
impl<T: Float> Point2<T> {
    /// Constructs point with given parameters \p x_ and \p y_.
    pub fn new(x_: T, y_: T) -> Point2<T> {
        return Point2 {
            x: x_,
            y: y_,
        };
    }

    /// Constructs point with initializer list.
    pub fn new_slice(lst: &[T]) -> Point2<T> {
        return Point2 {
            x: lst[0],
            y: lst[1],
        };
    }
}

/// # Basic setters
impl<T: Float> Point2<T> {
    /// Set both x and y components to **s**.
    pub fn set_scalar(&mut self, s: T) {
        self.x = s;
        self.y = s;
    }

    /// Set x and y components with given parameters.
    pub fn set(&mut self, x: T, y: T) {
        self.x = x;
        self.y = y;
    }

    /// Set x and y components with given initializer list.
    pub fn set_slice(&mut self, lst: &[T]) {
        self.x = lst[0];
        self.y = lst[1];
    }

    /// Set x and y with other point **pt**.
    pub fn set_self(&mut self, pt: &Point2<T>) {
        self.x = pt.x;
        self.y = pt.y;
    }

    /// Set both x and y to zero.
    pub fn set_zero(&mut self) {
        self.x = T::zero();
        self.y = T::zero();
    }
}

/// # Binary operations: new instance = this (+) v
impl<T: Float> Point2<T> {
    /// Computes self + (v, v).
    pub fn add_scalar(&self, v: T) -> Point2<T> {
        return Point2::new(self.x + v, self.y + v);
    }

    /// Computes self + (v.x, v.y).
    pub fn add_vec(&self, v: &Point2<T>) -> Point2<T> {
        return Point2::new(self.x + v.x, self.y + v.y);
    }

    /// Computes self - (v, v).
    pub fn sub_scalar(&self, v: T) -> Point2<T> {
        return Point2::new(self.x - v, self.y - v);
    }

    /// Computes self - (v.x, v.y).
    pub fn sub_vec(&self, v: &Point2<T>) -> Point2<T> {
        return Point2::new(self.x - v.x, self.y - v.y);
    }

    /// Computes self * (v, v).
    pub fn mul_scalar(&self, v: T) -> Point2<T> {
        return Point2::new(self.x * v, self.y * v);
    }

    /// Computes self * (v.x, v.y).
    pub fn mul_vec(&self, v: &Point2<T>) -> Point2<T> {
        return Point2::new(self.x * v.x, self.y * v.y);
    }

    /// Computes self / (v, v).
    pub fn div_scalar(&self, v: T) -> Point2<T> {
        return Point2::new(self.x / v, self.y / v);
    }

    /// Computes self / (v.x, v.y).
    pub fn div_vec(&self, v: &Point2<T>) -> Point2<T> {
        return Point2::new(self.x / v.x, self.y / v.y);
    }
}

/// # Binary operations: new instance = v (+) this
impl<T: Float> Point2<T> {
    /// Computes (v, v) - self.
    pub fn rsub_scalar(&self, v: T) -> Point2<T> {
        return Point2::new(v - self.x, v - self.y);
    }

    /// Computes (v.x, v.y) - self.
    pub fn rsub_vec(&self, v: &Point2<T>) -> Point2<T> {
        return Point2::new(v.x - self.x, v.y - self.y);
    }

    /// Computes (v, v) / self.
    pub fn rdiv_scalar(&self, v: T) -> Point2<T> {
        return Point2::new(v / self.x, v / self.y);
    }

    /// Computes (v.x, v.y) / self.
    pub fn rdiv_vec(&self, v: &Point2<T>) -> Point2<T> {
        return Point2::new(v.x / self.x, v.y / self.y);
    }
}

/// # Augmented operations: this (+)= v
impl<T: Float> Point2<T> {
    /// Computes self += (v, v).
    pub fn iadd_scalar(&mut self, v: T) {
        self.x = T::add(self.x, v);
        self.y = T::add(self.y, v);
    }

    /// Computes self += (v.x, v.y).
    pub fn iadd_vec(&mut self, v: &Point2<T>) {
        self.x = T::add(self.x, v.x);
        self.y = T::add(self.y, v.y);
    }

    /// Computes self -= (v, v).
    pub fn isub_scalar(&mut self, v: T) {
        self.x = T::sub(self.x, v);
        self.y = T::sub(self.y, v);
    }

    /// Computes self -= (v.x, v.y).
    pub fn isub_vec(&mut self, v: &Point2<T>) {
        self.x = T::sub(self.x, v.x);
        self.y = T::sub(self.y, v.y);
    }

    /// Computes self *= (v, v).
    pub fn imul_scalar(&mut self, v: T) {
        self.x = T::mul(self.x, v);
        self.y = T::mul(self.y, v);
    }

    /// Computes self *= (v.x, v.y).
    pub fn imul_vec(&mut self, v: &Point2<T>) {
        self.x = T::mul(self.x, v.x);
        self.y = T::mul(self.y, v.y);
    }

    /// Computes self /= (v, v).
    pub fn idiv_scalar(&mut self, v: T) {
        self.x = T::div(self.x, v);
        self.y = T::div(self.y, v);
    }

    /// Computes self /= (v.x, v.y).
    pub fn idiv_vec(&mut self, v: &Point2<T>) {
        self.x = T::div(self.x, v.x);
        self.y = T::div(self.y, v.y);
    }
}

/// # Basic getters
impl<T: Float> Point2<T> {
    /// Returns const reference to the **i** -th element of the point.
    pub fn at(&self, i: usize) -> &T {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the point.
    pub fn at_mut(&mut self, i: usize) -> &mut T {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y).
    pub fn sum(&self) -> T {
        return self.x + self.y;
    }

    /// Returns the minimum value among x and y.
    pub fn min(&self) -> T {
        return self.x.min(self.y);
    }

    /// Returns the maximum value among x and y.
    pub fn max(&self) -> T {
        return self.x.max(self.y);
    }

    /// Returns the absolute minimum value among x and y.
    pub fn absmin(&self) -> T {
        return crate::math_utils::absmin(self.x, self.y);
    }

    /// Returns the absolute maximum value among x and y.
    pub fn absmax(&self) -> T {
        return crate::math_utils::absmax(self.x, self.y);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        match self.x.abs() > self.y.abs() {
            true => 0,
            false => 1
        }
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        match self.x.abs() < self.y.abs() {
            true => 0,
            false => 1
        }
    }

    /// Returns true if **other** is the same as self point.
    pub fn is_equal(&self, other: &Point2<T>) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the point.
impl<T: Float> Index<usize> for Point2<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the point.
impl<T: Float> IndexMut<usize> for Point2<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v)
impl<T: Float> AddAssign<T> for Point2<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y)
impl<T: Float> AddAssign for Point2<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v)
impl<T: Float> SubAssign<T> for Point2<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y)
impl<T: Float> SubAssign for Point2<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v)
impl<T: Float> MulAssign<T> for Point2<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y)
impl<T: Float> MulAssign for Point2<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v)
impl<T: Float> DivAssign<T> for Point2<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y)
impl<T: Float> DivAssign for Point2<T> {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self point.
impl<T: Float> PartialEq for Point2<T> {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl<T: Float> Eq for Point2<T> {}

impl<T: Float> Neg for Point2<T> {
    type Output = Point2<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Point2::new(-self.x, -self.y);
    }
}

/// Computes (a, a) + (b.x, b.y).
impl<T: Float> Add<T> for Point2<T> {
    type Output = Point2<T>;
    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y) + (b.x, b.y).
impl<T: Float> Add for Point2<T> {
    type Output = Point2<T>;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

/// Computes (a.x, a.y) - (b, b).
impl<T: Float> Sub<T> for Point2<T> {
    type Output = Point2<T>;
    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y) - (b.x, b.y).
impl<T: Float> Sub for Point2<T> {
    type Output = Point2<T>;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

/// Computes (a.x, a.y) * (b, b).
impl<T: Float> Mul<T> for Point2<T> {
    type Output = Point2<T>;
    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y) * (b.x, b.y).
impl<T: Float> Mul for Point2<T> {
    type Output = Point2<T>;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y) / (b, b).
impl<T: Float> Div<T> for Point2<T> {
    type Output = Point2<T>;
    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y) / (b.x, b.y).
impl<T: Float> Div for Point2<T> {
    type Output = Point2<T>;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl<T: Float + Debug> Debug for Point2<T> {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::point2::Point2F;
    /// let vec = Point2F::new(10.0, 20.0);
    /// assert_eq!(format!("{:?}", vec), "(10.0, 20.0)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10.0,
    ///     20.0,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .finish()
    }
}

/// # utility
/// Returns element-wise min point: (min(a.x, b.x), min(a.y, b.y)).
pub fn min<T: Float>(a: &Point2<T>, b: &Point2<T>) -> Point2<T> {
    return Point2::new(T::min(a.x, b.x), T::min(a.y, b.y));
}

/// Returns element-wise max point: (max(a.x, b.x), max(a.y, b.y)).
pub fn max<T: Float>(a: &Point2<T>, b: &Point2<T>) -> Point2<T> {
    return Point2::new(T::max(a.x, b.x), T::max(a.y, b.y));
}

/// Returns element-wise clamped point.
pub fn clamp<T: Float>(v: &Point2<T>, low: &Point2<T>, high: &Point2<T>) -> Point2<T> {
    return Point2::new(crate::math_utils::clamp(v.x, low.x, high.x),
                       crate::math_utils::clamp(v.y, low.y, high.y));
}

/// Returns element-wise ceiled point.
pub fn ceil<T: Float>(a: &Point2<T>) -> Point2<T> {
    return Point2::new((a.x).ceil(), (a.y).ceil());
}

/// Returns element-wise floored point.
pub fn floor<T: Float>(a: &Point2<T>) -> Point2<T> {
    return Point2::new((a.x).floor(), (a.y).floor());
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod point2 {
    use crate::point2::*;

    #[test]
    fn constructors() {
        let pt = Point2F::default();
        assert_eq!(0.0, pt.x);
        assert_eq!(0.0, pt.y);

        let pt2 = Point2F::new(5.0, 3.0);
        assert_eq!(5.0, pt2.x);
        assert_eq!(3.0, pt2.y);

        let pt5 = Point2F::new_slice(&[7.0, 6.0]);
        assert_eq!(7.0, pt5.x);
        assert_eq!(6.0, pt5.y);

        let pt6 = pt5.clone();
        assert_eq!(7.0, pt6.x);
        assert_eq!(6.0, pt6.y);
    }

    #[test]
    fn set_methods() {
        let mut pt = Point2F::default();
        pt.set(4.0, 2.0);
        assert_eq!(4.0, pt.x);
        assert_eq!(2.0, pt.y);

        let lst = [0.0, 5.0];
        pt.set_slice(&lst);
        assert_eq!(0.0, pt.x);
        assert_eq!(5.0, pt.y);

        pt.set_self(&Point2F::new(9.0, 8.0));
        assert_eq!(9.0, pt.x);
        assert_eq!(8.0, pt.y);
    }

    #[test]
    fn basic_setter_methods() {
        let mut pt = Point2F::new(3.0, 9.0);
        pt.set_zero();
        assert_eq!(0.0, pt.x);
        assert_eq!(0.0, pt.y);
    }

    #[test]
    fn binary_operator_methods() {
        let mut pt = Point2F::new(3.0, 9.0);
        pt = pt.add_scalar(4.0);
        assert_eq!(7.0, pt.x);
        assert_eq!(13.0, pt.y);

        pt = pt.add_vec(&Point2F::new(-2.0, 1.0));
        assert_eq!(5.0, pt.x);
        assert_eq!(14.0, pt.y);

        pt = pt.sub_scalar(8.0);
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt = pt.sub_vec(&Point2F::new(-5.0, 3.0));
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);

        pt = pt.mul_scalar(2.0);
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt = pt.mul_vec(&Point2F::new(3.0, -2.0));
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);

        pt = pt.div_scalar(4.0);
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);

        pt = pt.div_vec(&Point2F::new(3.0, -1.0));
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
    }

    #[test]
    fn binary_inverse_operator_methods() {
        let mut pt = Point2F::new(3.0, 9.0);
        pt = pt.rsub_scalar(8.0);
        assert_eq!(5.0, pt.x);
        assert_eq!(-1.0, pt.y);

        pt = pt.rsub_vec(&Point2F::new(-5.0, 3.0));
        assert_eq!(-10.0, pt.x);
        assert_eq!(4.0, pt.y);

        pt = Point2F::new(-4.0, -3.0);
        pt = pt.rdiv_scalar(12.0);
        assert_eq!(-3.0, pt.x);
        assert_eq!(pt.y, -4.0);

        pt = pt.rdiv_vec(&Point2F::new(3.0, -16.0));
        assert_eq!(-1.0, pt.x);
        assert_eq!(4.0, pt.y);
    }

    #[test]
    fn augmented_operator_methods() {
        let mut pt = Point2F::new(3.0, 9.0);
        pt.iadd_scalar(4.0);
        assert_eq!(7.0, pt.x);
        assert_eq!(pt.y, 13.0);

        pt.iadd_vec(&Point2F::new(-2.0, 1.0));
        assert_eq!(5.0, pt.x);
        assert_eq!(pt.y, 14.0);

        pt.isub_scalar(8.0);
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt.isub_vec(&Point2F::new(-5.0, 3.0));
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);

        pt.imul_scalar(2.0);
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt.imul_vec(&Point2F::new(3.0, -2.0));
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);

        pt.idiv_scalar(4.0);
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);

        pt.idiv_vec(&Point2F::new(3.0, -1.0));
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
    }

    #[test]
    fn at_method() {
        let mut pt = Point2F::new(8.0, 9.0);
        assert_eq!(*pt.at(0), 8.0);
        assert_eq!(*pt.at(1), 9.0);

        *pt.at_mut(0) = 7.0;
        *pt.at_mut(1) = 6.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(6.0, pt.y);
    }

    #[test]
    fn basic_getter_methods() {
        let pt = Point2F::new(3.0, 7.0);
        let pt2 = Point2F::new(-3.0, -7.0);

        let sum = pt.sum();
        assert_eq!(sum, 10.0);

        let min = pt.min();
        assert_eq!(min, 3.0);

        let max = pt.max();
        assert_eq!(max, 7.0);

        let absmin = pt2.absmin();
        assert_eq!(absmin, -3.0);

        let absmax = pt2.absmax();
        assert_eq!(absmax, -7.0);

        let daxis = pt.dominant_axis();
        assert_eq!(daxis, 1);

        let saxis = pt.subminant_axis();
        assert_eq!(saxis, 0);
    }

    #[test]
    fn bracket_operator() {
        let mut pt = Point2F::new(8.0, 9.0);
        assert_eq!(pt[0], 8.0);
        assert_eq!(pt[1], 9.0);

        pt[0] = 7.0;
        pt[1] = 6.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(6.0, pt.y);
    }

    #[test]
    fn assignment_operator() {
        let pt = Point2F::new(5.0, 1.0);
        let pt2 = pt;
        assert_eq!(5.0, pt2.x);
        assert_eq!(pt2.y, 1.0);
    }

    #[test]
    fn augmented_operators() {
        let mut pt = Point2F::new(3.0, 9.0);
        pt += 4.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(pt.y, 13.0);

        pt += Point2F::new(-2.0, 1.0);
        assert_eq!(5.0, pt.x);
        assert_eq!(pt.y, 14.0);

        pt -= 8.0;
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt -= Point2F::new(-5.0, 3.0);
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);

        pt *= 2.0;
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt *= Point2F::new(3.0, -2.0);
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);

        pt /= 4.0;
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);

        pt /= Point2F::new(3.0, -1.0);
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
    }

    #[test]
    fn equal_operator() {
        let pt2 = Point2F::new(3.0, 7.0);
        let pt3 = Point2F::new(3.0, 5.0);
        let pt4 = Point2F::new(5.0, 1.0);
        let pt = pt2;
        assert_eq!(pt == pt2, true);
        assert_eq!(pt == pt3, false);
        assert_eq!(pt != pt2, false);
        assert_eq!(pt != pt3, true);
        assert_eq!(pt != pt4, true);
    }

    #[test]
    fn min_max_function() {
        let pt = Point2F::new(5.0, 1.0);
        let pt2 = Point2F::new(3.0, 3.0);
        let min_point = min(&pt, &pt2);
        let max_point = max(&pt, &pt2);
        assert_eq!(Point2F::new(3.0, 1.0), min_point);
        assert_eq!(Point2F::new(5.0, 3.0), max_point);
    }

    #[test]
    fn clamp_function() {
        let pt = Point2F::new(2.0, 4.0);
        let low = Point2F::new(3.0, -1.0);
        let high = Point2F::new(5.0, 2.0);
        let clamped_vec = clamp(&pt, &low, &high);
        assert_eq!(Point2F::new(3.0, 2.0), clamped_vec);
    }

    #[test]
    fn ceil_floor_function() {
        let pt = Point2F::new(2.2, 4.7);
        let ceil_vec = ceil(&pt);
        assert_eq!(Point2F::new(3.0, 5.0), ceil_vec);

        let floor_vec = floor(&pt);
        assert_eq!(Point2F::new(2.0, 4.0), floor_vec);
    }

    #[test]
    fn binary_operators() {
        let mut pt = Point2F::new(3.0, 9.0);
        pt = pt + 4.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(pt.y, 13.0);

        pt = pt + Point2F::new(-2.0, 1.0);
        assert_eq!(5.0, pt.x);
        assert_eq!(pt.y, 14.0);

        pt = pt - 8.0;
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt = pt - Point2F::new(-5.0, 3.0);
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);

        pt = pt * 2.0;
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);

        pt = pt * Point2F::new(3.0, -2.0);
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);

        pt = pt / 4.0;
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);

        pt = pt / Point2F::new(3.0, -1.0);
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
    }
}