/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector3::Vector3D;
use crate::quaternion::QuaternionD;
use crate::matrix3x3::Matrix3x3D;
use crate::ray3::Ray3D;
use crate::bounding_box3::BoundingBox3D;

///
/// # Represents 3-D rigid body transform.
///
pub struct Transform3 {
    _translation: Vector3D,
    _orientation: QuaternionD,
    _orientation_mat3: Matrix3x3D,
    _inverse_orientation_mat3: Matrix3x3D,
}

impl Transform3 {
    /// Constructs identity transform.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// let t1 = Transform3::default();
    ///
    /// assert_eq!(Vector3D::default(), t1.translation());
    /// assert_eq!(0.0, t1.orientation().angle());
    /// ```
    pub fn default() -> Transform3 {
        return Transform3 {
            _translation: Vector3D::default(),
            _orientation: QuaternionD::default(),
            _orientation_mat3: Matrix3x3D::default(),
            _inverse_orientation_mat3: Matrix3x3D::default(),
        };
    }

    /// Constructs a transform with translation and orientation.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::constants::K_QUARTER_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t2 = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_QUARTER_PI_D));
    ///
    /// assert_eq!(Vector3D::new(2.0, -5.0, 1.0), t2.translation());
    /// assert_eq!(Vector3D::new(0.0, 1.0, 0.0), t2.orientation().axis());
    /// assert_delta!(K_QUARTER_PI_D, t2.orientation().angle(), f64::EPSILON);
    /// ```
    pub fn new(translation: Vector3D, orientation: QuaternionD) -> Transform3 {
        return Transform3 {
            _translation: translation,
            _orientation: orientation,
            _orientation_mat3: orientation.matrix3(),
            _inverse_orientation_mat3: orientation.inverse().matrix3(),
        };
    }
}

impl Transform3 {
    /// Returns the translation.
    pub fn translation(&self) -> Vector3D {
        return self._translation;
    }

    /// Sets the translation.
    pub fn set_translation(&mut self, translation: Vector3D) {
        self._translation = translation;
    }

    /// Returns the orientation.
    pub fn orientation(&self) -> QuaternionD {
        return self._orientation;
    }

    /// Sets the orientation.
    pub fn set_orientation(&mut self, orientation: QuaternionD) {
        self._orientation = orientation;
        self._orientation_mat3 = orientation.matrix3();
        self._inverse_orientation_mat3 = orientation.inverse().matrix3();
    }

    /// Transforms a point in world coordinate to the local frame.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::constants::K_HALF_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_HALF_PI_D));
    ///
    /// let r1 = t.to_world_vec(&Vector3D::new(4.0, 1.0, -3.0));
    /// let r2 = t.to_local_vec(&r1);
    /// assert_delta!(4.0, r2.x, 1e-9);
    /// assert_delta!(1.0, r2.y, 1e-9);
    /// assert_delta!(-3.0, r2.z, 1e-9);
    /// ```
    pub fn to_local_vec(&self, point_in_world: &Vector3D) -> Vector3D {
        return self._inverse_orientation_mat3 * (*point_in_world - self._translation);
    }

    /// Transforms a direction in world coordinate to the local frame.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::constants::K_HALF_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_HALF_PI_D));
    ///
    /// let r3 = t.to_world_direction(&Vector3D::new(4.0, 1.0, -3.0));
    /// let r4 = t.to_local_direction(&r3);
    /// assert_delta!(4.0, r4.x, 1e-9);
    /// assert_delta!(1.0, r4.y, 1e-9);
    /// assert_delta!(-3.0, r4.z, 1e-9);
    /// ```
    pub fn to_local_direction(&self, dir_in_world: &Vector3D) -> Vector3D {
        return self._inverse_orientation_mat3 * *dir_in_world;
    }

    /// Transforms a ray in world coordinate to the local frame.
    pub fn to_local_ray(&self, ray_in_world: &Ray3D) -> Ray3D {
        return Ray3D::new(
            self.to_local_vec(&ray_in_world.origin),
            self.to_local_direction(&ray_in_world.direction));
    }

    /// Transforms a bounding box in world coordinate to the local frame.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::bounding_box3::BoundingBox3D;
    /// use vox_geometry_rust::constants::K_HALF_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_HALF_PI_D));
    ///
    /// let bbox = BoundingBox3D::new(Vector3D::new(-2.0, -1.0, -3.0), Vector3D::new(2.0, 1.0, 3.0));
    /// let r5 = t.to_world_aabb(&bbox);
    /// let r6 = t.to_local_aabb(&r5);
    /// assert_eq!(bbox.lower_corner.is_similar(&r6.lower_corner, Some(1e-9)), true);
    /// assert_eq!(bbox.upper_corner.is_similar(&r6.upper_corner, Some(1e-9)), true);
    /// ```
    pub fn to_local_aabb(&self, bbox_in_world: &BoundingBox3D) -> BoundingBox3D {
        let mut bbox_in_local = BoundingBox3D::default();
        for i in 0..8 {
            let corner_in_local = self.to_local_vec(&bbox_in_world.corner(i));
            bbox_in_local.lower_corner
                = crate::vector3::min(&bbox_in_local.lower_corner, &corner_in_local);
            bbox_in_local.upper_corner
                = crate::vector3::max(&bbox_in_local.upper_corner, &corner_in_local);
        }
        return bbox_in_local;
    }

    /// Transforms a point in local space to the world coordinate.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::constants::K_HALF_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_HALF_PI_D));
    ///
    /// let r1 = t.to_world_vec(&Vector3D::new(4.0, 1.0, -3.0));
    /// assert_delta!(-1.0, r1.x, 1e-9);
    /// assert_delta!(-4.0, r1.y, 1e-9);
    /// assert_delta!(-3.0, r1.z, 1e-9);
    /// ```
    pub fn to_world_vec(&self, point_in_local: &Vector3D) -> Vector3D {
        return (self._orientation_mat3 * *point_in_local) + self._translation;
    }

    /// Transforms a direction in local space to the world coordinate.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::constants::K_HALF_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_HALF_PI_D));
    ///
    /// let r3 = t.to_world_direction(&Vector3D::new(4.0, 1.0, -3.0));
    /// assert_delta!(-3.0, r3.x, 1e-9);
    /// assert_delta!(1.0, r3.y, 1e-9);
    /// assert_delta!(-4.0, r3.z, 1e-9);
    /// ```
    pub fn to_world_direction(&self, dir_in_local: &Vector3D) -> Vector3D {
        return self._orientation_mat3 * *dir_in_local;
    }

    /// Transforms a ray in local space to the world coordinate.
    pub fn to_world_ray(&self, ray_in_local: &Ray3D) -> Ray3D {
        return Ray3D::new(
            self.to_world_vec(&ray_in_local.origin),
            self.to_world_direction(&ray_in_local.direction));
    }

    /// Transforms a bounding box in local space to the world coordinate.
    /// ```
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// use vox_geometry_rust::bounding_box3::BoundingBox3D;
    /// use vox_geometry_rust::constants::K_HALF_PI_D;
    /// use vox_geometry_rust::assert_delta;
    /// let t = Transform3::new(Vector3D::new(2.0, -5.0, 1.0), QuaternionD::new_axis(Vector3D::new(0.0, 1.0, 0.0), K_HALF_PI_D));
    ///
    /// let bbox = BoundingBox3D::new(Vector3D::new(-2.0, -1.0, -3.0), Vector3D::new(2.0, 1.0, 3.0));
    /// let r5 = t.to_world_aabb(&bbox);
    /// let result = BoundingBox3D::new(Vector3D::new(-1.0, -6.0, -1.0), Vector3D::new(5.0, -4.0, 3.0));
    /// assert_eq!(result.lower_corner.is_similar(&r5.lower_corner, Some(1e-9)), true);
    /// assert_eq!(result.upper_corner.is_similar(&r5.upper_corner, Some(1e-9)), true);
    /// ```
    pub fn to_world_aabb(&self, bbox_in_local: &BoundingBox3D) -> BoundingBox3D {
        let mut bbox_in_world = BoundingBox3D::default();
        for i in 0..8 {
            let corner_in_world = self.to_world_vec(&bbox_in_local.corner(i));
            bbox_in_world.lower_corner
                = crate::vector3::min(&bbox_in_world.lower_corner, &corner_in_world);
            bbox_in_world.upper_corner
                = crate::vector3::max(&bbox_in_world.upper_corner, &corner_in_world);
        }
        return bbox_in_world;
    }
}

impl Clone for Transform3 {
    fn clone(&self) -> Self {
        return Transform3 {
            _translation: self._translation,
            _orientation: self._orientation,
            _orientation_mat3: self._orientation_mat3,
            _inverse_orientation_mat3: self._inverse_orientation_mat3,
        };
    }
}