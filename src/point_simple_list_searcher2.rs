/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::point_neighbor_searcher2::*;
use std::sync::{RwLock, Arc};

///
/// # Simple ad-hoc 2-D point searcher.
///
/// This class implements 2-D point searcher simply by looking up every point in
/// the list. Thus, this class is not ideal for searches involving large number
/// of points, but only for small set of items.
///
pub struct PointSimpleListSearcher2 {
    _points: Vec<Vector2D>,
}

impl PointSimpleListSearcher2 {
    /// Default constructor.
    pub fn new() -> PointSimpleListSearcher2 {
        return PointSimpleListSearcher2 {
            _points: vec![]
        };
    }

    /// Returns builder fox PointSimpleListSearcher2.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    ///
    /// \brief      Creates a new instance of the object with same properties
    ///             than original.
    ///
    /// \return     Copy of this object.
    ///
    pub fn clone(&self) -> PointSimpleListSearcher2Ptr {
        let mut searcher = PointSimpleListSearcher2::new();
        searcher.set(self);
        return PointSimpleListSearcher2Ptr::new(RwLock::new(searcher));
    }

    /// Copy from the other instance.
    pub fn set(&mut self, other: &PointSimpleListSearcher2) {
        self._points = other._points.clone();
    }
}

impl PointNeighborSearcher2 for PointSimpleListSearcher2 {
    fn type_name() -> String {
        return "PointSimpleListSearcher2".parse().unwrap();
    }

    fn build(&mut self, points: &Vec<Vector2D>) {
        self._points = points.clone();
    }

    fn for_each_nearby_point<Callback>(&self, origin: &Vector2D, radius: f64, callback: &mut Callback)
        where Callback: ForEachNearbyPointFunc {
        let radius_squared = radius * radius;
        for i in 0..self._points.len() {
            let r = self._points[i] - *origin;
            let distance_squared = r.dot(&r);
            if distance_squared <= radius_squared {
                callback(i, &self._points[i]);
            }
        }
    }

    fn has_nearby_point(&self, origin: &Vector2D, radius: f64) -> bool {
        let radius_squared = radius * radius;
        for i in 0..self._points.len() {
            let r = self._points[i] - *origin;
            let distance_squared = r.dot(&r);
            if distance_squared <= radius_squared {
                return true;
            }
        }

        return false;
    }
}

/// Shared pointer for the PointSimpleListSearcher2 type.
pub type PointSimpleListSearcher2Ptr = Arc<RwLock<PointSimpleListSearcher2>>;

///
/// # Front-end to create PointSimpleListSearcher2 objects step by step.
///
pub struct Builder {}

impl Builder {
    /// Builds PointSimpleListSearcher2 instance.
    pub fn build(&self) -> PointSimpleListSearcher2 {
        return PointSimpleListSearcher2::new();
    }

    /// Builds shared pointer of PointSimpleListSearcher2 instance.
    pub fn make_shared(&self) -> PointSimpleListSearcher2Ptr {
        return PointSimpleListSearcher2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {};
    }
}
