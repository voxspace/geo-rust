/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::point_generator3::PointGenerator3;
use crate::vector3::Vector3D;
use crate::bounding_box3::BoundingBox3D;
use std::sync::{RwLock, Arc};

///
/// # 3-D regular-grid point generator.
///
pub struct GridPointGenerator3 {}

impl PointGenerator3 for GridPointGenerator3 {
    ///
    /// \brief Invokes \p callback function for each regular grid points inside
    /// \p bounding_box.
    ///
    /// This function iterates every regular grid points inside \p bounding_box
    /// where \p spacing is the size of the unit cell of regular grid structure.
    ///
    fn for_each_point<Callback>(&self, bounding_box: &BoundingBox3D, spacing: f64, callback: &mut Callback)
        where Callback: FnMut(&Vector3D) -> bool {
        let mut position = Vector3D::default();
        let box_width = bounding_box.width();
        let box_height = bounding_box.height();
        let box_depth = bounding_box.depth();

        let mut should_quit = false;
        let mut k = 0;
        while k as f64 * spacing <= box_depth && !should_quit {
            position.z = k as f64 * spacing + bounding_box.lower_corner.z;

            let mut j = 0;
            while j as f64 * spacing <= box_height && !should_quit {
                position.y = j as f64 * spacing + bounding_box.lower_corner.y;

                let mut i = 0;
                while i as f64 * spacing <= box_width && !should_quit {
                    position.x = i as f64 * spacing + bounding_box.lower_corner.x;
                    if !callback(&position) {
                        should_quit = true;
                        break;
                    }
                    i += 1;
                }
                j += 1;
            }
            k += 1;
        }
    }
}

/// Shared pointer type for the GridPointGenerator3.
pub type GridPointGenerator3Ptr = Arc<RwLock<GridPointGenerator3>>;