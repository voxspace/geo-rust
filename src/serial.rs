/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

///
/// #  Makes a for-loop from \p begin_index \p to end_index.
///
/// This function makes a for-loop specified by begin and end indices with
/// single thread. The order of the visit is deterministic.
///
/// \param[in]  begin_index The begin index.
/// \param[in]  end_index   The end index.
/// \param[in]  function   The function to call for each index.
///
pub fn serial_for<Callback: FnMut(usize)>(begin_index: usize, end_index: usize, mut function: Callback) {
    for i in begin_index..end_index {
        function(i);
    }
}

///
/// #  Makes a 2D nested for-loop.
///
/// This function makes a 2D nested for-loop specified by begin and end indices
/// for each dimension. X will be the inner-most loop while Y is the outer-most.
/// The order of the visit is deterministic.
///
/// \param[in]  begin_index_x The begin index in X dimension.
/// \param[in]  end_index_x   The end index in X dimension.
/// \param[in]  begin_index_y The begin index in Y dimension.
/// \param[in]  end_index_y   The end index in Y dimension.
/// \param[in]  function    The function to call for each index (i, j).
///
pub fn serial_for2<Callback: FnMut(usize, usize)>(begin_index_x: usize, end_index_x: usize,
                                                  begin_index_y: usize, end_index_y: usize,
                                                  mut function: Callback) {
    for j in begin_index_y..end_index_y {
        for i in begin_index_x..end_index_x {
            function(i, j);
        }
    }
}

///
/// #  Makes a 3D nested for-loop.
///
/// This function makes a 3D nested for-loop specified by begin and end indices
/// for each dimension. X will be the inner-most loop while Z is the outer-most.
/// The order of the visit is deterministic.
///
/// \param[in]  begin_index_x The begin index in X dimension.
/// \param[in]  end_index_x   The end index in X dimension.
/// \param[in]  begin_index_y The begin index in Y dimension.
/// \param[in]  end_index_y   The end index in Y dimension.
/// \param[in]  begin_index_z The begin index in Z dimension.
/// \param[in]  end_index_z   The end index in Z dimension.
/// \param[in]  function    The function to call for each index (i, j, k).
///
pub fn serial_for3<Callback: FnMut(usize, usize, usize)>(begin_index_x: usize, end_index_x: usize,
                                                         begin_index_y: usize, end_index_y: usize,
                                                         begin_index_z: usize, end_index_z: usize,
                                                         mut function: Callback) {
    for k in begin_index_z..end_index_z {
        for j in begin_index_y..end_index_y {
            for i in begin_index_x..end_index_x {
                function(i, j, k);
            }
        }
    }
}
