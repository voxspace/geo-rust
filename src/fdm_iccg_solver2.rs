/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::array_accessor2::ConstArrayAccessor2;
use crate::fdm_linear_system2::*;
use crate::cg::{PrecondTypeProtocol, pcg};
use crate::vector_n::VectorN;
use crate::matrix_csr::MatrixCsr;
use crate::fdm_linear_system_solver2::FdmLinearSystemSolver2;
use crate::math_utils::square;
use crate::matrix_expression::MatrixExpression;
use log::info;

#[derive(Default)]
struct Preconditioner<'a> {
    a: ConstArrayAccessor2<'a, FdmMatrixRow2>,
    d: FdmVector2,
    y: FdmVector2,
}

impl<'a> Preconditioner<'a> {
    fn build(&mut self, matrix: &'a FdmMatrix2) {
        let size = matrix.size();
        self.a = matrix.const_accessor();

        self.d.resize_with_packed_size(&size, Some(0.0));
        self.y.resize_with_packed_size(&size, Some(0.0));

        matrix.for_each_index(|i, j| {
            let denom =
                matrix[(i, j)].center -
                    (if i > 0 { square(matrix[(i - 1, j)].right) * self.d[(i - 1, j)] } else { 0.0 }) -
                    (if j > 0 { square(matrix[(i, j - 1)].up) * self.d[(i, j - 1)] } else { 0.0 });

            if f64::abs(denom) > 0.0 {
                self.d[(i, j)] = 1.0 / denom;
            } else {
                self.d[(i, j)] = 0.0;
            }
        });
    }
}

impl<'a> PrecondTypeProtocol<FdmBlas2> for Preconditioner<'a> {
    fn solve(&mut self, b: &FdmVector2, x: &mut FdmVector2) {
        let size = b.size();
        let sx = size.x as isize;
        let sy = size.y as isize;

        b.for_each_index(|i, j| {
            self.y[(i, j)] = (b[(i, j)] -
                (if i > 0 { self.a[(i - 1, j)].right * self.y[(i - 1, j)] } else { 0.0 }) -
                (if j > 0 { self.a[(i, j - 1)].up * self.y[(i, j - 1)] } else { 0.0 })) *
                self.d[(i, j)];
        });

        for j in sy as usize - 1..0 {
            for i in sx as usize - 1..0 {
                (*x)[(i, j)] =
                    (self.y[(i, j)] -
                        (if i + 1 < sx as usize { self.a[(i, j)].right * (*x)[(i + 1, j)] } else { 0.0 }) -
                        (if j + 1 < sy as usize { self.a[(i, j)].up * (*x)[(i, j + 1)] } else { 0.0 })) *
                        self.d[(i, j)];
            }
        }
    }
}

//--------------------------------------------------------------------------------------------------
#[derive(Default)]
struct PreconditionerCompressed<'a> {
    a: Option<&'a MatrixCsr>,
    d: VectorN,
    y: VectorN,
}

impl<'a> PreconditionerCompressed<'a> {
    fn build(&mut self, matrix: &'a MatrixCsr) {
        let size = matrix.cols();
        *self.a.as_mut().unwrap() = &matrix;

        self.d.resize(size, Some(0.0));
        self.y.resize(size, Some(0.0));

        let rp = self.a.as_ref().unwrap().row_pointers_data();
        let ci = self.a.as_ref().unwrap().column_indices_data();
        let nnz = self.a.as_ref().unwrap().non_zero_data();

        for i in 0..self.d.size() {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut denom = 0.0;
            for jj in row_begin..row_end {
                let j = ci[jj];

                if j == i {
                    denom += nnz[jj];
                } else if j < i {
                    denom -= square(nnz[jj]) * self.d[j];
                }
            }

            if f64::abs(denom) > 0.0 {
                self.d[i] = 1.0 / denom;
            } else {
                self.d[i] = 0.0;
            }
        };
    }
}

impl<'a> PrecondTypeProtocol<FdmCompressedBlas2> for PreconditionerCompressed<'a> {
    fn solve(&mut self, b: &VectorN, x: &mut VectorN) {
        let size = b.size() as isize;

        let rp = self.a.as_ref().unwrap().row_pointers_data();
        let ci = self.a.as_ref().unwrap().column_indices_data();
        let nnz = self.a.as_ref().unwrap().non_zero_data();

        b.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut sum = b[i];
            for jj in row_begin..row_end {
                let j = ci[jj];

                if j < i {
                    sum -= nnz[jj] * self.y[j];
                }
            }

            self.y[i] = sum * self.d[i];
        });

        for i in size as usize - 1..0 {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut sum = self.y[i];
            for jj in row_begin..row_end {
                let j = ci[jj] as isize;

                if j > i as isize {
                    sum -= nnz[jj] * (*x)[j as usize];
                }
            }

            (*x)[i] = sum * self.d[i];
        }
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # 2-D finite difference-type linear system solver using incomplete Cholesky conjugate gradient (ICCG).
///
pub struct FdmIccgSolver2<'a> {
    _max_number_of_iterations: u32,
    _last_number_of_iterations: u32,
    _tolerance: f64,
    _last_residual_norm: f64,

    // Uncompressed vectors and preconditioner
    _r: FdmVector2,
    _d: FdmVector2,
    _q: FdmVector2,
    _s: FdmVector2,
    _precond: Preconditioner<'a>,

    // Compressed vectors and preconditioner
    _r_comp: VectorN,
    _d_comp: VectorN,
    _q_comp: VectorN,
    _s_comp: VectorN,
    _precond_comp: PreconditionerCompressed<'a>,
}

impl<'a> FdmIccgSolver2<'a> {
    /// Constructs the solver with given parameters.
    pub fn new(max_number_of_iterations: u32, tolerance: f64) -> FdmIccgSolver2<'a> {
        return FdmIccgSolver2 {
            _max_number_of_iterations: max_number_of_iterations,
            _last_number_of_iterations: 0,
            _tolerance: tolerance,
            _last_residual_norm: crate::constants::K_MAX_D,
            _r: Default::default(),
            _d: Default::default(),
            _q: Default::default(),
            _s: Default::default(),
            _precond: Default::default(),
            _r_comp: Default::default(),
            _d_comp: Default::default(),
            _q_comp: Default::default(),
            _s_comp: Default::default(),
            _precond_comp: Default::default(),
        };
    }

    /// Returns the max number of Jacobi iterations.
    pub fn max_number_of_iterations(&self) -> u32 {
        return self._max_number_of_iterations;
    }

    /// Returns the last number of Jacobi iterations the solver made.
    pub fn last_number_of_iterations(&self) -> u32 {
        return self._last_number_of_iterations;
    }

    /// Returns the max residual tolerance for the Jacobi method.
    pub fn tolerance(&self) -> f64 {
        return self._tolerance;
    }

    /// Returns the last residual after the Jacobi iterations.
    pub fn last_residual(&self) -> f64 {
        return self._last_residual_norm;
    }
}

impl<'a> FdmLinearSystemSolver2<'a> for FdmIccgSolver2<'a> {
    fn solve(&'a mut self, system: &'a mut FdmLinearSystem2) -> bool {
        let matrix = &mut system.a;
        let solution = &mut system.x;
        let rhs = &mut system.b;

        debug_assert!(matrix.size() == rhs.size());
        debug_assert!(matrix.size() == solution.size());

        self.clear_compressed_vectors();

        let size = matrix.size();
        self._r.resize_with_packed_size(&size, None);
        self._d.resize_with_packed_size(&size, None);
        self._q.resize_with_packed_size(&size, None);
        self._s.resize_with_packed_size(&size, None);

        solution.set_scalar(0.0);
        self._r.set_scalar(0.0);
        self._d.set_scalar(0.0);
        self._q.set_scalar(0.0);
        self._s.set_scalar(0.0);

        self._precond.build(matrix);

        pcg::<FdmBlas2, Preconditioner>(
            matrix, rhs, self._max_number_of_iterations, self._tolerance, &mut self._precond, solution,
            &mut self._r, &mut self._d, &mut self._q, &mut self._s, &mut self._last_number_of_iterations, &mut self._last_residual_norm);

        info!("Residual after solving ICCG: {} Number of ICCG iterations: {}",
              self._last_residual_norm, self._last_number_of_iterations);

        return self._last_residual_norm <= self._tolerance ||
            self._last_number_of_iterations < self._max_number_of_iterations;
    }

    fn solve_compressed(&'a mut self, system: &'a mut FdmCompressedLinearSystem2) -> bool {
        let matrix = &mut system.a;
        let solution = &mut system.x;
        let rhs = &mut system.b;

        self.clear_uncompressed_vectors();

        let size = solution.size();
        self._r_comp.resize(size, None);
        self._d_comp.resize(size, None);
        self._q_comp.resize(size, None);
        self._s_comp.resize(size, None);

        solution.set_scalar(0.0);
        self._r_comp.set_scalar(0.0);
        self._d_comp.set_scalar(0.0);
        self._q_comp.set_scalar(0.0);
        self._s_comp.set_scalar(0.0);

        self._precond_comp.build(matrix);

        pcg::<FdmCompressedBlas2, PreconditionerCompressed>(
            matrix, rhs, self._max_number_of_iterations, self._tolerance, &mut self._precond_comp,
            solution, &mut self._r_comp, &mut self._d_comp, &mut self._q_comp, &mut self._s_comp,
            &mut self._last_number_of_iterations, &mut self._last_residual_norm);

        info!("Residual after solving ICCG: {} Number of ICCG iterations: {}",
              self._last_residual_norm, self._last_number_of_iterations);

        return self._last_residual_norm <= self._tolerance ||
            self._last_number_of_iterations < self._max_number_of_iterations;
    }
}

impl<'a> FdmIccgSolver2<'a> {
    fn clear_uncompressed_vectors(&mut self) {
        self._r.clear();
        self._d.clear();
        self._q.clear();
        self._s.clear();
    }

    fn clear_compressed_vectors(&mut self) {
        self._r_comp.clear();
        self._d_comp.clear();
        self._q_comp.clear();
        self._s_comp.clear();
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod fdm_iccg_solver2 {
    use crate::fdm_linear_system2::{FdmLinearSystem2, FdmCompressedLinearSystem2};
    use crate::usize2::USize2;
    use crate::fdm_iccg_solver2::FdmIccgSolver2;
    use crate::fdm_linear_system_solver2::FdmLinearSystemSolver2;

    #[test]
    fn solve_low_res() {
        todo!();
    }

    #[test]
    fn solve() {
        let mut system = FdmLinearSystem2::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper2::build_test_linear_system(
            &mut system, &USize2::new(128, 128));

        let mut solver = FdmIccgSolver2::new(200, 1e-4);
        assert_eq!(solver.solve(&mut system), true);
    }

    #[test]
    fn solve_compressed() {
        let mut system = FdmCompressedLinearSystem2::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper2::build_test_compressed_linear_system(
            &mut system, &USize2::new(3, 3));

        let mut solver = FdmIccgSolver2::new(200, 1e-4);
        assert_eq!(solver.solve_compressed(&mut system), true);
    }
}