/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::surface3::*;
use crate::bvh3::Bvh3;
use crate::transform3::Transform3;
use crate::vector3::Vector3D;
use crate::bounding_box3::BoundingBox3D;
use crate::ray3::Ray3D;
use crate::nearest_neighbor_query_engine3::NearestNeighborQueryEngine3;
use crate::intersection_query_engine3::IntersectionQueryEngine3;
use std::sync::{RwLock, Arc};

///
/// # 3-D surface set.
///
/// This class represents 3-D surface set which extends Surface3 by overriding
/// surface-related queries. This is class can hold a collection of other
/// surface instances.
///
pub struct SurfaceSet3 {
    _surfaces: Vec<Surface3Ptr>,
    _unbounded_surfaces: Vec<Surface3Ptr>,
    _bvh: RwLock<Bvh3<Surface3Ptr>>,
    _bvh_invalidated: RwLock<bool>,

    /// data from surface3
    pub surface_data: Surface3Data,
}

impl SurfaceSet3 {
    /// Constructs an empty surface set.
    /// ```
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// let sset1 = SurfaceSet3::default();
    /// assert_eq!(0, sset1.number_of_surfaces());
    /// ```
    pub fn default() -> SurfaceSet3 {
        return SurfaceSet3 {
            _surfaces: vec![],
            _unbounded_surfaces: vec![],
            _bvh: RwLock::new(Bvh3::new()),
            _bvh_invalidated: RwLock::new(true),
            surface_data: Surface3Data::new(None, None),
        };
    }

    /// Constructs with a list of other surfaces.
    /// ```
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::transform3::Transform3;
    /// use vox_geometry_rust::quaternion::QuaternionD;
    /// let sph1 = Sphere3::builder().with_radius(1.0).with_center(Vector3D::default()).make_shared();
    /// let sph2 = Sphere3::builder().with_radius(0.5).with_center(Vector3D::new(0.0, 3.0, 2.0)).make_shared();
    /// let sph3 = Sphere3::builder().with_radius(0.25).with_center(Vector3D::new(-2.0, 0.0, 0.0)).make_shared();
    /// let sset3 = SurfaceSet3::new(vec![sph1, sph2, sph3], Some(Transform3::default()), Some(false));
    ///
    /// assert_eq!(3, sset3.number_of_surfaces());
    /// assert_eq!(Vector3D::default(), sset3.transform.translation());
    /// assert_eq!(QuaternionD::default(), sset3.transform.orientation());
    /// ```
    pub fn new(others: Vec<Surface3Ptr>,
               transform: Option<Transform3>,
               is_normal_flipped: Option<bool>) -> SurfaceSet3 {
        let mut unbounded_surfaces: Vec<Surface3Ptr> = vec![];
        for surface in &others {
            if !surface.read().unwrap().is_bounded() {
                unbounded_surfaces.push(surface.clone());
            }
        }

        return SurfaceSet3 {
            _surfaces: others.clone(),
            _unbounded_surfaces: unbounded_surfaces,
            _bvh: RwLock::new(Bvh3::new()),
            _bvh_invalidated: RwLock::new(true),
            surface_data: Surface3Data::new(transform, is_normal_flipped),
        };
    }

    /// Returns builder fox SurfaceSet3.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    /// Returns the number of surfaces.
    pub fn number_of_surfaces(&self) -> usize {
        return self._surfaces.len();
    }

    /// Returns the i-th surface.
    pub fn surface_at(&self, i: usize) -> Surface3Ptr {
        return self._surfaces[i].clone();
    }

    /// Adds a surface instance.
    /// ```
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// let mut sset1 = SurfaceSet3::default();
    /// assert_eq!(0, sset1.number_of_surfaces());
    ///
    /// let sph1 = Sphere3::builder().with_radius(1.0).with_center(Vector3D::default()).make_shared();
    /// let sph2 = Sphere3::builder().with_radius(0.5).with_center(Vector3D::new(0.0, 3.0, 2.0)).make_shared();
    /// let sph3 = Sphere3::builder().with_radius(0.25).with_center(Vector3D::new(-2.0, 0.0, 0.0)).make_shared();
    ///
    /// sset1.add_surface(sph1);
    /// sset1.add_surface(sph2);
    /// sset1.add_surface(sph3);
    ///
    /// assert_eq!(3, sset1.number_of_surfaces());
    /// ```
    pub fn add_surface(&mut self, surface: Surface3Ptr) {
        self._surfaces.push(surface.clone());
        if !surface.read().unwrap().is_bounded() {
            self._unbounded_surfaces.push(surface.clone());
        }
        self.invalidate_bvh();
    }

    /// must call it manually in Rust!
    pub fn build_bvh(&self) {
        if *self._bvh_invalidated.read().unwrap() {
            let mut surfs: Vec<Surface3Ptr> = Vec::new();
            let mut bounds: Vec<BoundingBox3D> = Vec::new();
            for i in 0..self._surfaces.len() {
                if self._surfaces[i].read().unwrap().is_bounded() {
                    surfs.push(self._surfaces[i].clone());
                    bounds.push(self._surfaces[i].read().unwrap().bounding_box());
                }
            }
            self._bvh.write().unwrap().build(&surfs, &bounds);
            *(self._bvh_invalidated.write().unwrap()) = false;
        }
    }

    fn invalidate_bvh(&self) {
        *(self._bvh_invalidated.write().unwrap()) = true;
    }
}

impl Surface3 for SurfaceSet3 {
    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::Vector3D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    /// // Test empty set
    /// let empty_point = sset1.closest_point(&Vector3D::new(1.0, 2.0, 3.0));
    /// assert_eq!(f64::MAX, empty_point.x);
    /// assert_eq!(f64::MAX, empty_point.y);
    /// assert_eq!(f64::MAX, empty_point.z);
    ///
    /// let num_samples = get_number_of_sample_points3();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    ///
    /// let brute_force_search = |pt:&Vector3D| {
    ///     let mut min_dist3 = f64::MAX;
    ///     let mut result = Vector3D::default();
    ///     for i in 0..num_samples/2 {
    ///         let local_result = sset1.surface_at(i).read().unwrap().closest_point(pt);
    ///         let local_dist3 = pt.distance_squared_to(local_result);
    ///         if local_dist3 < min_dist3 {
    ///             min_dist3 = local_dist3;
    ///             result = local_result;
    ///         }
    ///     }
    ///     return result;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset1.closest_point(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     let expected = brute_force_search(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset3 = SurfaceSet3::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::default())
    ///                        .with_translation(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset3.add_surface(sph);
    /// }
    ///
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset3.closest_point(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     let expected = brute_force_search(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn closest_point_local(&self, other_point: &Vector3D) -> Vector3D {
        self.build_bvh();

        let mut distance_func = |surface: &Surface3Ptr, pt: &Vector3D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let mut result = Vector3D::new(f64::MAX, f64::MAX, f64::MAX);
        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);
        match query_result.item {
            None => {}
            Some(ptr) => {
                result = ptr.read().unwrap().closest_point(other_point);
            }
        }

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
                result = surface.read().unwrap().closest_point(other_point);
            }
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::bounding_box3::BoundingBox3D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    ///
    /// let num_samples = get_number_of_sample_points3();
    ///
    ///     // Use first half of the samples as the centers of the spheres
    /// let mut answer = BoundingBox3D::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph.clone());
    ///
    ///     answer.merge_box(&sph.read().unwrap().bounding_box());
    /// }
    /// assert_eq!(answer.lower_corner.is_similar(&sset1.bounding_box().lower_corner, Some(1e-9)), true);
    /// assert_eq!(answer.upper_corner.is_similar(&sset1.bounding_box().upper_corner, Some(1e-9)), true);
    ///
    /// // Now with translation instead of center
    /// let mut sset3 = SurfaceSet3::default();
    /// let mut debug = BoundingBox3D::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::default())
    ///                        .with_translation(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset3.add_surface(sph.clone());
    ///
    ///     debug.merge_box(&sph.read().unwrap().bounding_box());
    /// }
    /// assert_eq!(answer.lower_corner.is_similar(&debug.lower_corner, Some(1e-9)), true);
    /// assert_eq!(answer.upper_corner.is_similar(&debug.upper_corner, Some(1e-9)), true);
    ///
    /// assert_eq!(answer.lower_corner.is_similar(&sset3.bounding_box().lower_corner, Some(1e-9)), true);
    /// assert_eq!(answer.upper_corner.is_similar(&sset3.bounding_box().upper_corner, Some(1e-9)), true);
    /// ```
    fn bounding_box_local(&self) -> BoundingBox3D {
        self.build_bvh();

        return self._bvh.read().unwrap().bounding_box();
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::ray3::Ray3D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    ///
    /// let num_samples = get_number_of_sample_points3();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    /// let brute_force_test = |ray:&Ray3D| {
    ///     let mut result = SurfaceRayIntersection3::new();
    ///     for i in 0..num_samples/2 {
    ///         let local_result = sset1.surface_at(i).read().unwrap().closest_intersection(ray);
    ///         if local_result.distance < result.distance {
    ///             result = local_result;
    ///         }
    ///     }
    ///     return result;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray3D::new(Vector3D::new_slice(get_sample_points3()[i]), Vector3D::new_slice(get_sample_dirs3()[i]));
    ///     let actual = sset1.closest_intersection(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected.distance, actual.distance);
    ///     assert_eq!(expected.point, actual.point);
    ///     assert_eq!(expected.normal, actual.normal);
    ///     assert_eq!(expected.is_intersecting, actual.is_intersecting);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset3 = SurfaceSet3::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::default())
    ///                        .with_translation(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///         sset3.add_surface(sph);
    /// }
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray3D::new(Vector3D::new_slice(get_sample_points3()[i]), Vector3D::new_slice(get_sample_dirs3()[i]));
    ///     let actual = sset3.closest_intersection(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected.distance, actual.distance);
    ///     assert_eq!(expected.point, actual.point);
    ///     assert_eq!(expected.normal, actual.normal);
    ///     assert_eq!(expected.is_intersecting, actual.is_intersecting);
    /// }
    /// ```
    fn closest_intersection_local(&self, ray: &Ray3D) -> SurfaceRayIntersection3 {
        self.build_bvh();

        let mut test_func = |surface: &Surface3Ptr, ray: &Ray3D| {
            let result = surface.read().unwrap().closest_intersection(ray);
            return result.distance;
        };

        let query_result = self._bvh.read().unwrap().closest_intersection(ray, &mut test_func);
        let mut result = SurfaceRayIntersection3::new();
        result.distance = query_result.distance;
        match query_result.item {
            None => {
                result.is_intersecting = false;
            }
            Some(ptr) => {
                result.is_intersecting = true;
                result.point = ray.point_at(query_result.distance);
                result.normal = ptr.read().unwrap().closest_normal(&result.point);
            }
        }

        for surface in &self._unbounded_surfaces {
            let local_result = surface.read().unwrap().closest_intersection(ray);
            if local_result.distance < result.distance {
                result = local_result;
            }
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    ///
    /// let num_samples = get_number_of_sample_points3();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    ///
    /// let brute_force_search = |pt:&Vector3D| {
    ///     let mut min_dist3 = f64::MAX;
    ///     let mut result = Vector3D::default();
    ///     for i in 0..num_samples/2 {
    ///         let local_result = sset1.surface_at(i).read().unwrap().closest_normal(pt);
    ///         let closest_pt = sset1.surface_at(i).read().unwrap().closest_point(pt);
    ///         let local_dist3 = pt.distance_squared_to(closest_pt);
    ///         if local_dist3 < min_dist3 {
    ///             min_dist3 = local_dist3;
    ///             result = local_result;
    ///         }
    ///     }
    ///     return result;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset1.closest_normal(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     let expected = brute_force_search(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset3 = SurfaceSet3::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::default())
    ///                        .with_translation(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset3.add_surface(sph);
    /// }
    ///
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset3.closest_normal(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     let expected = brute_force_search(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn closest_normal_local(&self, other_point: &Vector3D) -> Vector3D {
        self.build_bvh();

        let mut distance_func = |surface: &Surface3Ptr, pt: &Vector3D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let mut result = Vector3D::new(1.0, 0.0, 0.0);
        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);
        match query_result.item {
            None => {}
            Some(ptr) => {
                result = ptr.read().unwrap().closest_normal(other_point);
            }
        }

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
                result = surface.read().unwrap().closest_normal(other_point);
            }
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::ray3::Ray3D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    ///
    /// let num_samples = get_number_of_sample_points3();
    ///
    ///  // Use first half of the samples as the centers of the spheres
    ///  for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    /// let brute_force_test = |ray:&Ray3D| {
    ///     for i in 0..num_samples/2 {
    ///         if sset1.surface_at(i).read().unwrap().intersects(ray) {
    ///             return true;
    ///         }
    ///     }
    ///     return false;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray3D::new(Vector3D::new_slice(get_sample_points3()[i]), Vector3D::new_slice(get_sample_dirs3()[i]));
    ///     let actual = sset1.intersects(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset3 = SurfaceSet3::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::default())
    ///                        .with_translation(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset3.add_surface(sph);
    /// }
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray3D::new(Vector3D::new_slice(get_sample_points3()[i]), Vector3D::new_slice(get_sample_dirs3()[i]));
    ///     let actual = sset3.intersects(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn intersects_local(&self, ray_local: &Ray3D) -> bool {
        self.build_bvh();

        let mut test_func = |surface: &Surface3Ptr, ray: &Ray3D| {
            return surface.read().unwrap().intersects(ray);
        };

        let mut result = self._bvh.read().unwrap().intersects_ray(ray_local, &mut test_func);
        for surface in &self._unbounded_surfaces {
            result |= surface.read().unwrap().intersects(ray_local);
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    ///
    /// let num_samples = get_number_of_sample_points3();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    /// let brute_force_search = |pt:&Vector3D| {
    ///     let mut min_dist = f64::MAX;
    ///     for i in 0..num_samples/2 {
    ///         let local_dist = sset1.surface_at(i).read().unwrap().closest_distance(pt);
    ///         if local_dist < min_dist {
    ///             min_dist = local_dist;
    ///         }
    ///     }
    ///     return min_dist;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset1.closest_distance(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     let expected = brute_force_search(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset3 = SurfaceSet3::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere3::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector3D::default())
    ///                        .with_translation(Vector3D::new_slice(get_sample_points3()[i]))
    ///                        .make_shared();
    ///         sset3.add_surface(sph);
    /// }
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset3.closest_distance(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     let expected = brute_force_search(&Vector3D::new_slice(get_sample_points3()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn closest_distance_local(&self, other_point: &Vector3D) -> f64 {
        self.build_bvh();

        let mut distance_func = |surface: &Surface3Ptr, pt: &Vector3D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
            }
        }

        return min_dist;
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::plane3::*;
    /// use vox_geometry_rust::bounding_box3::*;
    /// use vox_geometry_rust::transform3::*;
    /// use vox_geometry_rust::quaternion::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    ///
    /// let domain = BoundingBox3D::new(Vector3D::default(), Vector3D::new(1.0, 2.0, 1.0));
    /// let offset = Vector3D::new(1.0, 2.0, 3.0);
    ///
    /// let plane = Plane3::builder()
    ///                      .with_normal(Vector3D::new(0.0, 1.0, 0.0))
    ///                      .with_point(Vector3D::new(0.0, 0.25 * domain.height(), 0.0))
    ///                      .make_shared();
    ///
    /// let sphere = Sphere3::builder()
    ///                       .with_center(domain.mid_point())
    ///                       .with_radius(0.15 * domain.width())
    ///                       .make_shared();
    ///
    /// let surface_set = SurfaceSet3::builder()
    ///                           .with_surfaces(vec![plane, sphere])
    ///                           .with_transform(Transform3::new(offset, QuaternionD::default()))
    ///                           .make_shared();
    ///
    /// assert_eq!(surface_set.read().unwrap().is_inside(&(Vector3D::new(0.5, 0.25, 0.5) + offset)), true);
    /// assert_eq!(surface_set.read().unwrap().is_inside(&(Vector3D::new(0.5, 1.0, 0.5) + offset)), true);
    /// assert_eq!(surface_set.read().unwrap().is_inside(&(Vector3D::new(0.5, 1.5, 0.5) + offset)), false);
    /// ```
    fn is_inside_local(&self, other_point_local: &Vector3D) -> bool {
        for surface in &self._surfaces {
            if surface.read().unwrap().is_inside(other_point_local) {
                return true;
            }
        }

        return false;
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::plane3::*;
    /// use vox_geometry_rust::bounding_box3::*;
    /// use vox_geometry_rust::transform3::*;
    /// use vox_geometry_rust::quaternion::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    ///
    /// let sphere =
    ///         Sphere3::builder().with_center(Vector3D::new(-1.0, 1.0, 2.0)).with_radius(0.5).make_shared();
    ///
    /// let surface_set = SurfaceSet3::builder()
    ///                           .with_surfaces(vec![sphere.clone()])
    ///                           .with_transform(Transform3::new(Vector3D::new(1.0, 2.0, -1.0), QuaternionD::default()))
    ///                           .make_shared();
    ///
    /// let bbox1 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(-0.5, 2.5, 0.5), Vector3D::new(0.5, 3.5, 1.5)).upper_corner, bbox1.upper_corner);
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(-0.5, 2.5, 0.5), Vector3D::new(0.5, 3.5, 1.5)).lower_corner, bbox1.lower_corner);
    ///
    /// surface_set.write().unwrap().transform = Transform3::new(Vector3D::new(3.0, -4.0, 7.0), QuaternionD::default());
    /// surface_set.write().unwrap().update_query_engine();
    /// let bbox3 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(1.5, -3.5, 8.5), Vector3D::new(2.5, -2.5, 9.5)).upper_corner, bbox3.upper_corner);
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(1.5, -3.5, 8.5), Vector3D::new(2.5, -2.5, 9.5)).lower_corner, bbox3.lower_corner);
    ///
    /// sphere.write().unwrap().transform = Transform3::new(Vector3D::new(-6.0, 9.0, 2.0), QuaternionD::default());
    /// surface_set.write().unwrap().update_query_engine();
    /// let bbox3 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(-4.5, 5.5, 10.5), Vector3D::new(-3.5, 6.5, 11.5)).upper_corner, bbox3.upper_corner);
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(-4.5, 5.5, 10.5), Vector3D::new(-3.5, 6.5, 11.5)).lower_corner, bbox3.lower_corner);
    ///
    /// // Plane is unbounded. Total bbox should ignore it.
    /// let plane = Plane3::builder().with_normal(Vector3D::new(1.0, 0.0, 0.0)).make_shared();
    /// surface_set.write().unwrap().add_surface(plane);
    /// surface_set.write().unwrap().update_query_engine();
    /// let bbox4 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(-4.5, 5.5, 10.5), Vector3D::new(-3.5, 6.5, 11.5)).upper_corner, bbox4.upper_corner);
    /// assert_eq!(BoundingBox3D::new(Vector3D::new(-4.5, 5.5, 10.5), Vector3D::new(-3.5, 6.5, 11.5)).lower_corner, bbox4.lower_corner);
    /// ```
    fn update_query_engine(&self) {
        self.invalidate_bvh();
        self.build_bvh();
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::plane3::*;
    /// use vox_geometry_rust::bounding_box3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet3::default();
    ///
    /// let domain = BoundingBox3D::new(Vector3D::default(), Vector3D::new(1.0, 2.0, 1.0));
    ///
    /// let plane = Plane3::builder()
    ///                      .with_normal(Vector3D::new(0.0, 1.0, 0.0))
    ///                      .with_point(Vector3D::new(0.0, 0.25 * domain.height(), 0.0))
    ///                      .make_shared();
    ///
    /// let sphere = Sphere3::builder()
    ///                       .with_center(domain.mid_point())
    ///                       .with_radius(0.15 * domain.width())
    ///                       .make_shared();
    ///
    /// let surface_set = SurfaceSet3::builder().with_surfaces(vec![plane, sphere]).make_shared();
    /// assert_eq!(surface_set.read().unwrap().is_bounded(), false);
    ///
    /// let cp = surface_set.read().unwrap().closest_point(&Vector3D::new(0.5, 0.4, 0.5));
    /// let answer = Vector3D::new(0.5, 0.5, 0.5);
    ///
    /// assert_eq!(answer.is_similar(&cp, Some(1.0e-9)), true);
    /// ```
    fn is_bounded(&self) -> bool {
        // All surfaces should be bounded.
        for surface in &self._surfaces {
            if !surface.read().unwrap().is_bounded() {
                return false;
            }
        }

        // Empty set is not bounded.
        return !self._surfaces.is_empty();
    }

    /// ```
    /// use vox_geometry_rust::surface3::*;
    /// use vox_geometry_rust::surface_set3::SurfaceSet3;
    /// use vox_geometry_rust::sphere3::*;
    /// use vox_geometry_rust::plane3::*;
    /// use vox_geometry_rust::bounding_box3::*;
    /// use vox_geometry_rust::vector3::{Vector3D, Vector3};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let surfaceSet = SurfaceSet3::builder().make_shared();
    ///
    /// assert_eq!(surfaceSet.read().unwrap().is_valid_geometry(), false);
    ///
    /// let domain = BoundingBox3D::new(Vector3D::default(), Vector3D::new(1.0, 2.0, 1.0));
    ///
    /// let plane = Plane3::builder()
    ///                      .with_normal(Vector3D::new(0.0, 1.0, 0.0))
    ///                      .with_point(Vector3D::new(0.0, 0.25 * domain.height(), 0.0))
    ///                      .make_shared();
    ///
    /// let sphere = Sphere3::builder()
    ///                       .with_center(domain.mid_point())
    ///                       .with_radius(0.15 * domain.width())
    ///                       .make_shared();
    ///
    /// let surfaceSet3 =
    ///         SurfaceSet3::builder().with_surfaces(vec![plane, sphere]).make_shared();
    ///
    /// assert_eq!(surfaceSet3.read().unwrap().is_valid_geometry(), true);
    ///
    /// surfaceSet3.write().unwrap().add_surface(surfaceSet);
    ///
    /// assert_eq!(surfaceSet3.read().unwrap().is_valid_geometry(), false);
    /// ```
    fn is_valid_geometry(&self) -> bool {
        // All surfaces should be valid.
        for surface in &self._surfaces {
            if !surface.read().unwrap().is_valid_geometry() {
                return false;
            }
        }

        // Empty set is not valid.
        return !self._surfaces.is_empty();
    }

    fn view(&self) -> &Surface3Data {
        return &self.surface_data;
    }
}

/// Shared pointer for the SurfaceSet3 type.
pub type SurfaceSet3Ptr = Arc<RwLock<SurfaceSet3>>;

///
/// # Front-end to create SurfaceSet3 objects step by step.
///
pub struct Builder {
    _surfaces: Vec<Surface3Ptr>,

    _surface_data: Surface3Data,
}

impl Builder {
    /// Returns builder with other surfaces.
    pub fn with_surfaces(&mut self, others: Vec<Surface3Ptr>) -> &mut Self {
        self._surfaces = others;
        return self;
    }

    /// Builds SurfaceSet3.
    pub fn build(&mut self) -> SurfaceSet3 {
        return SurfaceSet3::new(self._surfaces.clone(),
                                Some(self._surface_data.transform.clone()),
                                Some(self._surface_data.is_normal_flipped));
    }

    /// Builds shared pointer of SurfaceSet3 instance.
    pub fn make_shared(&mut self) -> SurfaceSet3Ptr {
        return SurfaceSet3Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _surfaces: vec![],
            _surface_data: Surface3Data::new(None, None),
        };
    }
}

impl SurfaceBuilderBase3 for Builder {
    fn view(&mut self) -> &mut Surface3Data {
        return &mut self._surface_data;
    }
}