/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::particle_system_data3::ParticleSystemData3Ptr;
use std::sync::{RwLock, Arc};

pub struct ParticleEmitter3Data {
    _is_enabled: bool,
    _particles: Option<ParticleSystemData3Ptr>,
}

impl ParticleEmitter3Data {
    pub fn new() -> ParticleEmitter3Data {
        return ParticleEmitter3Data {
            _is_enabled: true,
            _particles: None,
        };
    }
}

pub trait ParticleEmitter3 {
    /// Updates the emitter state from \p current_time_in_seconds to the following
    /// time-step.
    fn update(&mut self, current_time_in_seconds: f64, time_interval_in_seconds: f64);

    /// Returns the target particle system to emit.
    fn target(&self) -> &Option<ParticleSystemData3Ptr> {
        return &self.view()._particles;
    }

    /// Sets the target particle system to emit.
    fn set_target(&mut self, particles: ParticleSystemData3Ptr) {
        self.view_mut()._particles = Some(particles.clone());

        self.on_set_target(particles.clone());
    }

    /// Returns true if the emitter is enabled.
    fn is_enabled(&self) -> bool {
        return self.view()._is_enabled;
    }

    /// Sets true/false to enable/disable the emitter.
    fn set_is_enabled(&mut self, enabled: bool) {
        self.view_mut()._is_enabled = enabled;
    }

    /// Called when ParticleEmitter3::set_target is executed.
    fn on_set_target(&self, _: ParticleSystemData3Ptr) {
        unimplemented!()
    }

    /// Called when ParticleEmitter3::update is executed.
    fn on_update(&mut self, current_time_in_seconds: f64,
                 time_interval_in_seconds: f64);

    fn view(&self) -> &ParticleEmitter3Data;

    fn view_mut(&mut self) -> &mut ParticleEmitter3Data;
}

/// Shared pointer for the ParticleEmitter3 type.
pub type ParticleEmitter3Ptr = Arc<RwLock<dyn ParticleEmitter3 + Send + Sync>>;