/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use std::ops::{Index, IndexMut, AddAssign, SubAssign, MulAssign, DivAssign, Add, Neg, Sub, Mul, Div};
use std::fmt::{Debug, Formatter, Result};
use crate::point2::Point2;

///
/// # 3-D point class.
///
/// This class defines simple 3-D point data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy)]
pub struct Point3<T: Float> {
    /// X (or the first) component of the point.
    pub x: T,

    /// Y (or the second) component of the point.
    pub y: T,

    /// Z (or the third) component of the point.
    pub z: T,
}

/// Float-type 3D point.
pub type Point3F = Point3<f32>;
/// Double-type 3D point.
pub type Point3D = Point3<f64>;

impl<T: Float> Default for Point3<T> {
    /// Constructs default point (0, 0, 0).
    fn default() -> Self {
        return Point3 {
            x: T::zero(),
            y: T::zero(),
            z: T::zero(),
        };
    }
}

/// # Constructors
impl<T: Float> Point3<T> {
    /// Constructs point with given parameters **x_**, **y_**, and **z_**.
    pub fn new(x_: T, y_: T, z_: T) -> Point3<T> {
        return Point3 {
            x: x_,
            y: y_,
            z: z_,
        };
    }

    /// Constructs point with a 2-D point and a scalar.
    pub fn new_vec(v: &Point2<T>, z_: T) -> Point3<T> {
        return Point3 {
            x: v.x,
            y: v.y,
            z: z_,
        };
    }

    /// Constructs point with initializer list.
    pub fn new_slice(lst: &[T]) -> Point3<T> {
        return Point3 {
            x: lst[0],
            y: lst[1],
            z: lst[2],
        };
    }
}

/// # Basic setters
impl<T: Float> Point3<T> {
    /// Set all x, y, and z components to **s**.
    pub fn set_scalar(&mut self, s: T) {
        self.x = s;
        self.y = s;
        self.z = s;
    }

    /// Set x, y, and z components with given parameters.
    pub fn set(&mut self, x: T, y: T, z: T) {
        self.x = x;
        self.y = y;
        self.z = z;
    }

    /// Set x, y, and z components with given **pt.x**, **pt.y**, and **z**.
    pub fn set_vec(&mut self, pt: &Point2<T>, z: T) {
        self.x = pt.x;
        self.y = pt.y;
        self.z = z;
    }

    /// Set x, y, and z components with given initializer list.
    pub fn set_slice(&mut self, lst: &[T]) {
        self.x = lst[0];
        self.y = lst[1];
        self.z = lst[2];
    }

    /// Set x, y, and z with other point **v**.
    pub fn set_self(&mut self, v: &Point3<T>) {
        self.x = v.x;
        self.y = v.y;
        self.z = v.z;
    }

    /// Set all x, y, and z to zero.
    pub fn set_zero(&mut self) {
        self.x = T::zero();
        self.y = T::zero();
        self.z = T::zero();
    }
}

/// # Binary operations: new instance = self (+) v
impl<T: Float> Point3<T> {
    /// Computes self + (v, v, v).
    pub fn add_scalar(&self, v: T) -> Point3<T> {
        return Point3::new(self.x + v, self.y + v, self.z + v);
    }

    /// Computes self + (v.x, v.y, v.z).
    pub fn add_vec(&self, v: &Point3<T>) -> Point3<T> {
        return Point3::new(self.x + v.x, self.y + v.y, self.z + v.z);
    }

    /// Computes self - (v, v, v).
    pub fn sub_scalar(&self, v: T) -> Point3<T> {
        return Point3::new(self.x - v, self.y - v, self.z - v);
    }

    /// Computes self - (v.x, v.y, v.z).
    pub fn sub_vec(&self, v: &Point3<T>) -> Point3<T> {
        return Point3::new(self.x - v.x, self.y - v.y, self.z - v.z);
    }

    /// Computes self * (v, v, v).
    pub fn mul_scalar(&self, v: T) -> Point3<T> {
        return Point3::new(self.x * v, self.y * v, self.z * v);
    }

    /// Computes self * (v.x, v.y, v.z).
    pub fn mul_vec(&self, v: &Point3<T>) -> Point3<T> {
        return Point3::new(self.x * v.x, self.y * v.y, self.z * v.z);
    }

    /// Computes self / (v, v, v).
    pub fn div_scalar(&self, v: T) -> Point3<T> {
        return Point3::new(self.x / v, self.y / v, self.z / v);
    }

    /// Computes self / (v.x, v.y, v.z).
    pub fn div_vec(&self, v: &Point3<T>) -> Point3<T> {
        return Point3::new(self.x / v.x, self.y / v.y, self.z / v.z);
    }
}

/// # Binary operations: new instance = v (+) self
impl<T: Float> Point3<T> {
    /// Computes (v, v, v) - self.
    pub fn rsub_scalar(&self, v: T) -> Point3<T> {
        return Point3::new(v - self.x, v - self.y, v - self.z);
    }

    /// Computes (v.x, v.y, v.z) - self.
    pub fn rsub_vec(&self, v: &Point3<T>) -> Point3<T> {
        return Point3::new(v.x - self.x, v.y - self.y, v.z - self.z);
    }

    /// Computes (v, v, v) / self.
    pub fn rdiv_scalar(&self, v: T) -> Point3<T> {
        return Point3::new(v / self.x, v / self.y, v / self.z);
    }

    /// Computes (v.x, v.y, v.z) / self.
    pub fn rdiv_vec(&self, v: &Point3<T>) -> Point3<T> {
        return Point3::new(v.x / self.x, v.y / self.y, v.z / self.z);
    }
}

/// # Augmented operators: self (+)= v
impl<T: Float> Point3<T> {
    /// Computes self += (v, v, v).
    pub fn iadd_scalar(&mut self, v: T) {
        self.x = self.x + v;
        self.y = self.y + v;
        self.z = self.z + v;
    }

    /// Computes self += (v.x, v.y, v.z).
    pub fn iadd_vec(&mut self, v: &Point3<T>) {
        self.x = self.x + v.x;
        self.y = self.y + v.y;
        self.z = self.z + v.z;
    }

    /// Computes self -= (v, v, v).
    pub fn isub_scalar(&mut self, v: T) {
        self.x = self.x - v;
        self.y = self.y - v;
        self.z = self.z - v;
    }

    /// Computes self -= (v.x, v.y, v.z).
    pub fn isub_vec(&mut self, v: &Point3<T>) {
        self.x = self.x - v.x;
        self.y = self.y - v.y;
        self.z = self.z - v.z;
    }

    /// Computes self *= (v, v, v).
    pub fn imul_scalar(&mut self, v: T) {
        self.x = self.x * v;
        self.y = self.y * v;
        self.z = self.z * v;
    }

    /// Computes self *= (v.x, v.y, v.z).
    pub fn imul_vec(&mut self, v: &Point3<T>) {
        self.x = self.x * v.x;
        self.y = self.y * v.y;
        self.z = self.z * v.z;
    }

    /// Computes self /= (v, v, v).
    pub fn idiv_scalar(&mut self, v: T) {
        self.x = self.x / v;
        self.y = self.y / v;
        self.z = self.z / v;
    }

    /// Computes self /= (v.x, v.y, v.z).
    pub fn idiv_vec(&mut self, v: &Point3<T>) {
        self.x = self.x / v.x;
        self.y = self.y / v.y;
        self.z = self.z / v.z;
    }
}

/// # Basic getters
impl<T: Float> Point3<T> {
    /// Returns const reference to the **i** -th element of the point.
    pub fn at(&self, i: usize) -> &T {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            2 => return &self.z,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the point.
    pub fn at_mut(&mut self, i: usize) -> &mut T {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            2 => return &mut self.z,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y + z).
    pub fn sum(&self) -> T {
        return self.x + self.y + self.z;
    }

    /// Returns the minimum value among x, y, and z.
    pub fn min(&self) -> T {
        return T::min(T::min(self.x, self.y), self.z);
    }

    /// Returns the maximum value among x, y, and z.
    pub fn max(&self) -> T {
        return T::max(T::max(self.x, self.y), self.z);
    }

    /// Returns the absolute minimum value among x, y, and z.
    pub fn absmin(&self) -> T {
        return crate::math_utils::absmin(crate::math_utils::absmin(self.x, self.y), self.z);
    }

    /// Returns the absolute maximum value among x, y, and z.
    pub fn absmax(&self) -> T {
        return crate::math_utils::absmax(crate::math_utils::absmax(self.x, self.y), self.z);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        return match T::abs(self.x) > T::abs(self.y) {
            true => match T::abs(self.x) > T::abs(self.z) {
                true => 0,
                false => 2
            }
            false => match T::abs(self.y) > T::abs(self.z) {
                true => 1,
                false => 2
            }
        };
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        return match T::abs(self.x) < T::abs(self.y) {
            true => match T::abs(self.x) < T::abs(self.z) {
                true => 0,
                false => 2
            }
            false => match T::abs(self.y) < T::abs(self.z) {
                true => 1,
                false => 2
            }
        };
    }

    /// Returns true if **other** is the same as self point.
    pub fn is_equal(&self, other: &Point3<T>) -> bool {
        return self.x == other.x && self.y == other.y && self.z == other.z;
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the point.
impl<T: Float> Index<usize> for Point3<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the point.
impl<T: Float> IndexMut<usize> for Point3<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v, v)
impl<T: Float> AddAssign<T> for Point3<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y, v.z)
impl<T: Float> AddAssign for Point3<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v, v)
impl<T: Float> SubAssign<T> for Point3<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y, v.z)
impl<T: Float> SubAssign for Point3<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v, v)
impl<T: Float> MulAssign<T> for Point3<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y, v.z)
impl<T: Float> MulAssign for Point3<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v, v)
impl<T: Float> DivAssign<T> for Point3<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y, v.z)
impl<T: Float> DivAssign for Point3<T> {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self point.
impl<T: Float> PartialEq for Point3<T> {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl<T: Float> Eq for Point3<T> {}

impl<T: Float> Neg for Point3<T> {
    type Output = Point3<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Point3::new(-self.x, -self.y, -self.z);
    }
}

/// Computes (a, a, a) + (b.x, b.y, b.z).
impl<T: Float> Add<T> for Point3<T> {
    type Output = Point3<T>;
    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) + (b.x, b.y, b.z).
impl<T: Float> Add for Point3<T> {
    type Output = Point3<T>;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) - (b, b, b).
impl<T: Float> Sub<T> for Point3<T> {
    type Output = Point3<T>;
    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) - (b.x, b.y, b.z).
impl<T: Float> Sub for Point3<T> {
    type Output = Point3<T>;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) * (b, b, b).
impl<T: Float> Mul<T> for Point3<T> {
    type Output = Point3<T>;
    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) * (b.x, b.y, b.z).
impl<T: Float> Mul for Point3<T> {
    type Output = Point3<T>;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y, a.z) / (b, b, b).
impl<T: Float> Div<T> for Point3<T> {
    type Output = Point3<T>;
    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y, a.z) / (b.x, b.y, b.z).
impl<T: Float> Div for Point3<T> {
    type Output = Point3<T>;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl<T: Float + Debug> Debug for Point3<T> {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::point3::Point3F;
    /// let vec = Point3F::new(10.0, 20.0, 30.0);
    /// assert_eq!(format!("{:?}", vec), "(10.0, 20.0, 30.0)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10.0,
    ///     20.0,
    ///     30.0,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .field(&self.z)
            .finish()
    }
}

/// Returns element-wise min point.
pub fn min<T: Float>(a: &Point3<T>, b: &Point3<T>) -> Point3<T> {
    return Point3::new(T::min(a.x, b.x),
                       T::min(a.y, b.y),
                       T::min(a.z, b.z));
}

/// Returns element-wise max point.
pub fn max<T: Float>(a: &Point3<T>, b: &Point3<T>) -> Point3<T> {
    return Point3::new(T::max(a.x, b.x),
                       T::max(a.y, b.y),
                       T::max(a.z, b.z));
}

/// Returns element-wise clamped point.
pub fn clamp<T: Float>(v: &Point3<T>, low: &Point3<T>, high: &Point3<T>) -> Point3<T> {
    return Point3::new(crate::math_utils::clamp(v.x, low.x, high.x),
                       crate::math_utils::clamp(v.y, low.y, high.y),
                       crate::math_utils::clamp(v.z, low.z, high.z));
}

/// Returns element-wise ceiled point.
pub fn ceil<T: Float>(a: &Point3<T>) -> Point3<T> {
    return Point3::new((a.x).ceil(), (a.y).ceil(), (a.z).ceil());
}

/// Returns element-wise floored point.
pub fn floor<T: Float>(a: &Point3<T>) -> Point3<T> {
    return Point3::new((a.x).floor(), (a.y).floor(), (a.z).floor());
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod point3 {
    use crate::point3::*;
    use crate::point2::Point2F;

    #[test]
    fn constructors() {
        let pt = Point3F::default();
        assert_eq!(0.0, pt.x);
        assert_eq!(0.0, pt.y);
        assert_eq!(0.0, pt.z);

        let pt2 = Point3F::new(5.0, 3.0, 8.0);
        assert_eq!(5.0, pt2.x);
        assert_eq!(3.0, pt2.y);
        assert_eq!(8.0, pt2.z);

        let pt3 = Point2F::new(4.0, 7.0);
        let pt4 = Point3F::new_vec(&pt3, 9.0);
        assert_eq!(4.0, pt4.x);
        assert_eq!(7.0, pt4.y);
        assert_eq!(9.0, pt4.z);

        let pt5 = Point3F::new_slice(&[7.0, 6.0, 1.0]);
        assert_eq!(7.0, pt5.x);
        assert_eq!(6.0, pt5.y);
        assert_eq!(1.0, pt5.z);

        let pt6 = pt5.clone();
        assert_eq!(7.0, pt6.x);
        assert_eq!(6.0, pt6.y);
        assert_eq!(1.0, pt6.z);
    }

    #[test]
    fn set_methods() {
        let mut pt = Point3F::default();
        pt.set(4.0, 2.0, 8.0);
        assert_eq!(4.0, pt.x);
        assert_eq!(2.0, pt.y);
        assert_eq!(8.0, pt.z);

        pt.set_vec(&Point2F::new(1.0, 3.0), 10.0);
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(10.0, pt.z);

        let lst = [0.0, 5.0, 6.0];
        pt.set_slice(&lst);
        assert_eq!(0.0, pt.x);
        assert_eq!(5.0, pt.y);
        assert_eq!(6.0, pt.z);

        pt.set_self(&Point3F::new(9.0, 8.0, 2.0));
        assert_eq!(9.0, pt.x);
        assert_eq!(8.0, pt.y);
        assert_eq!(2.0, pt.z);
    }

    #[test]
    fn basic_setter_methods() {
        let mut pt = Point3F::new(3.0, 9.0, 4.0);
        pt.set_zero();
        assert_eq!(0.0, pt.x);
        assert_eq!(0.0, pt.y);
        assert_eq!(0.0, pt.z);
    }

    #[test]
    fn binary_operator_methods() {
        let mut pt = Point3F::new(3.0, 9.0, 4.0);
        pt = pt.add_scalar(4.0);
        assert_eq!(7.0, pt.x);
        assert_eq!(13.0, pt.y);
        assert_eq!(8.0, pt.z);

        pt = pt.add_vec(&Point3F::new(-2.0, 1.0, 5.0));
        assert_eq!(5.0, pt.x);
        assert_eq!(14.0, pt.y);
        assert_eq!(13.0, pt.z);

        pt = pt.sub_scalar(8.0);
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(5.0, pt.z);

        pt = pt.sub_vec(&Point3F::new(-5.0, 3.0, 12.0));
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(-7.0, pt.z);

        pt = pt.mul_scalar(2.0);
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(-14.0, pt.z);

        pt = pt.mul_vec(&Point3F::new(3.0, -2.0, 0.5));
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);
        assert_eq!(-7.0, pt.z);

        pt = pt.div_scalar(4.0);
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);
        assert_eq!(-1.75, pt.z);

        pt = pt.div_vec(&Point3F::new(3.0, -1.0, 0.25));
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(-7.0, pt.z);
    }

    #[test]
    fn binary_inverse_operator_methods() {
        let mut pt = Point3F::new(5.0, 14.0, 13.0);
        pt = pt.rsub_scalar(8.0);
        assert_eq!(3.0, pt.x);
        assert_eq!(-6.0, pt.y);
        assert_eq!(-5.0, pt.z);

        pt = pt.rsub_vec(&Point3F::new(-5.0, 3.0, -1.0));
        assert_eq!(-8.0, pt.x);
        assert_eq!(9.0, pt.y);
        assert_eq!(4.0, pt.z);

        pt = Point3F::new(-12.0, -9.0, 8.0);
        pt = pt.rdiv_scalar(36.0);
        assert_eq!(-3.0, pt.x);
        assert_eq!(-4.0, pt.y);
        assert_eq!(4.5, pt.z);

        pt = pt.rdiv_vec(&Point3F::new(3.0, -16.0, 18.0));
        assert_eq!(-1.0, pt.x);
        assert_eq!(4.0, pt.y);
        assert_eq!(4.0, pt.z);
    }

    #[test]
    fn augmented_operator_methods() {
        let mut pt = Point3F::new(3.0, 9.0, 4.0);
        pt.iadd_scalar(4.0);
        assert_eq!(7.0, pt.x);
        assert_eq!(13.0, pt.y);
        assert_eq!(8.0, pt.z);

        pt.iadd_vec(&Point3F::new(-2.0, 1.0, 5.0));
        assert_eq!(5.0, pt.x);
        assert_eq!(14.0, pt.y);
        assert_eq!(13.0, pt.z);

        pt.isub_scalar(8.0);
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(5.0, pt.z);

        pt.isub_vec(&Point3F::new(-5.0, 3.0, 12.0));
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(-7.0, pt.z);

        pt.imul_scalar(2.0);
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(-14.0, pt.z);

        pt.imul_vec(&Point3F::new(3.0, -2.0, 0.5));
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);
        assert_eq!(-7.0, pt.z);

        pt.idiv_scalar(4.0);
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);
        assert_eq!(-1.75, pt.z);

        pt.idiv_vec(&Point3F::new(3.0, -1.0, 0.25));
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(-7.0, pt.z);
    }

    #[test]
    fn at_methods() {
        let mut pt = Point3F::new(8.0, 9.0, 1.0);
        assert_eq!(8.0, *pt.at(0));
        assert_eq!(9.0, *pt.at(1));
        assert_eq!(1.0, *pt.at(2));

        *pt.at_mut(0) = 7.0;
        *pt.at_mut(1) = 6.0;
        *pt.at_mut(2) = 5.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(5.0, pt.z);
    }

    #[test]
    fn basic_getter_methods() {
        let pt = Point3F::new(3.0, 7.0, -1.0);
        let pt2 = Point3F::new(-3.0, -7.0, 1.0);

        let sum = pt.sum();
        assert_eq!(9.0, sum);

        let min = pt.min();
        assert_eq!(-1.0, min);

        let max = pt.max();
        assert_eq!(7.0, max);

        let absmin = pt2.absmin();
        assert_eq!(1.0, absmin);

        let absmax = pt2.absmax();
        assert_eq!(-7.0, absmax);

        let daxis = pt.dominant_axis();
        assert_eq!(1, daxis);

        let saxis = pt.subminant_axis();
        assert_eq!(2, saxis);
    }

    #[test]
    fn bracket_operators() {
        let mut pt = Point3F::new(8.0, 9.0, 1.0);
        assert_eq!(8.0, pt[0]);
        assert_eq!(9.0, pt[1]);
        assert_eq!(1.0, pt[2]);

        pt[0] = 7.0;
        pt[1] = 6.0;
        pt[2] = 5.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(5.0, pt.z);
    }

    #[test]
    fn assignment_operators() {
        let pt = Point3F::new(5.0, 1.0, 0.0);
        let pt2 = pt;
        assert_eq!(5.0, pt2.x);
        assert_eq!(1.0, pt2.y);
        assert_eq!(0.0, pt2.z);
    }

    #[test]
    fn augmented_operators() {
        let mut pt = Point3F::new(3.0, 9.0, -2.0);
        pt += 4.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(13.0, pt.y);
        assert_eq!(2.0, pt.z);

        pt += Point3F::new(-2.0, 1.0, 5.0);
        assert_eq!(5.0, pt.x);
        assert_eq!(14.0, pt.y);
        assert_eq!(7.0, pt.z);

        pt -= 8.0;
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(-1.0, pt.z);

        pt -= Point3F::new(-5.0, 3.0, -6.0);
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(5.0, pt.z);

        pt *= 2.0;
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(10.0, pt.z);

        pt *= Point3F::new(3.0, -2.0, 0.4);
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);
        assert_eq!(4.0, pt.z);

        pt /= 4.0;
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);
        assert_eq!(1.0, pt.z);

        pt /= Point3F::new(3.0, -1.0, 2.0);
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(0.5, pt.z);
    }

    #[test]
    fn equal_operators() {
        let pt2 = Point3F::new(3.0, 7.0, 4.0);
        let pt3 = Point3F::new(3.0, 5.0, 4.0);
        let pt4 = Point3F::new(5.0, 1.0, 2.0);
        let pt = pt2;
        assert_eq!(pt == pt2, true);
        assert_eq!(pt == pt3, false);
        assert_eq!(pt != pt2, false);
        assert_eq!(pt != pt3, true);
        assert_eq!(pt != pt4, true);
    }

    #[test]
    fn min_max_functions() {
        let pt = Point3F::new(5.0, 1.0, 0.0);
        let pt2 = Point3F::new(3.0, 3.0, 3.0);
        let min_point = min(&pt, &pt2);
        let max_point = max(&pt, &pt2);
        assert_eq!(min_point == Point3F::new(3.0, 1.0, 0.0), true);
        assert_eq!(max_point == Point3F::new(5.0, 3.0, 3.0), true);
    }

    #[test]
    fn clamp_function() {
        let pt = Point3F::new(2.0, 4.0, 1.0);
        let low = Point3F::new(3.0, -1.0, 0.0);
        let high = Point3F::new(5.0, 2.0, 3.0);
        let clamped_vec = clamp(&pt, &low, &high);
        assert_eq!(clamped_vec == Point3F::new(3.0, 2.0, 1.0), true);
    }

    #[test]
    fn ceil_floor_functions() {
        let pt = Point3F::new(2.2, 4.7, -0.2);
        let ceil_vec = ceil(&pt);
        assert_eq!(ceil_vec == Point3F::new(3.0, 5.0, 0.0), true);

        let floor_vec = floor(&pt);
        assert_eq!(floor_vec == Point3F::new(2.0, 4.0, -1.0), true);
    }

    #[test]
    fn binary_operators() {
        let mut pt = Point3F::new(3.0, 9.0, 4.0);
        pt = pt + 4.0;
        assert_eq!(7.0, pt.x);
        assert_eq!(13.0, pt.y);
        assert_eq!(8.0, pt.z);

        pt = pt + Point3F::new(-2.0, 1.0, 5.0);
        assert_eq!(5.0, pt.x);
        assert_eq!(14.0, pt.y);
        assert_eq!(13.0, pt.z);

        pt = pt - 8.0;
        assert_eq!(-3.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(5.0, pt.z);

        pt = pt - Point3F::new(-5.0, 3.0, 12.0);
        assert_eq!(2.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(-7.0, pt.z);

        pt = pt * 2.0;
        assert_eq!(4.0, pt.x);
        assert_eq!(6.0, pt.y);
        assert_eq!(-14.0, pt.z);

        pt = pt * Point3F::new(3.0, -2.0, 0.5);
        assert_eq!(12.0, pt.x);
        assert_eq!(-12.0, pt.y);
        assert_eq!(-7.0, pt.z);

        pt = pt / 4.0;
        assert_eq!(3.0, pt.x);
        assert_eq!(-3.0, pt.y);
        assert_eq!(-1.75, pt.z);

        pt = pt / Point3F::new(3.0, -1.0, 0.25);
        assert_eq!(1.0, pt.x);
        assert_eq!(3.0, pt.y);
        assert_eq!(-7.0, pt.z);
    }
}