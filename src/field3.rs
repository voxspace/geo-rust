/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::sync::{RwLock, Arc};

/// Abstract base class for 3-D fields.
pub trait Field3 {}

pub type Field3Ptr = Arc<RwLock<dyn Field3>>;