/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::field2::Field2;
use crate::vector_field2::VectorField2;
use std::sync::{RwLock, Arc};

/// # 2-D constant vector field.
pub struct ConstantVectorField2 {
    _value: Vector2D,
}

impl ConstantVectorField2 {
    /// Constructs a constant vector field with given \p value.
    pub fn new(value: Option<Vector2D>) -> ConstantVectorField2 {
        return ConstantVectorField2 {
            _value: value.unwrap_or(Vector2D::default())
        };
    }

    /// Returns builder fox ConstantVectorField2.
    pub fn builder() -> Builder {
        return Builder::new();
    }
}

impl Field2 for ConstantVectorField2 {}

impl VectorField2 for ConstantVectorField2 {
    fn sample(&self, _: &Vector2D) -> Vector2D {
        return self._value;
    }
}

/// Shared pointer for the ConstantVectorField2 type.
pub type ConstantVectorField2Ptr = Arc<RwLock<ConstantVectorField2>>;

///
/// # Front-end to create ConstantVectorField2 objects step by step.
///
pub struct Builder {
    _value: Vector2D,
}

impl Builder {
    /// Returns builder with value.
    pub fn with_value(&mut self, value: Vector2D) -> &mut Self {
        self._value = value;
        return self;
    }

    /// Builds ConstantVectorField2.
    pub fn build(&mut self) -> ConstantVectorField2 {
        return ConstantVectorField2::new(Some(self._value));
    }

    /// Builds shared pointer of ConstantVectorField2 instance.
    pub fn make_shared(&mut self) -> ConstantVectorField2Ptr {
        return ConstantVectorField2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _value: Vector2D::default()
        };
    }
}