/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use crate::array3::Array3;
use crate::usize3::USize3;
use crate::matrix_csr::MatrixCsr;
use crate::vector_n::VectorN;
use crate::blas::Blas;

/// The row of FdmMatrix3 where row corresponds to (i, j) grid point.
#[derive(Clone)]
pub struct FdmMatrixRow3 {
    /// Diagonal component of the matrix (row, row).
    pub center: f64,

    /// Off-diagonal element where column refers to (i+1, j, k) grid point.
    pub right: f64,

    /// Off-diagonal element where column refers to (i, j+1, k) grid point.
    pub up: f64,

    /// OFf-diagonal element where column refers to (i, j, k+1) grid point.
    pub front: f64,
}

impl ZeroInit for FdmMatrixRow3 {
    fn zero_init() -> Self {
        return FdmMatrixRow3 {
            center: 0.0,
            right: 0.0,
            up: 0.0,
            front: 0.0,
        };
    }
}

/// Vector type for 3-D finite differencing.
pub type FdmVector3 = Array3<f64>;

/// Matrix type for 3-D finite differencing.
pub type FdmMatrix3 = Array3<FdmMatrixRow3>;

//--------------------------------------------------------------------------------------------------
/// Linear system (Ax=b) for 3-D finite differencing.
#[derive(Default)]
pub struct FdmLinearSystem3 {
    /// System matrix.
    pub a: FdmMatrix3,

    /// Solution vector.
    pub x: FdmVector3,

    /// RHS vector.
    pub b: FdmVector3,
}

impl FdmLinearSystem3 {
    /// Clears all the data.
    pub fn clear(&mut self) {
        self.a.clear();
        self.x.clear();
        self.b.clear();
    }

    /// Resizes the arrays with given grid size.
    pub fn resize(&mut self, size: &USize3) {
        self.a.resize_with_packed_size(size, None);
        self.x.resize_with_packed_size(size, None);
        self.b.resize_with_packed_size(size, None);
    }
}

//--------------------------------------------------------------------------------------------------
/// BLAS operator wrapper for 3-D finite differencing.
#[derive(Clone)]
pub struct FdmBlas3 {}

impl Blas for FdmBlas3 {
    type VectorType = FdmVector3;
    type MatrixType = FdmMatrix3;

    fn set_sv(s: f64, result: &mut FdmVector3) {
        result.set_scalar(s);
    }

    fn set_vv(v: &FdmVector3, result: &mut FdmVector3) {
        result.set_self(v);
    }

    fn set_sm(s: f64, result: &mut FdmMatrix3) {
        let mut row = FdmMatrixRow3::zero_init();
        row.center = s;
        row.right = s;
        row.up = s;
        row.front = s;
        result.set_scalar(row);
    }

    fn set_mm(m: &FdmMatrix3, result: &mut FdmMatrix3) {
        result.set_self(m);
    }

    fn dot(a: &FdmVector3, b: &FdmVector3) -> f64 {
        let size = a.size();

        debug_assert!(size == b.size());

        let mut result = 0.0;

        for k in 0..size.z {
            for j in 0..size.y {
                for i in 0..size.x {
                    result += a[(i, j, k)] * b[(i, j, k)];
                }
            }
        }

        return result;
    }

    fn axpy(a: f64, x: &FdmVector3, y: &FdmVector3, result: &mut FdmVector3) {
        let size = x.size();

        debug_assert!(size == y.size());
        debug_assert!(size == result.size());

        x.for_each_index(|i, j, k| {
            result[(i, j, k)] = a * x[(i, j, k)] + y[(i, j, k)];
        });
    }

    fn mvm(m: &FdmMatrix3, v: &FdmVector3, result: &mut FdmVector3) {
        let size = m.size();

        debug_assert!(size == v.size());
        debug_assert!(size == result.size());

        m.for_each_index(|i, j, k| {
            result[(i, j, k)] =
                m[(i, j, k)].center * v[(i, j, k)] +
                    (if i > 0 { m[(i - 1, j, k)].right * v[(i - 1, j, k)] } else { 0.0 }) +
                    (if i + 1 < size.x { m[(i, j, k)].right * v[(i + 1, j, k)] } else { 0.0 }) +
                    (if j > 0 { m[(i, j - 1, k)].up * v[(i, j - 1, k)] } else { 0.0 }) +
                    (if j + 1 < size.y { m[(i, j, k)].up * v[(i, j + 1, k)] } else { 0.0 }) +
                    (if k > 0 { m[(i, j, k - 1)].front * v[(i, j, k - 1)] } else { 0.0 }) +
                    (if k + 1 < size.z { m[(i, j, k)].front * v[(i, j, k + 1)] } else { 0.0 });
        });
    }

    fn residual(a: &FdmMatrix3, x: &FdmVector3, b: &FdmVector3, result: &mut FdmVector3) {
        let size = a.size();

        debug_assert!(size == x.size());
        debug_assert!(size == b.size());
        debug_assert!(size == result.size());

        a.for_each_index(|i, j, k| {
            result[(i, j, k)] =
                b[(i, j, k)] - a[(i, j, k)].center * x[(i, j, k)] -
                    (if i > 0 { a[(i - 1, j, k)].right * x[(i - 1, j, k)] } else { 0.0 }) -
                    (if i + 1 < size.x { a[(i, j, k)].right * x[(i + 1, j, k)] } else { 0.0 }) -
                    (if j > 0 { a[(i, j - 1, k)].up * x[(i, j - 1, k)] } else { 0.0 }) -
                    (if j + 1 < size.y { a[(i, j, k)].up * x[(i, j + 1, k)] } else { 0.0 }) -
                    (if k > 0 { a[(i, j, k - 1)].front * x[(i, j, k - 1)] } else { 0.0 }) -
                    (if k + 1 < size.z { a[(i, j, k)].front * x[(i, j, k + 1)] } else { 0.0 });
        });
    }

    fn l2norm(v: &FdmVector3) -> f64 {
        return f64::sqrt(FdmBlas3::dot(v, v));
    }

    fn l_inf_norm(v: &FdmVector3) -> f64 {
        let size = v.size();

        let mut result = 0.0;

        for k in 0..size.z {
            for j in 0..size.y {
                for i in 0..size.x {
                    result = crate::math_utils::absmax(result, v[(i, j, k)]);
                }
            }
        }

        return f64::abs(result);
    }
}

//--------------------------------------------------------------------------------------------------
/// Compressed linear system (Ax=b) for 3-D finite differencing.
#[derive(Default)]
pub struct FdmCompressedLinearSystem3 {
    /// System matrix.
    pub a: MatrixCsr,

    /// Solution vector.
    pub x: VectorN,

    /// RHS vector.
    pub b: VectorN,
}

impl FdmCompressedLinearSystem3 {
    /// Clears all the data.
    pub fn clear(&mut self) {
        self.a.clear();
        self.x.clear();
        self.b.clear();
    }
}

/// BLAS operator wrapper for compressed 3-D finite differencing.
#[derive(Clone)]
pub struct FdmCompressedBlas3 {}

impl Blas for FdmCompressedBlas3 {
    type VectorType = VectorN;
    type MatrixType = MatrixCsr;

    fn set_sv(s: f64, result: &mut VectorN) {
        result.set_scalar(s);
    }

    fn set_vv(v: &VectorN, result: &mut VectorN) {
        result.set_expression(v);
    }

    fn set_sm(s: f64, result: &mut MatrixCsr) {
        result.set_scalar(s);
    }

    fn set_mm(m: &MatrixCsr, result: &mut MatrixCsr) {
        result.set_self(m);
    }

    fn dot(a: &VectorN, b: &VectorN) -> f64 {
        return a.dot(b);
    }

    fn axpy(a: f64, x: &VectorN, y: &VectorN, result: &mut VectorN) {
        *result = VectorN::new_expression(&VectorN::new_expression(&x.mul_scalar(a)).add_expression(y));
    }

    fn mvm(m: &MatrixCsr, v: &VectorN, result: &mut VectorN) {
        let rp = m.row_pointers_data();
        let ci = m.column_indices_data();
        let nnz = m.non_zero_data();

        v.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut sum = 0.0;

            for jj in row_begin..row_end {
                let j = ci[jj];
                sum += nnz[jj] * v[j];
            }

            result[i] = sum;
        });
    }

    fn residual(a: &MatrixCsr, x: &VectorN, b: &VectorN, result: &mut VectorN) {
        let rp = a.row_pointers_data();
        let ci = a.column_indices_data();
        let nnz = a.non_zero_data();

        x.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut sum = 0.0;

            for jj in row_begin..row_end {
                let j = ci[jj];
                sum += nnz[jj] * x[j];
            }

            result[i] = b[i] - sum;
        });
    }

    fn l2norm(v: &VectorN) -> f64 {
        return f64::sqrt(v.dot(v));
    }

    fn l_inf_norm(v: &VectorN) -> f64 {
        return f64::abs(v.absmax());
    }
}