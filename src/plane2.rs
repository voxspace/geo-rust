/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::surface2::*;
use crate::vector2::Vector2D;
use crate::transform2::Transform2;
use crate::bounding_box2::BoundingBox2D;
use crate::ray2::Ray2D;
use std::sync::{RwLock, Arc};

///
/// # 2-D plane geometry.
///
/// This class represents 2-D plane geometry which extends Surface2 by
/// overriding surface-related queries.
///
pub struct Plane2 {
    /// Plane normal.
    pub normal: Vector2D,

    /// Point that lies on the plane.
    pub point: Vector2D,

    /// data from surface2
    pub surface_data: Surface2Data,
}

impl Plane2 {
    /// Constructs a plane that crosses (0, 0) with surface normal (0, 1).
    pub fn new_default(transform: Option<Transform2>,
                       is_normal_flipped: Option<bool>) -> Plane2 {
        return Plane2 {
            normal: Vector2D::new(0.0, 1.0),
            point: Vector2D::default(),
            surface_data: Surface2Data::new(transform, is_normal_flipped),
        };
    }

    /// Constructs a plane that cross \p point with surface normal \p normal.
    pub fn new(normal: Vector2D,
               point: Vector2D,
               transform: Option<Transform2>,
               is_normal_flipped: Option<bool>) -> Plane2 {
        return Plane2 {
            normal,
            point,
            surface_data: Surface2Data::new(transform, is_normal_flipped),
        };
    }

    /// Returns builder fox Plane2.
    pub fn builder() -> Builder {
        return Builder::new();
    }
}

impl Surface2 for Plane2 {
    fn closest_point_local(&self, other_point: &Vector2D) -> Vector2D {
        let r = *other_point - self.point;
        return r - self.normal * self.normal.dot(&r) + self.point;
    }

    fn bounding_box_local(&self) -> BoundingBox2D {
        return if f64::abs(self.normal.dot(&Vector2D::new(1.0, 0.0)) - 1.0) < f64::EPSILON {
            BoundingBox2D::new(self.point - Vector2D::new(0.0, f64::MAX),
                               self.point + Vector2D::new(0.0, f64::MAX))
        } else if f64::abs(self.normal.dot(&Vector2D::new(0.0, 1.0)) - 1.0) < f64::EPSILON {
            BoundingBox2D::new(self.point - Vector2D::new(f64::MAX, 0.0),
                               self.point + Vector2D::new(f64::MAX, 0.0))
        } else {
            BoundingBox2D::new(Vector2D::new(f64::MAX, f64::MAX),
                               Vector2D::new(f64::MAX, f64::MAX))
        };
    }

    fn closest_intersection_local(&self, ray: &Ray2D) -> SurfaceRayIntersection2 {
        let mut intersection = SurfaceRayIntersection2::new();
        let d_dot_n = ray.direction.dot(&self.normal);

        // Check if not parallel
        if f64::abs(d_dot_n) > 0.0 {
            let t = self.normal.dot(&(self.point - ray.origin)) / d_dot_n;
            if t >= 0.0 {
                intersection.is_intersecting = true;
                intersection.distance = t;
                intersection.point = ray.point_at(t);
                intersection.normal = self.normal;
            }
        }

        return intersection;
    }

    fn closest_normal_local(&self, _other_point: &Vector2D) -> Vector2D {
        return self.normal;
    }

    fn intersects_local(&self, ray_local: &Ray2D) -> bool {
        return f64::abs(ray_local.direction.dot(&self.normal)) > 0.0;
    }

    fn view(&self) -> &Surface2Data {
        return &self.surface_data;
    }
}

/// Shared pointer for the Plane2 type.
pub type Plane2Ptr = Arc<RwLock<Plane2>>;

///
/// # Front-end to create Plane2 objects step by step.
///
pub struct Builder {
    _normal: Vector2D,
    _point: Vector2D,

    _surface_data: Surface2Data,
}

impl Builder {
    /// Returns builder with plane normal.
    pub fn with_normal(&mut self, normal: Vector2D) -> &mut Self {
        self._normal = normal;
        return self;
    }

    /// Returns builder with point on the plane.
    pub fn with_point(&mut self, point: Vector2D) -> &mut Self {
        self._point = point;
        return self;
    }

    /// Builds Plane2.
    pub fn build(&mut self) -> Plane2 {
        return Plane2::new(self._normal,
                           self._point,
                           Some(self._surface_data.transform.clone()),
                           Some(self._surface_data.is_normal_flipped),
        );
    }

    /// Builds shared pointer of Plane2 instance.
    pub fn make_shared(&mut self) -> Plane2Ptr {
        return Plane2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _normal: Vector2D::new(0.0, 1.0),
            _point: Vector2D::default(),
            _surface_data: Surface2Data::new(None, None),
        };
    }
}

impl SurfaceBuilderBase2 for Builder {
    fn view(&mut self) -> &mut Surface2Data {
        return &mut self._surface_data;
    }
}