/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector3::Vector3D;
use crate::point_neighbor_searcher3::*;
use std::sync::{RwLock, Arc};

///
/// # Simple ad-hoc 3-D point searcher.
///
/// This class implements 3-D point searcher simply by looking up every point in
/// the list. Thus, this class is not ideal for searches involving large number
/// of points, but only for small set of items.
///
pub struct PointSimpleListSearcher3 {
    _points: Vec<Vector3D>,
}

impl PointSimpleListSearcher3 {
    /// Default constructor.
    pub fn new() -> PointSimpleListSearcher3 {
        return PointSimpleListSearcher3 {
            _points: vec![]
        };
    }

    /// Returns builder fox PointSimpleListSearcher3.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    ///
    /// \brief      Creates a new instance of the object with same properties
    ///             than original.
    ///
    /// \return     Copy of this object.
    ///
    pub fn clone(&self) -> PointSimpleListSearcher3Ptr {
        let mut searcher = PointSimpleListSearcher3::new();
        searcher.set(self);
        return PointSimpleListSearcher3Ptr::new(RwLock::new(searcher));
    }

    /// Copy from the other instance.
    pub fn set(&mut self, other: &PointSimpleListSearcher3) {
        self._points = other._points.clone();
    }
}

impl PointNeighborSearcher3 for PointSimpleListSearcher3 {
    fn type_name() -> String {
        return "PointSimpleListSearcher3".parse().unwrap();
    }

    fn build(&mut self, points: &Vec<Vector3D>) {
        self._points = points.clone();
    }

    fn for_each_nearby_point<Callback>(&self, origin: &Vector3D, radius: f64, callback: &mut Callback)
        where Callback: ForEachNearbyPointFunc {
        let radius_squared = radius * radius;
        for i in 0..self._points.len() {
            let r = self._points[i] - *origin;
            let distance_squared = r.dot(&r);
            if distance_squared <= radius_squared {
                callback(i, &self._points[i]);
            }
        }
    }

    fn has_nearby_point(&self, origin: &Vector3D, radius: f64) -> bool {
        let radius_squared = radius * radius;
        for i in 0..self._points.len() {
            let r = self._points[i] - *origin;
            let distance_squared = r.dot(&r);
            if distance_squared <= radius_squared {
                return true;
            }
        }

        return false;
    }
}

/// Shared pointer for the PointSimpleListSearcher3 type.
pub type PointSimpleListSearcher3Ptr = Arc<RwLock<PointSimpleListSearcher3>>;

///
/// # Front-end to create PointSimpleListSearcher3 objects step by step.
///
pub struct Builder {}

impl Builder {
    /// Builds PointSimpleListSearcher3 instance.
    pub fn build(&self) -> PointSimpleListSearcher3 {
        return PointSimpleListSearcher3::new();
    }

    /// Builds shared pointer of PointSimpleListSearcher3 instance.
    pub fn make_shared(&self) -> PointSimpleListSearcher3Ptr {
        return PointSimpleListSearcher3Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {};
    }
}
