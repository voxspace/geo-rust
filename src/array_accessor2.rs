/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use crate::usize2::USize2;
use std::mem::swap;
use std::ops::{Index, IndexMut};

///
/// \brief 2-D array accessor class.
///
/// This class represents 2-D array accessor. Array accessor provides array-like
/// data read/write functions, but does not handle memory management. Thus, it
/// is more like a random access iterator, but with multi-dimension support.
/// Similar to Array<T, 2>, this class interprets a linear array as a 2-D array
/// using i-major indexing.
///
/// \see Array<T, 2>
///
/// \tparam T - Array value type.
///
pub struct ArrayAccessor2<'a, T: ZeroInit> {
    _size: USize2,
    _data: Option<&'a mut [T]>,
}

impl<'a, T: ZeroInit> Default for ArrayAccessor2<'a, T> {
    /// Constructs empty 2-D array accessor.
    fn default() -> Self {
        return ArrayAccessor2 {
            _size: USize2::default(),
            _data: None,
        };
    }
}

impl<'a, T: ZeroInit> ArrayAccessor2<'a, T> {
    /// Constructs an array accessor that wraps given array.
    /// \param size Size of the 2-D array.
    /// \param data Raw array pointer.
    pub fn new_with_packed_size(size: &USize2, data: &'a mut [T]) -> ArrayAccessor2<'a, T> {
        let mut accessor = ArrayAccessor2::default();
        accessor.reset_with_packed_size(size, data);
        return accessor;
    }

    /// Constructs an array accessor that wraps given array.
    /// \param width Width of the 2-D array.
    /// \param height Height of the 2-D array.
    /// \param data Raw array pointer.
    pub fn new_with_size(width: usize, height: usize, data: &'a mut [T]) -> ArrayAccessor2<'a, T> {
        let mut accessor = ArrayAccessor2::default();
        accessor.reset_with_size(width, height, data);
        return accessor;
    }

    /// Replaces the content with given \p other array accessor.
    pub fn set(&mut self, other: &'a mut ArrayAccessor2<'a, T>) {
        self.reset_with_packed_size(&other._size, other._data.as_mut().unwrap());
    }

    /// Resets the array.
    pub fn reset_with_packed_size(&mut self, size: &USize2, data: &'a mut [T]) {
        self._size = *size;
        self._data = Some(data);
    }

    /// Resets the array.
    pub fn reset_with_size(&mut self, width: usize, height: usize, data: &'a mut [T]) {
        self.reset_with_packed_size(&USize2::new(width, height), data);
    }

    /// Returns the reference to the i-th element.
    pub fn at_index_mut(&mut self, i: usize) -> &mut T {
        debug_assert!(i < self._size.x * self._size.y);
        return &mut self._data.as_mut().unwrap()[i];
    }

    /// Returns the const reference to the i-th element.
    pub fn at_index(&self, i: usize) -> &T {
        debug_assert!(i < self._size.x * self._size.y);
        return &self._data.as_ref().unwrap()[i];
    }

    /// Returns the reference to the element at (pt.x, pt.y).
    pub fn at_with_packed_size_mut(&mut self, pt: &USize2) -> &mut T {
        return self.at_with_size_mut(pt.x, pt.y);
    }

    /// Returns the const reference to the element at (pt.x, pt.y).
    pub fn at_with_packed_size(&self, pt: &USize2) -> &T {
        return self.at_with_size(pt.x, pt.y);
    }

    /// Returns the reference to the element at (i, j).
    pub fn at_with_size_mut(&mut self, i: usize, j: usize) -> &mut T {
        debug_assert!(i < self._size.x && j < self._size.y);
        return &mut self._data.as_mut().unwrap()[i + self._size.x * j];
    }

    /// Returns the const reference to the element at (i, j).
    pub fn at_with_size(&self, i: usize, j: usize) -> &T {
        debug_assert!(i < self._size.x && j < self._size.y);
        return &self._data.as_ref().unwrap()[i + self._size.x * j];
    }

    /// Returns the size of the array.
    pub fn size(&self) -> USize2 {
        return self._size;
    }

    /// Returns the width of the array.
    pub fn width(&self) -> usize {
        return self._size.x;
    }

    /// Returns the height of the array.
    pub fn height(&self) -> usize {
        return self._size.y;
    }

    /// Returns the raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return &self._data.as_ref().unwrap();
    }

    /// Swaps the content of with \p other array accessor.
    pub fn swap(&mut self, other: &mut ArrayAccessor2<'a, T>) {
        swap(&mut other._data, &mut self._data);
        swap(&mut other._size, &mut self._size);
    }

    /// Returns the linear index of the given 2-D coordinate (pt.x, pt.y).
    pub fn index_with_packed_size(&self, pt: &USize2) -> usize {
        debug_assert!(pt.x < self._size.x && pt.y < self._size.y);
        return pt.x + self._size.x * pt.y;
    }

    /// Returns the linear index of the given 2-D coordinate (i, j).
    pub fn index_with_size(&self, i: usize, j: usize) -> usize {
        debug_assert!(i < self._size.x && j < self._size.y);
        return i + self._size.x * j;
    }
}

impl<'a, T: ZeroInit> ArrayAccessor2<'a, T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ArrayAccessor<int, 2> acc(2, 3, data);
    /// for (size_t j = 0; j < acc.height(); ++j) {
    ///     for (size_t i = 0; i < acc.width(); ++i) {
    ///         func(acc(i, j));
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ArrayAccessor<int, 2> acc(2, 3, data);
    /// acc.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, mut func: Callback) {
        for j in 0..self._size.y {
            for i in 0..self._size.x {
                func(self.at_with_size(i, j));
            }
        }
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes two parameters which are
    /// the (i, j) indices of the array. The order of execution will be the same
    /// as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ArrayAccessor<int, 2> acc(2, 3, data);
    /// for (size_t j = 0; j < acc.height(); ++j) {
    ///     for (size_t i = 0; i < acc.width(); ++i) {
    ///         func(i, j);
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ArrayAccessor<int, 2> acc(2, 3, data);
    /// acc.for_each_index([&](size_t i, size_t j) {
    ///     acc(i, j) = 4.f * i + 7.f * j + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize)>(&self, mut func: Callback) {
        for j in 0..self._size.y {
            for i in 0..self._size.x {
                func(i, j);
            }
        }
    }
}

impl<'a, T: ZeroInit> Index<usize> for ArrayAccessor2<'a, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data.as_ref().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> IndexMut<usize> for ArrayAccessor2<'a, T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._data.as_mut().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> Index<(usize, usize)> for ArrayAccessor2<'a, T> {
    type Output = T;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y);
        return &self._data.as_ref().unwrap()[index.0 + self._size.x * index.1];
    }
}

impl<'a, T: ZeroInit> IndexMut<(usize, usize)> for ArrayAccessor2<'a, T> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y);
        return &mut self._data.as_mut().unwrap()[index.0 + self._size.x * index.1];
    }
}

impl<'a, T: ZeroInit> Index<USize2> for ArrayAccessor2<'a, T> {
    type Output = T;

    fn index(&self, index: USize2) -> &Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y);
        return &self._data.as_ref().unwrap()[index.x + self._size.x * index.y];
    }
}

impl<'a, T: ZeroInit> IndexMut<USize2> for ArrayAccessor2<'a, T> {
    fn index_mut(&mut self, index: USize2) -> &mut Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y);
        return &mut self._data.as_mut().unwrap()[index.x + self._size.x * index.y];
    }
}

//--------------------------------------------------------------------------------------------------
///
/// \brief 2-D read-only array accessor class.
///
/// This class represents 2-D read-only array accessor. Array accessor provides
/// array-like data read/write functions, but does not handle memory management.
/// Thus, it is more like a random access iterator, but with multi-dimension
/// support. Similar to Array2<T, 2>, this class interprets a linear array as a
/// 2-D array using i-major indexing.
///
/// \see Array2<T, 2>
///
pub struct ConstArrayAccessor2<'a, T: ZeroInit> {
    _size: USize2,
    _data: Option<&'a [T]>,
}

impl<'a, T: ZeroInit> Default for ConstArrayAccessor2<'a, T> {
    /// Constructs empty 2-D read-only array accessor.
    fn default() -> Self {
        return ConstArrayAccessor2 {
            _size: USize2::default(),
            _data: None,
        };
    }
}

impl<'a, T: ZeroInit> ConstArrayAccessor2<'a, T> {
    /// Constructs a read-only array accessor that wraps given array.
    /// \param size Size of the 2-D array.
    /// \param data Raw array pointer.
    pub fn new_with_packed_size(size: &USize2, data: &'a [T]) -> ConstArrayAccessor2<'a, T> {
        let mut accessor = ConstArrayAccessor2::default();
        accessor._size = *size;
        accessor._data = Some(data);
        return accessor;
    }

    /// Constructs an array accessor that wraps given array.
    /// \param width Width of the 2-D array.
    /// \param height Height of the 2-D array.
    /// \param data Raw array pointer.
    pub fn new_with_size(width: usize, height: usize, data: &'a [T]) -> ConstArrayAccessor2<'a, T> {
        let mut accessor = ConstArrayAccessor2::default();
        accessor._size = USize2::new(width, height);
        accessor._data = Some(data);
        return accessor;
    }

    /// Returns the reference to the i-th element.
    pub fn at_index(&self, i: usize) -> &T {
        debug_assert!(i < self._size.x * self._size.y);
        return &self._data.as_ref().unwrap()[i];
    }

    /// Returns the const reference to the element at (pt.x, pt.y).
    pub fn at_with_packed_size(&self, pt: &USize2) -> &T {
        return self.at_with_size(pt.x, pt.y);
    }

    /// Returns the const reference to the element at (i, j).
    pub fn at_with_size(&self, i: usize, j: usize) -> &T {
        debug_assert!(i < self._size.x && j < self._size.y);
        return &self._data.as_ref().unwrap()[i + self._size.x * j];
    }

    /// Returns the size of the array.
    pub fn size(&self) -> USize2 {
        return self._size;
    }

    /// Returns the width of the array.
    pub fn width(&self) -> usize {
        return self._size.x;
    }

    /// Returns the height of the array.
    pub fn height(&self) -> usize {
        return self._size.y;
    }

    /// Returns the raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return self._data.as_ref().unwrap();
    }

    /// Returns the linear index of the given 2-D coordinate (pt.x, pt.y).
    pub fn index_with_packed_size(&self, pt: &USize2) -> usize {
        debug_assert!(pt.x < self._size.x && pt.y < self._size.y);
        return pt.x + self._size.x * pt.y;
    }

    /// Returns the linear index of the given 2-D coordinate (i, j).
    pub fn index_with_size(&self, i: usize, j: usize) -> usize {
        debug_assert!(i < self._size.x && j < self._size.y);
        return i + self._size.x * j;
    }
}

impl<'a, T: ZeroInit> ConstArrayAccessor2<'a, T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ConstArrayAccessor<int, 2> acc(2, 3, data);
    /// for (size_t j = 0; j < acc.height(); ++j) {
    ///     for (size_t i = 0; i < acc.width(); ++i) {
    ///         func(acc(i, j));
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ConstArrayAccessor<int, 2> acc(2, 3, data);
    /// acc.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, mut func: Callback) {
        for j in 0..self._size.y {
            for i in 0..self._size.x {
                func(self.at_with_size(i, j));
            }
        }
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes two parameters which are
    /// the (i, j) indices of the array. The order of execution will be the same
    /// as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ConstArrayAccessor<int, 2> acc(2, 3, data);
    /// for (size_t j = 0; j < acc.height(); ++j) {
    ///     for (size_t i = 0; i < acc.width(); ++i) {
    ///         func(i, j);
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ConstArrayAccessor<int, 2> acc(2, 3, data);
    /// acc.for_each_index([&](size_t i, size_t j) {
    ///     acc(i, j) = 4.f * i + 7.f * j + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize)>(&self, mut func: Callback) {
        for j in 0..self._size.y {
            for i in 0..self._size.x {
                func(i, j);
            }
        }
    }
}

impl<'a, T: ZeroInit> Index<usize> for ConstArrayAccessor2<'a, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data.as_ref().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> Index<(usize, usize)> for ConstArrayAccessor2<'a, T> {
    type Output = T;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y);
        return &self._data.as_ref().unwrap()[index.0 + self._size.x * index.1];
    }
}

impl<'a, T: ZeroInit> Index<USize2> for ConstArrayAccessor2<'a, T> {
    type Output = T;

    fn index(&self, index: USize2) -> &Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y);
        return &self._data.as_ref().unwrap()[index.x + self._size.x * index.y];
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod array_accessor2 {
    use crate::array_accessor2::ArrayAccessor2;
    use crate::usize2::USize2;
    use crate::array2::Array2;

    #[test]
    fn constructors() {
        let mut data = [0.0; 20];
        for i in 0..20 {
            data[i] = i as f64;
        }

        let acc = ArrayAccessor2::new_with_packed_size(&USize2::new(5, 4), &mut data);

        assert_eq!(5, acc.size().x);
        assert_eq!(4, acc.size().y);
    }

    #[test]
    fn iterators() {
        let mut arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);
        let acc = arr1.accessor();

        let mut cnt = 1.0;
        for elem in acc.data() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);

        let mut i = 0;
        arr1.for_each(|val| {
            assert_eq!(arr1[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);

        arr1.for_each_index(|i, j| {
            let idx = i + (4 * j) + 1;
            assert_eq!(idx as f64, arr1[(i, j)]);
        });
    }
}

//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod const_array_accessor2 {
    use crate::array_accessor2::ConstArrayAccessor2;
    use crate::usize2::USize2;
    use crate::array2::Array2;

    #[test]
    fn constructors() {
        let mut data = [0.0; 20];
        for i in 0..20 {
            data[i] = i as f64;
        }

        // Construct with ArrayAccessor2
        let acc = ConstArrayAccessor2::new_with_packed_size(&USize2::new(5, 4), &data);

        assert_eq!(5, acc.size().x);
        assert_eq!(4, acc.size().y);
    }

    #[test]
    fn iterators() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);
        let acc = arr1.const_accessor();

        let mut cnt = 1.0;
        for elem in acc.data() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);
        let acc = arr1.const_accessor();

        let mut i = 0;
        acc.for_each(|val| {
            assert_eq!(acc[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);
        let acc = arr1.const_accessor();

        acc.for_each_index(|i, j| {
            let idx = i + (4 * j) + 1;
            assert_eq!(idx as f64, acc[(i, j)]);
        });
    }
}