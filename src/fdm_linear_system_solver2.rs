/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::fdm_linear_system2::*;

/// Abstract base class for 2-D finite difference-type linear system solver.
pub trait FdmLinearSystemSolver2<'a> {
    /// Solves the given linear system.
    fn solve(&'a mut self, system: &'a mut FdmLinearSystem2) -> bool;

    /// Solves the given compressed linear system.
    fn solve_compressed(&'a mut self, _system: &'a mut FdmCompressedLinearSystem2) -> bool { return false; }
}