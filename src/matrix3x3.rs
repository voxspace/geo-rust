/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::vector3::Vector3;
use std::ops::{AddAssign, SubAssign, MulAssign, DivAssign, Index, IndexMut, Add, Neg, Sub, Mul, Div};

///
/// # 3-D matrix class.
///
/// This class is a row-major 3-D matrix class, which means each element of
/// the matrix is stored in order of (0, 0), (0, 1), (0, 2), (1, 0), (1, 1),
/// (1, 2), (2, 0), (2, 1), and (2, 2).
///
/// - tparam T - Type of the element.
///
#[derive(Clone, Copy)]
pub struct Matrix3x3<T: Float> {
    _elements: [T; 9],
}

/// Float-type 3x3 matrix.
pub type Matrix3x3F = Matrix3x3<f32>;

/// Double-type 3x3 matrix.
pub type Matrix3x3D = Matrix3x3<f64>;

impl<T: Float> Default for Matrix3x3<T> {
    /// Constructs identity matrix.
    fn default() -> Self {
        return Matrix3x3 {
            _elements: [T::one(), T::zero(), T::zero(),
                T::zero(), T::one(), T::zero(),
                T::zero(), T::zero(), T::one()]
        };
    }
}

/// # Constructors
impl<T: Float> Matrix3x3<T> {
    /// Constructs constant value matrix.
    pub fn new_scalar(s: T) -> Matrix3x3<T> {
        return Matrix3x3 {
            _elements: [s, s, s,
                s, s, s,
                s, s, s]
        };
    }

    /// Constructs a matrix with input elements.
    /// \warning Ordering of the input elements is row-major.
    pub fn new(m00: T, m01: T, m02: T, m10: T, m11: T, m12: T, m20: T, m21: T, m22: T) -> Matrix3x3<T> {
        return Matrix3x3 {
            _elements: [m00, m01, m02, m10, m11, m12, m20, m21, m22]
        };
    }

    ///
    /// Constructs a matrix with given initializer list **lst**.
    ///
    /// This constructor will build a matrix with given initializer list **lst**
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::matrix3x3::Matrix3x3F;
    /// let arr = Matrix3x3F::new_slice(
    ///     &[&[1.0, 2.0, 3.0],
    ///     &[4.0, 5.0, 6.0],
    ///     &[7.0, 8.0, 9.0]]);
    /// ```
    ///
    /// Note the initializer also has 2x2 structure.
    ///
    /// - parameter: lst Initializer list that should be copy to the new matrix.
    ///
    pub fn new_slice(lst: &[&[T]]) -> Matrix3x3<T> {
        return Matrix3x3 {
            _elements: [lst[0][0], lst[0][1], lst[0][2],
                lst[1][0], lst[1][1], lst[1][2],
                lst[2][0], lst[2][1], lst[2][2]]
        };
    }

    /// Constructs a matrix with input array.
    /// \warning Ordering of the input elements is row-major.
    pub fn new_array(arr: &[T; 9]) -> Matrix3x3<T> {
        return Matrix3x3 {
            _elements: *arr
        };
    }
}

/// # Basic setters
impl<T: Float> Matrix3x3<T> {
    /// Sets whole matrix with input scalar.
    pub fn set_scalar(&mut self, s: T) {
        self._elements[0] = s;
        self._elements[1] = s;
        self._elements[2] = s;
        self._elements[3] = s;
        self._elements[4] = s;
        self._elements[5] = s;
        self._elements[6] = s;
        self._elements[7] = s;
        self._elements[8] = s;
    }

    /// Sets this matrix with input elements.
    /// \warning Ordering of the input elements is row-major.
    pub fn set(&mut self, m00: T, m01: T, m02: T, m10: T, m11: T, m12: T, m20: T, m21: T, m22: T) {
        self._elements[0] = m00;
        self._elements[1] = m01;
        self._elements[2] = m02;
        self._elements[3] = m10;
        self._elements[4] = m11;
        self._elements[5] = m12;
        self._elements[6] = m20;
        self._elements[7] = m21;
        self._elements[8] = m22;
    }

    ///
    /// \brief Sets this matrix with given initializer list **lst**.
    ///
    /// This function will fill the matrix with given initializer list **lst**
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::matrix3x3::Matrix3x3F;
    /// let mut arr =  Matrix3x3F::default();
    /// arr.set_slice(&[
    ///     &[1.0, 2.0, 3.0],
    ///     &[9.0, 3.0, 4.0],
    ///     &[5.0, 6.0, 2.0]
    /// ]);
    /// ```
    ///
    /// Note the initializer also has 2x2 structure.
    ///
    /// - parameter: lst Initializer list that should be copy to the matrix.
    pub fn set_slice(&mut self, lst: &[&[T]]) {
        self._elements[0] = lst[0][0];
        self._elements[1] = lst[0][1];
        self._elements[2] = lst[0][2];
        self._elements[3] = lst[1][0];
        self._elements[4] = lst[1][1];
        self._elements[5] = lst[1][2];
        self._elements[6] = lst[2][0];
        self._elements[7] = lst[2][1];
        self._elements[8] = lst[2][2];
    }

    /// Copies from input matrix.
    pub fn set_self(&mut self, m: &Matrix3x3<T>) {
        self._elements = m._elements;
    }

    /// Copies from input array.
    /// *warning* Ordering of the input elements is row-major.
    pub fn set_array(&mut self, arr: &[T; 9]) {
        self._elements = *arr;
    }

    /// Sets diagonal elements with input scalar.
    pub fn set_diagonal(&mut self, s: T) {
        self._elements[0] = s;
        self._elements[4] = s;
        self._elements[8] = s;
    }

    /// Sets off-diagonal elements with input scalar.
    pub fn set_off_diagonal(&mut self, s: T) {
        self._elements[1] = s;
        self._elements[2] = s;
        self._elements[3] = s;
        self._elements[5] = s;
        self._elements[6] = s;
        self._elements[7] = s;
    }

    /// Sets i-th row with input vector.
    pub fn set_row(&mut self, i: usize, row: &Vector3<T>) {
        self._elements[3 * i] = row.x;
        self._elements[3 * i + 1] = row.y;
        self._elements[3 * i + 2] = row.z;
    }

    /// Sets i-th column with input vector.
    pub fn set_column(&mut self, j: usize, col: &Vector3<T>) {
        self._elements[j] = col.x;
        self._elements[j + 3] = col.y;
        self._elements[j + 6] = col.z;
    }
}

/// # Basic getters
impl<T: Float> Matrix3x3<T> {
    /// Returns true if this matrix is similar to the input matrix within the
    /// given tolerance.
    pub fn is_similar(&self, m: &Matrix3x3<T>, tol: Option<T>) -> bool {
        return (T::abs(self._elements[0] - m._elements[0]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[1] - m._elements[1]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[2] - m._elements[2]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[3] - m._elements[3]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[4] - m._elements[4]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[5] - m._elements[5]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[6] - m._elements[6]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[7] - m._elements[7]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[8] - m._elements[8]) < tol.unwrap_or(T::epsilon()));
    }

    /// Returns true if this matrix is a square matrix.
    pub fn is_square(&self) -> bool {
        return true;
    }

    /// Returns number of rows of this matrix.
    pub fn rows(&self) -> usize {
        return 3;
    }

    /// Returns number of columns of this matrix.
    pub fn cols(&self) -> usize {
        return 3;
    }

    /// Returns data pointer of this matrix.
    pub fn data_mut(&mut self) -> &mut [T; 9] {
        return &mut self._elements;
    }

    /// Returns constant pointer of this matrix.
    pub fn data(&self) -> &[T; 9] {
        return &self._elements;
    }
}

/// # Binary operator methods - new instance = this instance (+) input
impl<T: Float> Matrix3x3<T> {
    /// Returns this matrix + input scalar.
    pub fn add_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0] + s, self._elements[1] + s, self._elements[2] + s,
                              self._elements[3] + s, self._elements[4] + s, self._elements[5] + s,
                              self._elements[6] + s, self._elements[7] + s, self._elements[8] + s);
    }

    /// Returns this matrix + input matrix (element-wise).
    pub fn add_mat(&self, m: &Matrix3x3<T>) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0] + m._elements[0], self._elements[1] + m._elements[1], self._elements[2] + m._elements[2],
                              self._elements[3] + m._elements[3], self._elements[4] + m._elements[4], self._elements[5] + m._elements[5],
                              self._elements[6] + m._elements[6], self._elements[7] + m._elements[7], self._elements[8] + m._elements[8]);
    }

    /// Returns this matrix - input scalar.
    pub fn sub_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0] - s, self._elements[1] - s, self._elements[2] - s,
                              self._elements[3] - s, self._elements[4] - s, self._elements[5] - s,
                              self._elements[6] - s, self._elements[7] - s, self._elements[8] - s);
    }

    /// Returns this matrix - input matrix (element-wise).
    pub fn sub_mat(&self, m: &Matrix3x3<T>) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0] - m._elements[0], self._elements[1] - m._elements[1], self._elements[2] - m._elements[2],
                              self._elements[3] - m._elements[3], self._elements[4] - m._elements[4], self._elements[5] - m._elements[5],
                              self._elements[6] - m._elements[6], self._elements[7] - m._elements[7], self._elements[8] - m._elements[8]);
    }

    /// Returns this matrix * input scalar.
    pub fn mul_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0] * s, self._elements[1] * s, self._elements[2] * s,
                              self._elements[3] * s, self._elements[4] * s, self._elements[5] * s,
                              self._elements[6] * s, self._elements[7] * s, self._elements[8] * s);
    }

    /// Returns this matrix * input vector.
    pub fn mul_vec(&self, v: &Vector3<T>) -> Vector3<T> {
        return Vector3::new(self._elements[0] * v.x + self._elements[1] * v.y + self._elements[2] * v.z,
                            self._elements[3] * v.x + self._elements[4] * v.y + self._elements[5] * v.z,
                            self._elements[6] * v.x + self._elements[7] * v.y + self._elements[8] * v.z, );
    }

    /// Returns this matrix * input matrix.
    pub fn mul_mat(&self, m: &Matrix3x3<T>) -> Matrix3x3<T> {
        return Matrix3x3::new(
            self._elements[0] * m._elements[0] + self._elements[1] * m._elements[3] +
                self._elements[2] * m._elements[6],
            self._elements[0] * m._elements[1] + self._elements[1] * m._elements[4] +
                self._elements[2] * m._elements[7],
            self._elements[0] * m._elements[2] + self._elements[1] * m._elements[5] +
                self._elements[2] * m._elements[8],
            self._elements[3] * m._elements[0] + self._elements[4] * m._elements[3] +
                self._elements[5] * m._elements[6],
            self._elements[3] * m._elements[1] + self._elements[4] * m._elements[4] +
                self._elements[5] * m._elements[7],
            self._elements[3] * m._elements[2] + self._elements[4] * m._elements[5] +
                self._elements[5] * m._elements[8],
            self._elements[6] * m._elements[0] + self._elements[7] * m._elements[3] +
                self._elements[8] * m._elements[6],
            self._elements[6] * m._elements[1] + self._elements[7] * m._elements[4] +
                self._elements[8] * m._elements[7],
            self._elements[6] * m._elements[2] + self._elements[7] * m._elements[5] +
                self._elements[8] * m._elements[8]);
    }

    /// Returns this matrix / input scalar.
    pub fn div_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0] / s, self._elements[1] / s, self._elements[2] / s,
                              self._elements[3] / s, self._elements[4] / s, self._elements[5] / s,
                              self._elements[6] / s, self._elements[7] / s, self._elements[8] / s);
    }
}

/// # Binary operator methods - new instance = input (+) this instance
impl<T: Float> Matrix3x3<T> {
    /// Returns input scalar + this matrix.
    pub fn radd_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(s + self._elements[0], s + self._elements[1], s + self._elements[2],
                              s + self._elements[3], s + self._elements[4], s + self._elements[5],
                              s + self._elements[6], s + self._elements[7], s + self._elements[8]);
    }

    /// Returns input matrix + this matrix (element-wise).
    pub fn radd_mat(&self, m: &Matrix3x3<T>) -> Matrix3x3<T> {
        return Matrix3x3::new(m._elements[0] + self._elements[0], m._elements[1] + self._elements[1], m._elements[2] + self._elements[2],
                              m._elements[3] + self._elements[3], m._elements[4] + self._elements[4], m._elements[5] + self._elements[5],
                              m._elements[6] + self._elements[6], m._elements[7] + self._elements[7], m._elements[8] + self._elements[8]);
    }

    /// Returns input scalar - this matrix.
    pub fn rsub_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(s - self._elements[0], s - self._elements[1], s - self._elements[2],
                              s - self._elements[3], s - self._elements[4], s - self._elements[5],
                              s - self._elements[6], s - self._elements[7], s - self._elements[8]);
    }

    /// Returns input matrix - this matrix (element-wise).
    pub fn rsub_mat(&self, m: &Matrix3x3<T>) -> Matrix3x3<T> {
        return Matrix3x3::new(m._elements[0] - self._elements[0], m._elements[1] - self._elements[1], m._elements[2] - self._elements[2],
                              m._elements[3] - self._elements[3], m._elements[4] - self._elements[4], m._elements[5] - self._elements[5],
                              m._elements[6] - self._elements[6], m._elements[7] - self._elements[7], m._elements[8] - self._elements[8]);
    }

    /// Returns input scalar * this matrix.
    pub fn rmul_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(s * self._elements[0], s * self._elements[1], s * self._elements[2],
                              s * self._elements[3], s * self._elements[4], s * self._elements[5],
                              s * self._elements[6], s * self._elements[7], s * self._elements[8]);
    }

    /// Returns input matrix * this matrix.
    pub fn rmul_mat(&self, m: &Matrix3x3<T>) -> Matrix3x3<T> {
        return m.mul_mat(self);
    }

    /// Returns input scalar / this matrix.
    pub fn rdiv_scalar(&self, s: T) -> Matrix3x3<T> {
        return Matrix3x3::new(s / self._elements[0], s / self._elements[1], s / self._elements[2],
                              s / self._elements[3], s / self._elements[4], s / self._elements[5],
                              s / self._elements[6], s / self._elements[7], s / self._elements[8]);
    }
}

/// # Augmented operator methods - this instance (+)= input
impl<T: Float> Matrix3x3<T> {
    /// Adds input scalar to this matrix.
    pub fn iadd_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] + s;
        self._elements[1] = self._elements[1] + s;
        self._elements[2] = self._elements[2] + s;
        self._elements[3] = self._elements[3] + s;
        self._elements[4] = self._elements[4] + s;
        self._elements[5] = self._elements[5] + s;
        self._elements[6] = self._elements[6] + s;
        self._elements[7] = self._elements[7] + s;
        self._elements[8] = self._elements[8] + s;
    }

    /// Adds input matrix to this matrix (element-wise).
    pub fn iadd_mat(&mut self, m: &Matrix3x3<T>) {
        self._elements[0] = self._elements[0] + m._elements[0];
        self._elements[1] = self._elements[1] + m._elements[1];
        self._elements[2] = self._elements[2] + m._elements[2];
        self._elements[3] = self._elements[3] + m._elements[3];
        self._elements[4] = self._elements[4] + m._elements[4];
        self._elements[5] = self._elements[5] + m._elements[5];
        self._elements[6] = self._elements[6] + m._elements[6];
        self._elements[7] = self._elements[7] + m._elements[7];
        self._elements[8] = self._elements[8] + m._elements[8];
    }

    /// Subtracts input scalar from this matrix.
    pub fn isub_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] - s;
        self._elements[1] = self._elements[1] - s;
        self._elements[2] = self._elements[2] - s;
        self._elements[3] = self._elements[3] - s;
        self._elements[4] = self._elements[4] - s;
        self._elements[5] = self._elements[5] - s;
        self._elements[6] = self._elements[6] - s;
        self._elements[7] = self._elements[7] - s;
        self._elements[8] = self._elements[8] - s;
    }

    /// Subtracts input matrix from this matrix (element-wise).
    pub fn isub_mat(&mut self, m: &Matrix3x3<T>) {
        self._elements[0] = self._elements[0] - m._elements[0];
        self._elements[1] = self._elements[1] - m._elements[1];
        self._elements[2] = self._elements[2] - m._elements[2];
        self._elements[3] = self._elements[3] - m._elements[3];
        self._elements[4] = self._elements[4] - m._elements[4];
        self._elements[5] = self._elements[5] - m._elements[5];
        self._elements[6] = self._elements[6] - m._elements[6];
        self._elements[7] = self._elements[7] - m._elements[7];
        self._elements[8] = self._elements[8] - m._elements[8];
    }

    /// Multiplies input scalar to this matrix.
    pub fn imul_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] * s;
        self._elements[1] = self._elements[1] * s;
        self._elements[2] = self._elements[2] * s;
        self._elements[3] = self._elements[3] * s;
        self._elements[4] = self._elements[4] * s;
        self._elements[5] = self._elements[5] * s;
        self._elements[6] = self._elements[6] * s;
        self._elements[7] = self._elements[7] * s;
        self._elements[8] = self._elements[8] * s;
    }

    /// Multiplies input matrix to this matrix.
    pub fn imul_mat(&mut self, m: &Matrix3x3<T>) {
        self.set_self(&self.mul_mat(m));
    }

    /// Divides this matrix with input scalar.
    pub fn idiv_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] / s;
        self._elements[1] = self._elements[1] / s;
        self._elements[2] = self._elements[2] / s;
        self._elements[3] = self._elements[3] / s;
        self._elements[4] = self._elements[4] / s;
        self._elements[5] = self._elements[5] / s;
        self._elements[6] = self._elements[6] / s;
        self._elements[7] = self._elements[7] / s;
        self._elements[8] = self._elements[8] / s;
    }
}

/// # Modifiers
impl<T: Float> Matrix3x3<T> {
    /// Transposes this matrix.
    pub fn transpose(&mut self) {
        let temp = self._elements[1];
        self._elements[1] = self._elements[3];
        self._elements[3] = temp;

        let temp = self._elements[2];
        self._elements[2] = self._elements[6];
        self._elements[6] = temp;

        let temp = self._elements[5];
        self._elements[5] = self._elements[7];
        self._elements[7] = temp;
    }

    /// Inverts this matrix.
    pub fn invert(&mut self) {
        let d = self.determinant();
        let mut m = Matrix3x3::default();
        m._elements[0] = self._elements[4] * self._elements[8] - self._elements[5] * self._elements[7];
        m._elements[1] = self._elements[2] * self._elements[7] - self._elements[1] * self._elements[8];
        m._elements[2] = self._elements[1] * self._elements[5] - self._elements[2] * self._elements[4];
        m._elements[3] = self._elements[5] * self._elements[6] - self._elements[3] * self._elements[8];
        m._elements[4] = self._elements[0] * self._elements[8] - self._elements[2] * self._elements[6];
        m._elements[5] = self._elements[2] * self._elements[3] - self._elements[0] * self._elements[5];
        m._elements[6] = self._elements[3] * self._elements[7] - self._elements[4] * self._elements[6];
        m._elements[7] = self._elements[1] * self._elements[6] - self._elements[0] * self._elements[7];
        m._elements[8] = self._elements[0] * self._elements[4] - self._elements[1] * self._elements[3];
        m.idiv_scalar(d);

        self.set_self(&m);
    }
}

/// # Complex getters
impl<T: Float> Matrix3x3<T> {
    /// Returns sum of all elements.
    pub fn sum(&self) -> T {
        let mut s = T::zero();
        for i in 0..9 {
            s = s + self._elements[i];
        }
        return s;
    }

    /// Returns average of all elements.
    pub fn avg(&self) -> T {
        return self.sum() / T::from(9.0).unwrap();
    }

    /// Returns minimum among all elements.
    pub fn min(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = T::min(m, self._elements[i]);
        }
        return m;
    }

    /// Returns maximum among all elements.
    pub fn max(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = T::max(m, self._elements[i]);
        }
        return m;
    }

    /// Returns absolute minimum among all elements.
    pub fn absmin(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = crate::math_utils::absmin(m, self._elements[i]);
        }
        return m;
    }

    /// Returns absolute maximum among all elements.
    pub fn absmax(&self) -> T {
        let mut m = self._elements[0];
        for i in 1..self._elements.len() {
            m = crate::math_utils::absmax(m, self._elements[i]);
        }
        return m;
    }

    /// Returns sum of all diagonal elements.
    pub fn trace(&self) -> T {
        return self._elements[0] + self._elements[4] + self._elements[8];
    }

    /// Returns determinant of this matrix.
    pub fn determinant(&self) -> T {
        return self._elements[0] * self._elements[4] * self._elements[8] -
            self._elements[0] * self._elements[5] * self._elements[7] +
            self._elements[1] * self._elements[5] * self._elements[6] -
            self._elements[1] * self._elements[3] * self._elements[8] +
            self._elements[2] * self._elements[3] * self._elements[7] -
            self._elements[2] * self._elements[4] * self._elements[6];
    }

    /// Returns diagonal part of this matrix.
    pub fn diagonal(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0], T::zero(), T::zero(),
                              T::zero(), self._elements[4], T::zero(),
                              T::zero(), T::zero(), self._elements[8]);
    }

    /// Returns off-diagonal part of this matrix.
    pub fn off_diagonal(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(T::zero(), self._elements[1], self._elements[2],
                              self._elements[3], T::zero(), self._elements[5],
                              self._elements[6], self._elements[7], T::zero());
    }

    /// Returns strictly lower triangle part of this matrix.
    pub fn strict_lower_tri(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(T::zero(), T::zero(), T::zero(),
                              self._elements[3], T::zero(), T::zero(),
                              self._elements[6], self._elements[7], T::zero());
    }

    /// Returns strictly upper triangle part of this matrix.
    pub fn strict_upper_tri(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(T::zero(), self._elements[1], self._elements[2],
                              T::zero(), T::zero(), self._elements[5],
                              T::zero(), T::zero(), T::zero());
    }

    /// Returns lower triangle part of this matrix (including the diagonal).
    pub fn lower_tri(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0], T::zero(), T::zero(),
                              self._elements[3], self._elements[4], T::zero(),
                              self._elements[6], self._elements[7], self._elements[8]);
    }

    /// Returns upper triangle part of this matrix (including the diagonal).
    pub fn upper_tri(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0], self._elements[1], self._elements[2],
                              T::zero(), self._elements[4], self._elements[5],
                              T::zero(), T::zero(), self._elements[8]);
    }

    /// Returns transposed matrix.
    pub fn transposed(&self) -> Matrix3x3<T> {
        return Matrix3x3::new(self._elements[0], self._elements[3], self._elements[6],
                              self._elements[1], self._elements[4], self._elements[7],
                              self._elements[2], self._elements[5], self._elements[8]);
    }

    /// Returns inverse matrix.
    pub fn inverse(&self) -> Matrix3x3<T> {
        let mut m = *self;
        m.invert();
        return m;
    }

    /// Returns Frobenius norm.
    pub fn frobenius_norm(&self) -> T {
        return T::sqrt(self._elements[0] * self._elements[0] + self._elements[1] * self._elements[1] +
            self._elements[2] * self._elements[2] + self._elements[3] * self._elements[3] +
            self._elements[4] * self._elements[4] + self._elements[5] * self._elements[5] +
            self._elements[6] * self._elements[6] + self._elements[7] * self._elements[7] +
            self._elements[8] * self._elements[8]);
    }
}

/// # Setter operators

/// Addition assignment with input scalar.
impl<T: Float> AddAssign<T> for Matrix3x3<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Addition assignment with input matrix (element-wise).
impl<T: Float> AddAssign for Matrix3x3<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_mat(&rhs);
    }
}

/// Subtraction assignment with input scalar.
impl<T: Float> SubAssign<T> for Matrix3x3<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Subtraction assignment with input matrix (element-wise).
impl<T: Float> SubAssign for Matrix3x3<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_mat(&rhs);
    }
}

/// Multiplication assignment with input scalar.
impl<T: Float> MulAssign<T> for Matrix3x3<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Multiplication assignment with input matrix.
impl<T: Float> MulAssign for Matrix3x3<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_mat(&rhs);
    }
}

/// Division assignment with input scalar.
impl<T: Float> DivAssign<T> for Matrix3x3<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// # Getter operators
/// Returns constant reference of i-th element.eq!(1.0, mat[8]);
/// ```
impl<T: Float> Index<usize> for Matrix3x3<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return &self._elements[index];
    }
}

/// Returns reference of i-th element.
impl<T: Float> IndexMut<usize> for Matrix3x3<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._elements[index];
    }
}

/// Returns constant reference of i-th element.
impl<T: Float> Index<(usize, usize)> for Matrix3x3<T> {
    type Output = T;
    fn index(&self, index: (usize, usize)) -> &Self::Output {
        return &self._elements[3 * index.0 + index.1];
    }
}

/// Returns reference of i-th element.
impl<T: Float> IndexMut<(usize, usize)> for Matrix3x3<T> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        return &mut self._elements[3 * index.0 + index.1];
    }
}

/// Returns true if is equal to m.
impl<T: Float> PartialEq for Matrix3x3<T> {
    fn eq(&self, other: &Self) -> bool {
        return self._elements[0] == other._elements[0] && self._elements[1] == other._elements[1] && self._elements[2] == other._elements[2] &&
            self._elements[3] == other._elements[3] && self._elements[4] == other._elements[4] && self._elements[5] == other._elements[5] &&
            self._elements[6] == other._elements[6] && self._elements[7] == other._elements[7] && self._elements[8] == other._elements[8];
    }
}

impl<T: Float> Eq for Matrix3x3<T> {}

/// # Helpers
impl<T: Float> Matrix3x3<T> {
    /// Sets all matrix entries to zero.
    pub fn make_zero() -> Matrix3x3<T> {
        return Matrix3x3::new(T::zero(), T::zero(), T::zero(),
                              T::zero(), T::zero(), T::zero(),
                              T::zero(), T::zero(), T::zero());
    }

    /// Makes all diagonal elements to 1, and other elements to 0.
    pub fn make_identity() -> Matrix3x3<T> {
        return Matrix3x3::new(T::one(), T::zero(), T::zero(),
                              T::zero(), T::one(), T::zero(),
                              T::zero(), T::zero(), T::one());
    }

    /// Makes scale matrix.
    pub fn make_scale_matrix_scalar(sx: T, sy: T, sz: T) -> Matrix3x3<T> {
        return Matrix3x3::new(sx, T::zero(), T::zero(),
                              T::zero(), sy, T::zero(),
                              T::zero(), T::zero(), sz);
    }

    /// Makes scale matrix.
    pub fn make_scale_matrix_vec(s: Vector3<T>) -> Matrix3x3<T> {
        return Matrix3x3::new(s.x, T::zero(), T::zero(),
                              T::zero(), s.y, T::zero(),
                              T::zero(), T::zero(), s.z);
    }

    /// Makes rotation matrix.
    /// *warning* Input angle should be radian.
    pub fn make_rotation_matrix(axis: Vector3<T>, rad: T) -> Matrix3x3<T> {
        return Matrix3x3::new(
            T::one() + (T::one() - T::cos(rad)) * (axis.x * axis.x - T::one()),
            -axis.z * T::sin(rad) + (T::one() - T::cos(rad)) * axis.x * axis.y,
            axis.y * T::sin(rad) + (T::one() - T::cos(rad)) * axis.x * axis.z,
            axis.z * T::sin(rad) + (T::one() - T::cos(rad)) * axis.x * axis.y,
            T::one() + (T::one() - T::cos(rad)) * (axis.y * axis.y - T::one()),
            -axis.x * T::sin(rad) + (T::one() - T::cos(rad)) * axis.y * axis.z,
            -axis.y * T::sin(rad) + (T::one() - T::cos(rad)) * axis.x * axis.z,
            axis.x * T::sin(rad) + (T::one() - T::cos(rad)) * axis.y * axis.z,
            T::one() + (T::one() - T::cos(rad)) * (axis.z * axis.z - T::one()));
    }
}


impl<T: Float> Neg for Matrix3x3<T> {
    type Output = Matrix3x3<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Matrix3x3::new(-self._elements[0], -self._elements[1], -self._elements[2],
                              -self._elements[3], -self._elements[4], -self._elements[5],
                              -self._elements[6], -self._elements[7], -self._elements[8]);
    }
}

/// Returns a + b', where every element of matrix b' is b.
impl<T: Float> Add<T> for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Returns a + b (element-size).
impl<T: Float> Add for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn add(self, rhs: Self) -> Self::Output {
        return self.add_mat(&rhs);
    }
}

/// Returns a - b', where every element of matrix b' is b.
impl<T: Float> Sub<T> for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Returns a - b (element-size).
impl<T: Float> Sub for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_mat(&rhs);
    }
}

/// Returns a * b', where every element of matrix b' is b.
impl<T: Float> Mul<T> for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Returns a * b.
impl<T: Float> Mul<Vector3<T>> for Matrix3x3<T> {
    type Output = Vector3<T>;

    fn mul(self, rhs: Vector3<T>) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Returns a * b.
impl<T: Float> Mul for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_mat(&rhs);
    }
}

/// Returns a' / b, where every element of matrix a' is a.
impl<T: Float> Div<T> for Matrix3x3<T> {
    type Output = Matrix3x3<T>;

    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod matrix3x3 {
    use crate::vector3::*;
    use crate::matrix3x3::Matrix3x3D;

    #[test]
    fn constructors() {
        let mat = Matrix3x3D::default();
        assert_eq!(mat == Matrix3x3D::new(1.0, 0.0, 0.0,
                                           0.0, 1.0, 0.0,
                                           0.0, 0.0, 1.0), true);

        let mat2 = Matrix3x3D::new_scalar(3.1);
        for i in 0..9 {
            assert_eq!(3.1, mat2[i]);
        }

        let mat3 = Matrix3x3D::new(1.0, 2.0, 3.0,
                                   4.0, 5.0, 6.0,
                                   7.0, 8.0, 9.0);
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat3[i]);
        }

        let mat4 = Matrix3x3D::new_slice(
            &[&[1.0, 2.0, 3.0],
                &[4.0, 5.0, 6.0],
                &[7.0, 8.0, 9.0]]);
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat4[i]);
        }

        let mat5 = mat4.clone();
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat5[i]);
        }

        let arr = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0];
        let mat6 = Matrix3x3D::new_array(&arr);
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat6[i]);
        }
    }

    #[test]
    fn set_methods() {
        let mut mat = Matrix3x3D::default();

        mat.set_scalar(3.1);
        for i in 0..9 {
            assert_eq!(3.1, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set(1.0, 2.0, 3.0,
                4.0, 5.0, 6.0,
                7.0, 8.0, 9.0);
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0], &[7.0, 8.0, 9.0]]);
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_self(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                      4.0, 5.0, 6.0,
                                      7.0, 8.0, 9.0));
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        let arr = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0];
        mat.set_array(&arr);
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_diagonal(3.1);
        for i in 0..3 {
            for j in 0..3 {
                if i == j {
                    assert_eq!(3.1, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat.set_scalar(0.0);
        mat.set_off_diagonal(4.2);
        for i in 0..3 {
            for j in 0..3 {
                if i != j {
                    assert_eq!(4.2, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat.set_scalar(0.0);
        mat.set_row(0, &Vector3D::new(1.0, 2.0, 3.0));
        mat.set_row(1, &Vector3D::new(4.0, 5.0, 6.0));
        mat.set_row(2, &Vector3D::new(7.0, 8.0, 9.0));
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_column(0, &Vector3D::new(1.0, 4.0, 7.0));
        mat.set_column(1, &Vector3D::new(2.0, 5.0, 8.0));
        mat.set_column(2, &Vector3D::new(3.0, 6.0, 9.0));
        for i in 0..9 {
            assert_eq!((i + 1) as f64, mat[i]);
        }
    }

    #[test]
    fn basic_getters() {
        let mat = Matrix3x3D::new(1.0, 2.0, 3.0,
                                  4.0, 5.0, 6.0,
                                  7.0, 8.0, 9.0);
        let mat2 = Matrix3x3D::new(1.01, 2.01, 2.99,
                                   4.0, 4.99, 6.001,
                                   7.0003, 8.0, 8.99);

        assert_eq!(mat.is_similar(&mat2, Some(0.02)), true);
        assert_eq!(mat.is_similar(&mat2, Some(0.001)), false);

        assert_eq!(mat.is_square(), true);

        assert_eq!(3, mat.rows());
        assert_eq!(3, mat.cols());
    }

    #[test]
    fn binary_operators() {
        let mat = Matrix3x3D::new(9.0, -8.0, 7.0,
                                  -6.0, 5.0, -4.0,
                                  3.0, -2.0, 1.0);

        let mut mat2 = mat.add_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(11.0, -6.0, 9.0,
                             -4.0, 7.0, -2.0,
                             5.0, 0.0, 3.0), None), true);

        mat2 = mat.add_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                            4.0, 5.0, 6.0,
                                            7.0, 8.0, 9.0));
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(10.0, -6.0, 10.0,
                             -2.0, 10.0, 2.0,
                             10.0, 6.0, 10.0), None), true);

        mat2 = mat.sub_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(7.0, -10.0, 5.0,
                             -8.0, 3.0, -6.0,
                             1.0, -4.0, -1.0), None), true);

        mat2 = mat.sub_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                            4.0, 5.0, 6.0,
                                            7.0, 8.0, 9.0));
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(8.0, -10.0, 4.0,
                             -10.0, 0.0, -10.0,
                             -4.0, -10.0, -8.0), None), true);

        mat2 = mat.mul_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(18.0, -16.0, 14.0,
                             -12.0, 10.0, -8.0,
                             6.0, -4.0, 2.0), None), true);

        let vec = mat.mul_vec(&Vector3D::new(1.0, 2.0, 3.0));
        assert_eq!(vec.is_similar(&Vector3D::new(14.0, -8.0, 2.0), None), true);

        mat2 = mat.mul_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                            4.0, 5.0, 6.0,
                                            7.0, 8.0, 9.0));
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(26.0, 34.0, 42.0,
                             -14.0, -19.0, -24.0,
                             2.0, 4.0, 6.0), None), true);

        mat2 = mat.div_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(4.5, -4.0, 3.5,
                             -3.0, 2.5, -2.0,
                             1.5, -1.0, 0.5), None), true);


        mat2 = mat.radd_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(11.0, -6.0, 9.0,
                             -4.0, 7.0, -2.0,
                             5.0, 0.0, 3.0), None), true);

        mat2 = mat.radd_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                        4.0, 5.0, 6.0,
                                        7.0, 8.0, 9.0));
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(10.0, -6.0, 10.0,
                             -2.0, 10.0, 2.0,
                             10.0, 6.0, 10.0), None), true);

        mat2 = mat.rsub_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(-7.0, 10.0, -5.0,
                             8.0, -3.0, 6.0,
                             -1.0, 4.0, 1.0), None), true);

        mat2 = mat.rsub_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                        4.0, 5.0, 6.0,
                                        7.0, 8.0, 9.0));
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(-8.0, 10.0, -4.0,
                             10.0, 0.0, 10.0,
                             4.0, 10.0, 8.0), None), true);

        mat2 = mat.rmul_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(18.0, -16.0, 14.0,
                             -12.0, 10.0, -8.0,
                             6.0, -4.0, 2.0), None), true);

        mat2 = mat.rmul_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                        4.0, 5.0, 6.0,
                                        7.0, 8.0, 9.0));
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(6.0, -4.0, 2.0,
                             24.0, -19.0, 14.0,
                             42.0, -34.0, 26.0), None), true);

        mat2 = mat.rdiv_scalar(2.0);
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(
                2.0 / 9.0, -0.25, 2.0 / 7.0,
                -1.0 / 3.0, 0.4, -0.5,
                2.0 / 3.0, -1.0, 2.0), None), true);
    }

    #[test]
    fn augmented_operators() {
        let mut mat = Matrix3x3D::new(9.0, -8.0, 7.0,
                                      -6.0, 5.0, -4.0,
                                      3.0, -2.0, 1.0);

        mat.iadd_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(11.0, -6.0, 9.0,
                             -4.0, 7.0, -2.0,
                             5.0, 0.0, 3.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat.iadd_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                      4.0, 5.0, 6.0,
                                      7.0, 8.0, 9.0));
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(10.0, -6.0, 10.0,
                             -2.0, 10.0, 2.0,
                             10.0, 6.0, 10.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat.isub_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(7.0, -10.0, 5.0,
                             -8.0, 3.0, -6.0,
                             1.0, -4.0, -1.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat.isub_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                      4.0, 5.0, 6.0,
                                      7.0, 8.0, 9.0));
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(8.0, -10.0, 4.0,
                             -10.0, 0.0, -10.0,
                             -4.0, -10.0, -8.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat.imul_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(18.0, -16.0, 14.0,
                             -12.0, 10.0, -8.0,
                             6.0, -4.0, 2.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat.imul_mat(&Matrix3x3D::new(1.0, 2.0, 3.0,
                                      4.0, 5.0, 6.0,
                                      7.0, 8.0, 9.0));
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(26.0, 34.0, 42.0,
                             -14.0, -19.0, -24.0,
                             2.0, 4.0, 6.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat.idiv_scalar(2.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(4.5, -4.0, 3.5,
                             -3.0, 2.5, -2.0,
                             1.5, -1.0, 0.5), None), true);
    }

    #[test]
    fn modifiers() {
        let mut mat = Matrix3x3D::new(9.0, -8.0, 7.0,
                                      -6.0, 5.0, -4.0,
                                      3.0, -2.0, 1.0);

        mat.transpose();
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(9.0, -6.0, 3.0,
                             -8.0, 5.0, -2.0,
                             7.0, -4.0, 1.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 2.0);
        mat.invert();
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(-2.0 / 3.0, -2.0 / 3.0, 1.0,
                             0.0, 1.0, 2.0,
                             1.0, 2.0, 1.0), None), true);
    }

    #[test]
    fn complex_getters() {
        let mut mat = Matrix3x3D::new(9.0, -8.0, 7.0,
                                      -6.0, 5.0, -4.0,
                                      3.0, -2.0, 1.0);

        assert_eq!(5.0, mat.sum());

        assert_eq!(5.0 / 9.0, mat.avg());

        assert_eq!(-8.0, mat.min());

        assert_eq!(9.0, mat.max());

        assert_eq!(1.0, mat.absmin());

        assert_eq!(9.0, mat.absmax());

        assert_eq!(15.0, mat.trace());

        assert_eq!(0.0, mat.determinant());

        let mut mat2 = mat.diagonal();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(9.0, 0.0, 0.0,
                             0.0, 5.0, 0.0,
                             0.0, 0.0, 1.0), None), true);

        mat2 = mat.off_diagonal();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(0.0, -8.0, 7.0,
                             -6.0, 0.0, -4.0,
                             3.0, -2.0, 0.0), None), true);

        mat2 = mat.strict_lower_tri();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(0.0, 0.0, 0.0,
                             -6.0, 0.0, 0.0,
                             3.0, -2.0, 0.0), None), true);

        mat2 = mat.strict_upper_tri();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(0.0, -8.0, 7.0,
                             0.0, 0.0, -4.0,
                             0.0, 0.0, 0.0), None), true);

        mat2 = mat.lower_tri();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(9.0, 0.0, 0.0,
                             -6.0, 5.0, 0.0,
                             3.0, -2.0, 1.0), None), true);

        mat2 = mat.upper_tri();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(9.0, -8.0, 7.0,
                             0.0, 5.0, -4.0,
                             0.0, 0.0, 1.0), None), true);

        mat2 = mat.transposed();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(9.0, -6.0, 3.0,
                             -8.0, 5.0, -2.0,
                             7.0, -4.0, 1.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 2.0);
        mat2 = mat.inverse();
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(-2.0 / 3.0, -2.0 / 3.0, 1.0,
                             0.0, 1.0, 2.0,
                             1.0, 2.0, 1.0), None), true);
    }

    #[test]
    fn setter_operator_overloadings() {
        let mut mat = Matrix3x3D::new(9.0, -8.0, 7.0,
                                      -6.0, 5.0, -4.0,
                                      3.0, -2.0, 1.0);

        let mat2 = mat;
        assert_eq!(mat2.is_similar(
            &Matrix3x3D::new(9.0, -8.0, 7.0,
                             -6.0, 5.0, -4.0,
                             3.0, -2.0, 1.0), None), true);

        mat += 2.0;
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(11.0, -6.0, 9.0,
                             -4.0, 7.0, -2.0,
                             5.0, 0.0, 3.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat += Matrix3x3D::new(1.0, 2.0, 3.0,
                               4.0, 5.0, 6.0,
                               7.0, 8.0, 9.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(10.0, -6.0, 10.0,
                             -2.0, 10.0, 2.0,
                             10.0, 6.0, 10.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat -= 2.0;
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(7.0, -10.0, 5.0,
                             -8.0, 3.0, -6.0,
                             1.0, -4.0, -1.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat -= Matrix3x3D::new(1.0, 2.0, 3.0,
                               4.0, 5.0, 6.0,
                               7.0, 8.0, 9.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(8.0, -10.0, 4.0,
                             -10.0, 0.0, -10.0,
                             -4.0, -10.0, -8.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat *= 2.0;
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(18.0, -16.0, 14.0,
                             -12.0, 10.0, -8.0,
                             6.0, -4.0, 2.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat *= Matrix3x3D::new(1.0, 2.0, 3.0,
                               4.0, 5.0, 6.0,
                               7.0, 8.0, 9.0);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(26.0, 34.0, 42.0,
                             -14.0, -19.0, -24.0,
                             2.0, 4.0, 6.0), None), true);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        mat /= 2.0;
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(4.5, -4.0, 3.5,
                             -3.0, 2.5, -2.0,
                             1.5, -1.0, 0.5), None), true);
    }

    #[test]
    fn getter_operator_overloadings() {
        let mut mat = Matrix3x3D::new(9.0, -8.0, 7.0,
                                      -6.0, 5.0, -4.0,
                                      3.0, -2.0, 1.0);

        assert_eq!(9.0, mat[0]);
        assert_eq!(-8.0, mat[1]);
        assert_eq!(7.0, mat[2]);
        assert_eq!(-6.0, mat[3]);
        assert_eq!(5.0, mat[4]);
        assert_eq!(-4.0, mat[5]);
        assert_eq!(3.0, mat[6]);
        assert_eq!(-2.0, mat[7]);
        assert_eq!(1.0, mat[8]);

        mat[0] = -9.0;
        mat[1] = 8.0;
        mat[2] = -7.0;
        mat[3] = 6.0;
        mat[4] = -5.0;
        mat[5] = 4.0;
        mat[6] = -3.0;
        mat[7] = 2.0;
        mat[8] = -1.0;
        assert_eq!(-9.0, mat[0]);
        assert_eq!(8.0, mat[1]);
        assert_eq!(-7.0, mat[2]);
        assert_eq!(6.0, mat[3]);
        assert_eq!(-5.0, mat[4]);
        assert_eq!(4.0, mat[5]);
        assert_eq!(-3.0, mat[6]);
        assert_eq!(2.0, mat[7]);
        assert_eq!(-1.0, mat[8]);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        assert_eq!(9.0, mat[0]);
        assert_eq!(-8.0, mat[1]);
        assert_eq!(7.0, mat[2]);
        assert_eq!(-6.0, mat[3]);
        assert_eq!(5.0, mat[4]);
        assert_eq!(-4.0, mat[5]);
        assert_eq!(3.0, mat[6]);
        assert_eq!(-2.0, mat[7]);
        assert_eq!(1.0, mat[8]);

        mat[(0, 0)] = -9.0;
        mat[(0, 1)] = 8.0;
        mat[(0, 2)] = -7.0;
        mat[(1, 0)] = 6.0;
        mat[(1, 1)] = -5.0;
        mat[(1, 2)] = 4.0;
        mat[(2, 0)] = -3.0;
        mat[(2, 1)] = 2.0;
        mat[(2, 2)] = -1.0;
        assert_eq!(-9.0, mat[0]);
        assert_eq!(8.0, mat[1]);
        assert_eq!(-7.0, mat[2]);
        assert_eq!(6.0, mat[3]);
        assert_eq!(-5.0, mat[4]);
        assert_eq!(4.0, mat[5]);
        assert_eq!(-3.0, mat[6]);
        assert_eq!(2.0, mat[7]);
        assert_eq!(-1.0, mat[8]);
        assert_eq!(-9.0, mat[(0, 0)]);
        assert_eq!(8.0, mat[(0, 1)]);
        assert_eq!(-7.0, mat[(0, 2)]);
        assert_eq!(6.0, mat[(1, 0)]);
        assert_eq!(-5.0, mat[(1, 1)]);
        assert_eq!(4.0, mat[(1, 2)]);
        assert_eq!(-3.0, mat[(2, 0)]);
        assert_eq!(2.0, mat[(2, 1)]);
        assert_eq!(-1.0, mat[(2, 2)]);

        mat.set(9.0, -8.0, 7.0,
                -6.0, 5.0, -4.0,
                3.0, -2.0, 1.0);
        assert_eq!(
            mat == Matrix3x3D::new(9.0, -8.0, 7.0,
                                   -6.0, 5.0, -4.0,
                                   3.0, -2.0, 1.0), true);

        mat.set(9.0, 8.0, 7.0,
                6.0, 5.0, 4.0,
                3.0, 2.0, 1.0);
        assert_eq!(
            mat != Matrix3x3D::new(9.0, -8.0, 7.0,
                                   -6.0, 5.0, -4.0,
                                   3.0, -2.0, 1.0), true);
    }

    #[test]
    fn helpers() {
        let mut mat = Matrix3x3D::make_zero();
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(0.0, 0.0, 0.0,
                             0.0, 0.0, 0.0,
                             0.0, 0.0, 0.0), None), true);

        mat = Matrix3x3D::make_identity();
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(1.0, 0.0, 0.0,
                             0.0, 1.0, 0.0,
                             0.0, 0.0, 1.0), None), true);

        mat = Matrix3x3D::make_scale_matrix_scalar(3.0, -4.0, 2.4);
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(3.0, 0.0, 0.0,
                             0.0, -4.0, 0.0,
                             0.0, 0.0, 2.4), None), true);

        mat = Matrix3x3D::make_scale_matrix_vec(Vector3D::new(-2.0, 5.0, 3.5));
        assert_eq!(mat.is_similar(
            &Matrix3x3D::new(-2.0, 0.0, 0.0,
                             0.0, 5.0, 0.0,
                             0.0, 0.0, 3.5), None), true);

        mat = Matrix3x3D::make_rotation_matrix(
            Vector3D::new(-1.0 / 3.0, 2.0 / 3.0, 2.0 / 3.0),
            -74.0 / 180.0 * crate::constants::K_PI_D);
        assert_eq!(mat.is_similar(&Matrix3x3D::new(
            0.36, 0.48, -0.8,
            -0.8, 0.60, 0.0,
            0.48, 0.64, 0.6), Some(0.05)), true);
    }
}