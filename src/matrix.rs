/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::matrix_expression::*;
use crate::usize2::USize2;
use crate::vector_expression::VectorExpression;
use std::ops::*;

///
/// # Static-sized M x N matrix class.
///
/// This class defines M x N row-major matrix data where its size is determined
/// statically at compile time.
///
/// - tparam T - Real number type.
/// - tparam M - Number of rows.
/// - tparam N - Number of columns.
///
#[derive(Clone, Copy)]
pub struct Matrix<const M: usize, const N: usize, const TOTAL: usize> {
    _elements: [f64; TOTAL],
}

impl<const M: usize, const N: usize, const TOTAL: usize> MatrixExpression for Matrix<M, N, TOTAL> {
    fn size(&self) -> USize2 {
        return USize2::new(M, N);
    }

    fn rows(&self) -> usize {
        return M;
    }

    fn cols(&self) -> usize {
        return N;
    }

    fn eval(&self, x: usize, y: usize) -> f64 {
        return self._elements[x * N + y];
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> Default for Matrix<M, N, TOTAL> {
    fn default() -> Self {
        return Matrix {
            _elements: [0.0; TOTAL]
        };
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Constructs matrix instance with parameters.
    pub fn new(array: [f64; TOTAL]) -> Matrix<M, N, TOTAL> {
        return Matrix {
            _elements: array
        };
    }

    ///
    /// \brief Constructs a matrix with given initializer list \p lst.
    ///
    /// This constructor will build a matrix with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// Matrix<float, 3, 4> mat = {
    ///     {1.f, 2.f, 4.f, 3.f},
    ///     {9.f, 3.f, 5.f, 1.f},
    ///     {4.f, 8.f, 1.f, 5.f}
    /// };
    /// \endcode
    ///
    /// \param lst Initializer list that should be copy to the new matrix.
    ///
    pub fn new_slice(lst: &[&[f64]]) -> Matrix<M, N, TOTAL> {
        let mut mat = Matrix::<M, N, TOTAL>::default();
        mat.set_slice(lst);
        return mat;
    }

    /// Constructs a matrix with expression template.
    pub fn new_expression<E: MatrixExpression>(other: &E) -> Matrix<M, N, TOTAL> {
        let mut mat = Matrix::<M, N, TOTAL>::default();
        mat.set_expression(other);
        return mat;
    }
}

/// # Basic setters
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Sets whole matrix with input scalar.
    pub fn set_scalar(&mut self, s: f64) {
        self._elements.fill(s);
    }

    ///
    /// \brief Sets a matrix with given initializer list \p lst.
    ///
    /// This function will fill the matrix with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// Matrix<float, 3, 4> mat;
    /// mat.set({
    ///     {1.f, 2.f, 4.f, 3.f},
    ///     {9.f, 3.f, 5.f, 1.f},
    ///     {4.f, 8.f, 1.f, 5.f}
    /// });
    /// \endcode
    ///
    /// \param lst Initializer list that should be copy to the new matrix.
    ///
    pub fn set_slice(&mut self, lst: &[&[f64]]) {
        let rows = lst.len();
        let cols = if rows > 0 { lst[0].len() } else { 0 };

        debug_assert!(rows == M);
        debug_assert!(cols == N);

        for i in 0..rows {
            for j in 0..cols {
                self[(i, j)] = lst[i][j];
            }
        }
    }

    /// Copies from input matrix expression.
    pub fn set_expression<E: MatrixExpression>(&mut self, other: &E) {
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                self[(i, j)] = other.eval(i, j);
            }
        }
    }

    /// Sets diagonal elements with input scalar.
    pub fn set_diagonal(&mut self, s: f64) {
        let l = usize::min(self.rows(), self.cols());
        for i in 0..l {
            self[(i, i)] = s;
        }
    }

    /// Sets off-diagonal elements with input scalar.
    pub fn set_off_diagonal(&mut self, s: f64) {
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                if i != j {
                    self[(i, j)] = s;
                }
            }
        }
    }

    /// Sets i-th row with input vector.
    pub fn set_row<E: VectorExpression>(&mut self, i: usize, row: &E) {
        debug_assert!(self.cols() == row.size());

        for j in 0..N {
            self[(i, j)] = row.eval(j);
        }
    }

    /// Sets j-th column with input vector.
    pub fn set_column<E: VectorExpression>(&mut self, j: usize, col: &E) {
        debug_assert!(self.rows() == col.size());

        for i in 0..M {
            self[(i, j)] = col.eval(j);
        }
    }
}

/// # Basic getters
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    pub fn is_equal<E: MatrixExpression>(&self, other: &E) -> bool {
        if self.size() != other.size() {
            return false;
        }

        for i in 0..self.rows() {
            for j in 0..self.cols() {
                if self[(i, j)] != other.eval(i, j) {
                    return false;
                }
            }
        }

        return true;
    }

    /// Returns true if this matrix is similar to the input matrix within the
    /// given tolerance.
    pub fn is_similar<E: MatrixExpression>(&self, other: &E, tol: Option<f64>) -> bool {
        if self.size() != other.size() {
            return false;
        }

        for i in 0..self.rows() {
            for j in 0..self.cols() {
                if f64::abs(self[(i, j)] - other.eval(i, j)) > tol.unwrap_or(f64::EPSILON) {
                    return false;
                }
            }
        }

        return true;
    }

    /// Returns true if this matrix is a square matrix.
    pub fn is_square(&self) -> bool {
        return M == N;
    }

    /// Returns data pointer of this matrix.
    pub fn data_mut(&mut self) -> &mut [f64] {
        return &mut self._elements;
    }

    /// Returns constant pointer of this matrix.
    pub fn data(&self) -> &[f64] {
        return &self._elements;
    }
}

/// # Binary operator methods - new instance = this instance (+) input
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Returns this matrix + input scalar.
    pub fn add_scalar(&self, s: f64) -> MatrixScalarAdd<Matrix<M, N, TOTAL>> {
        return MatrixScalarAdd::new(self, s);
    }

    /// Returns this matrix + input matrix (element-wise).
    pub fn add_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixAdd<Matrix<M, N, TOTAL>, E> {
        return MatrixAdd::new(self, m);
    }

    /// Returns this matrix - input scalar.
    pub fn sub_scalar(&self, s: f64) -> MatrixScalarSub<Matrix<M, N, TOTAL>> {
        return MatrixScalarSub::new(self, s);
    }

    /// Returns this matrix - input matrix (element-wise).
    pub fn sub_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixSub<Matrix<M, N, TOTAL>, E> {
        return MatrixSub::new(self, m);
    }

    /// Returns this matrix * input scalar.
    pub fn mul_scalar(&self, s: f64) -> MatrixScalarMul<Matrix<M, N, TOTAL>> {
        return MatrixScalarMul::new(self, s);
    }

    /// Returns this matrix * input vector.
    pub fn mul_vec_expression<'a, VE: VectorExpression>(&'a self, v: &'a VE) -> MatrixVectorMul<Matrix<M, N, TOTAL>, VE> {
        return MatrixVectorMul::new(self, v);
    }

    /// Returns this matrix * input matrix.
    pub fn mul_mat<'a, const L: usize, const L_TOTAL: usize>(&'a self, m: &'a Matrix<N, L, L_TOTAL>)
                                                             -> MatrixMul<Matrix<M, N, TOTAL>, Matrix<N, L, L_TOTAL>> {
        return MatrixMul::new(self, m);
    }

    /// Returns this matrix / input scalar.
    pub fn div_scalar(&self, s: f64) -> MatrixScalarDiv<Matrix<M, N, TOTAL>> {
        return MatrixScalarDiv::new(self, s);
    }
}

/// # Binary operator methods - new instance = input (+) this instance
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Returns input scalar + this matrix.
    pub fn radd_scalar(&self, s: f64) -> MatrixScalarAdd<Matrix<M, N, TOTAL>> {
        return MatrixScalarAdd::new(self, s);
    }

    /// Returns input matrix + this matrix (element-wise).
    pub fn radd_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixAdd<E, Matrix<M, N, TOTAL>> {
        return MatrixAdd::new(m, self);
    }

    /// Returns input scalar - this matrix.
    pub fn rsub_scalar(&self, s: f64) -> MatrixScalarRSub<Matrix<M, N, TOTAL>> {
        return MatrixScalarRSub::new(self, s);
    }

    /// Returns input matrix - this matrix (element-wise).
    pub fn rsub_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixSub<E, Matrix<M, N, TOTAL>> {
        return MatrixSub::new(m, self);
    }

    /// Returns input scalar * this matrix.
    pub fn rmul_scalar(&self, s: f64) -> MatrixScalarMul<Matrix<M, N, TOTAL>> {
        return MatrixScalarMul::new(self, s);
    }

    /// Returns input matrix * this matrix.
    pub fn rmul_mat<'a, const L: usize, const L_TOTAL: usize>(&'a self, m: &'a Matrix<N, L, L_TOTAL>)
                                                              -> MatrixMul<Matrix<N, L, L_TOTAL>, Matrix<M, N, TOTAL>> {
        return MatrixMul::new(m, self);
    }

    /// Returns input matrix / this scalar.
    pub fn rdiv_scalar(&self, s: f64) -> MatrixScalarRDiv<Matrix<M, N, TOTAL>> {
        return MatrixScalarRDiv::new(self, s);
    }
}

/// # Augmented operator methods - this instance (+)= input
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Adds input scalar to this matrix.
    pub fn iadd_scalar(&mut self, s: f64) {
        let expression = self.add_scalar(s);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }

    /// Adds input matrix to this matrix (element-wise).
    pub fn iadd_expression<E: MatrixExpression>(&mut self, m: &E) {
        let expression = self.add_expression(m);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }

    /// Subtracts input scalar from this matrix.
    pub fn isub_scalar(&mut self, s: f64) {
        let expression = self.sub_scalar(s);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }

    /// Subtracts input matrix from this matrix (element-wise).
    pub fn isub_expression<E: MatrixExpression>(&mut self, m: &E) {
        let expression = self.sub_expression(m);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }

    /// Multiplies input scalar to this matrix.
    pub fn imul_scalar(&mut self, s: f64) {
        let expression = self.mul_scalar(s);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }

    /// Multiplies input matrix to this matrix.
    pub fn imul_mat<const L: usize, const L_TOTAL: usize>(&mut self, m: &Matrix<N, L, L_TOTAL>) {
        let expression = self.mul_mat(m);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }

    /// Divides this matrix with input scalar.
    pub fn idiv_scalar(&mut self, s: f64) {
        let expression = self.div_scalar(s);
        let mut element = [0.0; TOTAL];
        // Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                element[i * N + j] = expression.eval(i, j);
            }
        }
        self._elements.swap_with_slice(&mut element);
    }
}

/// # Modifiers
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Transposes this matrix.
    pub fn transpose(&mut self) {
        self.set_expression(&self.transposed());
    }

    ///
    /// \brief Inverts this matrix.
    ///
    /// This function computes the inverse using Gaussian elimination method.
    ///
    pub fn invert(&mut self) {
        debug_assert!(self.is_square());

        // Computes inverse matrix using Gaussian elimination method.
        // https://martin-thoma.com/solving-linear-equations-with-gaussian-elimination/
        let n = self.rows();
        let mut rhs = Matrix::<M, N, TOTAL>::new_expression(&Matrix::<M, N, TOTAL>::make_identity());

        for i in 0..n {
            // Search for maximum in this column
            let mut max_el = f64::abs(self[(i, i)]);
            let mut max_row = i;
            for k in i + 1..n {
                if f64::abs(self[(k, i)]) > max_el {
                    max_el = f64::abs(self[(k, i)]);
                    max_row = k;
                }
            }

            // Swap maximum row with current row (column by column)
            if max_row != i {
                for k in i..n {
                    let temp = self[(max_row, k)];
                    self[(max_row, k)] = self[(i, k)];
                    self[(i, k)] = temp;
                }
                for k in 0..n {
                    let temp = rhs[(max_row, k)];
                    rhs[(max_row, k)] = rhs[(i, k)];
                    rhs[(i, k)] = temp;
                }
            }

            // Make all rows except this one 0 in current column
            for k in 0..n {
                if k == i {
                    continue;
                }
                let c = -self[(k, i)] / self[(i, i)];
                for j in 0..n {
                    rhs[(k, j)] += c * rhs[(i, j)];
                    if i == j {
                        self[(k, j)] = 0.0;
                    } else if i < j {
                        self[(k, j)] += c * self[(i, j)];
                    }
                }
            }

            // Scale
            for k in 0..n {
                let c = 1.0 / self[(k, k)];
                for j in 0..n {
                    self[(k, j)] *= c;
                    rhs[(k, j)] *= c;
                }
            }
        }

        self.set_expression(&rhs);
    }

    // MARK: Complex getters
    /// Returns sum of all elements.
    pub fn sum(&self) -> f64 {
        let mut ret = 0.0;
        for v in &self._elements {
            ret += *v;
        }
        return ret;
    }

    /// Returns average of all elements.
    pub fn avg(&self) -> f64 {
        return self.sum() / ((self.rows() * self.cols()) as f64);
    }

    /// Returns minimum among all elements.
    pub fn min(&self) -> f64 {
        let mut ret = self._elements[0];
        for v in &self._elements {
            ret = f64::min(ret, *v);
        }
        return ret;
    }

    /// Returns maximum among all elements.
    pub fn max(&self) -> f64 {
        let mut ret = self._elements[0];
        for v in &self._elements {
            ret = f64::max(ret, *v);
        }
        return ret;
    }

    /// Returns absolute minimum among all elements.
    pub fn absmin(&self) -> f64 {
        let mut ret = self._elements[0];
        for v in &self._elements {
            ret = crate::math_utils::absmin(ret, *v);
        }
        return ret;
    }

    /// Returns absolute maximum among all elements.
    pub fn absmax(&self) -> f64 {
        let mut ret = self._elements[0];
        for v in &self._elements {
            ret = crate::math_utils::absmax(ret, *v);
        }
        return ret;
    }

    /// Returns sum of all diagonal elements.
    /// \warning Should be a square matrix.
    pub fn trace(&self) -> f64 {
        debug_assert!(self.is_square());
        let mut ret = 0.0;
        for i in 0..M {
            ret += self[(i, i)];
        }
        return ret;
    }

    /// Returns determinant of this matrix.
    pub fn determinant(&self) -> f64 {
        debug_assert!(self.is_square());

        // Computes inverse matrix using Gaussian elimination method.
        // https://martin-thoma.com/solving-linear-equations-with-gaussian-elimination/
        let n = self.rows();
        let mut a = self.clone();

        let mut result = 1.0;
        for i in 0..n {
            // Search for maximum in this column
            let mut max_el = f64::abs(a[(i, i)]);
            let mut max_row = i;
            for k in i + 1..n {
                if f64::abs(a[(k, i)]) > max_el {
                    max_el = f64::abs(a[(k, i)]);
                    max_row = k;
                }
            }

            // Swap maximum row with current row (column by column)
            if max_row != i {
                for k in i..n {
                    let temp = a[(max_row, k)];
                    a[(max_row, k)] = a[(i, k)];
                    a[(i, k)] = temp;
                }
                result *= -1.0;
            }

            // Make all rows below this one 0 in current column
            for k in i + 1..n {
                let c = -a[(k, i)] / a[(i, i)];
                for j in i..n {
                    if i == j {
                        a[(k, j)] = 0.0;
                    } else {
                        a[(k, j)] += c * a[(i, j)];
                    }
                }
            }
        }

        for i in 0..n {
            result *= a[(i, i)];
        }
        return result;
    }

    /// Returns diagonal part of this matrix.
    pub fn diagonal(&self) -> MatrixDiagonal<Matrix<M, N, TOTAL>> {
        return MatrixDiagonal::new(self, true);
    }

    /// Returns off-diagonal part of this matrix.
    pub fn off_diagonal(&self) -> MatrixDiagonal<Matrix<M, N, TOTAL>> {
        return MatrixDiagonal::new(self, false);
    }

    /// Returns strictly lower triangle part of this matrix.
    pub fn strict_lower_tri(&self) -> MatrixTriangular<Matrix<M, N, TOTAL>> {
        return MatrixTriangular::new(self, false, true);
    }

    /// Returns strictly upper triangle part of this matrix.
    pub fn strict_upper_tri(&self) -> MatrixTriangular<Matrix<M, N, TOTAL>> {
        return MatrixTriangular::new(self, true, true);
    }

    /// Returns lower triangle part of this matrix (including the diagonal).
    pub fn lower_tri(&self) -> MatrixTriangular<Matrix<M, N, TOTAL>> {
        return MatrixTriangular::new(self, false, false);
    }

    /// Returns upper triangle part of this matrix (including the diagonal).
    pub fn upper_tri(&self) -> MatrixTriangular<Matrix<M, N, TOTAL>> {
        return MatrixTriangular::new(self, true, false);
    }

    /// Returns transposed matrix.
    pub fn transposed(&self) -> Matrix<N, M, TOTAL> {
        let mut mt = Matrix::<N, M, TOTAL>::default();
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mt[(j, i)] = self[(i, j)];
            }
        }
        return mt;
    }

    /// Returns inverse matrix.
    pub fn inverse(&self) -> Matrix<M, N, TOTAL> {
        let mut m_inv = self.clone();
        m_inv.invert();
        return m_inv;
    }
}

/// # Builders
impl<const M: usize, const N: usize, const TOTAL: usize> Matrix<M, N, TOTAL> {
    /// Makes a M x N matrix with zeros.
    pub fn make_zero() -> MatrixConstant {
        return MatrixConstant::new(M, N, 0.0);
    }

    /// Makes a M x N matrix with all diagonal elements to 1, and other elements
    /// to 0.
    pub fn make_identity() -> MatrixIdentity {
        debug_assert!(M == N, "Should be a square matrix.");
        return MatrixIdentity::new(M);
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> Index<usize> for Matrix<M, N, TOTAL> {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._elements[index];
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> IndexMut<usize> for Matrix<M, N, TOTAL> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._elements[index];
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> Index<(usize, usize)> for Matrix<M, N, TOTAL> {
    type Output = f64;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        return &self._elements[N * index.0 + index.1];
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> IndexMut<(usize, usize)> for Matrix<M, N, TOTAL> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        return &mut self._elements[N * index.0 + index.1];
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> AddAssign<f64> for Matrix<M, N, TOTAL> {
    fn add_assign(&mut self, rhs: f64) {
        self.iadd_scalar(rhs);
    }
}

impl<E: MatrixExpression, const M: usize, const N: usize, const TOTAL: usize> AddAssign<&E> for Matrix<M, N, TOTAL> {
    fn add_assign(&mut self, rhs: &E) {
        self.iadd_expression(rhs);
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> SubAssign<f64> for Matrix<M, N, TOTAL> {
    fn sub_assign(&mut self, rhs: f64) {
        self.isub_scalar(rhs);
    }
}

impl<E: MatrixExpression, const M: usize, const N: usize, const TOTAL: usize> SubAssign<&E> for Matrix<M, N, TOTAL> {
    fn sub_assign(&mut self, rhs: &E) {
        self.isub_expression(rhs);
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> MulAssign<f64> for Matrix<M, N, TOTAL> {
    fn mul_assign(&mut self, rhs: f64) {
        self.imul_scalar(rhs);
    }
}

impl<const L: usize, const L_TOTAL: usize, const M: usize, const N: usize, const TOTAL: usize> MulAssign<&Matrix<N, L, L_TOTAL>> for Matrix<M, N, TOTAL> {
    fn mul_assign(&mut self, rhs: &Matrix<N, L, L_TOTAL>) {
        self.imul_mat(rhs);
    }
}

impl<const M: usize, const N: usize, const TOTAL: usize> DivAssign<f64> for Matrix<M, N, TOTAL> {
    fn div_assign(&mut self, rhs: f64) {
        self.idiv_scalar(rhs);
    }
}

impl<E: MatrixExpression, const M: usize, const N: usize, const TOTAL: usize> PartialEq<E> for Matrix<M, N, TOTAL> {
    fn eq(&self, other: &E) -> bool {
        return self.is_equal(other);
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod matrix {
    use crate::matrix::Matrix;
    use crate::matrix_expression::MatrixExpression;
    use crate::vector::Vector;
    use crate::assert_delta;

    #[test]
    fn constructors() {
        let mat = Matrix::<2, 3, 6>::default();

        assert_eq!(2, mat.rows());
        assert_eq!(3, mat.cols());

        for elem in mat.data() {
            assert_eq!(0.0, *elem);
        }

        let mat2 = Matrix::<2, 3, 6>::new([1.0, 2.0, 3.0, 4.0, 5.0, 6.0]);

        for i in 0..6 {
            assert_eq!(i as f64 + 1.0, mat2[i]);
        }

        let mat3 = Matrix::<2, 3, 6>::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);

        for i in 0..6 {
            assert_eq!(i as f64 + 1.0, mat3[i]);
        }

        let mat4 = mat3.clone();

        for i in 0..6 {
            assert_eq!(i as f64 + 1.0, mat4[i]);
        }
    }

    #[test]
    fn basic_setters() {
        let mut mat = Matrix::<4, 2, 8>::default();
        mat.set_scalar(5.0);
        assert_eq!(4, mat.rows());
        assert_eq!(2, mat.cols());
        for i in 0..8 {
            assert_eq!(5.0, mat[i]);
        }

        mat.set_scalar(7.0);
        for i in 0..8 {
            assert_eq!(7.0, mat[i]);
        }

        mat.set_slice(&[&[1.0, 2.0], &[3.0, 4.0], &[5.0, 6.0], &[7.0, 8.0]]);
        for i in 0..8 {
            assert_eq!(i as f64 + 1.0, mat[i]);
        }

        let mut mat2 = Matrix::<4, 2, 8>::default();
        mat2.set_expression(&mat);
        for i in 0..8 {
            assert_eq!(i as f64 + 1.0, mat2[i]);
        }

        mat.set_diagonal(10.0);
        for i in 0..8 {
            if i == 0 || i == 3 {
                assert_eq!(10.0, mat[i]);
            } else {
                assert_eq!(mat2[i], mat[i]);
            }
        }

        mat.set_off_diagonal(-1.0);
        for i in 0..8 {
            if i == 0 || i == 3 {
                assert_eq!(10.0, mat[i]);
            } else {
                assert_eq!(-1.0, mat[i]);
            }
        }

        let row = Vector::<2>::new(&[2.0, 3.0]);
        mat.set_row(2, &row);
        for i in 0..8 {
            if i == 0 || i == 3 {
                assert_eq!(10.0, mat[i]);
            } else if i == 4 {
                assert_eq!(2.0, mat[i]);
            } else if i == 5 {
                assert_eq!(3.0, mat[i]);
            } else {
                assert_eq!(-1.0, mat[i]);
            }
        }

        mat.set_slice(&[&[1.0, 2.0], &[3.0, 4.0], &[5.0, 6.0], &[7.0, 8.0]]);
        mat2.set_slice(&[&[1.0, 2.0], &[3.0, 4.0], &[5.0, 6.0], &[7.0, 8.0]]);
        assert_eq!(mat.is_equal(&mat2), true);

        mat2.set_slice(&[&[1.01, 2.01], &[3.01, 4.01], &[4.99, 5.99], &[6.99, 7.99]]);
        assert_eq!(mat.is_similar(&mat2, Some(0.02)), true);
        assert_eq!(mat.is_similar(&mat2, Some(0.005)), false);

        assert_eq!(mat.is_square(), false);
    }

    #[test]
    fn binary_operator_method() {
        let mat_a = Matrix::<2, 3, 6>::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);

        let mut mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.add_scalar(3.5));
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat_b[i]);
        }

        let mut mat_c = Matrix::<2, 3, 6>::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);
        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.add_expression(&mat_c));
        let mut ans = Matrix::<2, 3, 6>::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.sub_scalar(1.5));
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, mat_b[i]);
        }

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.sub_expression(&mat_c));
        ans = Matrix::<2, 3, 6>::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.mul_scalar(2.0));
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat_b[i]);
        }

        let mut mat_d = Matrix::<3, 2, 6>::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]]);
        let mat_e = mat_a.mul_mat(&mat_d);
        assert_eq!(13.0, mat_e.eval(0, 0));
        assert_eq!(41.0, mat_e.eval(0, 1));
        assert_eq!(34.0, mat_e.eval(1, 0));
        assert_eq!(89.0, mat_e.eval(1, 1));

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.div_scalar(2.0));
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, mat_b[i]);
        }

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.radd_scalar(3.5));
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat_b[i]);
        }

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.rsub_scalar(1.5));
        for i in 0..6 {
            assert_eq!(0.5 - i as f64, mat_b[i]);
        }

        mat_c = Matrix::<2, 3, 6>::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);
        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.rsub_expression(&mat_c));
        ans = Matrix::<2, 3, 6>::new_slice(&[&[2.0, -3.0, -1.0], &[5.0, -3.0, 2.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.rmul_scalar(2.0));
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat_b[i]);
        }

        mat_d = Matrix::<3, 2, 6>::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]]);
        let mat_f = mat_d.rmul_mat(&mat_a);
        assert_eq!(13.0, mat_f.eval(0, 0));
        assert_eq!(41.0, mat_f.eval(0, 1));
        assert_eq!(34.0, mat_f.eval(1, 0));
        assert_eq!(89.0, mat_f.eval(1, 1));

        mat_b = Matrix::<2, 3, 6>::new_expression(&mat_a.rdiv_scalar(2.0));
        for i in 0..6 {
            assert_eq!(2.0 / (i as f64 + 1.0), mat_b[i]);
        }
    }

    #[test]
    fn augmented_operator_method() {
        let mat_a = Matrix::<2, 3, 6>::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);
        let mat_b = Matrix::<2, 3, 6>::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);

        let mut mat = mat_a;
        mat.iadd_scalar(3.5);
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat[i]);
        }

        mat = mat_a;
        mat.iadd_expression(&mat_b);
        let mut ans = Matrix::<2, 3, 6>::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a;
        mat.isub_scalar(1.5);
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, mat[i]);
        }

        mat = mat_a;
        mat.isub_expression(&mat_b);
        ans = Matrix::<2, 3, 6>::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a;
        mat.imul_scalar(2.0);
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat[i]);
        }

        let mut mat_a2 = Matrix::<5, 5, 25>::default();
        let mut mat_c2 = Matrix::<5, 5, 25>::default();
        for i in 0..25 {
            mat_a2[i] = i as f64 + 1.0;
            mat_c2[i] = 25.0 - i as f64;
        }
        mat_a2.imul_mat(&mat_c2);

        let ans2 = Matrix::<5, 5, 25>::new_slice(&[
            &[175.0, 160.0, 145.0, 130.0, 115.0],
            &[550.0, 510.0, 470.0, 430.0, 390.0],
            &[925.0, 860.0, 795.0, 730.0, 665.0],
            &[1300.0, 1210.0, 1120.0, 1030.0, 940.0],
            &[1675.0, 1560.0, 1445.0, 1330.0, 1215.0]]);
        assert_eq!(ans2.is_equal(&mat_a2), true);

        mat = mat_a;
        mat.idiv_scalar(2.0);
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, mat[i]);
        }
    }

    #[test]
    fn complex_getters() {
        let mat_a = Matrix::<2, 3, 6>::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);

        assert_eq!(21.0, mat_a.sum());
        assert_eq!(21.0 / 6.0, mat_a.avg());

        let mat_b = Matrix::<2, 3, 6>::new_slice(&[&[3.0, -1.0, 2.0], &[-9.0, 2.0, 8.0]]);
        assert_eq!(-9.0, mat_b.min());
        assert_eq!(8.0, mat_b.max());
        assert_eq!(-1.0, mat_b.absmin());
        assert_eq!(-9.0, mat_b.absmax());

        let mat_c = Matrix::<5, 5, 25>::new_slice(&[
            &[3.0, -1.0, 2.0, 4.0, 5.0],
            &[-9.0, 2.0, 8.0, -1.0, 2.0],
            &[4.0, 3.0, 6.0, 7.0, -5.0],
            &[-2.0, 6.0, 7.0, 1.0, 0.0],
            &[4.0, 2.0, 3.0, 3.0, -9.0]]);
        assert_eq!(3.0, mat_c.trace());

        assert_delta!(-6380.0, mat_c.determinant(), f64::EPSILON);

        let mut mat = Matrix::<2, 3, 6>::new_expression(&mat_a.diagonal());
        let mut ans = Matrix::<2, 3, 6>::new_slice(&[&[1.0, 0.0, 0.0], &[0.0, 5.0, 0.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = Matrix::<2, 3, 6>::new_expression(&mat_a.off_diagonal());
        ans = Matrix::<2, 3, 6>::new_slice(&[&[0.0, 2.0, 3.0], &[4.0, 0.0, 6.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        let mat_cstrict_lower_tri = Matrix::<5, 5, 25>::new_expression(&mat_c.strict_lower_tri());
        let ans_strict_lower_tri = Matrix::<5, 5, 25>::new_slice(&[
            &[0.0, 0.0, 0.0, 0.0, 0.0],
            &[-9.0, 0.0, 0.0, 0.0, 0.0],
            &[4.0, 3.0, 0.0, 0.0, 0.0],
            &[-2.0, 6.0, 7.0, 0.0, 0.0],
            &[4.0, 2.0, 3.0, 3.0, 0.0]]);
        assert_eq!(ans_strict_lower_tri.is_similar(&mat_cstrict_lower_tri, None), true);

        let mat_cstrict_upper_tri = Matrix::<5, 5, 25>::new_expression(&mat_c.strict_upper_tri());
        let ans_strict_upper_tri = Matrix::<5, 5, 25>::new_slice(&[
            &[0.0, -1.0, 2.0, 4.0, 5.0],
            &[0.0, 0.0, 8.0, -1.0, 2.0],
            &[0.0, 0.0, 0.0, 7.0, -5.0],
            &[0.0, 0.0, 0.0, 0.0, 0.0],
            &[0.0, 0.0, 0.0, 0.0, 0.0]]);
        assert_eq!(ans_strict_upper_tri.is_equal(&mat_cstrict_upper_tri), true);

        let mat_clower_tri = Matrix::<5, 5, 25>::new_expression(&mat_c.lower_tri());
        let ans_lower_tri = Matrix::<5, 5, 25>::new_slice(&[
            &[3.0, 0.0, 0.0, 0.0, 0.0],
            &[-9.0, 2.0, 0.0, 0.0, 0.0],
            &[4.0, 3.0, 6.0, 0.0, 0.0],
            &[-2.0, 6.0, 7.0, 1.0, 0.0],
            &[4.0, 2.0, 3.0, 3.0, -9.0]]);
        assert_eq!(ans_lower_tri.is_equal(&mat_clower_tri), true);

        let mat_upper_tri = Matrix::<5, 5, 25>::new_expression(&mat_c.upper_tri());
        let ans_upper_tri = Matrix::<5, 5, 25>::new_slice(&[
            &[3.0, -1.0, 2.0, 4.0, 5.0],
            &[0.0, 2.0, 8.0, -1.0, 2.0],
            &[0.0, 0.0, 6.0, 7.0, -5.0],
            &[0.0, 0.0, 0.0, 1.0, 0.0],
            &[0.0, 0.0, 0.0, 0.0, -9.0]]);
        assert_eq!(ans_upper_tri.is_equal(&mat_upper_tri), true);

        let mat_t = Matrix::<3, 2, 6>::new_expression(&mat_a.transposed());
        let ans_t = Matrix::<3, 2, 6>::new_slice(&[&[1.0, 4.0], &[2.0, 5.0], &[3.0, 6.0]]);
        assert_eq!(ans_t.is_equal(&mat_t), true);

        let mat_i = mat_c;
        let mat2i = mat_i.inverse();
        let ans_i = Matrix::<5, 5, 25>::new_slice(&[
            &[0.260345, -0.0484326, -0.300157, 0.109404, 0.300627],
            &[-0.215517, -0.138715, 0.188871, 0.167712, -0.255486],
            &[0.294828, 0.108307, -0.315831, 0.0498433, 0.363323],
            &[-0.25, -0.0227273, 0.477273, -0.136364, -0.409091],
            &[0.0827586, -0.0238245, -0.0376176, 0.0570533, -0.0495298]]);
        assert_eq!(mat2i.is_similar(&ans_i, Some(1e-6)), true);
    }

    #[test]
    fn modifiers() {
        let mut mat = Matrix::<5, 5, 25>::new_slice(&[
            &[3.0, -1.0, 2.0, 4.0, 5.0],
            &[-9.0, 2.0, 8.0, -1.0, 2.0],
            &[4.0, 3.0, 6.0, 7.0, -5.0],
            &[-2.0, 6.0, 7.0, 1.0, 0.0],
            &[4.0, 2.0, 3.0, 3.0, -9.0]]);
        mat.transpose();

        let mut ans = Matrix::<5, 5, 25>::new_slice(&[
            &[3.0, -9.0, 4.0, -2.0, 4.0],
            &[-1.0, 2.0, 3.0, 6.0, 2.0],
            &[2.0, 8.0, 6.0, 7.0, 3.0],
            &[4.0, -1.0, 7.0, 1.0, 3.0],
            &[5.0, 2.0, -5.0, 0.0, -9.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = Matrix::<5, 5, 25>::new_slice(&[
            &[3.0, -1.0, 2.0, 4.0, 5.0],
            &[-9.0, 2.0, 8.0, -1.0, 2.0],
            &[4.0, 3.0, 6.0, 7.0, -5.0],
            &[-2.0, 6.0, 7.0, 1.0, 0.0],
            &[4.0, 2.0, 3.0, 3.0, -9.0]]);
        mat.invert();
        ans = Matrix::<5, 5, 25>::new_slice(&[
            &[151.0 / 580.0, -309.0 / 6380.0, -383.0 / 1276.0, 349.0 / 3190.0, 959.0 / 3190.0],
            &[-25.0 / 116.0, -177.0 / 1276.0, 241.0 / 1276.0, 107.0 / 638.0, -163.0 / 638.0],
            &[171.0 / 580.0, 691.0 / 6380.0, -403.0 / 1276.0, 159.0 / 3190.0, 1159.0 / 3190.0],
            &[-1.0 / 4.0, -1.0 / 44.0, 21.0 / 44.0, -3.0 / 22.0, -9.0 / 22.0],
            &[12.0 / 145.0, -38.0 / 1595.0, -12.0 / 319.0, 91.0 / 1595.0, -79.0 / 1595.0]]);
        assert_eq!(mat.is_similar(&ans, Some(1e-9)), true);
    }

    #[test]
    fn setter_operators() {
        let mat_a = Matrix::<2, 3, 6>::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);
        let mat_b = Matrix::<2, 3, 6>::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);

        let mut mat = mat_a;
        mat += 3.5;
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat[i]);
        }

        mat = mat_a;
        mat += &mat_b;
        let mut ans = Matrix::<2, 3, 6>::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a;
        mat -= 1.5;
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, mat[i]);
        }

        mat = mat_a;
        mat -= &mat_b;
        ans = Matrix::<2, 3, 6>::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a;
        mat *= 2.0;
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat[i]);
        }

        let mut mat_a2 = Matrix::<5, 5, 25>::default();
        let mut mat_c2 = Matrix::<5, 5, 25>::default();
        for i in 0..25 {
            mat_a2[i] = i as f64 + 1.0;
            mat_c2[i] = 25.0 - i as f64;
        }
        mat_a2 *= &mat_c2;

        let ans2 = Matrix::<5, 5, 25>::new_slice(&[
            &[175.0, 160.0, 145.0, 130.0, 115.0],
            &[550.0, 510.0, 470.0, 430.0, 390.0],
            &[925.0, 860.0, 795.0, 730.0, 665.0],
            &[1300.0, 1210.0, 1120.0, 1030.0, 940.0],
            &[1675.0, 1560.0, 1445.0, 1330.0, 1215.0]]);
        assert_eq!(ans2.is_equal(&mat_a2), true);

        mat = mat_a;
        mat /= 2.0;
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, mat[i]);
        }
    }

    #[test]
    fn getter_operator() {
        let mut mat = Matrix::<2, 4, 8>::default();
        let mut mat2 = Matrix::<2, 4, 8>::default();
        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        let mut cnt = 1.0;
        for i in 0..2 {
            for j in 0..4 {
                assert_eq!(cnt, mat[(i, j)]);
                cnt += 1.0;
            }
        }

        for i in 0..8 {
            assert_eq!(i as f64 + 1.0, mat[i]);
        }

        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        mat2.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        assert_eq!(mat.is_equal(&mat2), true);
    }

    #[test]
    fn builders() {
        let mat = Matrix::<3, 4, 12>::new_expression(&Matrix::<3, 4, 12>::make_zero());
        for i in 0..12 {
            assert_eq!(0.0, mat[i]);
        }

        let mat2 = Matrix::<5, 5, 25>::new_expression(&Matrix::<5, 5, 25>::make_identity());
        for i in 0..25 {
            if i % 6 == 0 {
                assert_eq!(1.0, mat2[i]);
            } else {
                assert_eq!(0.0, mat2[i]);
            }
        }
    }
}