/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::vector2::Vector2;

///
/// # Class for 2-D ray.
///
/// - tparam     T     The value type.
///
pub struct Ray2<T: Float> {
    /// The origin of the ray.
    pub origin: Vector2<T>,

    /// The direction of the ray.
    pub direction: Vector2<T>,
}

/// Float-type 2-D ray.
pub type Ray2F = Ray2<f32>;

/// Double-type 2-D ray.
pub type Ray2D = Ray2<f64>;

impl<T: Float> Ray2<T> {
    /// Constructs an empty ray that points (1, 0) from (0, 0).
    pub fn default() -> Ray2<T> {
        return Ray2 {
            origin: Vector2::default(),
            direction: Vector2::new(T::one(), T::zero()),
        };
    }

    /// Constructs a ray with given origin and direction.
    pub fn new(new_origin: Vector2<T>, new_direction: Vector2<T>) -> Ray2<T> {
        return Ray2 {
            origin: new_origin,
            direction: new_direction.normalized(),
        };
    }

    /// Returns a point on the ray at distance **t**.
    pub fn point_at(&self, t: T) -> Vector2<T> {
        return self.origin + self.direction * t;
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod ray2 {
    #[test]
    fn constructors() {
        todo!()
    }

    #[test]
    fn point_at() {
        todo!()
    }
}