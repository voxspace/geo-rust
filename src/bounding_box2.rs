/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::vector2::Vector2;
use crate::ray2::Ray2;
use std::mem::swap;
use std::fmt::{Debug, Formatter, Result};

///
/// #      2-D box-ray intersection result.
///
/// - tparam     T     The value type.
///
pub struct BoundingBoxRayIntersection2<T: Float> {
    /// True if the box and ray intersects.
    pub is_intersecting: bool,

    /// Distance to the first intersection point.
    pub t_near: T,

    /// Distance to the second (and the last) intersection point.
    pub t_far: T,
}

/// Float-type 2-D box-ray intersection result.
pub type BoundingBoxRayIntersection2F = BoundingBoxRayIntersection2<f32>;

/// Double-type 2-D box-ray intersection result.
pub type BoundingBoxRayIntersection2D = BoundingBoxRayIntersection2<f64>;

impl<T: Float> BoundingBoxRayIntersection2<T> {
    pub fn new() -> BoundingBoxRayIntersection2<T> {
        return BoundingBoxRayIntersection2 {
            is_intersecting: false,
            t_near: T::max_value(),
            t_far: T::max_value(),
        };
    }
}

///
/// # 2-D axis-aligned bounding box class.
///
/// - tparam T - Real number type.
/// - tparam N - Dimension.
///
pub struct BoundingBox2<T: Float> {
    /// Lower corner of the bounding box.
    pub lower_corner: Vector2<T>,

    /// Upper corner of the bounding box.
    pub upper_corner: Vector2<T>,
}

/// Float-type 2-D BoundingBox.
pub type BoundingBox2F = BoundingBox2<f32>;

/// Double-type 2-D BoundingBox.
pub type BoundingBox2D = BoundingBox2<f64>;

/// # Constructor
impl<T: Float> BoundingBox2<T> {
    /// Default constructor.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::constants::K_MAX_D;
    /// let aabb = BoundingBox2D::default();
    ///
    /// assert_eq!(K_MAX_D, aabb.lower_corner.x);
    /// assert_eq!(K_MAX_D, aabb.lower_corner.y);
    ///
    /// assert_eq!(-K_MAX_D, aabb.upper_corner.x);
    /// assert_eq!(-K_MAX_D, aabb.upper_corner.y);
    /// ```
    pub fn default() -> BoundingBox2<T> {
        return BoundingBox2 {
            lower_corner: Vector2::new(T::max_value(), T::max_value()),
            upper_corner: Vector2::new(-T::max_value(), -T::max_value()),
        };
    }

    /// Constructs a box that tightly covers two points.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, 3.0), Vector2D::new(4.0, -2.0));
    ///
    /// assert_eq!(-2.0, aabb.lower_corner.x);
    /// assert_eq!(-2.0, aabb.lower_corner.y);
    ///
    /// assert_eq!(4.0, aabb.upper_corner.x);
    /// assert_eq!(3.0, aabb.upper_corner.y);
    /// ```
    pub fn new(point1: Vector2<T>, point2: Vector2<T>) -> BoundingBox2<T> {
        return BoundingBox2 {
            lower_corner: Vector2::new(T::min(point1.x, point2.x),
                                       T::min(point1.y, point2.y)),
            upper_corner: Vector2::new(T::max(point1.x, point2.x),
                                       T::max(point1.y, point2.y)),
        };
    }
}

impl<T: Float> BoundingBox2<T> {
    /// Returns width of the box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, 3.0), Vector2D::new(4.0, -2.0));
    ///
    /// assert_eq!(6.0, aabb.width());
    /// ```
    pub fn width(&self) -> T {
        return self.upper_corner.x - self.lower_corner.x;
    }

    /// Returns height of the box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, 3.0), Vector2D::new(4.0, -2.0));
    ///
    /// assert_eq!(5.0, aabb.height());
    /// ```
    pub fn height(&self) -> T {
        return self.upper_corner.y - self.lower_corner.y;
    }

    /// Returns length of the box in given axis.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, 3.0), Vector2D::new(4.0, -2.0));
    ///
    /// assert_eq!(6.0, aabb.length(0));
    /// assert_eq!(5.0, aabb.length(1));
    /// ```
    pub fn length(&self, axis: usize) -> T {
        return self.upper_corner[axis] - self.lower_corner[axis];
    }

    /// Returns true of this box and other box overlaps.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// // x-axis is not overlapping
    /// {
    ///     let box1 = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///     let box2 = BoundingBox2D::new(Vector2D::new(5.0, 1.0), Vector2D::new(8.0, 2.0));
    ///
    ///     assert_eq!(box1.overlaps(&box2), false);
    /// }
    ///
    /// // y-axis is not overlapping
    /// {
    ///     let box1 = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///     let box2 = BoundingBox2D::new(Vector2D::new(3.0, 4.0), Vector2D::new(8.0, 6.0));
    ///
    ///     assert_eq!(box1.overlaps(&box2), false);
    /// }
    ///
    /// // overlapping
    /// {
    ///     let box1 = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///     let box2 = BoundingBox2D::new(Vector2D::new(3.0, 1.0), Vector2D::new(8.0, 2.0));
    ///
    ///     assert_eq!(box1.overlaps(&box2), true);
    /// }
    /// ```
    pub fn overlaps(&self, other: &BoundingBox2<T>) -> bool {
        if self.upper_corner.x < other.lower_corner.x ||
            self.lower_corner.x > other.upper_corner.x {
            return false;
        }

        if self.upper_corner.y < other.lower_corner.y ||
            self.lower_corner.y > other.upper_corner.y {
            return false;
        }

        return true;
    }

    /// Returns true if the input point is inside of this box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// // Not containing (x-axis is out)
    /// {
    ///     let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///     let point = Vector2D::new(-3.0, 0.0);
    ///
    ///     assert_eq!(aabb.contains(&point), false);
    /// }
    ///
    /// // Not containing (y-axis is out)
    /// {
    ///     let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///     let point = Vector2D::new(2.0, 3.5);
    ///
    ///     assert_eq!(aabb.contains(&point), false);
    /// }
    ///
    /// // Containing
    /// {
    ///     let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///     let point = Vector2D::new(2.0, 0.0);
    ///
    ///     assert_eq!(aabb.contains(&point), true);
    /// }
    /// ```
    pub fn contains(&self, point: &Vector2<T>) -> bool {
        if self.upper_corner.x < point.x || self.lower_corner.x > point.x {
            return false;
        }

        if self.upper_corner.y < point.y || self.lower_corner.y > point.y {
            return false;
        }

        return true;
    }

    /// Returns true if the input ray is intersecting with this box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::ray2::Ray2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    ///
    /// let ray1 = Ray2D::new(Vector2D::new(-3.0, 0.0), Vector2D::new(2.0, 1.0).normalized());
    /// assert_eq!(aabb.intersects(&ray1), true);
    ///
    /// let ray2 = Ray2D::new(Vector2D::new(3.0, -1.0), Vector2D::new(-1.0, 2.0).normalized());
    /// assert_eq!(aabb.intersects(&ray2), true);
    ///
    /// let ray3 = Ray2D::new(Vector2D::new(1.0, -5.0), Vector2D::new(2.0, 1.0).normalized());
    /// assert_eq!(aabb.intersects(&ray3), false);
    /// ```
    pub fn intersects(&self, ray: &Ray2<T>) -> bool {
        let mut t_min = T::zero();
        let mut t_max = T::max_value();

        let ray_inv_dir = ray.direction.rdiv_scalar(T::one());

        for i in 0..2 {
            let mut t_near = (self.lower_corner[i] - ray.origin[i]) * ray_inv_dir[i];
            let mut t_far = (self.upper_corner[i] - ray.origin[i]) * ray_inv_dir[i];

            if t_near > t_far {
                swap(&mut t_near, &mut t_far);
            }

            t_min = T::max(t_near, t_min);
            t_max = T::min(t_far, t_max);

            if t_min > t_max {
                return false;
            }
        }

        return true;
    }

    /// Returns intersection.isIntersecting = true if the input ray is
    /// intersecting with this box. If intersects, intersection.tNear is
    /// assigned with distant to the closest intersecting point, and
    /// intersection.tFar with furthest.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::bounding_box2::BoundingBoxRayIntersection2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::ray2::Ray2D;
    ///  use vox_geometry_rust::assert_delta;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(1.0, 0.0));
    ///
    /// let ray1 = Ray2D::new(Vector2D::new(-4.0, -3.0), Vector2D::new(1.0, 1.0).normalized());
    /// let intersection1 = aabb.closest_intersection(&ray1);
    /// assert_eq!(intersection1.is_intersecting, true);
    /// assert_delta!(Vector2D::new(2.0, 2.0).length(), intersection1.t_near, f64::EPSILON);
    /// assert_eq!(Vector2D::new(3.0, 3.0).length(), intersection1.t_far);
    ///
    /// let ray2 = Ray2D::new(Vector2D::new(0.0, -1.0), Vector2D::new(-2.0, 1.0).normalized());
    /// let intersection2 = aabb.closest_intersection(&ray2);
    /// assert_eq!(intersection2.is_intersecting, true);
    /// assert_delta!(Vector2D::new(2.0, 1.0).length(), intersection2.t_near, f64::EPSILON);
    /// ```
    pub fn closest_intersection(&self, ray: &Ray2<T>) -> BoundingBoxRayIntersection2<T> {
        let mut intersection = BoundingBoxRayIntersection2::new();

        let mut t_min = T::zero();
        let mut t_max = T::max_value();

        let ray_inv_dir = ray.direction.rdiv_scalar(T::one());

        for i in 0..2 {
            let mut t_near = (self.lower_corner[i] - ray.origin[i]) * ray_inv_dir[i];
            let mut t_far = (self.upper_corner[i] - ray.origin[i]) * ray_inv_dir[i];

            if t_near > t_far {
                swap(&mut t_near, &mut t_far);
            }

            t_min = T::max(t_near, t_min);
            t_max = T::min(t_far, t_max);

            if t_min > t_max {
                intersection.is_intersecting = false;
                return intersection;
            }
        }

        intersection.is_intersecting = true;

        if self.contains(&ray.origin) {
            intersection.t_near = t_max;
            intersection.t_far = T::max_value();
        } else {
            intersection.t_near = t_min;
            intersection.t_far = t_max;
        }

        return intersection;
    }

    /// Returns the mid-point of this box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// let mid_point = aabb.mid_point();
    ///
    /// assert_eq!(1.0, mid_point.x);
    /// assert_eq!(0.5, mid_point.y);
    /// ```
    pub fn mid_point(&self) -> Vector2<T> {
        return (self.upper_corner + self.lower_corner) / T::from(2.0).unwrap();
    }

    /// Returns diagonal length of this box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// let diagonal_length = aabb.diagonal_length();
    ///
    /// assert_eq!(f64::sqrt(6.0 * 6.0 + 5.0 * 5.0), diagonal_length);
    /// ```
    pub fn diagonal_length(&self) -> T {
        return (self.upper_corner - self.lower_corner).length();
    }

    /// Returns squared diagonal length of this box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// let diagonal_length_squared = aabb.diagonal_length_squared();
    ///
    /// assert_eq!(6.0 * 6.0 + 5.0 * 5.0, diagonal_length_squared);
    /// ```
    pub fn diagonal_length_squared(&self) -> T {
        return (self.upper_corner - self.lower_corner).length_squared();
    }

    /// Resets this box to initial state (min=infinite, max=-infinite).
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let mut aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// aabb.reset();
    ///
    /// let max_double = f64::MAX;
    ///
    /// assert_eq!(max_double, aabb.lower_corner.x);
    /// assert_eq!(max_double, aabb.lower_corner.y);
    ///
    /// assert_eq!(-max_double, aabb.upper_corner.x);
    /// assert_eq!(-max_double, aabb.upper_corner.y);
    /// ```
    pub fn reset(&mut self) {
        self.lower_corner.x = T::max_value();
        self.lower_corner.y = T::max_value();
        self.upper_corner.x = -T::max_value();
        self.upper_corner.y = -T::max_value();
    }

    /// Merges this and other point.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let mut aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// let point = Vector2D::new(5.0, 1.0);
    /// 
    /// aabb.merge_vec(&point);
    /// 
    /// assert_eq!(-2.0, aabb.lower_corner.x);
    /// assert_eq!(-2.0, aabb.lower_corner.y);
    /// 
    /// assert_eq!(5.0, aabb.upper_corner.x);
    /// assert_eq!(3.0, aabb.upper_corner.y);
    /// ```
    pub fn merge_vec(&mut self, point: &Vector2<T>) {
        self.lower_corner.x = T::min(self.lower_corner.x, point.x);
        self.lower_corner.y = T::min(self.lower_corner.y, point.y);
        self.upper_corner.x = T::max(self.upper_corner.x, point.x);
        self.upper_corner.y = T::max(self.upper_corner.y, point.y);
    }

    /// Merges this and other box.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let mut box1 = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// let box2 = BoundingBox2D::new(Vector2D::new(3.0, 1.0), Vector2D::new(8.0, 2.0));
    ///
    /// box1.merge_box(&box2);
    ///
    /// assert_eq!(-2.0, box1.lower_corner.x);
    /// assert_eq!(-2.0, box1.lower_corner.y);
    ///
    /// assert_eq!(8.0, box1.upper_corner.x);
    /// assert_eq!(3.0, box1.upper_corner.y);
    /// ```
    pub fn merge_box(&mut self, other: &BoundingBox2<T>) {
        self.lower_corner.x = T::min(self.lower_corner.x, other.lower_corner.x);
        self.lower_corner.y = T::min(self.lower_corner.y, other.lower_corner.y);
        self.upper_corner.x = T::max(self.upper_corner.x, other.upper_corner.x);
        self.upper_corner.y = T::max(self.upper_corner.y, other.upper_corner.y);
    }

    /// Expands this box by given delta to all direction.
    /// If the width of the box was x, expand(y) will result a box with
    /// x+y+y width.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let mut aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// aabb.expand(3.0);
    ///
    /// assert_eq!(-5.0, aabb.lower_corner.x);
    /// assert_eq!(-5.0, aabb.lower_corner.y);
    ///
    /// assert_eq!(7.0, aabb.upper_corner.x);
    /// assert_eq!(6.0, aabb.upper_corner.y);
    /// ```
    pub fn expand(&mut self, delta: T) {
        self.lower_corner -= delta;
        self.upper_corner += delta;
    }

    /// Returns corner position. Index starts from x-first order.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// assert_eq!(Vector2D::new(-2.0, -2.0), aabb.corner(0));
    /// assert_eq!(Vector2D::new(4.0, -2.0), aabb.corner(1));
    /// assert_eq!(Vector2D::new(-2.0, 3.0), aabb.corner(2));
    /// assert_eq!(Vector2D::new(4.0, 3.0), aabb.corner(3));
    /// ```
    pub fn corner(&self, idx: usize) -> Vector2<T> {
        let h = T::from(0.5).unwrap();
        let offset = [
            [-h, -h], [h, -h], [-h, h], [h, h]];

        return Vector2::new(self.width(), self.height()) * Vector2::new_slice(&offset[idx]) + self.mid_point();
    }

    /// Returns the clamped point.
    pub fn clamp(&self, pt: &Vector2<T>) -> Vector2<T> {
        return crate::vector2::clamp(&pt, &self.lower_corner, &self.upper_corner);
    }

    /// Returns true if the box is empty.
    /// ```
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let mut aabb = BoundingBox2D::new(Vector2D::new(-2.0, -2.0), Vector2D::new(4.0, 3.0));
    /// assert_eq!(aabb.is_empty(), false);
    /// 
    /// aabb.lower_corner = Vector2D::new(5.0, 1.0);
    /// assert_eq!(aabb.is_empty(), true);
    /// 
    /// aabb.lower_corner = Vector2D::new(2.0, 4.0);
    /// assert_eq!(aabb.is_empty(), true);
    /// 
    /// aabb.lower_corner = Vector2D::new(4.0, 1.0);
    /// assert_eq!(aabb.is_empty(), true);
    /// ```
    pub fn is_empty(&self) -> bool {
        return self.lower_corner.x >= self.upper_corner.x || self.lower_corner.y >= self.upper_corner.y;
    }
}

impl<T: Float + Debug> Debug for BoundingBox2<T> {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::vector2::Vector2F;
    /// let vec = Vector2F::new(10.0, 20.0);
    /// assert_eq!(format!("{:?}", vec), "(10.0, 20.0)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10.0,
    ///     20.0,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.lower_corner)
            .field(&self.upper_corner)
            .finish()
    }
}

impl<T: Float> Clone for BoundingBox2<T> {
    fn clone(&self) -> Self {
        return BoundingBox2 {
            lower_corner: self.lower_corner,
            upper_corner: self.upper_corner,
        };
    }
}