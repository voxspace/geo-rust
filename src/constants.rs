/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;

// MARK: Zero

/// Zero size_t.
pub const K_ZERO_SIZE: usize = 0;

/// Zero ssize_t.
pub const K_ZERO_SSIZE: isize = 0;

/// Zero for type T.
pub fn zero<T: Float>() -> T {
    return T::from(0.0).unwrap();
}

// MARK: One

/// One size_t.
pub const K_ONE_SIZE: usize = 1;

/// One ssize_t.
pub const K_ONE_SSIZE: isize = 1;

/// One for type T.
pub fn one<T: Float>() -> T {
    return T::from(1.0).unwrap();
}

// MARK: Epsilon

/// Float-type epsilon.
pub const K_EPSILON_F: f32 = f32::EPSILON;

/// Double-type epsilon.
pub const K_EPSILON_D: f64 = f64::EPSILON;

// MARK: Max

/// Max size_t.
pub const K_MAX_SIZE: usize = usize::MAX;

/// Max ssize_t.
pub const K_MAX_SSIZE: isize = isize::MAX;

/// Max float.
pub const K_MAX_F: f32 = f32::MAX;

/// Max double.
pub const K_MAX_D: f64 = f64::MAX;

// MARK: Pi

/// Float-type pi.
pub const K_PI_F: f32 = std::f32::consts::PI;

/// Double-type pi.
pub const K_PI_D: f64 = std::f64::consts::PI;

/// Pi for type T.
pub fn pi<T: Float>() -> T {
    return T::from(K_PI_D).unwrap();
}

// MARK: Pi/2

/// Float-type pi/2.
pub const K_HALF_PI_F: f32 = std::f32::consts::FRAC_PI_2;

/// Double-type pi/2.
pub const K_HALF_PI_D: f64 = std::f64::consts::FRAC_PI_2;

/// Pi/2 for type T.
pub fn half_pi<T: Float>() -> T {
    return T::from(K_HALF_PI_D).unwrap();
}

// MARK: Pi/4

/// Float-type pi/4.
pub const K_QUARTER_PI_F: f32 = std::f32::consts::FRAC_PI_4;

/// Double-type pi/4.
pub const K_QUARTER_PI_D: f64 = std::f64::consts::FRAC_PI_4;

/// Pi/4 for type T.
pub fn quarter_pi<T: Float>() -> T {
    return T::from(K_QUARTER_PI_D).unwrap();
}

// MARK: 2*Pi

/// Float-type 2*pi.
pub const K_TWO_PI_F: f32 = std::f32::consts::TAU;

/// Double-type 2*pi.
pub const K_TWO_PI_D: f64 = std::f64::consts::TAU;

/// 2*pi for type T.
pub fn two_pi<T: Float>() -> T {
    return T::from(K_TWO_PI_D).unwrap();
}

// MARK: 4*Pi

/// Float-type 4*pi.
pub const K_FOUR_PI_F: f32 = 2.0 * std::f32::consts::TAU;

/// Double-type 4*pi.
pub const K_FOUR_PI_D: f64 = 2.0 * std::f64::consts::TAU;

/// 4*pi for type T.
pub fn four_pi<T: Float>() -> T {
    return T::from(K_FOUR_PI_D).unwrap();
}

// MARK: 1/Pi

/// Float-type 1/pi.
pub const K_INV_PI_F: f32 = std::f32::consts::FRAC_1_PI;

/// Double-type 1/pi.
pub const K_INV_PI_D: f64 = std::f64::consts::FRAC_1_PI;

/// 1/pi for type T.
pub fn inv_pi<T: Float>() -> T {
    return T::from(K_INV_PI_D).unwrap();
}

// MARK: 1/2*Pi

/// Float-type 1/2*pi.
pub const K_INV_TWO_PI_F: f32 = std::f32::consts::FRAC_1_PI * 0.5;

/// Double-type 1/2*pi.
pub const K_INV_TWO_PI_D: f64 = std::f64::consts::FRAC_1_PI * 0.5;

/// 1/2*pi for type T.
pub fn inv_two_pi<T: Float>() -> T {
    return T::from(K_INV_TWO_PI_D).unwrap();
}

// MARK: 1/4*Pi

/// Float-type 1/4*pi.
pub const K_INV_FOUR_PI_F: f32 = std::f32::consts::FRAC_1_PI * 0.25;

/// Double-type 1/4*pi.
pub const K_INV_FOUR_PI_D: f64 = std::f64::consts::FRAC_1_PI * 0.25;

/// 1/4*pi for type T.
pub fn inv_four_pi<T: Float>() -> T {
    return T::from(K_INV_FOUR_PI_D).unwrap();
}

// MARK: Physics

/// Gravity.
pub const K_GRAVITY: f64 = -9.8;

/// Water density.
pub const K_WATER_DENSITY: f64 = 1000.0;

/// Speed of sound in water at 20 degrees Celsius.
pub const K_SPEED_OF_SOUND_IN_WATER: f64 = 1482.0;

// MARK: Common enums

/// No direction.
pub const K_DIRECTION_NONE: i32 = 0;

/// Left direction.
pub const K_DIRECTION_LEFT: i32 = 1 << 0;

/// Right direction.
pub const K_DIRECTION_RIGHT: i32 = 1 << 1;

/// Down direction.
pub const K_DIRECTION_DOWN: i32 = 1 << 2;

/// Up direction.
pub const K_DIRECTION_UP: i32 = 1 << 3;

/// Back direction.
pub const K_DIRECTION_BACK: i32 = 1 << 4;

/// Front direction.
pub const K_DIRECTION_FRONT: i32 = 1 << 5;

/// All direction.
pub const K_DIRECTION_ALL: i32 = K_DIRECTION_LEFT | K_DIRECTION_RIGHT |
    K_DIRECTION_DOWN | K_DIRECTION_UP | K_DIRECTION_BACK |
    K_DIRECTION_FRONT;


