/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::ray2::Ray2D;
use crate::bounding_box2::BoundingBox2D;
use crate::intersection_query_engine2::*;
use crate::nearest_neighbor_query_engine2::*;
use std::cmp::Ordering;

#[derive(Clone)]
struct Node {
    first_child: usize,
    items: Vec<usize>,
}

impl Node {
    fn new() -> Node {
        return Node {
            first_child: usize::MAX,
            items: vec![],
        };
    }

    fn is_leaf(&self) -> bool {
        return self.first_child == usize::MAX;
    }
}

///
/// #  Generic quadtree data structure.
///
/// This class is a generic quadtree representation to store arbitrary spatial
/// data. The quadtree supports closest neighbor search, overlapping test, and
/// ray intersection test.
///
/// - tparam     T     Value type.
///
pub struct Quadtree<T: Clone> {
    _max_depth: usize,
    _bbox: BoundingBox2D,
    _items: Vec<T>,
    _nodes: Vec<Node>,
}

impl<T: Clone> Quadtree<T> {
    /// Default constructor.
    /// ```
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::quadtree::Quadtree;
    /// let quadtree:Quadtree<Vector2D> = Quadtree::new();
    /// assert_eq!(quadtree.number_of_nodes(), 0);
    /// ```
    pub fn new() -> Quadtree<T> {
        return Quadtree {
            _max_depth: 0,
            _bbox: BoundingBox2D::default(),
            _items: vec![],
            _nodes: vec![],
        };
    }

    /// Builds an quadtree with given list of items, bounding box of the items,
    /// overlapping test function, and max depth of the tree.
    pub fn build<Func>(&mut self, items: &Vec<T>, bound: &BoundingBox2D,
                       test_func: &mut Func, max_depth: usize) where Func: BoxIntersectionTestFunc2<T> {
        // Reset items
        self._max_depth = max_depth;
        self._items = items.clone();
        self._nodes.clear();

        // Normalize bounding box
        self._bbox = bound.clone();
        let max_edge_len = f64::max(self._bbox.width(), self._bbox.height());
        self._bbox.upper_corner = self._bbox.lower_corner + Vector2D::new(max_edge_len as f64, max_edge_len as f64);

        // Build
        self._nodes.resize(1, Node::new());
        self._nodes[0].items = (0..self._items.len()).collect();

        let aabb = self._bbox.clone();
        self.build_internal(0, 1, &aabb, test_func);
    }

    /// Clears all the contents of this instance.
    pub fn clear(&mut self) {
        self._max_depth = 1;
        self._items.clear();
        self._nodes.clear();
        self._bbox = BoundingBox2D::default();
    }

    /// Returns the number of items.
    pub fn number_of_items(&self) -> usize {
        return self._items.len();
    }

    /// Returns the item at \p i.
    pub fn item(&self, i: usize) -> T {
        return self._items[i].clone();
    }

    /// Returns the number of quadtree nodes.
    pub fn number_of_nodes(&self) -> usize {
        return self._nodes.len();
    }

    /// Returns the list of the items for given node index.
    pub fn items_at_node(&self, node_idx: usize) -> Vec<usize> {
        return self._nodes[node_idx].clone().items;
    }

    ///
    /// # Returns a child's index for given node.
    ///
    /// For a given node, its children is stored continuously, such that if the
    /// node's first child's index is i, then i + 1, i + 2, ... , i + 7 are the
    /// indices for its children. The order of octant is x-major.
    ///
    /// - parameter:  node_idx The node index.
    /// - parameter:  child_idx The child index (0 to 7).
    ///
    /// - return     Index of the selected child.
    ///
    pub fn child_index(&self, node_idx: usize, child_idx: usize) -> usize {
        return self._nodes[node_idx].first_child + child_idx;
    }

    /// Returns the bounding box of this quadtree.
    pub fn bounding_box(&self) -> BoundingBox2D {
        return self._bbox.clone();
    }

    /// Returns the maximum depth of the tree.
    pub fn max_depth(&self) -> usize {
        return self._max_depth;
    }
}

impl<T: Clone> Quadtree<T> {
    fn build_internal<Func>(&mut self, node_idx: usize, depth: usize,
                            bound: &BoundingBox2D,
                            test_func: &mut Func)
        where Func: BoxIntersectionTestFunc2<T> {
        if depth < self._max_depth && !self._nodes[node_idx].items.is_empty() {
            let first_child = self._nodes.len();
            self._nodes[node_idx].first_child = self._nodes.len();
            self._nodes.resize(self._nodes[node_idx].first_child + 4, Node::new());

            let mut bbox_per_node = [
                BoundingBox2D::default(),
                BoundingBox2D::default(),
                BoundingBox2D::default(),
                BoundingBox2D::default()];

            for i in 0..4 {
                bbox_per_node[i] = BoundingBox2D::new(bound.corner(i), bound.mid_point());
            }

            for i in 0..self._nodes[node_idx].items.len() {
                let current_item = self._nodes[node_idx].items[i];
                for j in 0..4 {
                    if (*test_func)(&self._items[current_item], &bbox_per_node[j]) {
                        self._nodes[first_child + j].items.push(current_item);
                    }
                }
            }

            // Remove non-leaf data
            self._nodes[node_idx].items.clear();

            // Refine
            for i in 0..4 {
                self.build_internal(first_child + i, depth + 1, &bbox_per_node[i], test_func);
            }
        }
    }

    fn intersects_aabb<Func>(&self, aabb: &BoundingBox2D,
                             test_func: &mut Func, node_idx: usize,
                             bound: &BoundingBox2D) -> bool
        where Func: BoxIntersectionTestFunc2<T> {
        if !aabb.overlaps(bound) {
            return false;
        }

        let node = &self._nodes[node_idx];

        if node.items.len() > 0 {
            for item_idx in &node.items {
                if test_func(&self._items[*item_idx], aabb) {
                    return true;
                }
            }
        }

        if node.first_child != usize::MAX {
            for i in 0..4 {
                if self.intersects_aabb(aabb, test_func, node.first_child + i,
                                        &BoundingBox2D::new(bound.corner(i),
                                                            bound.mid_point())) {
                    return true;
                }
            }
        }

        return false;
    }

    fn intersects_ray<Func>(&self, ray: &Ray2D,
                            test_func: &mut Func, node_idx: usize,
                            bound: &BoundingBox2D) -> bool
        where Func: RayIntersectionTestFunc2<T> {
        if !bound.intersects(ray) {
            return false;
        }

        let node = &self._nodes[node_idx];

        if node.items.len() > 0 {
            for item_idx in &node.items {
                if test_func(&self._items[*item_idx], ray) {
                    return true;
                }
            }
        }

        if node.first_child != usize::MAX {
            for i in 0..4 {
                if self.intersects_ray(ray, test_func, node.first_child + i,
                                       &BoundingBox2D::new(bound.corner(i),
                                                           bound.mid_point())) {
                    return true;
                }
            }
        }

        return false;
    }

    fn for_each_intersecting_item_aabb<TestFunc, Visitor>(&self, aabb: &BoundingBox2D,
                                                          test_func: &mut TestFunc,
                                                          visitor_func: &mut Visitor,
                                                          node_idx: usize,
                                                          bound: &BoundingBox2D)
        where TestFunc: BoxIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T>, {
        if !aabb.overlaps(bound) {
            return;
        }

        let node = &self._nodes[node_idx];

        if node.items.len() > 0 {
            for item_idx in &node.items {
                if test_func(&self._items[*item_idx], aabb) {
                    visitor_func(&self._items[*item_idx]);
                }
            }
        }

        if node.first_child != usize::MAX {
            for i in 0..4 {
                self.for_each_intersecting_item_aabb(
                    aabb, test_func, visitor_func, node.first_child + i,
                    &BoundingBox2D::new(bound.corner(i), bound.mid_point()));
            }
        }
    }

    fn for_each_intersecting_item_ray<TestFunc, Visitor>(&self, ray: &Ray2D,
                                                         test_func: &mut TestFunc,
                                                         visitor_func: &mut Visitor,
                                                         node_idx: usize,
                                                         bound: &BoundingBox2D)
        where TestFunc: RayIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T> {
        if !bound.intersects(ray) {
            return;
        }

        let node = &self._nodes[node_idx];

        if node.items.len() > 0 {
            for item_idx in &node.items {
                if test_func(&self._items[*item_idx], ray) {
                    visitor_func(&self._items[*item_idx]);
                }
            }
        }

        if node.first_child != usize::MAX {
            for i in 0..4 {
                self.for_each_intersecting_item_ray(
                    ray, test_func, visitor_func, node.first_child + i,
                    &BoundingBox2D::new(bound.corner(i), bound.mid_point()));
            }
        }
    }

    fn closest_intersection<Func>(&self, ray: &Ray2D, test_func: &mut Func,
                                  node_idx: usize, bound: &BoundingBox2D,
                                  best: ClosestIntersectionQueryResult2<T>) -> ClosestIntersectionQueryResult2<T>
        where Func: GetRayIntersectionFunc2<T> {
        let mut best = best;

        if !bound.intersects(ray) {
            return best;
        }

        let node = &self._nodes[node_idx];

        if node.items.len() > 0 {
            for item_idx in &node.items {
                let dist = test_func(&self._items[*item_idx], ray);
                if dist < best.distance {
                    best.distance = dist;
                    best.item = Some(self._items[*item_idx].clone());
                }
            }
        }

        if node.first_child != usize::MAX {
            for i in 0..4 {
                best = self.closest_intersection(
                    ray, test_func, node.first_child + i,
                    &BoundingBox2D::new(bound.corner(i), bound.mid_point()), best);
            }
        }

        return best;
    }
}

impl<T: Clone> IntersectionQueryEngine2<T> for Quadtree<T> {
    /// ```
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::quadtree::Quadtree;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// use vox_geometry_rust::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
    /// use vox_geometry_rust::intersection_query_engine2::IntersectionQueryEngine2;
    /// let mut quadtree:Quadtree<Vector2D> = Quadtree::new();
    ///
    /// let mut overlaps_func = |pt:&Vector2D, bbox:&BoundingBox2D| {
    ///         return bbox.contains(pt);
    ///     };
    ///
    /// let num_samples = get_number_of_sample_points2();
    /// let points = (0..100).map(|index| Vector2D::new_slice(get_sample_points2()[index])).collect();
    ///
    /// quadtree.build(&points, &BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 1.0)), &mut overlaps_func, 5);
    ///
    /// let test_box = BoundingBox2D::new(Vector2D::new(0.25, 0.15), Vector2D::new(0.5, 0.6));
    /// let mut has_overlaps = false;
    /// for i in 0..num_samples {
    ///     has_overlaps |= overlaps_func(&Vector2D::new_slice(get_sample_points2()[i]), &test_box);
    /// }
    ///
    /// assert_eq!(has_overlaps, quadtree.intersects_aabb(&test_box, &mut overlaps_func));
    ///
    /// let test_box2 = BoundingBox2D::new(Vector2D::new(0.2, 0.2), Vector2D::new(0.6, 0.5));
    /// has_overlaps = false;
    /// for i in 0..num_samples {
    ///     has_overlaps |= overlaps_func(&Vector2D::new_slice(get_sample_points2()[i]), &test_box2);
    /// }
    ///
    /// assert_eq!(has_overlaps, quadtree.intersects_aabb(&test_box2, &mut overlaps_func));
    /// ```
    fn intersects_aabb<Callback>(&self, aabb: &BoundingBox2D, test_func: &mut Callback) -> bool
        where Callback: BoxIntersectionTestFunc2<T> {
        return self.intersects_aabb(aabb, test_func, 0, &self._bbox);
    }

    /// ```
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::ray2::Ray2D;
    /// use vox_geometry_rust::quadtree::Quadtree;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// use vox_geometry_rust::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
    /// use vox_geometry_rust::intersection_query_engine2::IntersectionQueryEngine2;
    /// let mut quadtree:Quadtree<BoundingBox2D> = Quadtree::new();
    ///
    /// let mut overlaps_func = |a:&BoundingBox2D, bbox:&BoundingBox2D| {
    ///     return bbox.overlaps(a);
    /// };
    ///
    /// let mut intersects_func = |a:&BoundingBox2D, ray:&Ray2D| {
    ///     return a.intersects(ray);
    /// };
    ///
    /// let num_samples = get_number_of_sample_points2();
    /// let items = (0..num_samples / 2).map(|index| {
    ///     let c = Vector2D::new_slice(get_sample_points2()[index]);
    ///     let mut aabb = BoundingBox2D::new(c, c);
    ///     aabb.expand(0.1);
    ///     aabb
    /// }).collect();
    ///
    /// quadtree.build(&items, &BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 1.0)), &mut overlaps_func, 5);
    /// for i in 0..num_samples/2 {
    ///     let ray = Ray2D::new(Vector2D::new_slice(get_sample_points2()[i + num_samples / 2]),
    ///               Vector2D::new_slice(get_sample_dirs2()[i + num_samples / 2]));
    ///     // ad-hoc search
    ///     let mut ans_ints = false;
    ///     for j in 0..num_samples / 2 {
    ///         if intersects_func(&items[j], &ray) {
    ///             ans_ints = true;
    ///             break;
    ///         }
    ///     }
    ///
    ///     // quadtree search
    ///     let oct_ints = quadtree.intersects_ray(&ray, &mut intersects_func);
    ///
    ///     assert_eq!(ans_ints, oct_ints);
    /// }
    /// ```
    fn intersects_ray<Callback>(&self, ray: &Ray2D, test_func: &mut Callback) -> bool
        where Callback: RayIntersectionTestFunc2<T> {
        return self.intersects_ray(ray, test_func, 0, &self._bbox);
    }

    /// ```
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::quadtree::Quadtree;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// use vox_geometry_rust::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
    /// use vox_geometry_rust::intersection_query_engine2::IntersectionQueryEngine2;
    /// let mut quadtree:Quadtree<Vector2D> = Quadtree::new();
    ///
    /// let mut overlaps_func = |pt:&Vector2D, bbox:&BoundingBox2D| {
    ///         return bbox.contains(pt);
    /// };
    ///
    /// let overlaps_func2 = |pt:&Vector2D, bbox:&BoundingBox2D| {
    ///         return bbox.contains(pt);
    /// };
    ///
    /// let num_samples = get_number_of_sample_points2();
    /// let points = (0..100).map(|index| Vector2D::new_slice(get_sample_points2()[index])).collect();
    ///
    /// quadtree.build(&points, &BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 1.0)), &mut overlaps_func, 5);
    ///
    /// let test_box = BoundingBox2D::new(Vector2D::new(0.2, 0.2), Vector2D::new(0.6, 0.5));
    /// let mut num_overlaps = 0;
    /// for i in 0..num_samples {
    ///     num_overlaps += overlaps_func(&Vector2D::new_slice(get_sample_points2()[i]), &test_box) as usize;
    /// }
    ///
    /// let mut measured = 0;
    /// quadtree.for_each_intersecting_item_aabb(&test_box, &mut overlaps_func,
    ///                              &mut |pt:&Vector2D| {
    ///                                  assert_eq!(overlaps_func2(pt, &test_box), true);
    ///                                  measured += 1;
    ///                              });
    ///
    /// assert_eq!(num_overlaps, measured);
    /// ```
    fn for_each_intersecting_item_aabb<Callback, Visitor>(&self, aabb: &BoundingBox2D, test_func: &mut Callback, visitor_func: &mut Visitor)
        where Callback: BoxIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T> {
        self.for_each_intersecting_item_aabb(aabb, test_func, visitor_func, 0, &self._bbox);
    }

    fn for_each_intersecting_item_ray<Callback, Visitor>(&self, ray: &Ray2D, test_func: &mut Callback, visitor_func: &mut Visitor)
        where Callback: RayIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T> {
        self.for_each_intersecting_item_ray(ray, test_func, visitor_func, 0, &self._bbox);
    }

    /// ```
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::bounding_box2::{BoundingBox2D, BoundingBox2};
    /// use vox_geometry_rust::ray2::Ray2D;
    /// use vox_geometry_rust::quadtree::Quadtree;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// use vox_geometry_rust::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
    /// use vox_geometry_rust::intersection_query_engine2::*;
    /// let mut quadtree:Quadtree<BoundingBox2D> = Quadtree::new();
    ///
    /// let mut overlaps_func = |a:&BoundingBox2D, bbox:&BoundingBox2D| {
    ///     return bbox.overlaps(a);
    /// };
    ///
    /// let mut intersects_func = |a:&BoundingBox2D, ray:&Ray2D| {
    ///     let bbox_result = a.closest_intersection(ray);
    ///     return if bbox_result.is_intersecting {
    ///          bbox_result.t_near
    ///     } else {
    ///          f64::MAX
    ///     };
    /// };
    ///
    /// let num_samples = get_number_of_sample_points2();
    /// let items = (0..num_samples / 2).map(|index| {
    ///     let c = Vector2D::new_slice(get_sample_points2()[index]);
    ///     let mut aabb = BoundingBox2D::new(c, c);
    ///     aabb.expand(0.1);
    ///     aabb
    /// }).collect();
    ///
    /// quadtree.build(&items, &BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 1.0)), &mut overlaps_func, 5);
    ///
    /// for i in 0..num_samples/2 {
    ///     let ray = Ray2D::new(Vector2D::new_slice(get_sample_points2()[i + num_samples / 2]),
    ///               Vector2D::new_slice(get_sample_dirs2()[i + num_samples / 2]));
    ///     // ad-hoc search
    ///     let mut ans_ints:ClosestIntersectionQueryResult2<BoundingBox2D> = ClosestIntersectionQueryResult2::new();
    ///     for j in 0..num_samples/2 {
    ///         let dist = intersects_func(&items[j], &ray);
    ///         if dist < ans_ints.distance {
    ///             ans_ints.distance = dist;
    ///             ans_ints.item = Some(quadtree.item(j));
    ///         }
    ///     }
    ///
    ///     // quadtree search
    ///     let oct_ints = quadtree.closest_intersection(&ray, &mut intersects_func);
    ///
    ///     assert_eq!(ans_ints.distance, oct_ints.distance);
    ///     let ans_aabb = ans_ints.item.unwrap_or(BoundingBox2D::default());
    ///     let oct_aabb = oct_ints.item.unwrap_or(BoundingBox2D::default());
    ///     assert_eq!(ans_aabb.upper_corner, oct_aabb.upper_corner);
    ///     assert_eq!(ans_aabb.lower_corner, oct_aabb.lower_corner);
    /// }
    /// ```
    fn closest_intersection<Callback>(&self, ray: &Ray2D, test_func: &mut Callback) -> ClosestIntersectionQueryResult2<T>
        where Callback: GetRayIntersectionFunc2<T> {
        let mut best = ClosestIntersectionQueryResult2::new();
        best.distance = f64::MAX;
        best.item = None;

        return self.closest_intersection(ray, test_func, 0, &self._bbox, best);
    }
}

impl<T: Clone> NearestNeighborQueryEngine2<T> for Quadtree<T> {
    /// ```
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::quadtree::Quadtree;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// use vox_geometry_rust::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
    /// let mut quadtree:Quadtree<Vector2D> = Quadtree::new();
    ///
    /// let mut overlaps_func = |pt:&Vector2D, bbox:&BoundingBox2D| {
    ///     return bbox.contains(pt);
    /// };
    ///
    /// // Single point
    /// quadtree.build(&vec![Vector2D::new(0.2, 0.7)], &BoundingBox2D::new(Vector2D::new(0.0, 0.0), Vector2D::new(0.9, 1.0)),
    ///                &mut overlaps_func, 3);
    ///
    /// assert_eq!(3, quadtree.max_depth());
    /// assert_eq!(Vector2D::new(0.0, 0.0), quadtree.bounding_box().lower_corner);
    /// assert_eq!(Vector2D::new(1.0, 1.0), quadtree.bounding_box().upper_corner);
    /// assert_eq!(9, quadtree.number_of_nodes());
    ///
    /// let mut child = quadtree.child_index(0, 2);
    /// assert_eq!(3, child);
    ///
    /// child = quadtree.child_index(child, 0);
    /// assert_eq!(5, child);
    ///
    /// let the_non_empty_leaf_node = child + 0;
    /// for i in 0..9 {
    ///     if i == the_non_empty_leaf_node {
    ///         assert_eq!(1, quadtree.items_at_node(i).len());
    ///     } else {
    ///         assert_eq!(0, quadtree.items_at_node(i).len());
    ///     }
    /// }
    ///
    /// ```
    fn nearest<Callback>(&self, pt: &Vector2D, distance_func: &mut Callback) -> NearestNeighborQueryResult2<T>
        where Callback: NearestNeighborDistanceFunc2<T> {
        let mut best = NearestNeighborQueryResult2::new();
        best.distance = f64::MAX;
        best.item = None;

        // Prepare to traverse octree
        let mut todo: Vec<(Option<usize>, BoundingBox2D)> = Vec::new();

        // Traverse octree nodes
        let mut node = 0;
        let mut bound = self._bbox.clone();
        while node < self._nodes.len() {
            if self._nodes[node].is_leaf() {
                for item_idx in &self._nodes[node].items {
                    let d = distance_func(&self._items[*item_idx], pt);
                    if d < best.distance {
                        best.distance = d;
                        best.item = Some(self._items[*item_idx].clone());
                    }
                }

                // Grab next node to process from todo stack
                if todo.is_empty() {
                    break;
                } else {
                    node = todo.first().unwrap().0.unwrap();
                    bound = todo.first().unwrap().1.clone();
                    todo.pop();
                }
            } else {
                let best_dist_sqr = best.distance * best.distance;
                let mut child_dist_sqr_pairs = [
                    (0, 0.0, BoundingBox2D::default()),
                    (0, 0.0, BoundingBox2D::default()),
                    (0, 0.0, BoundingBox2D::default()),
                    (0, 0.0, BoundingBox2D::default())];
                let mid_point = bound.mid_point();
                for i in 0..4 {
                    let child = self._nodes[node].first_child + i;
                    let child_bound = BoundingBox2D::new(bound.corner(i), mid_point);
                    let cp = child_bound.clamp(pt);
                    let dist_min_sqr = cp.distance_squared_to(*pt);

                    child_dist_sqr_pairs[i] = (child, dist_min_sqr, child_bound);
                }
                child_dist_sqr_pairs.sort_by(|a: &(usize, f64, BoundingBox2D), b: &(usize, f64, BoundingBox2D)| {
                    return match a.1 > b.1 {
                        true => Ordering::Greater,
                        false => Ordering::Less
                    };
                });

                for i in 0..4 {
                    let child_pair = child_dist_sqr_pairs[i].clone();
                    if child_pair.1 < best_dist_sqr {
                        todo.push((Some(child_pair.0), child_pair.2));
                    }
                }

                if todo.is_empty() {
                    break;
                }

                node = todo.first().unwrap().0.unwrap();
                bound = todo.first().unwrap().1.clone();
                todo.pop();
            }
        }

        return best;
    }
}