/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

/// Type with zero element
pub trait ZeroInit: Clone {
    fn zero_init() -> Self;
}

impl ZeroInit for f32 {
    fn zero_init() -> f32 {
        return 0.0;
    }
}

impl ZeroInit for f64 {
    fn zero_init() -> f64 {
        return 0.0;
    }
}

impl ZeroInit for usize {
    fn zero_init() -> Self {
        return 0;
    }
}

impl ZeroInit for isize {
    fn zero_init() -> Self {
        return 0;
    }
}