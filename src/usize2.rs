/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::ops::{Index, IndexMut, AddAssign, SubAssign, MulAssign, DivAssign, Add, Sub, Mul, Div};
use std::fmt::{Debug, Formatter, Result};

///
/// # 2-D size class.
///
/// This class defines simple 2-D size data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy, Default)]
pub struct USize2 {
    /// X (or the first) component of the size.
    pub x: usize,

    /// Y (or the second) component of the size.
    pub y: usize,
}

/// # Constructors
impl USize2 {
    /// Constructs size with given parameters \p x_ and \p y_.
    pub fn new(x_: usize, y_: usize) -> USize2 {
        return USize2 {
            x: x_,
            y: y_,
        };
    }

    /// Constructs size with initializer list.
    pub fn new_slice(lst: &[usize]) -> USize2 {
        return USize2 {
            x: lst[0],
            y: lst[1],
        };
    }
}

/// # Basic setters
impl USize2 {
    /// Set both x and y components to **s**.
    pub fn set_scalar(&mut self, s: usize) {
        self.x = s;
        self.y = s;
    }

    /// Set x and y components with given parameters.
    pub fn set(&mut self, x: usize, y: usize) {
        self.x = x;
        self.y = y;
    }

    /// Set x and y components with given initializer list.
    pub fn set_slice(&mut self, lst: &[usize]) {
        self.x = lst[0];
        self.y = lst[1];
    }

    /// Set x and y with other size **pt**.
    pub fn set_self(&mut self, pt: &USize2) {
        self.x = pt.x;
        self.y = pt.y;
    }

    /// Set both x and y to zero.
    pub fn set_zero(&mut self) {
        self.x = 0;
        self.y = 0;
    }
}

/// # Binary operations: new instance = this (+) v
impl USize2 {
    /// Computes self + (v, v).
    pub fn add_scalar(&self, v: usize) -> USize2 {
        return USize2::new(self.x + v, self.y + v);
    }

    /// Computes self + (v.x, v.y).
    pub fn add_vec(&self, v: &USize2) -> USize2 {
        return USize2::new(self.x + v.x, self.y + v.y);
    }

    /// Computes self - (v, v).
    pub fn sub_scalar(&self, v: usize) -> USize2 {
        return USize2::new(self.x - v, self.y - v);
    }

    /// Computes self - (v.x, v.y).
    pub fn sub_vec(&self, v: &USize2) -> USize2 {
        return USize2::new(self.x - v.x, self.y - v.y);
    }

    /// Computes self * (v, v).
    pub fn mul_scalar(&self, v: usize) -> USize2 {
        return USize2::new(self.x * v, self.y * v);
    }

    /// Computes self * (v.x, v.y).
    pub fn mul_vec(&self, v: &USize2) -> USize2 {
        return USize2::new(self.x * v.x, self.y * v.y);
    }

    /// Computes self / (v, v).
    pub fn div_scalar(&self, v: usize) -> USize2 {
        return USize2::new(self.x / v, self.y / v);
    }

    /// Computes self / (v.x, v.y).
    pub fn div_vec(&self, v: &USize2) -> USize2 {
        return USize2::new(self.x / v.x, self.y / v.y);
    }
}

/// # Binary operations: new instance = v (+) this
impl USize2 {
    /// Computes (v, v) - self.
    pub fn rsub_scalar(&self, v: usize) -> USize2 {
        return USize2::new(v - self.x, v - self.y);
    }

    /// Computes (v.x, v.y) - self.
    pub fn rsub_vec(&self, v: &USize2) -> USize2 {
        return USize2::new(v.x - self.x, v.y - self.y);
    }

    /// Computes (v, v) / self.
    pub fn rdiv_scalar(&self, v: usize) -> USize2 {
        return USize2::new(v / self.x, v / self.y);
    }

    /// Computes (v.x, v.y) / self.
    pub fn rdiv_vec(&self, v: &USize2) -> USize2 {
        return USize2::new(v.x / self.x, v.y / self.y);
    }
}

/// # Augmented operations: self (+)= v
impl USize2 {
    /// Computes self += (v, v).
    pub fn iadd_scalar(&mut self, v: usize) {
        self.x = usize::add(self.x, v);
        self.y = usize::add(self.y, v);
    }

    /// Computes self += (v.x, v.y).
    pub fn iadd_vec(&mut self, v: &USize2) {
        self.x = usize::add(self.x, v.x);
        self.y = usize::add(self.y, v.y);
    }

    /// Computes self -= (v, v).
    pub fn isub_scalar(&mut self, v: usize) {
        self.x = usize::sub(self.x, v);
        self.y = usize::sub(self.y, v);
    }

    /// Computes self -= (v.x, v.y).
    pub fn isub_vec(&mut self, v: &USize2) {
        self.x = usize::sub(self.x, v.x);
        self.y = usize::sub(self.y, v.y);
    }

    /// Computes self *= (v, v).
    pub fn imul_scalar(&mut self, v: usize) {
        self.x = usize::mul(self.x, v);
        self.y = usize::mul(self.y, v);
    }

    /// Computes self *= (v.x, v.y).
    pub fn imul_vec(&mut self, v: &USize2) {
        self.x = usize::mul(self.x, v.x);
        self.y = usize::mul(self.y, v.y);
    }

    /// Computes self /= (v, v).
    pub fn idiv_scalar(&mut self, v: usize) {
        self.x = usize::div(self.x, v);
        self.y = usize::div(self.y, v);
    }

    /// Computes self /= (v.x, v.y).
    pub fn idiv_vec(&mut self, v: &USize2) {
        self.x = usize::div(self.x, v.x);
        self.y = usize::div(self.y, v.y);
    }
}

/// # Basic getters
impl USize2 {
    /// Returns const reference to the **i** -th element of the size.
    pub fn at(&self, i: usize) -> &usize {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the size.
    pub fn at_mut(&mut self, i: usize) -> &mut usize {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y).
    pub fn sum(&self) -> usize {
        return self.x + self.y;
    }

    /// Returns the minimum value among x and y.
    pub fn min(&self) -> usize {
        return self.x.min(self.y);
    }

    /// Returns the maximum value among x and y.
    pub fn max(&self) -> usize {
        return self.x.max(self.y);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        match self.x > self.y {
            true => 0,
            false => 1
        }
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        match self.x < self.y {
            true => 0,
            false => 1
        }
    }

    /// Returns true if **other** is the same as self size.
    pub fn is_equal(&self, other: &USize2) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the size.
impl Index<usize> for USize2 {
    type Output = usize;
    fn index(&self, index: usize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the size.
impl IndexMut<usize> for USize2 {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v)
impl AddAssign<usize> for USize2 {
    fn add_assign(&mut self, rhs: usize) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y)
impl AddAssign for USize2 {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v)
impl SubAssign<usize> for USize2 {
    fn sub_assign(&mut self, rhs: usize) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y)
impl SubAssign for USize2 {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v)
impl MulAssign<usize> for USize2 {
    fn mul_assign(&mut self, rhs: usize) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y)
impl MulAssign for USize2 {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v)
impl DivAssign<usize> for USize2 {
    fn div_assign(&mut self, rhs: usize) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y)
impl DivAssign for USize2 {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self size.
impl PartialEq for USize2 {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl Eq for USize2 {}

/// Computes (a, a) + (b.x, b.y).
impl Add<usize> for USize2 {
    type Output = USize2;
    fn add(self, rhs: usize) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y) + (b.x, b.y).
impl Add for USize2 {
    type Output = USize2;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

/// Computes (a.x, a.y) - (b, b).
impl Sub<usize> for USize2 {
    type Output = USize2;
    fn sub(self, rhs: usize) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y) - (b.x, b.y).
impl Sub for USize2 {
    type Output = USize2;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

/// Computes (a.x, a.y) * (b, b).
impl Mul<usize> for USize2 {
    type Output = USize2;
    fn mul(self, rhs: usize) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y) * (b.x, b.y).
impl Mul for USize2 {
    type Output = USize2;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y) / (b, b).
impl Div<usize> for USize2 {
    type Output = USize2;
    fn div(self, rhs: usize) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y) / (b.x, b.y).
impl Div for USize2 {
    type Output = USize2;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl Debug for USize2 {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::usize2::USize2;
    /// let vec = USize2::new(10, 20);
    /// assert_eq!(format!("{:?}", vec), "(10, 20)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10,
    ///     20,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .finish()
    }
}

/// # utility
/// Returns element-wise min size: (min(a.x, b.x), min(a.y, b.y)).
pub fn min(a: &USize2, b: &USize2) -> USize2 {
    return USize2::new(usize::min(a.x, b.x), usize::min(a.y, b.y));
}

/// Returns element-wise max size: (max(a.x, b.x), max(a.y, b.y)).
pub fn max(a: &USize2, b: &USize2) -> USize2 {
    return USize2::new(usize::max(a.x, b.x), usize::max(a.y, b.y));
}

/// Returns element-wise clamped size.
pub fn clamp(v: &USize2, low: &USize2, high: &USize2) -> USize2 {
    return USize2::new(usize::clamp(v.x, low.x, high.x),
                       usize::clamp(v.y, low.y, high.y));
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod usize2 {
    #[test]
    fn constructors() {}
}