/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::bvh2::Bvh2;
use crate::implicit_surface2::*;
use crate::surface2::*;
use crate::vector2::Vector2D;
use crate::transform2::Transform2;
use crate::bounding_box2::BoundingBox2D;
use crate::ray2::Ray2D;
use crate::surface_to_implicit2::SurfaceToImplicit2;
use crate::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
use crate::intersection_query_engine2::IntersectionQueryEngine2;
use std::sync::{RwLock, Arc};

///
/// # 2-D implicit surface set.
///
/// This class represents 2-D implicit surface set which extends
/// ImplicitSurface2 by overriding implicit surface-related quries. This is
/// class can hold a collection of other implicit surface instances.
///
pub struct ImplicitSurfaceSet2 {
    _surfaces: Vec<ImplicitSurface2Ptr>,
    _unbounded_surfaces: Vec<ImplicitSurface2Ptr>,
    _bvh: RwLock<Bvh2<ImplicitSurface2Ptr>>,
    _bvh_invalidated: RwLock<bool>,

    /// data from surface2
    pub surface_data: Surface2Data,
}

impl ImplicitSurfaceSet2 {
    /// Constructs an empty implicit surface set.
    pub fn default() -> ImplicitSurfaceSet2 {
        return ImplicitSurfaceSet2 {
            _surfaces: vec![],
            _unbounded_surfaces: vec![],
            _bvh: RwLock::new(Bvh2::new()),
            _bvh_invalidated: RwLock::new(true),
            surface_data: Surface2Data::new(None, None),
        };
    }

    /// Constructs an implicit surface set using list of other surfaces.
    pub fn new(surfaces: Vec<ImplicitSurface2Ptr>,
               transform: Option<Transform2>,
               is_normal_flipped: Option<bool>) -> ImplicitSurfaceSet2 {
        let mut unbounded_surfaces: Vec<ImplicitSurface2Ptr> = vec![];
        for surface in &surfaces {
            if !surface.read().unwrap().is_bounded() {
                unbounded_surfaces.push(surface.clone());
            }
        }

        return ImplicitSurfaceSet2 {
            _surfaces: surfaces,
            _unbounded_surfaces: unbounded_surfaces,
            _bvh: RwLock::new(Bvh2::new()),
            _bvh_invalidated: RwLock::new(true),
            surface_data: Surface2Data::new(transform, is_normal_flipped),
        };
    }

    /// Constructs an implicit surface set using list of other surfaces.
    pub fn new_explicit(surfaces: Vec<Surface2Ptr>,
                        transform: Option<Transform2>,
                        is_normal_flipped: Option<bool>) -> ImplicitSurfaceSet2 {
        let mut set = ImplicitSurfaceSet2::default();
        set.surface_data.transform = transform.unwrap_or(Transform2::default());
        set.surface_data.is_normal_flipped = is_normal_flipped.unwrap_or(false);
        for surface in &surfaces {
            set.add_explicit_surface(surface.clone());
        }

        return set;
    }

    /// Returns builder fox ImplicitSurfaceSet2.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    /// Returns the number of implicit surfaces.
    pub fn number_of_surfaces(&self) -> usize {
        return self._surfaces.len();
    }

    /// Returns the i-th implicit surface.
    pub fn surface_at(&self, i: usize) -> ImplicitSurface2Ptr {
        return self._surfaces[i].clone();
    }

    /// Adds an explicit surface instance.
    pub fn add_explicit_surface(&mut self, surface: Surface2Ptr) {
        self.add_surface(Arc::new(RwLock::new(SurfaceToImplicit2::new(surface, None, None))));
    }

    /// Adds an implicit surface instance.
    pub fn add_surface(&mut self, surface: ImplicitSurface2Ptr) {
        self._surfaces.push(surface.clone());
        if !surface.read().unwrap().is_bounded() {
            self._unbounded_surfaces.push(surface);
        }
        self.invalidate_bvh();
    }

    fn invalidate_bvh(&self) {
        *(self._bvh_invalidated.write().unwrap()) = true;
    }

    fn build_bvh(&self) {
        if *self._bvh_invalidated.read().unwrap() {
            let mut surfs: Vec<ImplicitSurface2Ptr> = Vec::new();
            let mut bounds: Vec<BoundingBox2D> = Vec::new();
            for i in 0..self._surfaces.len() {
                if self._surfaces[i].read().unwrap().is_bounded() {
                    surfs.push(self._surfaces[i].clone());
                    bounds.push(self._surfaces[i].read().unwrap().bounding_box());
                }
            }
            self._bvh.write().unwrap().build(&surfs, &bounds);
            *(self._bvh_invalidated.write().unwrap()) = false;
        }
    }
}

impl Surface2 for ImplicitSurfaceSet2 {
    fn closest_point_local(&self, other_point: &Vector2D) -> Vector2D {
        self.build_bvh();

        let mut distance_func = |surface: &ImplicitSurface2Ptr, pt: &Vector2D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let mut result = Vector2D::new(f64::MAX, f64::MAX);
        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);
        if let Some(item) = query_result.item {
            result = item.read().unwrap().closest_point(other_point);
        }

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
                result = surface.read().unwrap().closest_point(other_point);
            }
        }

        return result;
    }

    fn bounding_box_local(&self) -> BoundingBox2D {
        self.build_bvh();

        return self._bvh.read().unwrap().bounding_box();
    }

    fn closest_intersection_local(&self, ray: &Ray2D) -> SurfaceRayIntersection2 {
        self.build_bvh();

        let mut test_func = |surface: &ImplicitSurface2Ptr, ray: &Ray2D| {
            let result = surface.read().unwrap().closest_intersection(ray);
            return result.distance;
        };

        let query_result = self._bvh.read().unwrap().closest_intersection(ray, &mut test_func);
        let mut result = SurfaceRayIntersection2::new();
        result.distance = query_result.distance;
        result.is_intersecting = match query_result.item {
            None => false,
            Some(_) => true
        };
        if let Some(item) = query_result.item {
            result.point = ray.point_at(query_result.distance);
            result.normal = item.read().unwrap().closest_normal(&result.point);
        }

        for surface in &self._unbounded_surfaces {
            let local_result = surface.read().unwrap().closest_intersection(ray);
            if local_result.distance < result.distance {
                result = local_result;
            }
        }

        return result;
    }

    fn closest_normal_local(&self, other_point: &Vector2D) -> Vector2D {
        self.build_bvh();

        let mut distance_func = |surface: &ImplicitSurface2Ptr, pt: &Vector2D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let mut result = Vector2D::new(1.0, 0.0);
        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);
        if let Some(item) = query_result.item {
            result = item.read().unwrap().closest_normal(other_point);
        }

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
                result = surface.read().unwrap().closest_normal(other_point);
            }
        }

        return result;
    }

    fn intersects_local(&self, ray: &Ray2D) -> bool {
        self.build_bvh();

        let mut test_func = |surface: &ImplicitSurface2Ptr, ray: &Ray2D| {
            return surface.read().unwrap().intersects(ray);
        };

        let mut result = self._bvh.read().unwrap().intersects_ray(ray, &mut test_func);
        for surface in &self._unbounded_surfaces {
            result |= surface.read().unwrap().intersects(ray);
        }

        return result;
    }

    fn closest_distance_local(&self, other_point: &Vector2D) -> f64 {
        self.build_bvh();

        let mut distance_func = |surface: &ImplicitSurface2Ptr, pt: &Vector2D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
            }
        }

        return min_dist;
    }

    fn update_query_engine(&self) {
        self.invalidate_bvh();
        self.build_bvh();
    }

    fn is_bounded(&self) -> bool {
        // All surfaces should be bounded.
        for surface in &self._surfaces {
            if !surface.read().unwrap().is_bounded() {
                return false;
            }
        }

        // Empty set is not bounded.
        return !self._surfaces.is_empty();
    }

    fn is_valid_geometry(&self) -> bool {
        // All surfaces should be valid.
        for surface in &self._surfaces {
            if !surface.read().unwrap().is_valid_geometry() {
                return false;
            }
        }

        // Empty set is not valid.
        return !self._surfaces.is_empty();
    }

    fn view(&self) -> &Surface2Data {
        return &self.surface_data;
    }
}

impl ImplicitSurface2 for ImplicitSurfaceSet2 {
    fn signed_distance_local(&self, other_point: &Vector2D) -> f64 {
        let mut sdf = f64::MAX;
        for surface in &self._surfaces {
            sdf = f64::min(sdf, surface.read().unwrap().signed_distance(other_point));
        }

        return sdf;
    }

    fn is_inside_local(&self, other_point: &Vector2D) -> bool {
        for surface in &self._surfaces {
            if surface.read().unwrap().is_inside(other_point) {
                return true;
            }
        }

        return false;
    }
}

/// Shared pointer type for the ImplicitSurfaceSet2.
pub type ImplicitSurfaceSet2Ptr = Arc<RwLock<ImplicitSurfaceSet2>>;

///
/// # Front-end to create ImplicitSurfaceSet2 objects step by step.
///
pub struct Builder {
    _surfaces: Vec<ImplicitSurface2Ptr>,

    _surface_data: Surface2Data,
}

impl Builder {
    /// Returns builder with surfaces.
    pub fn with_surfaces(&mut self, surfaces: Vec<ImplicitSurface2Ptr>) -> &mut Self {
        self._surfaces = surfaces;
        return self;
    }

    /// Returns builder with explicit surfaces.
    pub fn with_explicit_surfaces(&mut self, surfaces: Vec<Surface2Ptr>) -> &mut Self {
        self._surfaces.clear();
        for surface in surfaces {
            self._surfaces.push(Arc::new(RwLock::new(SurfaceToImplicit2::new(surface, None, None))));
        }

        return self;
    }

    /// Builds ImplicitSurfaceSet2.
    pub fn build(&mut self) -> ImplicitSurfaceSet2 {
        return ImplicitSurfaceSet2::new(self._surfaces.clone(),
                                        Some(self._surface_data.transform.clone()),
                                        Some(self._surface_data.is_normal_flipped));
    }

    /// Builds shared pointer of ImplicitSurfaceSet2 instance.
    pub fn make_shared(&mut self) -> ImplicitSurfaceSet2Ptr {
        return ImplicitSurfaceSet2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _surfaces: vec![],
            _surface_data: Surface2Data::new(None, None),
        };
    }
}

impl SurfaceBuilderBase2 for Builder {
    fn view(&mut self) -> &mut Surface2Data {
        return &mut self._surface_data;
    }
}