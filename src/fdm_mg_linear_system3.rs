/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::mg::*;
use crate::fdm_linear_system3::{FdmBlas3, FdmVector3};
use crate::usize3::USize3;
use crate::array3::Array3;
use crate::common_trait::ZeroInit;

/// Multigrid-style 3-D FDM matrix.
pub type FdmMgMatrix3 = MgMatrix<FdmBlas3>;

/// Multigrid-style 3-D FDM vector.
pub type FdmMgVector3 = MgVector<FdmBlas3>;

/// Multigrid-style 3-D linear system.
pub struct FdmMgLinearSystem3 {
    /// The system matrix.
    pub a: FdmMgMatrix3,

    /// The solution vector.
    pub x: FdmMgVector3,

    /// The RHS vector.
    pub b: FdmMgVector3,
}

impl FdmMgLinearSystem3 {
    /// Clears the linear system.
    pub fn clear(&mut self) {
        self.a.levels.clear();
        self.x.levels.clear();
        self.b.levels.clear();
    }

    /// Returns the number of multigrid levels.
    pub fn number_of_levels(&self) -> usize {
        return self.a.levels.len();
    }

    /// Resizes the system with the coarsest resolution and number of levels.
    pub fn resize_with_coarsest(&mut self, coarsest_resolution: &USize3, number_of_levels: usize) {
        FdmMgUtils3::resize_array_with_coarsest(coarsest_resolution, number_of_levels,
                                                &mut self.a.levels);
        FdmMgUtils3::resize_array_with_coarsest(coarsest_resolution, number_of_levels,
                                                &mut self.x.levels);
        FdmMgUtils3::resize_array_with_coarsest(coarsest_resolution, number_of_levels,
                                                &mut self.b.levels);
    }

    ///
    /// \brief Resizes the system with the finest resolution and max number of
    /// levels.
    ///
    /// This function resizes the system with multiple levels until the
    /// resolution is divisible with 3^(level-1).
    ///
    /// \param finest_resolution - The finest grid resolution.
    /// \param max_number_of_levels - Maximum number of multigrid levels.
    ///
    pub fn resize_with_finest(&mut self, finest_resolution: &USize3, max_number_of_levels: usize) {
        FdmMgUtils3::resize_array_with_finest(finest_resolution, max_number_of_levels,
                                              &mut self.a.levels);
        FdmMgUtils3::resize_array_with_finest(finest_resolution, max_number_of_levels,
                                              &mut self.x.levels);
        FdmMgUtils3::resize_array_with_finest(finest_resolution, max_number_of_levels,
                                              &mut self.b.levels);
    }
}

/// Multigrid utilities for 3-D FDM system.
pub struct FdmMgUtils3 {}

impl FdmMgUtils3 {
    /// Restricts given finer grid to the coarser grid.
    pub fn restrict(finer: &FdmVector3, coarser: &mut FdmVector3) {
        debug_assert!(finer.size().x == 2 * coarser.size().x);
        debug_assert!(finer.size().y == 2 * coarser.size().y);
        debug_assert!(finer.size().z == 2 * coarser.size().z);

        // --*--|--*--|--*--|--*--
        //  1/8   3/8   3/8   1/8
        //           to
        // -----|-----*-----|-----
        let kernel = [0.125, 0.375, 0.375, 0.125];

        let n = coarser.size();
        let mut k_indices = [0_usize; 4];

        for k in 0..n.z {
            k_indices[0] = if k > 0 { 2 * k - 1 } else { 2 * k };
            k_indices[1] = 2 * k;
            k_indices[2] = 2 * k + 1;
            k_indices[3] = if k + 1 < n.z { 2 * k + 2 } else { 2 * k + 1 };

            let mut j_indices = [0_usize; 4];

            for j in 0..n.y {
                j_indices[0] = if j > 0 { 2 * j - 1 } else { 2 * j };
                j_indices[1] = 2 * j;
                j_indices[2] = 2 * j + 1;
                j_indices[3] = if j + 1 < n.y { 2 * j + 2 } else { 2 * j + 1 };

                let mut i_indices = [0_usize; 4];
                for i in 0..n.x {
                    i_indices[0] = if i > 0 { 2 * i - 1 } else { 2 * i };
                    i_indices[1] = 2 * i;
                    i_indices[2] = 2 * i + 1;
                    i_indices[3] = if i + 1 < n.x { 2 * i + 2 } else { 2 * i + 1 };

                    let mut sum = 0.0;
                    for z in 0..4 {
                        for y in 0..4 {
                            for x in 0..4 {
                                let w =
                                    kernel[x] * kernel[y] * kernel[z];
                                sum += w * finer[(i_indices[x], j_indices[y], k_indices[z])];
                            }
                        }
                    }
                    coarser[(i, j, k)] = sum;
                }
            }
        }
    }

    /// Corrects given coarser grid to the finer grid.
    pub fn correct(coarser: &FdmVector3, finer: &mut FdmVector3) {
        debug_assert!(finer.size().x == 2 * coarser.size().x);
        debug_assert!(finer.size().y == 2 * coarser.size().y);
        debug_assert!(finer.size().z == 2 * coarser.size().z);

        // -----|-----*-----|-----
        //           to
        //  1/4   3/4   3/4   1/4
        // --*--|--*--|--*--|--*--
        let n = finer.size();
        for k in 0..n.z {
            for j in 0..n.y {
                for i in 0..n.x {
                    let mut i_indices = [0_usize; 2];
                    let mut j_indices = [0_usize; 2];
                    let mut k_indices = [0_usize; 2];
                    let mut i_weights = [0.0; 2];
                    let mut j_weights = [0.0; 2];
                    let mut k_weights = [0.0; 2];

                    let ci = i / 2;
                    let cj = j / 2;
                    let ck = k / 2;

                    if i % 2 == 0 {
                        i_indices[0] = if i > 1 { ci - 1 } else { ci };
                        i_indices[1] = ci;
                        i_weights[0] = 0.25;
                        i_weights[1] = 0.75;
                    } else {
                        i_indices[0] = ci;
                        i_indices[1] = if i + 1 < n.x { ci + 1 } else { ci };
                        i_weights[0] = 0.75;
                        i_weights[1] = 0.25;
                    }

                    if j % 2 == 0 {
                        j_indices[0] = if j > 1 { cj - 1 } else { cj };
                        j_indices[1] = cj;
                        j_weights[0] = 0.25;
                        j_weights[1] = 0.75;
                    } else {
                        j_indices[0] = cj;
                        j_indices[1] = if j + 1 < n.y { cj + 1 } else { cj };
                        j_weights[0] = 0.75;
                        j_weights[1] = 0.25;
                    }

                    if k % 2 == 0 {
                        k_indices[0] = if k > 1 { ck - 1 } else { ck };
                        k_indices[1] = ck;
                        k_weights[0] = 0.25;
                        k_weights[1] = 0.75;
                    } else {
                        k_indices[0] = ck;
                        k_indices[1] = if k + 1 < n.y { ck + 1 } else { ck };
                        k_weights[0] = 0.75;
                        k_weights[1] = 0.25;
                    }

                    for z in 0..2 {
                        for y in 0..2 {
                            for x in 0..2 {
                                let w = i_weights[x] * j_weights[y] *
                                    k_weights[z] *
                                    coarser[(i_indices[x], j_indices[y],
                                             k_indices[z])];
                                finer[(i, j, k)] += w;
                            }
                        }
                    }
                }
            }
        }
    }

    /// Resizes the array with the coarsest resolution and number of levels.
    pub fn resize_array_with_coarsest<T: ZeroInit>(coarsest_resolution: &USize3,
                                                   mut number_of_levels: usize, levels: &mut Vec<Array3<T>>) {
        number_of_levels = usize::max(number_of_levels, crate::constants::K_ONE_SIZE);

        levels.resize(number_of_levels, Array3::default());

        // Level 0 is the finest level, thus takes coarsest_resolution ^
        // number_of_levels.
        // Level number_of_levels - 1 is the coarsest, taking coarsest_resolution.
        let mut res = *coarsest_resolution;
        for level in 0..number_of_levels {
            levels[number_of_levels - level - 1].resize_with_packed_size(&res, None);
            res.x = res.x << 1;
            res.y = res.y << 1;
            res.z = res.z << 1;
        }
    }

    ///
    /// \brief Resizes the array with the finest resolution and max number of
    /// levels.
    ///
    /// This function resizes the system with multiple levels until the
    /// resolution is divisible with 3^(level-1).
    ///
    /// \param finest_resolution - The finest grid resolution.
    /// \param max_number_of_levels - Maximum number of multigrid levels.
    ///
    pub fn resize_array_with_finest<T: ZeroInit>(finest_resolution: &USize3,
                                                 max_number_of_levels: usize, levels: &mut Vec<Array3<T>>) {
        let mut res = *finest_resolution;
        let mut i = 1;
        while i < max_number_of_levels {
            if res.x % 2 == 0 && res.y % 2 == 0 && res.z % 2 == 0 {
                res.x = res.x >> 1;
                res.y = res.y >> 1;
                res.z = res.z >> 1;
            } else {
                break;
            }
            i += 1;
        }
        FdmMgUtils3::resize_array_with_coarsest(&res, i, levels);
    }
}