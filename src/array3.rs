/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use crate::usize3::USize3;
use crate::array_accessor3::*;
use std::mem::swap;
use std::ops::{Index, IndexMut};

///
/// # 3-D array class.
///
/// This class represents 3-D array data structure. Internally, the 3-D data is
/// mapped to a linear array such that (i, j, k) element is actually stroed at
/// (i + width * (j + height * k))th element of the linear array. This mapping
/// means iterating i first, j and k next will give better performance such as:
///
/// \code{.cpp}
/// Array<int, 3> array;
/// for (size_t k = 0; k < array.depth(); ++k) {
///     for (size_t j = 0; j < array.height(); ++j) {
///         for (size_t i = 0; i < array.width(); ++i) {
///             // Read or write array(i, j, k)
///         }
///     }
/// }
/// \endcode
///
/// \tparam T - Type to store in the array.
///
#[derive(Clone)]
pub struct Array3<T: ZeroInit> {
    _size: USize3,
    _data: Vec<T>,
}

impl<T: ZeroInit> Default for Array3<T> {
    /// Constructs zero-sized 3-D array.
    fn default() -> Self {
        return Array3 {
            _size: USize3::default(),
            _data: vec![],
        };
    }
}

impl<T: ZeroInit> Array3<T> {
    /// Constructs 3-D array with given \p size and fill it with \p initVal.
    /// \param size Initial size of the array.
    /// \param initVal Initial value of each array element.
    pub fn new_with_packed_size(size: &USize3, init_val: Option<T>) -> Array3<T> {
        let mut array = Array3::default();
        array.resize_with_packed_size(size, init_val);
        return array;
    }

    /// Constructs 3-D array with size \p width x \p height x \p depth and fill
    /// it with \p initVal.
    /// \param width Initial width of the array.
    /// \param height Initial height of the array.
    /// \param depth Initial depth of the array.
    /// \param initVal Initial value of each array element.
    pub fn new_with_size(width: usize, height: usize, depth: usize, init_val: Option<T>) -> Array3<T> {
        let mut array = Array3::default();
        array.resize_with_size(width, height, depth, init_val);
        return array;
    }

    ///
    /// \brief Constructs 3-D array with given initializer list \p lst.
    ///
    /// This constructor will build 3-D array with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// Array<int, 3> arr = {
    ///     {{ 1.0,  2.0,  3.0,  4.0},
    ///      { 5.0,  6.0,  7.0,  8.0},
    ///      { 9.0, 10.0, 11.0, 12.0}},
    ///     {{13.0, 14.0, 15.0, 16.0},
    ///      {17.0, 18.0, 19.0, 20.0},
    ///      {21.0, 22.0, 23.0, 24.0}}};
    /// \endcode
    ///
    /// Note the initializer also has 3-D structure. The code above will
    /// construct 4 x 3 x 2 array.
    ///
    /// \param lst Initializer list that should be copy to the new array.
    ///
    pub fn new_slice(lst: &[&[&[T]]]) -> Array3<T> {
        let mut array = Array3::default();
        array.set_slice(lst);
        return array;
    }
}

impl<T: ZeroInit> Array3<T> {
    /// Sets entire array with given \p value.
    pub fn set_scalar(&mut self, value: T) {
        self._data.fill(value);
    }

    /// Copies given array \p other to this array.
    pub fn set_self(&mut self, other: &Array3<T>) {
        self._data.resize(other._data.len(), T::zero_init());
        self._data = other._data.clone();
        self._size = other._size;
    }

    ///
    /// Copies given initializer list \p lst to this array.
    ///
    /// This function copies given initializer list \p lst to the array such as
    ///
    /// \code{.cpp}
    /// Array<int, 3> arr;
    /// arr = {
    ///     {{ 1.0,  2.0,  3.0,  4.0},
    ///      { 5.0,  6.0,  7.0,  8.0},
    ///      { 9.0, 10.0, 11.0, 12.0}},
    ///     {{13.0, 14.0, 15.0, 16.0},
    ///      {17.0, 18.0, 19.0, 20.0},
    ///      {21.0, 22.0, 23.0, 24.0}}};
    /// \endcode
    ///
    /// Note the initializer also has 3-D structure. The code above will
    /// construct 4 x 3 x 2 array.
    ///
    /// \param lst Initializer list that should be copy to the new array.
    ///
    pub fn set_slice(&mut self, lst: &[&[&[T]]]) {
        let depth = lst.len();
        let height = if depth > 0 { lst[0].len() } else { 0 };
        let width = if height > 0 { lst[0][0].len() } else { 0 };
        self.resize_with_packed_size(&USize3::new(width, height, depth), None);
        for k in 0..depth {
            debug_assert!(height == lst[0].len());
            for j in 0..height {
                debug_assert!(width == lst[0][0].len());
                for i in 0..width {
                    self[(i, j, k)] = lst[k][j][i].clone();
                }
            }
        }
    }

    /// Clears the array and resizes to zero.
    pub fn clear(&mut self) {
        self._data.clear();
        self._size = USize3::default();
    }

    /// Resizes the array with \p size and fill the new element with \p init_val.
    pub fn resize_with_packed_size(&mut self, size: &USize3, init_val: Option<T>) {
        let mut grid = Array3::default();
        grid._data.resize(size.x * size.y * size.z, init_val.unwrap_or(T::zero_init()));
        grid._size = *size;
        let i_min = usize::min(size.x, self._size.x);
        let j_min = usize::min(size.y, self._size.y);
        let k_min = usize::min(size.z, self._size.z);
        for k in 0..k_min {
            for j in 0..j_min {
                for i in 0..i_min {
                    grid[(i, j, k)] = self.at_with_size(i, j, k).clone();
                }
            }
        }

        self.swap(&mut grid);
    }

    /// Resizes the array with size \p width x \p height and fill the new
    /// element with \p init_val.
    pub fn resize_with_size(&mut self, width: usize, height: usize, depth: usize, init_val: Option<T>) {
        self.resize_with_packed_size(&USize3::new(width, height, depth), init_val);
    }

    ///
    /// \brief Returns the reference to the i-th element.
    ///
    /// This function returns the reference to the i-th element of the array
    /// where i is the index of linearly mapped elements such that
    /// i = x + width * (y + height * z) (x, y and z are the 3-D coordinates of
    /// the element).
    ///
    pub fn at_index_mut(&mut self, i: usize) -> &mut T {
        debug_assert!(i < self._size.x * self._size.y * self._size.z);
        return &mut self._data[i];
    }

    ///
    /// \brief Returns the const reference to the i-th element.
    ///
    /// This function returns the reference to the i-th element of the array
    /// where i is the index of linearly mapped elements such that
    /// i = x + width * (y + height * z) (x, y and z are the 3-D coordinates of
    /// the element).
    ///
    pub fn at_index(&self, i: usize) -> &T {
        debug_assert!(i < self._size.x * self._size.y * self._size.z);
        return &self._data[i];
    }

    /// Returns the reference to the element at (pt.x, pt.y, pt.z).
    pub fn at_with_packed_size_mut(&mut self, pt: &USize3) -> &mut T {
        return self.at_with_size_mut(pt.x, pt.y, pt.z);
    }

    /// Returns the const reference to the element at (pt.x, pt.y, pt.z).
    pub fn at_with_packed_size(&self, pt: &USize3) -> &T {
        return self.at_with_size(pt.x, pt.y, pt.z);
    }

    /// Returns the reference to the element at (i, j, k).
    pub fn at_with_size_mut(&mut self, i: usize, j: usize, k: usize) -> &mut T {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return &mut self._data[i + self._size.x * (j + self._size.y * k)];
    }

    /// Returns the const reference to the element at (i, j, k).
    pub fn at_with_size(&self, i: usize, j: usize, k: usize) -> &T {
        debug_assert!(i < self._size.x && j < self._size.y && k < self._size.z);
        return &self._data[i + self._size.x * (j + self._size.y * k)];
    }

    /// Returns the size of the array.
    pub fn size(&self) -> USize3 {
        return self._size;
    }

    /// Returns the width of the array.
    pub fn width(&self) -> usize {
        return self._size.x;
    }

    /// Returns the height of the array.
    pub fn height(&self) -> usize {
        return self._size.y;
    }

    /// Returns the depth of the array.
    pub fn depth(&self) -> usize {
        return self._size.z;
    }

    /// Returns the raw pointer to the array data.
    pub fn data_mut(&mut self) -> &mut [T] {
        return &mut self._data;
    }

    /// Returns the const raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return &self._data;
    }

    /// Returns the array accessor.
    pub fn accessor(&mut self) -> ArrayAccessor3<T> {
        return ArrayAccessor3::new_with_packed_size(&self.size(), self.data_mut());
    }

    /// Returns the const array accessor.
    pub fn const_accessor(&self) -> ConstArrayAccessor3<T> {
        return ConstArrayAccessor3::new_with_packed_size(&self.size(), self.data());
    }

    /// Swaps the content of the array with \p other array.
    pub fn swap(&mut self, other: &mut Array3<T>) {
        swap(&mut other._data, &mut self._data);
        swap(&mut other._size, &mut self._size);
    }
}

impl<T: ZeroInit> Array3<T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// Array<int, 3> array;
    /// for (size_t k = 0; k < array.depth(); ++k) {
    ///     for (size_t j = 0; j < array.height(); ++j) {
    ///         for (size_t i = 0; i < array.width(); ++i) {
    ///             func(i, j, k);
    ///         }
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// Array<int, 3> array(100, 200, 150, 4);
    /// array.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, func: Callback) {
        self.const_accessor().for_each(func);
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes three parameters which are
    /// the (i, j, j) indices of the array. The order of execution will be the
    /// same as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// Array<int, 3> array;
    /// for (size_t k = 0; k < array.depth(); ++k) {
    ///     for (size_t j = 0; j < array.height(); ++j) {
    ///         for (size_t i = 0; i < array.width(); ++i) {
    ///             func(i, j, k);
    ///         }
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// Array<int, 3> array(10, 4);
    /// array.for_each_index([&](size_t i, size_t j, size_t k) {
    ///     array(i, j, k) = 4.f * i + 7.f * j + 3.f * k + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize, usize)>(&self, func: Callback) {
        self.const_accessor().for_each_index(func);
    }
}

impl<T: ZeroInit> Index<usize> for Array3<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data[index];
    }
}

impl<T: ZeroInit> IndexMut<usize> for Array3<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._data[index];
    }
}

impl<T: ZeroInit> Index<USize3> for Array3<T> {
    type Output = T;

    fn index(&self, index: USize3) -> &Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y && index.z < self._size.z);
        return &self._data[index.x + self._size.x * (index.y + self._size.y * index.z)];
    }
}

impl<T: ZeroInit> IndexMut<USize3> for Array3<T> {
    fn index_mut(&mut self, index: USize3) -> &mut Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y && index.z < self._size.z);
        return &mut self._data[index.x + self._size.x * (index.y + self._size.y * index.z)];
    }
}

impl<T: ZeroInit> Index<(usize, usize, usize)> for Array3<T> {
    type Output = T;

    fn index(&self, index: (usize, usize, usize)) -> &Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y && index.2 < self._size.z);
        return &self._data[index.0 + self._size.x * (index.1 + self._size.y * index.2)];
    }
}

impl<T: ZeroInit> IndexMut<(usize, usize, usize)> for Array3<T> {
    fn index_mut(&mut self, index: (usize, usize, usize)) -> &mut Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y && index.2 < self._size.z);
        return &mut self._data[index.0 + self._size.x * (index.1 + self._size.y * index.2)];
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod array3 {
    use crate::array3::Array3;
    use crate::usize3::USize3;

    #[test]
    fn constructors() {
        {
            let arr = Array3::<f32>::default();
            assert_eq!(0, arr.width());
            assert_eq!(0, arr.height());
            assert_eq!(0, arr.depth());
        }
        {
            let arr = Array3::<f32>::new_with_packed_size(&USize3::new(3, 7, 4), None);
            assert_eq!(3, arr.width());
            assert_eq!(7, arr.height());
            assert_eq!(4, arr.depth());
            for i in 0..84 {
                assert_eq!(0.0, arr[i]);
            }
        }
        {
            let arr = Array3::new_with_packed_size(&USize3::new(1, 9, 5), Some(1.50));
            assert_eq!(1, arr.width());
            assert_eq!(9, arr.height());
            assert_eq!(5, arr.depth());
            for i in 0..45 {
                assert_eq!(1.5, arr[i]);
            }
        }
        {
            let arr = Array3::<f32>::new_with_size(5, 2, 8, None);
            assert_eq!(5, arr.width());
            assert_eq!(2, arr.height());
            assert_eq!(8, arr.depth());
            for i in 0..80 {
                assert_eq!(0.0, arr[i]);
            }
        }
        {
            let arr = Array3::new_with_size(3, 4, 2, Some(7.0));
            assert_eq!(3, arr.width());
            assert_eq!(4, arr.height());
            assert_eq!(2, arr.depth());
            for i in 0..24 {
                assert_eq!(7.0, arr[i]);
            }
        }
        {
            let arr = Array3::new_slice(
                &[&[&[1.0, 2.0, 3.0, 4.0],
                    &[5.0, 6.0, 7.0, 8.0],
                    &[9.0, 10.0, 11.0, 12.0]],
                    &[&[13.0, 14.0, 15.0, 16.0],
                        &[17.0, 18.0, 19.0, 20.0],
                        &[21.0, 22.0, 23.0, 24.0]]]);

            assert_eq!(4, arr.width());
            assert_eq!(3, arr.height());
            assert_eq!(2, arr.depth());
            for i in 0..24 {
                assert_eq!(i as f32 + 1.0, arr[i]);
            }
        }
    }

    #[test]
    fn clear() {
        let mut arr = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);

        arr.clear();
        assert_eq!(0, arr.width());
        assert_eq!(0, arr.height());
        assert_eq!(0, arr.depth());
    }

    #[test]
    fn resize_method() {
        {
            let mut arr = Array3::default();
            arr.resize_with_packed_size(&USize3::new(2, 9, 5), None);
            assert_eq!(2, arr.width());
            assert_eq!(9, arr.height());
            assert_eq!(5, arr.depth());
            for i in 0..90 {
                assert_eq!(0.0, arr[i]);
            }

            arr.resize_with_packed_size(&USize3::new(8, 13, 7), Some(4.0));
            assert_eq!(8, arr.width());
            assert_eq!(13, arr.height());
            assert_eq!(7, arr.depth());
            for k in 0..7 {
                for j in 0..13 {
                    for i in 0..8 {
                        if i < 2 && j < 9 && k < 5 {
                            assert_eq!(0.0, arr[(i, j, k)]);
                        } else {
                            assert_eq!(4.0, arr[(i, j, k)]);
                        }
                    }
                }
            }
        }
        {
            let mut arr = Array3::default();
            arr.resize_with_size(7, 6, 3, None);
            assert_eq!(7, arr.width());
            assert_eq!(6, arr.height());
            assert_eq!(3, arr.depth());
            for i in 0..126 {
                assert_eq!(0.0, arr[i]);
            }

            arr.resize_with_size(1, 9, 4, Some(3.0));
            assert_eq!(1, arr.width());
            assert_eq!(9, arr.height());
            assert_eq!(4, arr.depth());
            for k in 0..4 {
                for j in 0..9 {
                    for i in 0..1 {
                        if j < 6 && k < 3 {
                            assert_eq!(0.0, arr[(i, j, k)]);
                        } else {
                            assert_eq!(3.0, arr[(i, j, k)]);
                        }
                    }
                }
            }
        }
    }

    #[test]
    fn iterators() {
        let mut arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);

        let mut cnt = 1.0;
        for elem in arr1.data_mut() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }

        cnt = 1.0;
        for elem in arr1.data() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);

        let mut i = 0;
        arr1.for_each(|val| {
            assert_eq!(arr1[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array3::new_slice(
            &[&[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]],
                &[&[13.0, 14.0, 15.0, 16.0],
                    &[17.0, 18.0, 19.0, 20.0],
                    &[21.0, 22.0, 23.0, 24.0]]]);

        arr1.for_each_index(|i, j, k| {
            let idx = i + (4 * (j + 3 * k)) + 1;
            assert_eq!(idx as f32, arr1[(i, j, k)]);
        });
    }
}