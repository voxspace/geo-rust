/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::vector3::Vector3D;
use crate::vector4::Vector4D;
use crate::matrix2x2::Matrix2x2D;
use crate::matrix3x3::Matrix3x3D;
use crate::matrix4x4::Matrix4x4D;

///
/// # Generic BLAS operator wrapper class
///
/// This class provides BLAS (Basic Linear Algebra Subprograms)-like set of
/// operators for vector and matrix class. By default, it supports Vector<T, 2>,
/// Vector<T, 3>, Vector<T, 4>, Matrix<T, 2, 2>, Matrix<T, 3, 3> and
/// Matrix<T, 4, 4>. For custom vector/matrix classes, create a new BLAS class
/// that conforms the function interfaces defined in this class. It will enable
/// performing linear algebra routines (such as conjugate gradient) for the
/// custom vector/matrix types.
///
pub trait Blas: Clone {
    type VectorType: Default + Clone;
    type MatrixType: Default + Clone;

    /// Sets entire element of given vector \p result with scalar \p s.
    fn set_sv(s: f64, result: &mut Self::VectorType);

    /// Copies entire element of given vector \p result with other vector \p v.
    fn set_vv(v: &Self::VectorType, result: &mut Self::VectorType);

    /// Sets entire element of given matrix \p result with scalar \p s.
    fn set_sm(s: f64, result: &mut Self::MatrixType);

    /// Copies entire element of given matrix \p result with other matrix \p v.
    fn set_mm(m: &Self::MatrixType, result: &mut Self::MatrixType);

    /// Performs dot product with vector \p a and \p b.
    fn dot(a: &Self::VectorType, b: &Self::VectorType) -> f64;

    /// Performs ax + y operation where \p a is a matrix and \p x and \p y are
    /// vectors.
    fn axpy(a: f64,
            x: &Self::VectorType,
            y: &Self::VectorType,
            result: &mut Self::VectorType);

    /// Performs matrix-vector multiplication.
    fn mvm(m: &Self::MatrixType,
           v: &Self::VectorType,
           result: &mut Self::VectorType);

    /// Computes residual vector (b - ax).
    fn residual(a: &Self::MatrixType,
                x: &Self::VectorType,
                b: &Self::VectorType,
                result: &mut Self::VectorType);

    /// Returns L2-norm of the given vector \p v.
    fn l2norm(v: &Self::VectorType) -> f64;

    /// Returns L_inf-norm of the given vector \p v.
    fn l_inf_norm(v: &Self::VectorType) -> f64;
}

//--------------------------------------------------------------------------------------------------
#[derive(Clone)]
pub struct BlasVector2 {}

#[derive(Clone)]
pub struct BlasVector3 {}

#[derive(Clone)]
pub struct BlasVector4 {}

macro_rules! impl_blas {
    ($VectorType:ty, $MatrixType:ty, $BlasType:ty) => {
        impl Blas for $BlasType {
            type VectorType = $VectorType;
            type MatrixType = $MatrixType;

            fn set_sv(s: f64, result: &mut $VectorType) {
                result.set_scalar(s);
            }

            fn set_vv(v: &$VectorType, result: &mut $VectorType) {
                result.set_self(v);
            }

            fn set_sm(s: f64, result: &mut $MatrixType) {
                result.set_scalar(s);
            }

            fn set_mm(m: &$MatrixType, result: &mut $MatrixType) {
                result.set_self(m);
            }

            fn dot(a: &$VectorType, b: &$VectorType) -> f64 {
                return a.dot(b);
            }

            fn axpy(a: f64, x: &$VectorType, y: &$VectorType, result: &mut $VectorType) {
                *result = *x * a + *y;
            }

            fn mvm(m: &$MatrixType, v: &$VectorType, result: &mut $VectorType) {
                *result = m.mul_vec(v);
            }

            fn residual(a: &$MatrixType, x: &$VectorType, b: &$VectorType, result: &mut $VectorType) {
                *result = *b - a.mul_vec(x);
            }

            fn l2norm(v: &$VectorType) -> f64 {
                return f64::sqrt(v.dot(v));
            }

            fn l_inf_norm(v: &$VectorType) -> f64 {
                return f64::abs(v.absmax());
            }
        }
    };
}

impl_blas!(Vector2D, Matrix2x2D, BlasVector2);
impl_blas!(Vector3D, Matrix3x3D, BlasVector3);
impl_blas!(Vector4D, Matrix4x4D, BlasVector4);

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod blas {
    use crate::vector3::Vector3D;
    use crate::matrix3x3::Matrix3x3D;
    use crate::blas::{BlasVector3, Blas};

    #[test]
    fn set() {
        let mut vec = Vector3D::default();

        BlasVector3::set_sv(3.14, &mut vec);

        assert_eq!(3.14, vec.x);
        assert_eq!(3.14, vec.y);
        assert_eq!(3.14, vec.z);

        let vec2 = Vector3D::new(5.1, 3.7, 8.2);
        BlasVector3::set_vv(&vec2, &mut vec);

        assert_eq!(5.1, vec.x);
        assert_eq!(3.7, vec.y);
        assert_eq!(8.2, vec.z);

        let mut mat = Matrix3x3D::default();

        BlasVector3::set_sm(0.414, &mut mat);

        for i in 0..9 {
            let elem = mat[i];
            assert_eq!(0.414, elem);
        }

        let mat2 = Matrix3x3D::new(1.0, 2.0, 3.0,
                                   4.0, 5.0, 6.0,
                                   7.0, 8.0, 9.0);
        BlasVector3::set_mm(&mat2, &mut mat);
        for i in 0..9 {
            assert_eq!(i as f64 + 1.0, mat[i]);
        }
    }

    #[test]
    fn dot() {
        let vec = Vector3D::new(1.0, 2.0, 3.0);
        let vec2 = Vector3D::new(4.0, 5.0, 6.0);
        let result = BlasVector3::dot(&vec, &vec2);
        assert_eq!(32.0, result);
    }

    #[test]
    fn axpy() {
        let mut result = Vector3D::default();
        BlasVector3::axpy(2.5, &Vector3D::new(1.0, 2.0, 3.0),
                          &Vector3D::new(4.0, 5.0, 6.0), &mut result);

        assert_eq!(6.5, result.x);
        assert_eq!(10.0, result.y);
        assert_eq!(13.5, result.z);
    }

    #[test]
    fn mvm() {
        let mat = Matrix3x3D::new(1.0, 2.0, 3.0,
                                  4.0, 5.0, 6.0,
                                  7.0, 8.0, 9.0);

        let mut result = Vector3D::default();
        BlasVector3::mvm(
            &mat, &Vector3D::new(1.0, 2.0, 3.0), &mut result);

        assert_eq!(14.0, result.x);
        assert_eq!(32.0, result.y);
        assert_eq!(50.0, result.z);
    }

    #[test]
    fn residual() {
        let mat = Matrix3x3D::new(1.0, 2.0, 3.0,
                                  4.0, 5.0, 6.0,
                                  7.0, 8.0, 9.0);

        let mut result = Vector3D::default();
        BlasVector3::residual(&mat, &Vector3D::new(1.0, 2.0, 3.0),
                              &Vector3D::new(4.0, 5.0, 6.0), &mut result);

        assert_eq!(-10.0, result.x);
        assert_eq!(-27.0, result.y);
        assert_eq!(-44.0, result.z);
    }

    #[test]
    fn l2norm() {
        let vec = Vector3D::new(-1.0, 2.0, -3.0);
        let result = BlasVector3::l2norm(&vec);
        assert_eq!(f64::sqrt(14.0), result);
    }

    #[test]
    fn linf_norm() {
        let vec = Vector3D::new(-1.0, 2.0, -3.0);
        let result = BlasVector3::l_inf_norm(&vec);
        assert_eq!(3.0, result);
    }
}