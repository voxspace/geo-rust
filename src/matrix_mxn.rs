/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::array2::Array2;
use crate::matrix_expression::*;
use crate::vector_expression::VectorExpression;
use crate::usize2::USize2;
use std::ops::*;
use rayon::prelude::*;

/// # MatrixMxN

///
/// \brief M x N matrix class.
///
/// This class defines M x N row-major matrix.
///
/// \tparam T Type of the element.
///
#[derive(Clone)]
pub struct MatrixMxN {
    _elements: Array2<f64>,
}

impl MatrixExpression for MatrixMxN {
    /// Returns the size of this matrix.
    fn size(&self) -> USize2 {
        return USize2::new(self.rows(), self.cols());
    }

    /// Returns number of rows of this matrix.
    fn rows(&self) -> usize {
        return self._elements.height();
    }

    /// Returns number of columns of this matrix.
    fn cols(&self) -> usize {
        return self._elements.width();
    }

    fn eval(&self, x: usize, y: usize) -> f64 {
        return self._elements[(y, x)];
    }
}

impl Default for MatrixMxN {
    /// Constructs an empty matrix.
    fn default() -> Self {
        return MatrixMxN {
            _elements: Array2::default()
        };
    }
}

/// # Constructors
impl MatrixMxN {
    /// Constructs m x n constant value matrix.
    pub fn new(m: usize, n: usize, s: Option<f64>) -> MatrixMxN {
        let mut mat = MatrixMxN::default();
        mat.resize(m, n, s);
        return mat;
    }

    ///
    /// \brief Constructs a matrix with given initializer list \p lst.
    ///
    /// This constructor will build a matrix with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// MatrixMxN<float> mat = {
    ///     {1.f, 2.f, 4.f, 3.f},
    ///     {9.f, 3.f, 5.f, 1.f},
    ///     {4.f, 8.f, 1.f, 5.f}
    /// };
    /// \endcode
    ///
    /// Note the initializer has 4x3 structure which will create 4x3 matrix.
    ///
    /// \param lst Initializer list that should be copy to the new matrix.
    ///
    pub fn new_slice(lst: &[&[f64]]) -> MatrixMxN {
        let mut mat = MatrixMxN::default();
        mat._elements.set_slice(lst);
        return mat;
    }

    /// Constructs a matrix with expression template.
    pub fn new_expression<E: MatrixExpression>(other: &E) -> MatrixMxN {
        let mut mat = MatrixMxN::default();
        mat.set_expression(other);
        return mat;
    }

    /// Constructs a m x n matrix with input array.
    /// \warning Ordering of the input elements is row-major.
    pub fn new_array(m: usize, n: usize, arr: &[f64]) -> MatrixMxN {
        let mut mat = MatrixMxN::default();
        mat.set_array(m, n, arr);
        return mat;
    }
}

/// # Basic setters
impl MatrixMxN {
    /// Resizes to m x n matrix with initial value \p s.
    pub fn resize(&mut self, m: usize, n: usize, s: Option<f64>) {
        // Note that m and n are flipped.
        self._elements.resize_with_size(n, m, s);
    }

    /// Sets whole matrix with input scalar.
    pub fn set_scalar(&mut self, s: f64) {
        self._elements.set_scalar(s);
    }

    ///
    /// \brief Sets a matrix with given initializer list \p lst.
    ///
    /// This function will fill the matrix with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// MatrixMxN<float> mat;
    /// mat.set({
    ///     {1.f, 2.f, 4.f, 3.f},
    ///     {9.f, 3.f, 5.f, 1.f},
    ///     {4.f, 8.f, 1.f, 5.f}
    /// });
    /// \endcode
    ///
    /// Note the initializer has 4x3 structure which will resize to 4x3 matrix.
    ///
    /// \param lst Initializer list that should be copy to the new matrix.
    ///
    pub fn set_slice(&mut self, lst: &[&[f64]]) {
        self._elements.set_slice(lst);
    }

    /// Copies from input matrix expression.
    pub fn set_expression<E: MatrixExpression>(&mut self, other: &E) {
        self.resize(other.rows(), other.cols(), None);

        // todo Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                self[(i, j)] = other.eval(i, j);
            }
        }
    }

    /// Copies from input array.
    /// \warning Ordering of the input elements is row-major.
    pub fn set_array(&mut self, m: usize, n: usize, arr: &[f64]) {
        self.resize(m, n, None);
        let sz = m * n;
        for i in 0..sz {
            self._elements[i] = arr[i];
        }
    }

    /// Sets diagonal elements with input scalar.
    pub fn set_diagonal(&mut self, s: f64) {
        let l = usize::min(self.rows(), self.cols());
        for i in 0..l {
            self[(i, i)] = s;
        }
    }

    /// Sets off-diagonal elements with input scalar.
    pub fn set_off_diagonal(&mut self, s: f64) {
        //todo parallelism
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                if i != j {
                    self[(i, j)] = s;
                }
            }
        }
    }

    /// Sets i-th row with input vector.
    pub fn set_row<E: VectorExpression>(&mut self, i: usize, row: &E) {
        debug_assert!(self.cols() == row.size());

        //todo parallelism
        (0..self.cols()).for_each(|j| {
            self[(i, j)] = row.eval(j);
        });
    }

    /// Sets j-th column with input vector.
    pub fn set_column<E: VectorExpression>(&mut self, j: usize, col: &E) {
        debug_assert!(self.rows() == col.size());

        //todo parallelism
        (0..self.rows()).for_each(|i| {
            self[(i, j)] = col.eval(i);
        });
    }
}

/// # Basic getters
impl MatrixMxN {
    pub fn is_equal<E: MatrixExpression>(&self, other: &E) -> bool {
        if self.size() != other.size() {
            return false;
        }

        for i in 0..self.rows() {
            for j in 0..self.cols() {
                if self[(i, j)] != other.eval(i, j) {
                    return false;
                }
            }
        }

        return true;
    }

    /// Returns true if this matrix is similar to the input matrix within the
    /// given tolerance.
    pub fn is_similar<E: MatrixExpression>(&self, other: &E, tol: Option<f64>) -> bool {
        if self.size() != other.size() {
            return false;
        }

        for i in 0..self.rows() {
            for j in 0..self.cols() {
                if f64::abs(self[(i, j)] - other.eval(i, j)) > tol.unwrap_or(f64::EPSILON) {
                    return false;
                }
            }
        }

        return true;
    }

    /// Returns true if this matrix is a square matrix.
    pub fn is_square(&self) -> bool {
        return self.rows() == self.cols();
    }

    /// Returns data pointer of this matrix.
    pub fn data_mut(&mut self) -> &mut [f64] {
        return self._elements.data_mut();
    }

    /// Returns constant pointer of this matrix.
    pub fn data(&self) -> &[f64] {
        return self._elements.data();
    }
}

/// # Binary operator methods - new instance = this instance (+) input
impl MatrixMxN {
    /// Returns this matrix + input scalar.
    pub fn add_scalar(&self, s: f64) -> MatrixScalarAdd<MatrixMxN> {
        return MatrixScalarAdd::new(self, s);
    }

    /// Returns this matrix + input matrix (element-wise).
    pub fn add_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixAdd<MatrixMxN, E> {
        return MatrixAdd::new(self, m);
    }

    /// Returns this matrix - input scalar.
    pub fn sub_scalar(&self, s: f64) -> MatrixScalarSub<MatrixMxN> {
        return MatrixScalarSub::new(self, s);
    }

    /// Returns this matrix - input matrix (element-wise).
    pub fn sub_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixSub<MatrixMxN, E> {
        return MatrixSub::new(self, m);
    }

    /// Returns this matrix * input scalar.
    pub fn mul_scalar(&self, s: f64) -> MatrixScalarMul<MatrixMxN> {
        return MatrixScalarMul::new(self, s);
    }

    /// Returns this matrix * input vector.
    pub fn mul_vec_expression<'a, VE: VectorExpression>(&'a self, v: &'a VE) -> MatrixVectorMul<MatrixMxN, VE> {
        return MatrixVectorMul::new(self, v);
    }

    /// Returns this matrix * input matrix.
    pub fn mul_mat_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixMul<MatrixMxN, E> {
        return MatrixMul::new(self, m);
    }

    /// Returns this matrix / input scalar.
    pub fn div_scalar(&self, s: f64) -> MatrixScalarDiv<MatrixMxN> {
        return MatrixScalarDiv::new(self, s);
    }
}

/// # Binary operator methods - new instance = input (+) this instance
impl MatrixMxN {
    /// Returns input scalar + this matrix.
    pub fn radd_scalar(&self, s: f64) -> MatrixScalarAdd<MatrixMxN> {
        return MatrixScalarAdd::new(self, s);
    }

    /// Returns input matrix + this matrix (element-wise).
    pub fn radd_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixAdd<E, MatrixMxN> {
        return MatrixAdd::new(m, self);
    }

    /// Returns input scalar - this matrix.
    pub fn rsub_scalar(&self, s: f64) -> MatrixScalarRSub<MatrixMxN> {
        return MatrixScalarRSub::new(self, s);
    }

    /// Returns input matrix - this matrix (element-wise).
    pub fn rsub_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixSub<E, MatrixMxN> {
        return MatrixSub::new(m, self);
    }

    /// Returns input scalar * this matrix.
    pub fn rmul_scalar(&self, s: f64) -> MatrixScalarMul<MatrixMxN> {
        return MatrixScalarMul::new(self, s);
    }

    /// Returns input matrix * this matrix.
    pub fn rmul_expression<'a, E: MatrixExpression>(&'a self, m: &'a E) -> MatrixMul<E, MatrixMxN> {
        return MatrixMul::new(m, self);
    }

    /// Returns input matrix / this scalar.
    pub fn rdiv_scalar(&self, s: f64) -> MatrixScalarRDiv<MatrixMxN> {
        return MatrixScalarRDiv::new(self, s);
    }
}

/// # Augmented operator methods - this instance (+)= input
impl MatrixMxN {
    /// Adds input scalar to this matrix.
    pub fn iadd_scalar(&mut self, s: f64) {
        let expression = self.add_scalar(s);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);
        // todo Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }

    /// Adds input matrix to this matrix (element-wise).
    pub fn iadd_expression<E: MatrixExpression>(&mut self, m: &E) {
        let expression = self.add_expression(m);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);
        // todo Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }

    /// Subtracts input scalar from this matrix.
    pub fn isub_scalar(&mut self, s: f64) {
        let expression = self.sub_scalar(s);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);
        // todo Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }

    /// Subtracts input matrix from this matrix (element-wise).
    pub fn isub_expression<E: MatrixExpression>(&mut self, m: &E) {
        let expression = self.sub_expression(m);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }

    /// Multiplies input scalar to this matrix.
    pub fn imul_scalar(&mut self, s: f64) {
        let expression = self.mul_scalar(s);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);
        // todo Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }

    /// Multiplies input matrix to this matrix.
    pub fn imul_expression<E: MatrixExpression>(&mut self, m: &E) {
        let expression = self.mul_mat_expression(m);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);

        // todo Parallel evaluation of the expression
        for i in 0..mat.rows() {
            for j in 0..mat.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }

    /// Divides this matrix with input scalar.
    pub fn idiv_scalar(&mut self, s: f64) {
        let expression = self.div_scalar(s);
        let mut mat = MatrixMxN::default();
        mat.resize(expression.rows(), expression.cols(), None);
        // todo Parallel evaluation of the expression
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                mat[(i, j)] = expression.eval(i, j);
            }
        }

        *self = mat;
    }
}

/// # Modifiers
impl MatrixMxN {
    /// Transposes this matrix.
    pub fn transpose(&mut self) {
        self.set_expression(&self.transposed());
    }

    ///
    /// \brief Inverts this matrix.
    ///
    /// This function computes the inverse using Gaussian elimination method.
    ///
    pub fn invert(&mut self) {
        debug_assert!(self.is_square());

        // Computes inverse matrix using Gaussian elimination method.
        // https://martin-thoma.com/solving-linear-equations-with-gaussian-elimination/
        let n = self.rows();
        let mut rhs = MatrixMxN::new_expression(&MatrixMxN::make_identity(n));

        for i in 0..n {
            // Search for maximum in this column
            let mut max_el = f64::abs(self[(i, i)]);
            let mut max_row = i;
            for k in i + 1..n {
                if f64::abs(self[(k, i)]) > max_el {
                    max_el = f64::abs(self[(k, i)]);
                    max_row = k;
                }
            }

            // Swap maximum row with current row (column by column)
            if max_row != i {
                for k in i..n {
                    {
                        let temp = self[(max_row, k)];
                        self[(max_row, k)] = self[(i, k)];
                        self[(i, k)] = temp;
                    }
                    {
                        let temp = rhs[(max_row, k)];
                        rhs[(max_row, k)] = rhs[(i, k)];
                        rhs[(i, k)] = temp;
                    }
                }
            }

            // Make all rows except this one 0 in current column
            for k in 0..n {
                if k == i {
                    continue;
                }
                let c = -self[(k, i)] / self[(i, i)];
                for j in 0..n {
                    rhs[(k, j)] += c * rhs[(i, j)];
                    if i == j {
                        self[(k, j)] = 0.0;
                    } else if i < j {
                        self[(k, j)] += c * self[(i, j)];
                    }
                }
            }

            // Scale
            for k in 0..n {
                let c = 1.0 / self[(k, k)];
                for j in 0..n {
                    self[(k, j)] *= c;
                    rhs[(k, j)] *= c;
                }
            }
        }

        self.set_expression(&rhs);
    }
}

/// # Complex getters
impl MatrixMxN {
    /// Returns sum of all elements.
    pub fn sum(&self) -> f64 {
        return self._elements.data().par_iter().sum();
    }

    /// Returns average of all elements.
    pub fn avg(&self) -> f64 {
        return self.sum() / (self.rows() * self.cols()) as f64;
    }

    /// Returns minimum among all elements.
    pub fn min(&self) -> f64 {
        return self._elements.data().par_iter().cloned().reduce(|| { f64::MAX }, |a, b| {
            return f64::min(a, b);
        });
    }

    /// Returns maximum among all elements.
    pub fn max(&self) -> f64 {
        return self._elements.data().par_iter().cloned().reduce(|| { f64::MIN }, |a, b| {
            return f64::max(a, b);
        });
    }

    /// Returns absolute minimum among all elements.
    pub fn absmin(&self) -> f64 {
        return self._elements.data().par_iter().cloned().reduce(|| { f64::MAX }, |a, b| {
            return crate::math_utils::absmin(a, b);
        });
    }

    /// Returns absolute maximum among all elements.
    pub fn absmax(&self) -> f64 {
        return self._elements.data().par_iter().cloned().reduce(|| { 0.0 }, |a, b| {
            return crate::math_utils::absmax(a, b);
        });
    }

    /// Returns sum of all diagonal elements.
    /// \warning Should be a square matrix.
    pub fn trace(&self) -> f64 {
        //todo parallelism
        let mut result = 0.0;
        for i in 0..self.rows() {
            result += self._elements[(i, i)];
        }
        return result;
    }

    /// Returns determinant of this matrix.
    pub fn determinant(&self) -> f64 {
        debug_assert!(self.is_square());

        // Computes inverse matrix using Gaussian elimination method.
        // https://martin-thoma.com/solving-linear-equations-with-gaussian-elimination/
        let n = self.rows();
        let mut a = self.clone();

        let mut result = 1.0;
        for i in 0..n {
            // Search for maximum in this column
            let mut max_el = f64::abs(a[(i, i)]);
            let mut max_row = i;
            for k in i + 1..n {
                if f64::abs(a[(k, i)]) > max_el {
                    max_el = f64::abs(a[(k, i)]);
                    max_row = k;
                }
            }

            // Swap maximum row with current row (column by column)
            if max_row != i {
                for k in i..n {
                    let temp = a[(max_row, k)];
                    a[(max_row, k)] = a[(i, k)];
                    a[(i, k)] = temp;
                }
                result *= -1.0;
            }

            // Make all rows below this one 0 in current column
            for k in i + 1..n {
                let c = -a[(k, i)] / a[(i, i)];
                for j in i..n {
                    if i == j {
                        a[(k, j)] = 0.0;
                    } else {
                        a[(k, j)] += c * a[(i, j)];
                    }
                }
            }
        }

        for i in 0..n {
            result *= a[(i, i)];
        }
        return result;
    }

    /// Returns diagonal part of this matrix.
    pub fn diagonal(&self) -> MatrixDiagonal<MatrixMxN> {
        return MatrixDiagonal::new(self, true);
    }

    /// Returns off-diagonal part of this matrix.
    pub fn off_diagonal(&self) -> MatrixDiagonal<MatrixMxN> {
        return MatrixDiagonal::new(self, false);
    }

    /// Returns strictly lower triangle part of this matrix.
    pub fn strict_lower_tri(&self) -> MatrixTriangular<MatrixMxN> {
        return MatrixTriangular::new(self, false, true);
    }

    /// Returns strictly upper triangle part of this matrix.
    pub fn strict_upper_tri(&self) -> MatrixTriangular<MatrixMxN> {
        return MatrixTriangular::new(self, true, true);
    }

    /// Returns lower triangle part of this matrix (including the diagonal).
    pub fn lower_tri(&self) -> MatrixTriangular<MatrixMxN> {
        return MatrixTriangular::new(self, false, false);
    }

    /// Returns upper triangle part of this matrix (including the diagonal).
    pub fn upper_tri(&self) -> MatrixTriangular<MatrixMxN> {
        return MatrixTriangular::new(self, true, false);
    }

    /// Returns transposed matrix.
    pub fn transposed(&self) -> MatrixMxN {
        let mut mt = MatrixMxN::new(self.cols(), self.rows(), None);
        self.for_each_index(|i, j| {
            mt[(j, i)] = self[(i, j)];
        });
        return mt;
    }

    /// Returns inverse matrix.
    pub fn inverse(&self) -> MatrixMxN {
        let mut m_inv = self.clone();
        m_inv.invert();
        return m_inv;
    }
}

/// # Helpers
impl MatrixMxN {
    ///
    /// \brief Iterates the matrix and invoke given \p func for each index.
    ///
    /// This function iterates the matrix elements and invoke the callback
    /// function \p func. The callback function takes matrix's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// MatrixMxN<double> mat(100, 200, 4.0);
    /// for (size_t i = 0; i < mat.rows(); ++i) {
    ///     for (size_t j = 0; j < mat.cols(); ++j) {
    ///         func(mat(i, j));
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// MatrixMxN<double> mat(100, 200, 4.0);
    /// mat.for_each([](double elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&f64)>(&self, mut func: Callback) {
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                func(&self[(i, j)]);
            }
        }
    }

    ///
    /// \brief Iterates the matrix and invoke given \p func for each index.
    ///
    /// This function iterates the matrix elements and invoke the callback
    /// function \p func. The callback function takes two parameters which are
    /// the (i, j) indices of the matrix. The order of execution will be the
    /// same as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// MatrixMxN<double> mat(100, 200, 4.0);
    /// for (size_t i = 0; i < mat.rows(); ++i) {
    ///     for (size_t j = 0; j < mat.cols(); ++j) {
    ///         func(i, j);
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// MatrixMxN<double> mat(100, 200, 4.0);
    /// mat.for_each_index([&](size_t i, size_t j) {
    ///     mat(i, j) = 4.0 * i + 7.0 * j + 1.5;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize)>(&self, mut func: Callback) {
        for i in 0..self.rows() {
            for j in 0..self.cols() {
                func(i, j);
            }
        }
    }
}

/// # Builders
impl MatrixMxN {
    /// Makes a m x n matrix with zeros.
    pub fn make_zero(m: usize, n: usize) -> MatrixConstant {
        return MatrixConstant::new(m, n, 0.0);
    }

    /// Makes a m x m matrix with all diagonal elements to 1, and other elements
    /// to 0.
    pub fn make_identity(m: usize) -> MatrixIdentity {
        return MatrixIdentity::new(m);
    }
}

impl AddAssign<f64> for MatrixMxN {
    fn add_assign(&mut self, rhs: f64) {
        self.iadd_scalar(rhs);
    }
}

impl<E: MatrixExpression> AddAssign<&E> for MatrixMxN {
    fn add_assign(&mut self, rhs: &E) {
        self.iadd_expression(rhs);
    }
}

impl SubAssign<f64> for MatrixMxN {
    fn sub_assign(&mut self, rhs: f64) {
        self.isub_scalar(rhs);
    }
}

impl<E: MatrixExpression> SubAssign<&E> for MatrixMxN {
    fn sub_assign(&mut self, rhs: &E) {
        self.isub_expression(rhs);
    }
}

impl MulAssign<f64> for MatrixMxN {
    fn mul_assign(&mut self, rhs: f64) {
        self.imul_scalar(rhs);
    }
}

impl<E: MatrixExpression> MulAssign<&E> for MatrixMxN {
    fn mul_assign(&mut self, rhs: &E) {
        self.imul_expression(rhs);
    }
}

impl DivAssign<f64> for MatrixMxN {
    fn div_assign(&mut self, rhs: f64) {
        self.idiv_scalar(rhs);
    }
}

impl Index<usize> for MatrixMxN {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._elements[index];
    }
}

impl IndexMut<usize> for MatrixMxN {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._elements[index];
    }
}

impl Index<(usize, usize)> for MatrixMxN {
    type Output = f64;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        return &self._elements[(index.1, index.0)];
    }
}

impl IndexMut<(usize, usize)> for MatrixMxN {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        return &mut self._elements[(index.1, index.0)];
    }
}

impl PartialEq for MatrixMxN {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod matrix_mxn {
    use crate::matrix_mxn::MatrixMxN;
    use crate::matrix_expression::MatrixExpression;
    use crate::vector_n::VectorN;
    use crate::assert_delta;

    #[test]
    fn constructors() {
        let mat = MatrixMxN::default();
        assert_eq!(0, mat.rows());
        assert_eq!(0, mat.cols());

        let mat3 = MatrixMxN::new(4, 2, Some(5.0));
        assert_eq!(4, mat3.rows());
        assert_eq!(2, mat3.cols());
        for i in 0..8 {
            assert_eq!(5.0, mat3[i]);
        }

        let mat4 = MatrixMxN::new_slice(&[
            &[1.0, 2.0, 3.0, 4.0],
            &[5.0, 6.0, 7.0, 8.0],
            &[9.0, 10.0, 11.0, 12.0]]);
        assert_eq!(3, mat4.rows());
        assert_eq!(4, mat4.cols());
        for i in 0..12 {
            assert_eq!(i as f64 + 1.0, mat4[i]);
        }

        let mat5 = mat4.clone();
        assert_eq!(3, mat5.rows());
        assert_eq!(4, mat5.cols());
        for i in 0..12 {
            assert_eq!(mat4[i], mat5[i]);
        }

        let ans6 = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0];
        let mat6 = MatrixMxN::new_array(3, 2, &ans6);
        assert_eq!(3, mat6.rows());
        assert_eq!(2, mat6.cols());
        for i in 0..6 {
            assert_eq!(ans6[i], mat6[i]);
        }
    }

    #[test]
    fn basic_setters() {
        let mut mat = MatrixMxN::default();
        mat.resize(4, 2, Some(5.0));
        assert_eq!(4, mat.rows());
        assert_eq!(2, mat.cols());
        for i in 0..8 {
            assert_eq!(5.0, mat[i]);
        }

        mat.set_scalar(7.0);
        for i in 0..8 {
            assert_eq!(7.0, mat[i]);
        }

        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        assert_eq!(2, mat.rows());
        assert_eq!(4, mat.cols());
        for i in 0..8 {
            assert_eq!(i as f64 + 1.0, mat[i]);
        }

        let mut mat2 = MatrixMxN::default();
        mat2.set_expression(&mat);
        assert_eq!(2, mat2.rows());
        assert_eq!(4, mat2.cols());
        for i in 0..8 {
            assert_eq!(i as f64 + 1.0, mat2[i]);
        }

        let arr = [8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0];
        mat.set_array(4, 2, &arr);
        assert_eq!(4, mat.rows());
        assert_eq!(2, mat.cols());
        for i in 0..8 {
            assert_eq!(arr[i], mat[i]);
        }

        mat.set_diagonal(10.0);
        for i in 0..8 {
            if i == 0 || i == 3 {
                assert_eq!(10.0, mat[i]);
            } else {
                assert_eq!(arr[i], mat[i]);
            }
        }

        mat.set_off_diagonal(-1.0);
        for i in 0..8 {
            if i == 0 || i == 3 {
                assert_eq!(10.0, mat[i]);
            } else {
                assert_eq!(-1.0, mat[i]);
            }
        }

        let row = VectorN::new_slice(&[2.0, 3.0]);
        mat.set_row(2, &row);
        for i in 0..8 {
            if i == 0 || i == 3 {
                assert_eq!(10.0, mat[i]);
            } else if i == 4 {
                assert_eq!(2.0, mat[i]);
            } else if i == 5 {
                assert_eq!(3.0, mat[i]);
            } else {
                assert_eq!(-1.0, mat[i]);
            }
        }

        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        mat2.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        assert_eq!(mat.is_equal(&mat2), true);

        mat2.set_slice(&[&[1.01, 2.01, 3.01, 4.01], &[4.99, 5.99, 6.99, 7.99]]);
        assert_eq!(mat.is_similar(&mat2, Some(0.02)), true);
        assert_eq!(mat.is_similar(&mat2, Some(0.005)), false);

        assert_eq!(mat.is_square(), false);
        mat.set_slice(&[&[1.0, 2.0], &[3.0, 4.0]]);
        assert_eq!(mat.is_square(), true);
    }

    #[test]
    fn binary_operator_method() {
        let mat_a = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);

        let mut mat_b = MatrixMxN::new_expression(&mat_a.add_scalar(3.5));
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat_b[i]);
        }

        let mut mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);
        mat_b = MatrixMxN::new_expression(&mat_a.add_expression(&mat_c));
        let mut ans = MatrixMxN::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = MatrixMxN::new_expression(&mat_a.sub_scalar(1.5));
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, mat_b[i]);
        }

        mat_b = MatrixMxN::new_expression(&mat_a.sub_expression(&mat_c));
        ans = MatrixMxN::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = MatrixMxN::new_expression(&mat_a.mul_scalar(2.0));
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat_b[i]);
        }

        mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]]);
        mat_b = MatrixMxN::new_expression(&mat_a.mul_mat_expression(&mat_c));
        ans = MatrixMxN::new_slice(&[&[13.0, 41.0], &[34.0, 89.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = MatrixMxN::new_expression(&mat_a.div_scalar(2.0));
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, mat_b[i]);
        }

        mat_b = MatrixMxN::new_expression(&mat_a.radd_scalar(3.5));
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat_b[i]);
        }

        mat_b = MatrixMxN::new_expression(&mat_a.rsub_scalar(1.5));
        for i in 0..6 {
            assert_eq!(0.5 - i as f64, mat_b[i]);
        }

        mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);
        mat_b = MatrixMxN::new_expression(&mat_a.rsub_expression(&mat_c));
        ans = MatrixMxN::new_slice(&[&[2.0, -3.0, -1.0], &[5.0, -3.0, 2.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = MatrixMxN::new_expression(&mat_a.rmul_scalar(2.0));
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat_b[i]);
        }

        mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]]);
        mat_b = MatrixMxN::new_expression(&mat_c.rmul_expression(&mat_a));
        ans = MatrixMxN::new_slice(&[&[13.0, 41.0], &[34.0, 89.0]]);
        assert_eq!(ans.is_equal(&mat_b), true);

        mat_b = MatrixMxN::new_expression(&mat_a.rdiv_scalar(2.0));
        for i in 0..6 {
            assert_eq!(2.0 / (i as f64 + 1.0), mat_b[i]);
        }
    }

    #[test]
    fn augmented_operator_method() {
        let mat_a = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);
        let mat_b = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);

        let mut mat = mat_a.clone();
        mat.iadd_scalar(3.5);
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat[i]);
        }

        mat = mat_a.clone();
        mat.iadd_expression(&mat_b);
        let mut ans = MatrixMxN::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat.isub_scalar(1.5);
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, mat[i]);
        }

        mat = mat_a.clone();
        mat.isub_expression(&mat_b);
        ans = MatrixMxN::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat.imul_scalar(2.0);
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat[i]);
        }

        mat = mat_a.clone();
        let mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]]);
        mat.imul_expression(&mat_c);
        assert_eq!(2, mat.rows());
        assert_eq!(2, mat.cols());
        ans = MatrixMxN::new_slice(&[&[13.0, 41.0], &[34.0, 89.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat.idiv_scalar(2.0);
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, mat[i]);
        }
    }

    #[test]
    fn complex_getters() {
        let mat_a = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);

        assert_eq!(21.0, mat_a.sum());
        assert_eq!(21.0 / 6.0, mat_a.avg());

        let mat_b = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[-9.0, 2.0, 8.0]]);
        assert_eq!(-9.0, mat_b.min());
        assert_eq!(8.0, mat_b.max());
        assert_eq!(-1.0, mat_b.absmin());
        assert_eq!(-9.0, mat_b.absmax());

        let mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[-9.0, 2.0, 8.0], &[4.0, 3.0, 6.0]]);
        assert_eq!(11.0, mat_c.trace());

        assert_delta!(-192.0, mat_c.determinant(), f64::EPSILON);

        let mut mat = MatrixMxN::new_expression(&mat_a.diagonal());
        let mut ans = MatrixMxN::new_slice(&[&[1.0, 0.0, 0.0], &[0.0, 5.0, 0.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_expression(&mat_a.off_diagonal());
        ans = MatrixMxN::new_slice(&[&[0.0, 2.0, 3.0], &[4.0, 0.0, 6.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_expression(&mat_c.strict_lower_tri());
        ans = MatrixMxN::new_slice(&[&[0.0, 0.0, 0.0], &[-9.0, 0.0, 0.0], &[4.0, 3.0, 0.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_expression(&mat_c.strict_upper_tri());
        ans = MatrixMxN::new_slice(&[&[0.0, -1.0, 2.0], &[0.0, 0.0, 8.0], &[0.0, 0.0, 0.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_expression(&mat_c.lower_tri());
        ans = MatrixMxN::new_slice(&[&[3.0, 0.0, 0.0], &[-9.0, 2.0, 0.0], &[4.0, 3.0, 6.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_expression(&mat_c.upper_tri());
        ans = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[0.0, 2.0, 8.0], &[0.0, 0.0, 6.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_expression(&mat_a.transposed());
        ans = MatrixMxN::new_slice(&[&[1.0, 4.0], &[2.0, 5.0], &[3.0, 6.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[2.0, 5.0, 3.0], &[1.0, 0.0, 8.0]]);
        let mut mat2 = mat.inverse();
        ans = MatrixMxN::new_slice(&[&[-40.0, 16.0, 9.0], &[13.0, -5.0, -3.0], &[5.0, -2.0, -1.0]]);
        assert_eq!(mat2.is_similar(&ans, Some(1e-9)), true);

        mat = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[0.0, 1.0, 4.0], &[5.0, 6.0, 0.0]]);
        mat2 = mat.inverse();
        ans = MatrixMxN::new_slice(&[&[-24.0, 18.0, 5.0], &[20.0, -15.0, -4.0], &[-5.0, 4.0, 1.0]]);
        assert_eq!(mat2.is_similar(&ans, Some(1e-9)), true);

        mat = MatrixMxN::new_slice(&[&[0.0, 1.0, 4.0], &[1.0, 2.0, 3.0], &[5.0, 6.0, 0.0]]);
        mat2 = mat.inverse();
        ans = MatrixMxN::new_slice(&[&[18.0, -24.0, 5.0], &[-15.0, 20.0, -4.0], &[4.0, -5.0, 1.0]]);
        assert_eq!(mat2.is_similar(&ans, Some(1e-9)), true);
    }

    #[test]
    fn modifiers() {
        let mut mat = MatrixMxN::new_slice(&[&[9.0, -8.0, 7.0], &[-6.0, 5.0, -4.0], &[3.0, -2.0, 1.0]]);
        mat.transpose();

        let mut ans = MatrixMxN::new_slice(&[&[9.0, -6.0, 3.0], &[-8.0, 5.0, -2.0], &[7.0, -4.0, 1.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[2.0, 5.0, 3.0], &[1.0, 0.0, 8.0]]);
        mat.invert();
        ans = MatrixMxN::new_slice(&[&[-40.0, 16.0, 9.0], &[13.0, -5.0, -3.0], &[5.0, -2.0, -1.0]]);
        assert_eq!(mat.is_similar(&ans, Some(1e-9)), true);
    }

    #[test]
    fn setter_operators() {
        let mat_a = MatrixMxN::new_slice(&[&[1.0, 2.0, 3.0], &[4.0, 5.0, 6.0]]);
        let mat_b = MatrixMxN::new_slice(&[&[3.0, -1.0, 2.0], &[9.0, 2.0, 8.0]]);

        let mut mat = mat_a.clone();
        mat += 3.5;
        for i in 0..6 {
            assert_eq!(i as f64 + 4.5, mat[i]);
        }

        mat = mat_a.clone();
        mat += &mat_b;
        let mut ans = MatrixMxN::new_slice(&[&[4.0, 1.0, 5.0], &[13.0, 7.0, 14.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat -= 1.5;
        for i in 0..6 {
            assert_eq!(i as f64 - 0.5, mat[i]);
        }

        mat = mat_a.clone();
        mat -= &mat_b;
        ans = MatrixMxN::new_slice(&[&[-2.0, 3.0, 1.0], &[-5.0, 3.0, -2.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat *= 2.0;
        for i in 0..6 {
            assert_eq!(2.0 * (i as f64 + 1.0), mat[i]);
        }

        mat = mat_a.clone();
        let mat_c = MatrixMxN::new_slice(&[&[3.0, -1.0], &[2.0, 9.0], &[2.0, 8.0]]);
        mat *= &mat_c;
        assert_eq!(2, mat.rows());
        assert_eq!(2, mat.cols());
        ans = MatrixMxN::new_slice(&[&[13.0, 41.0], &[34.0, 89.0]]);
        assert_eq!(ans.is_equal(&mat), true);

        mat = mat_a.clone();
        mat /= 2.0;
        for i in 0..6 {
            assert_eq!((i as f64 + 1.0) / 2.0, mat[i]);
        }
    }

    #[test]
    fn getter_operator() {
        let mut mat = MatrixMxN::default();
        let mut mat2 = MatrixMxN::default();
        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        let mut cnt = 1.0;
        for i in 0..2 {
            for j in 0..4 {
                assert_eq!(cnt, mat[(i, j)]);
                cnt += 1.0;
            }
        }

        for i in 0..8 {
            assert_eq!(i as f64 + 1.0, mat[i]);
        }

        mat.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        mat2.set_slice(&[&[1.0, 2.0, 3.0, 4.0], &[5.0, 6.0, 7.0, 8.0]]);
        assert_eq!(mat.is_equal(&mat2), true);
    }

    #[test]
    fn builders() {
        let mut mat = MatrixMxN::new_expression(&MatrixMxN::make_zero(3, 4));
        assert_eq!(3, mat.rows());
        assert_eq!(4, mat.cols());
        for i in 0..12 {
            assert_eq!(0.0, mat[i]);
        }

        mat = MatrixMxN::new_expression(&MatrixMxN::make_identity(5));
        assert_eq!(5, mat.rows());
        assert_eq!(5, mat.cols());
        for i in 0..25 {
            if i % 6 == 0 {
                assert_eq!(1.0, mat[i]);
            } else {
                assert_eq!(0.0, mat[i]);
            }
        }
    }
}