/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use crate::usize2::USize2;
use crate::array_accessor2::*;
use std::ops::{Index, IndexMut};
use std::mem::swap;

///
/// \brief 2-D array class.
///
/// This class represents 2-D array data structure. Internally, the 2-D data is
/// mapped to a linear array such that (i, j) element is actually stroed at
/// (i + (width * j))th element of the linear array. This mapping means
/// iterating i first and j next will give better performance such as:
///
/// \code{.cpp}
/// Array<int, 2> array;
/// for (size_t j = 0; j < array.height(); ++j) {
///     for (size_t i = 0; i < array.width(); ++i) {
///         // Read or write array(i, j)
///     }
/// }
/// \endcode
///
/// \tparam T - Type to store in the array.
///
#[derive(Clone)]
pub struct Array2<T: ZeroInit> {
    _size: USize2,
    _data: Vec<T>,
}

impl<T: ZeroInit> Default for Array2<T> {
    /// Constructs zero-sized 2-D array.
    fn default() -> Self {
        return Array2 {
            _size: USize2::default(),
            _data: vec![],
        };
    }
}

impl<T: ZeroInit> Array2<T> {
    /// Constructs 2-D array with given \p size and fill it with \p init_val.
    /// \param size Initial size of the array.
    /// \param init_val Initial value of each array element.
    pub fn new_with_packed_size(size: &USize2, init_val: Option<T>) -> Array2<T> {
        let mut array = Array2::default();
        array.resize_with_packed_size(size, init_val);
        return array;
    }

    /// Constructs 2-D array with size \p width x \p height and fill it with
    /// \p init_val.
    /// \param width Initial width of the array.
    /// \param height Initial height of the array.
    /// \param init_val Initial value of each array element.
    pub fn new_with_size(width: usize, height: usize, init_val: Option<T>) -> Array2<T> {
        let mut array = Array2::default();
        array.resize_with_size(width, height, init_val);
        return array;
    }

    ///
    /// \brief Constructs 2-D array with given initializer list \p lst.
    ///
    /// This constructor will build 2-D array with given initializer list \p lst
    /// such as
    ///
    /// \code{.cpp}
    /// Array<int, 2> arr = {
    ///     {1, 2, 4},
    ///     {9, 3, 5}
    /// };
    /// \endcode
    ///
    /// Note the initializer also has 2-D structure. The code above will
    /// construct 3 x 2 array.
    ///
    /// \param lst Initializer list that should be copy to the new array.
    ///
    pub fn new_slice(lst: &[&[T]]) -> Array2<T> {
        let mut array = Array2::default();
        array.set_slice(lst);

        return array;
    }
}

impl<T: ZeroInit> Array2<T> {
    /// Sets entire array with given \p value.
    pub fn set_scalar(&mut self, value: T) {
        self._data.fill(value);
    }

    /// Copies given array \p other to this array.
    pub fn set_self(&mut self, other: &Array2<T>) {
        self._data.resize(other._data.len(), T::zero_init());
        self._data = other._data.clone();
        self._size = other._size;
    }

    ///
    /// Copies given initializer list \p lst to this array.
    ///
    /// This function copies given initializer list \p lst to the array such as
    ///
    /// \code{.cpp}
    /// Array<int, 2> arr;
    /// arr = {
    ///     {1, 2, 4},
    ///     {9, 3, 5}
    /// };
    /// \endcode
    ///
    /// Note the initializer also has 2-D structure. The code above will
    /// build 3 x 2 array.
    ///
    /// \param lst Initializer list that should be copy to the new array.
    ///
    pub fn set_slice(&mut self, lst: &[&[T]]) {
        let height = lst.len();
        let width = if height > 0 { lst[0].len() } else { 0 };
        self.resize_with_packed_size(&USize2::new(width, height), None);
        for j in 0..height {
            debug_assert!(width == lst[0].len());
            for i in 0..width {
                self[(i, j)] = lst[j][i].clone();
            }
        }
    }

    /// Clears the array and resizes to zero.
    pub fn clear(&mut self) {
        self._data.clear();
        self._size = USize2::new(0, 0);
    }

    /// Resizes the array with \p size and fill the new element with \p init_val.
    pub fn resize_with_packed_size(&mut self, size: &USize2, init_val: Option<T>) {
        let mut grid = Array2::default();
        grid._data.resize(size.x * size.y, init_val.unwrap_or(T::zero_init()));
        grid._size = *size;
        let i_min = usize::min(size.x, self._size.x);
        let j_min = usize::min(size.y, self._size.y);
        for j in 0..j_min {
            for i in 0..i_min {
                grid[(i, j)] = self.at_with_size(i, j).clone();
            }
        }

        self.swap(&mut grid);
    }

    /// Resizes the array with size \p width x \p height and fill the new
    /// element with \p init_val.
    pub fn resize_with_size(&mut self, width: usize, height: usize, init_val: Option<T>) {
        self.resize_with_packed_size(&USize2::new(width, height), init_val);
    }

    ///
    /// \brief Returns the reference to the i-th element.
    ///
    /// This function returns the reference to the i-th element of the array
    /// where i is the index of linearly mapped elements such that
    /// i = x + (width * y) (x and y are the 2-D coordinates of the element).
    ///
    pub fn at_index_mut(&mut self, i: usize) -> &mut T {
        debug_assert!(i < self._size.x * self._size.y);
        return &mut self._data[i];
    }

    ///
    /// \brief Returns the const reference to the i-th element.
    ///
    /// This function returns the const reference to the i-th element of the
    /// array where i is the index of linearly mapped elements such that
    /// i = x + (width * y) (x and y are the 2-D coordinates of the element).
    ///
    pub fn at_index(&self, i: usize) -> &T {
        debug_assert!(i < self._size.x * self._size.y);
        return &self._data[i];
    }

    /// Returns the reference to the element at (pt.x, pt.y).
    pub fn at_with_packed_size_mut(&mut self, pt: &USize2) -> &mut T {
        return self.at_with_size_mut(pt.x, pt.y);
    }

    /// Returns the const reference to the element at (pt.x, pt.y).
    pub fn at_with_packed_size(&self, pt: &USize2) -> &T {
        return self.at_with_size(pt.x, pt.y);
    }

    /// Returns the reference to the element at (i, j).
    pub fn at_with_size_mut(&mut self, i: usize, j: usize) -> &mut T {
        debug_assert!(i < self._size.x && j < self._size.y);
        return &mut self._data[i + self._size.x * j];
    }

    /// Returns the const reference to the element at (i, j).
    pub fn at_with_size(&self, i: usize, j: usize) -> &T {
        debug_assert!(i < self._size.x && j < self._size.y);
        return &self._data[i + self._size.x * j];
    }

    /// Returns the size of the array.
    pub fn size(&self) -> USize2 {
        return self._size;
    }

    /// Returns the width of the array.
    pub fn width(&self) -> usize {
        return self._size.x;
    }

    /// Returns the height of the array.
    pub fn height(&self) -> usize {
        return self._size.y;
    }

    /// Returns the raw pointer to the array data.
    pub fn data_mut(&mut self) -> &mut [T] {
        return &mut self._data;
    }

    /// Returns the const raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return &self._data;
    }

    /// Returns the array accessor.
    pub fn accessor(&mut self) -> ArrayAccessor2<T> {
        return ArrayAccessor2::new_with_packed_size(&self.size(), self.data_mut());
    }

    /// Returns the const array accessor.
    pub fn const_accessor(&self) -> ConstArrayAccessor2<T> {
        return ConstArrayAccessor2::new_with_packed_size(&self.size(), self.data());
    }

    /// Swaps the content of the array with \p other array.
    pub fn swap(&mut self, other: &mut Array2<T>) {
        swap(&mut other._data, &mut self._data);
        swap(&mut other._size, &mut self._size);
    }
}

impl<T: ZeroInit> Array2<T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be the same as the nested for-loop
    /// below:
    ///
    /// \code{.cpp}
    /// Array<int, 2> array;
    /// for (size_t j = 0; j < array.height(); ++j) {
    ///     for (size_t i = 0; i < array.width(); ++i) {
    ///         func(array(i, j));
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// Array<int, 2> array(100, 200, 4);
    /// array.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, func: Callback) {
        self.const_accessor().for_each(func);
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes two parameters which are
    /// the (i, j) indices of the array. The order of execution will be the same
    /// as the nested for-loop below:
    ///
    /// \code{.cpp}
    /// Array<int, 2> array;
    /// for (size_t j = 0; j < array.height(); ++j) {
    ///     for (size_t i = 0; i < array.width(); ++i) {
    ///         func(i, j);
    ///     }
    /// }
    /// \endcode
    ///
    /// Below is the sample usage:
    ///
    /// \code{.cpp}
    /// Array<int, 2> array(10, 20, 4);
    /// array.for_each_index([&](size_t i, size_t j) {
    ///     array(i, j) = 4.f * i + 7.f * j + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize, usize)>(&self, func: Callback) {
        self.const_accessor().for_each_index(func);
    }
}

impl<T: ZeroInit> Index<usize> for Array2<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data[index];
    }
}

impl<T: ZeroInit> IndexMut<usize> for Array2<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._data[index];
    }
}

impl<T: ZeroInit> Index<USize2> for Array2<T> {
    type Output = T;

    fn index(&self, index: USize2) -> &Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y);
        return &self._data[index.x + self._size.x * index.y];
    }
}

impl<T: ZeroInit> IndexMut<USize2> for Array2<T> {
    fn index_mut(&mut self, index: USize2) -> &mut Self::Output {
        debug_assert!(index.x < self._size.x && index.y < self._size.y);
        return &mut self._data[index.x + self._size.x * index.y];
    }
}

impl<T: ZeroInit> Index<(usize, usize)> for Array2<T> {
    type Output = T;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y);
        return &self._data[index.0 + self._size.x * index.1];
    }
}

impl<T: ZeroInit> IndexMut<(usize, usize)> for Array2<T> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        debug_assert!(index.0 < self._size.x && index.1 < self._size.y);
        return &mut self._data[index.0 + self._size.x * index.1];
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod array2 {
    use crate::array2::Array2;
    use crate::usize2::USize2;

    #[test]
    fn constructors() {
        {
            let arr = Array2::<f32>::default();
            assert_eq!(0, arr.width());
            assert_eq!(0, arr.height());
        }
        {
            let arr = Array2::<f32>::new_with_packed_size(&USize2::new(3, 7), None);
            assert_eq!(3, arr.width());
            assert_eq!(7, arr.height());
            for i in 0..21 {
                assert_eq!(0.0, arr[i]);
            }
        }
        {
            let arr = Array2::new_with_packed_size(&USize2::new(1, 9), Some(1.5));
            assert_eq!(1, arr.width());
            assert_eq!(9, arr.height());
            for i in 0..9 {
                assert_eq!(1.5, arr[i]);
            }
        }
        {
            let arr = Array2::<f32>::new_with_size(5, 2, None);
            assert_eq!(5, arr.width());
            assert_eq!(2, arr.height());
            for i in 0..10 {
                assert_eq!(0.0, arr[i]);
            }
        }
        {
            let arr = Array2::new_with_size(3, 4, Some(7.0));
            assert_eq!(3, arr.width());
            assert_eq!(4, arr.height());
            for i in 0..12 {
                assert_eq!(7.0, arr[i]);
            }
        }
        {
            let arr = Array2::new_slice(
                &[&[1.0, 2.0, 3.0, 4.0],
                    &[5.0, 6.0, 7.0, 8.0],
                    &[9.0, 10.0, 11.0, 12.0]]);
            assert_eq!(4, arr.width());
            assert_eq!(3, arr.height());
            for i in 0..12 {
                assert_eq!(i as f32 + 1.0, arr[i]);
            }
        }
    }

    #[test]
    fn clear() {
        let mut arr = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);

        arr.clear();
        assert_eq!(0, arr.width());
        assert_eq!(0, arr.height());
    }

    #[test]
    fn resize_method() {
        {
            let mut arr = Array2::default();
            arr.resize_with_packed_size(&USize2::new(2, 9), None);
            assert_eq!(2, arr.width());
            assert_eq!(9, arr.height());
            for i in 0..18 {
                assert_eq!(0.0, arr[i]);
            }

            arr.resize_with_packed_size(&USize2::new(8, 13), Some(4.0));
            assert_eq!(8, arr.width());
            assert_eq!(13, arr.height());
            for i in 0..8 {
                for j in 0..13 {
                    if i < 2 && j < 9 {
                        assert_eq!(0.0, arr[(i, j)]);
                    } else {
                        assert_eq!(4.0, arr[(i, j)]);
                    }
                }
            }
        }
        {
            let mut arr = Array2::default();
            arr.resize_with_size(7, 6, None);
            assert_eq!(7, arr.width());
            assert_eq!(6, arr.height());
            for i in 0..42 {
                assert_eq!(0.0, arr[i]);
            }

            arr.resize_with_size(1, 9, Some(3.0));
            assert_eq!(1, arr.width());
            assert_eq!(9, arr.height());
            for i in 0..1 {
                for j in 0..9 {
                    if j < 6 {
                        assert_eq!(0.0, arr[(i, j)]);
                    } else {
                        assert_eq!(3.0, arr[(i, j)]);
                    }
                }
            }
        }
    }

    #[test]
    fn at_method() {
        {
            let values =
                [0.0, 1.0, 2.0,
                    3.0, 4.0, 5.0,
                    6.0, 7.0, 8.0,
                    9.0, 10.0, 11.0];
            let mut arr = Array2::new_with_size(4, 3, None);
            for i in 0..12 {
                arr[i] = values[i];
            }

            // Test row-major
            assert_eq!(0.0, arr[(0, 0)]);
            assert_eq!(1.0, arr[(1, 0)]);
            assert_eq!(2.0, arr[(2, 0)]);
            assert_eq!(3.0, arr[(3, 0)]);
            assert_eq!(4.0, arr[(0, 1)]);
            assert_eq!(5.0, arr[(1, 1)]);
            assert_eq!(6.0, arr[(2, 1)]);
            assert_eq!(7.0, arr[(3, 1)]);
            assert_eq!(8.0, arr[(0, 2)]);
            assert_eq!(9.0, arr[(1, 2)]);
            assert_eq!(10.0, arr[(2, 2)]);
            assert_eq!(11.0, arr[(3, 2)]);
        }
    }

    #[test]
    fn iterators() {
        let mut arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);

        let mut cnt = 1.0;
        for elem in arr1.data_mut() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }

        cnt = 1.0;
        for elem in arr1.data() {
            assert_eq!(cnt, *elem);
            cnt += 1.0;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);

        let mut i = 0;
        arr1.for_each(|val| {
            assert_eq!(arr1[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array2::new_slice(
            &[&[1.0, 2.0, 3.0, 4.0],
                &[5.0, 6.0, 7.0, 8.0],
                &[9.0, 10.0, 11.0, 12.0]]);

        arr1.for_each_index(|i, j| {
            let idx = i + (4 * j) + 1;
            assert_eq!(idx as f32, arr1[(i, j)]);
        });
    }
}