/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::usize2::USize2;
use crate::operators::*;
use crate::vector_expression::*;

///
/// # Base class for matrix expression.
///
/// Matrix expression is a meta type that enables template expression pattern.
///
/// - tparam T  Real number type.
/// - tparam E  Subclass type.
///
pub trait MatrixExpression {
    /// Size of the matrix.
    fn size(&self) -> USize2;

    /// Number of rows.
    fn rows(&self) -> usize;

    /// Number of columns.
    fn cols(&self) -> usize;

    /// Returns matrix element at (i, j).
    fn eval(&self, x: usize, y: usize) -> f64;
}

//--------------------------------------------------------------------------------------------------
///
/// # Constant matrix expression.
///
/// This matrix expression represents a constant matrix.
///
/// - tparam T  Real number type.
///
pub struct MatrixConstant {
    _m: usize,
    _n: usize,
    _c: f64,
}

impl MatrixConstant {
    /// Constructs m x n constant matrix expression.
    pub fn new(m: usize, n: usize, c: f64) -> MatrixConstant {
        return MatrixConstant {
            _m: m,
            _n: n,
            _c: c,
        };
    }
}

impl MatrixExpression for MatrixConstant {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return USize2::new(self.rows(), self.cols());
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._m;
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._n;
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, _i: usize, _j: usize) -> f64 {
        return self._c;
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Identity matrix expression.
///
/// This matrix expression represents an identity matrix.
///
/// - tparam T  Real number type.
///
pub struct MatrixIdentity {
    _m: usize,
}

impl MatrixIdentity {
    /// Constructs m x m identity matrix expression.
    pub fn new(m: usize) -> MatrixIdentity {
        return MatrixIdentity {
            _m: m
        };
    }
}

impl MatrixExpression for MatrixIdentity {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return USize2::new(self._m, self._m);
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._m;
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._m;
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        return match i == j {
            true => 1.0,
            false => 0.0
        };
    }
}

//--------------------------------------------------------------------------------------------------
/// # MatrixUnaryOp

///
/// # Matrix expression for unary operation.
///
/// This matrix expression represents an unary matrix operation that takes
/// single input matrix expression.
///
/// - tparam T   Real number type.
/// - tparam E   Input expression type.
/// - tparam Op  Unary operation.
///
pub struct MatrixUnaryOp<'a, E: MatrixExpression, Op: UnaryOp> {
    _u: &'a E,
    _op: Op,
}

impl<'a, E: MatrixExpression, Op: UnaryOp> MatrixUnaryOp<'a, E, Op> {
    /// Constructs unary operation expression for given input expression.
    pub fn new(u: &'a E) -> MatrixUnaryOp<E, Op> {
        return MatrixUnaryOp {
            _u: u,
            _op: Op::new(),
        };
    }
}

impl<'a, E: MatrixExpression, Op: UnaryOp> MatrixExpression for MatrixUnaryOp<'a, E, Op> {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return self._u.size();
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._u.rows();
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._u.cols();
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        return self._op.eval(self._u.eval(i, j));
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Diagonal matrix expression.
///
/// This matrix expression represents a diagonal matrix for given input matrix
/// expression.
///
/// - tparam T  Real number type.
/// - tparam E  Input expression type.
///
pub struct MatrixDiagonal<'a, E: MatrixExpression> {
    _u: &'a E,
    _is_diag: bool,
}

impl<'a, E: MatrixExpression> MatrixDiagonal<'a, E> {
    /// Constructs diagonal matrix expression for given input expression.
    /// - parameter: is_diag - True for diagonal matrix, false for off-diagonal.
    pub fn new(u: &'a E, is_diag: bool) -> MatrixDiagonal<E> {
        return MatrixDiagonal {
            _u: u,
            _is_diag: is_diag,
        };
    }
}

impl<'a, E: MatrixExpression> MatrixExpression for MatrixDiagonal<'a, E> {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return self._u.size();
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._u.rows();
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._u.cols();
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        return if self._is_diag {
            match i == j {
                true => self._u.eval(i, j),
                false => 0.0
            }
        } else {
            match i != j {
                true => self._u.eval(i, j),
                false => 0.0
            }
        };
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Triangular matrix expression.
///
/// This matrix expression represents a triangular matrix for given input matrix
/// expression.
///
/// - tparam T  Real number type.
/// - tparam E  Input expression type.
///
pub struct MatrixTriangular<'a, E: MatrixExpression> {
    _u: &'a E,
    _is_upper: bool,
    _is_strict: bool,
}

impl<'a, E: MatrixExpression> MatrixTriangular<'a, E> {
    /// Constructs triangular matrix expression for given input expression.
    /// - parameter: is_upper - True for upper tri matrix, false for lower tri matrix.
    /// - parameter: is_strict - True for strictly upper/lower triangular matrix.
    pub fn new(u: &'a E, is_upper: bool, is_strict: bool) -> MatrixTriangular<E> {
        return MatrixTriangular {
            _u: u,
            _is_upper: is_upper,
            _is_strict: is_strict,
        };
    }
}

impl<'a, E: MatrixExpression> MatrixExpression for MatrixTriangular<'a, E> {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return self._u.size();
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._u.rows();
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._u.cols();
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        return if i < j {
            match self._is_upper {
                true => self._u.eval(i, j),
                false => 0.0
            }
        } else if i > j {
            match !self._is_upper {
                true => self._u.eval(i, j),
                false => 0.0
            }
        } else {
            match !self._is_strict {
                true => self._u.eval(i, j),
                false => 0.0
            }
        };
    }
}

//--------------------------------------------------------------------------------------------------
/// # MatrixBinaryOp

///
/// # Matrix expression for binary operation.
///
/// This matrix expression represents a binary matrix operation that takes
/// two input matrix expressions.
///
/// - tparam T   Real number type.
/// - tparam E1  First input expression type.
/// - tparam E2  Second input expression type.
/// - tparam Op  Binary operation.
///
pub struct MatrixBinaryOp<'a, E1: MatrixExpression, E2: MatrixExpression, Op: BinaryOp> {
    _u: &'a E1,
    _v: &'a E2,
    _op: Op,
}

impl<'a, E1: MatrixExpression, E2: MatrixExpression, Op: BinaryOp> MatrixBinaryOp<'a, E1, E2, Op> {
    /// Constructs binary operation expression for given input matrix
    /// expressions.
    pub fn new(u: &'a E1, v: &'a E2) -> MatrixBinaryOp<'a, E1, E2, Op> {
        return MatrixBinaryOp {
            _u: u,
            _v: v,
            _op: Op::new(),
        };
    }
}

impl<'a, E1: MatrixExpression, E2: MatrixExpression, Op: BinaryOp> MatrixExpression for MatrixBinaryOp<'a, E1, E2, Op> {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return self._v.size();
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._v.rows();
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._v.cols();
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        return self._op.eval(self._u.eval(i, j), self._v.eval(i, j));
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Matrix expression for matrix-scalar binary operation.
///
/// This matrix expression represents a binary matrix operation that takes
/// one input matrix expression and one scalar.
///
/// - tparam T   Real number type.
/// - tparam E   Input expression type.
/// - tparam Op  Binary operation.
///
pub struct MatrixScalarBinaryOp<'a, E: MatrixExpression, Op: BinaryOp> {
    _u: &'a E,
    _v: f64,
    _op: Op,
}

impl<'a, E: MatrixExpression, Op: BinaryOp> MatrixScalarBinaryOp<'a, E, Op> {
    /// Constructs a binary expression for given matrix and scalar.
    pub fn new(u: &'a E, v: f64) -> MatrixScalarBinaryOp<E, Op> {
        return MatrixScalarBinaryOp {
            _u: u,
            _v: v,
            _op: Op::new(),
        };
    }
}

impl<'a, E: MatrixExpression, Op: BinaryOp> MatrixExpression for MatrixScalarBinaryOp<'a, E, Op> {
    fn size(&self) -> USize2 {
        return self._u.size();
    }

    fn rows(&self) -> usize {
        return self._u.rows();
    }

    fn cols(&self) -> usize {
        return self._u.cols();
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        return self._op.eval(self._u.eval(i, j), self._v);
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Vector expression for matrix-vector multiplication.
///
/// This vector expression represents a matrix-vector operation that takes
/// one input matrix expression and one vector expression.
///
/// - tparam T   Element value type.
/// - tparam ME  Matrix expression.
/// - tparam VE  Vector expression.
///
pub struct MatrixVectorMul<'a, ME: MatrixExpression, VE: VectorExpression> {
    _m: &'a ME,
    _v: &'a VE,
}

impl<'a, ME: MatrixExpression, VE: VectorExpression> MatrixVectorMul<'a, ME, VE> {
    pub fn new(m: &'a ME, v: &'a VE) -> MatrixVectorMul<'a, ME, VE> {
        return MatrixVectorMul {
            _m: m,
            _v: v,
        };
    }

    /// Returns vector element at i.
    pub fn eval(&self, i: usize) -> f64 {
        let mut sum: f64 = 0.0;
        let n = self._m.cols();
        for j in 0..n {
            sum += self._m.eval(i, j) * self._v.eval(j);
        }
        return sum;
    }

    /// Size of the vector.
    pub fn size(&self) -> usize {
        return self._v.size();
    }
}

impl<'a, ME: MatrixExpression, VE: VectorExpression> MatrixExpression for MatrixVectorMul<'a, ME, VE> {
    fn size(&self) -> USize2 {
        unimplemented!();
    }

    fn rows(&self) -> usize {
        unimplemented!();
    }

    fn cols(&self) -> usize {
        unimplemented!();
    }

    fn eval(&self, _x: usize, _y: usize) -> f64 {
        unimplemented!();
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Matrix expression for matrix-matrix multiplication.
///
/// This matrix expression represents a matrix-matrix operation that takes
/// two input matrices.
///
/// - tparam T   Element value type.
/// - tparam ME  Matrix expression.
/// - tparam VE  Vector expression.
///
pub struct MatrixMul<'a, E1: MatrixExpression, E2: MatrixExpression> {
    _u: &'a E1,
    _v: &'a E2,
}

impl<'a, E1: MatrixExpression, E2: MatrixExpression> MatrixMul<'a, E1, E2> {
    /// Constructs matrix-matrix multiplication expression for given two input
    /// matrices.
    pub fn new(u: &'a E1, v: &'a E2) -> MatrixMul<'a, E1, E2> {
        return MatrixMul {
            _u: u,
            _v: v,
        };
    }
}

impl<'a, E1: MatrixExpression, E2: MatrixExpression> MatrixExpression for MatrixMul<'a, E1, E2> {
    /// Size of the matrix.
    fn size(&self) -> USize2 {
        return USize2::new(self._u.rows(), self._v.cols());
    }

    /// Number of rows.
    fn rows(&self) -> usize {
        return self._u.rows();
    }

    /// Number of columns.
    fn cols(&self) -> usize {
        return self._v.cols();
    }

    /// Returns matrix element at (i, j).
    fn eval(&self, i: usize, j: usize) -> f64 {
        // todo: Unoptimized mat-mat-mul
        let mut sum: f64 = 0.0;
        let n = self._u.cols();
        for k in 0..n {
            sum += self._u.eval(i, k) * self._v.eval(k, j);
        }
        return sum;
    }
}

//--------------------------------------------------------------------------------------------------
/// # MatrixBinaryOp Aliases

/// Matrix-matrix addition expression.
pub type MatrixAdd<'a, E1, E2> = MatrixBinaryOp<'a, E1, E2, Plus>;
/// Matrix-matrix addition expression.
pub type MatrixScalarAdd<'a, E> = MatrixScalarBinaryOp<'a, E, Plus>;


/// Matrix-matrix subtraction expression.
pub type MatrixSub<'a, E1, E2> = MatrixBinaryOp<'a, E1, E2, Minus>;

/// Matrix-scalar subtraction expression.
pub type MatrixScalarSub<'a, E> = MatrixScalarBinaryOp<'a, E, Minus>;


/// Matrix-matrix subtraction expression with inverse order.
pub type MatrixScalarRSub<'a, E> = MatrixScalarBinaryOp<'a, E, RMinus>;


/// Matrix-scalar multiplication expression.
pub type MatrixScalarMul<'a, E> = MatrixScalarBinaryOp<'a, E, Multiplies>;

/// Matrix-scalar division expression.
pub type MatrixScalarDiv<'a, E> = MatrixScalarBinaryOp<'a, E, Divides>;


/// Matrix-scalar division expression with inverse order.
pub type MatrixScalarRDiv<'a, E> = MatrixScalarBinaryOp<'a, E, RDivides>;