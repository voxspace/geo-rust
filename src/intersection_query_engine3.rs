/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector3::Vector3D;
use crate::ray3::Ray3D;
use crate::bounding_box3::BoundingBox3D;

/// Closest intersection query result.
pub struct ClosestIntersectionQueryResult3<T> {
    pub item: Option<T>,
    pub distance: f64,
}

impl<T> ClosestIntersectionQueryResult3<T> {
    pub fn new() -> ClosestIntersectionQueryResult3<T> {
        return ClosestIntersectionQueryResult3 {
            item: None,
            distance: f64::MAX,
        };
    }
}

///Closest intersection distance measure function.
pub trait ClosestIntersectionDistanceFunc3<T>: FnMut(&T, &Vector3D) -> f64 {}

impl<T, Super: FnMut(&T, &Vector3D) -> f64> ClosestIntersectionDistanceFunc3<T> for Super {}

/// Box-item intersection test function.
pub trait BoxIntersectionTestFunc3<T>: FnMut(&T, &BoundingBox3D) -> bool {}

impl<T, Super: FnMut(&T, &BoundingBox3D) -> bool> BoxIntersectionTestFunc3<T> for Super {}

/// Ray-item intersection test function.
pub trait RayIntersectionTestFunc3<T>: FnMut(&T, &Ray3D) -> bool {}

impl<T, Super: FnMut(&T, &Ray3D) -> bool> RayIntersectionTestFunc3<T> for Super {}

/// Ray-item closest intersection evaluation function.
pub trait GetRayIntersectionFunc3<T>: FnMut(&T, &Ray3D) -> f64 {}

impl<T, Super: FnMut(&T, &Ray3D) -> f64> GetRayIntersectionFunc3<T> for Super {}

/// Visitor function which is invoked for each intersecting item.
pub trait IntersectionVisitorFunc3<T>: FnMut(&T) {}

impl<T, Super: FnMut(&T)> IntersectionVisitorFunc3<T> for Super {}

/// Abstract base class for 3-D intersection test query engine.
pub trait IntersectionQueryEngine3<T> {
    /// Returns true if given \p box intersects with any of the stored items.
    fn intersects_aabb<Callback>(&self, aabb: &BoundingBox3D,
                                 test_func: &mut Callback) -> bool
        where Callback: BoxIntersectionTestFunc3<T>;

    /// Returns true if given \p ray intersects with any of the stored items.
    fn intersects_ray<Callback>(&self, ray: &Ray3D,
                                test_func: &mut Callback) -> bool
        where Callback: RayIntersectionTestFunc3<T>;

    /// Invokes \p visitor_func for every intersecting items.
    fn for_each_intersecting_item_aabb<Callback, Visitor>(&self, aabb: &BoundingBox3D,
                                                          test_func: &mut Callback,
                                                          visitor_func: &mut Visitor)
        where Callback: BoxIntersectionTestFunc3<T>,
              Visitor: IntersectionVisitorFunc3<T>;

    /// Invokes \p visitor_func for every intersecting items.
    fn for_each_intersecting_item_ray<Callback, Visitor>(&self, ray: &Ray3D,
                                                         test_func: &mut Callback,
                                                         visitor_func: &mut Visitor)
        where Callback: RayIntersectionTestFunc3<T>,
              Visitor: IntersectionVisitorFunc3<T>;

    /// Returns the closest intersection for given \p ray.
    fn closest_intersection<Callback>(&self, ray: &Ray3D,
                                      test_func: &mut Callback) -> ClosestIntersectionQueryResult3<T>
        where Callback: GetRayIntersectionFunc3<T>;
}