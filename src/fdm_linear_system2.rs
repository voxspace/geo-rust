/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use crate::array2::Array2;
use crate::usize2::USize2;
use crate::matrix_csr::MatrixCsr;
use crate::vector_n::VectorN;
use crate::blas::Blas;

/// The row of FdmMatrix2 where row corresponds to (i, j) grid point.
#[derive(Clone)]
pub struct FdmMatrixRow2 {
    /// Diagonal component of the matrix (row, row).
    pub center: f64,

    /// Off-diagonal element where column refers to (i+1, j) grid point.
    pub right: f64,

    /// Off-diagonal element where column refers to (i, j+1) grid point.
    pub up: f64,
}

impl ZeroInit for FdmMatrixRow2 {
    fn zero_init() -> Self {
        return FdmMatrixRow2 {
            center: 0.0,
            right: 0.0,
            up: 0.0,
        };
    }
}

/// Vector type for 2-D finite differencing.
pub type FdmVector2 = Array2<f64>;

/// Matrix type for 2-D finite differencing.
pub type FdmMatrix2 = Array2<FdmMatrixRow2>;

//--------------------------------------------------------------------------------------------------
/// Linear system (Ax=b) for 2-D finite differencing.
#[derive(Default)]
pub struct FdmLinearSystem2 {
    /// System matrix.
    pub a: FdmMatrix2,

    /// Solution vector.
    pub x: FdmVector2,

    /// RHS vector.
    pub b: FdmVector2,
}

impl FdmLinearSystem2 {
    /// Clears all the data.
    pub fn clear(&mut self) {
        self.a.clear();
        self.x.clear();
        self.b.clear();
    }

    /// Resizes the arrays with given grid size.
    pub fn resize(&mut self, size: &USize2) {
        self.a.resize_with_packed_size(size, None);
        self.x.resize_with_packed_size(size, None);
        self.b.resize_with_packed_size(size, None);
    }
}

//--------------------------------------------------------------------------------------------------
/// BLAS operator wrapper for 2-D finite differencing.
#[derive(Clone)]
pub struct FdmBlas2 {}

impl Blas for FdmBlas2 {
    type VectorType = FdmVector2;
    type MatrixType = FdmMatrix2;

    fn set_sv(s: f64, result: &mut FdmVector2) {
        result.set_scalar(s);
    }

    fn set_vv(v: &FdmVector2, result: &mut FdmVector2) {
        result.set_self(v);
    }

    fn set_sm(s: f64, result: &mut FdmMatrix2) {
        let mut row = FdmMatrixRow2::zero_init();
        row.center = s;
        row.right = s;
        row.up = s;
        result.set_scalar(row);
    }

    fn set_mm(m: &FdmMatrix2, result: &mut FdmMatrix2) {
        result.set_self(m);
    }

    fn dot(a: &FdmVector2, b: &FdmVector2) -> f64 {
        let size = a.size();

        debug_assert!(size == b.size());

        let mut result = 0.0;

        for j in 0..size.y {
            for i in 0..size.x {
                result += a[(i, j)] * b[(i, j)];
            }
        }

        return result;
    }

    fn axpy(a: f64, x: &FdmVector2, y: &FdmVector2, result: &mut FdmVector2) {
        let size = x.size();

        debug_assert!(size == y.size());
        debug_assert!(size == result.size());

        x.for_each_index(|i, j| {
            result[(i, j)] = a * x[(i, j)] + y[(i, j)];
        });
    }

    fn mvm(m: &FdmMatrix2, v: &FdmVector2, result: &mut FdmVector2) {
        let size = m.size();

        debug_assert!(size == v.size());
        debug_assert!(size == result.size());

        m.for_each_index(|i, j| {
            result[(i, j)] =
                m[(i, j)].center * v[(i, j)] +
                    (if i > 0 { m[(i - 1, j)].right * v[(i - 1, j)] } else { 0.0 }) +
                    (if i + 1 < size.x { m[(i, j)].right * v[(i + 1, j)] } else { 0.0 }) +
                    (if j > 0 { m[(i, j - 1)].up * v[(i, j - 1)] } else { 0.0 }) +
                    (if j + 1 < size.y { m[(i, j)].up * v[(i, j + 1)] } else { 0.0 });
        });
    }

    fn residual(a: &FdmMatrix2, x: &FdmVector2, b: &FdmVector2, result: &mut FdmVector2) {
        let size = a.size();

        debug_assert!(size == x.size());
        debug_assert!(size == b.size());
        debug_assert!(size == result.size());

        a.for_each_index(|i, j| {
            result[(i, j)] =
                b[(i, j)] - a[(i, j)].center * x[(i, j)] -
                    (if i > 0 { a[(i - 1, j)].right * x[(i - 1, j)] } else { 0.0 }) -
                    (if i + 1 < size.x { a[(i, j)].right * x[(i + 1, j)] } else { 0.0 }) -
                    (if j > 0 { a[(i, j - 1)].up * x[(i, j - 1)] } else { 0.0 }) -
                    (if j + 1 < size.y { a[(i, j)].up * x[(i, j + 1)] } else { 0.0 });
        });
    }

    fn l2norm(v: &FdmVector2) -> f64 {
        return f64::sqrt(FdmBlas2::dot(v, v));
    }

    fn l_inf_norm(v: &FdmVector2) -> f64 {
        let size = v.size();

        let mut result = 0.0;

        for j in 0..size.y {
            for i in 0..size.x {
                result = crate::math_utils::absmax(result, v[(i, j)]);
            }
        }

        return f64::abs(result);
    }
}

//--------------------------------------------------------------------------------------------------
/// Compressed linear system (Ax=b) for 2-D finite differencing.
#[derive(Default)]
pub struct FdmCompressedLinearSystem2 {
    /// System matrix.
    pub a: MatrixCsr,

    /// Solution vector.
    pub x: VectorN,

    /// RHS vector.
    pub b: VectorN,
}

impl FdmCompressedLinearSystem2 {
    /// Clears all the data.
    pub fn clear(&mut self) {
        self.a.clear();
        self.x.clear();
        self.b.clear();
    }
}

/// BLAS operator wrapper for compressed 2-D finite differencing.
#[derive(Clone)]
pub struct FdmCompressedBlas2 {}

impl Blas for FdmCompressedBlas2 {
    type VectorType = VectorN;
    type MatrixType = MatrixCsr;

    fn set_sv(s: f64, result: &mut VectorN) {
        result.set_scalar(s);
    }

    fn set_vv(v: &VectorN, result: &mut VectorN) {
        result.set_expression(v);
    }

    fn set_sm(s: f64, result: &mut MatrixCsr) {
        result.set_scalar(s);
    }

    fn set_mm(m: &MatrixCsr, result: &mut MatrixCsr) {
        result.set_self(m);
    }

    fn dot(a: &VectorN, b: &VectorN) -> f64 {
        return a.dot(b);
    }

    fn axpy(a: f64, x: &VectorN, y: &VectorN, result: &mut VectorN) {
        *result = VectorN::new_expression(&VectorN::new_expression(&x.mul_scalar(a)).add_expression(y));
    }

    fn mvm(m: &MatrixCsr, v: &VectorN, result: &mut VectorN) {
        let rp = m.row_pointers_data();
        let ci = m.column_indices_data();
        let nnz = m.non_zero_data();

        v.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut sum = 0.0;

            for jj in row_begin..row_end {
                let j = ci[jj];
                sum += nnz[jj] * v[j];
            }

            result[i] = sum;
        });
    }

    fn residual(a: &MatrixCsr, x: &VectorN, b: &VectorN, result: &mut VectorN) {
        let rp = a.row_pointers_data();
        let ci = a.column_indices_data();
        let nnz = a.non_zero_data();

        x.for_each_index(|i| {
            let row_begin = rp[i];
            let row_end = rp[i + 1];

            let mut sum = 0.0;

            for jj in row_begin..row_end {
                let j = ci[jj];
                sum += nnz[jj] * x[j];
            }

            result[i] = b[i] - sum;
        });
    }

    fn l2norm(v: &VectorN) -> f64 {
        return f64::sqrt(v.dot(v));
    }

    fn l_inf_norm(v: &VectorN) -> f64 {
        return f64::abs(v.absmax());
    }
}