/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector3::Vector3D;

/// Nearest neighbor query result.
pub struct NearestNeighborQueryResult3<T> {
    pub item: Option<T>,
    pub distance: f64,
}

impl<T> NearestNeighborQueryResult3<T> {
    pub fn new() -> NearestNeighborQueryResult3<T> {
        return NearestNeighborQueryResult3 {
            item: None,
            distance: f64::MAX,
        };
    }
}

/// Nearest neighbor distance measure function.
pub trait NearestNeighborDistanceFunc3<T>: FnMut(&T, &Vector3D) -> f64 {}

impl<T, Super: FnMut(&T, &Vector3D) -> f64> NearestNeighborDistanceFunc3<T> for Super {}

/// Abstract base class for 3-D nearest neighbor query engine.
pub trait NearestNeighborQueryEngine3<T> {
    /// Returns the nearest neighbor for given point and distance measure function.
    fn nearest<Callback>(&self, pt: &Vector3D,
                         distance_func: &mut Callback) -> NearestNeighborQueryResult3<T>
        where Callback: NearestNeighborDistanceFunc3<T>;
}