mod macros;
pub mod unit_tests_utils;

pub mod common_trait;

pub mod serial;
pub mod constants;
pub mod math_utils;
pub mod vector2;
pub mod vector3;
pub mod vector4;
pub mod matrix2x2;
pub mod matrix3x3;
pub mod matrix4x4;
pub mod point2;
pub mod point3;
pub mod usize2;
pub mod usize3;
pub mod isize2;
pub mod isize3;
pub mod quaternion;

pub mod array1;
pub mod array2;
pub mod array3;
pub mod array_accessor1;
pub mod array_accessor2;
pub mod array_accessor3;

pub mod operators;

pub mod vector_expression;
pub mod vector;
pub mod vector_n;

pub mod matrix_expression;
pub mod matrix;
pub mod matrix_mxn;
pub mod matrix_csr;

pub mod blas;
pub mod cg;
pub mod fdm_linear_system2;
pub mod fdm_linear_system3;
pub mod fdm_linear_system_solver2;
pub mod fdm_linear_system_solver3;
pub mod fdm_cg_solver2;
pub mod fdm_cg_solver3;
pub mod fdm_iccg_solver2;
pub mod fdm_iccg_solver3;
pub mod fdm_jacobi_solver2;
pub mod fdm_jacobi_solver3;
pub mod fdm_gauss_seidel_solver2;
pub mod fdm_gauss_seidel_solver3;

pub mod mg;
pub mod fdm_mg_linear_system2;
pub mod fdm_mg_linear_system3;
pub mod fdm_mg_solver2;
pub mod fdm_mg_solver3;
pub mod fdm_mgpcg_solver2;
pub mod fdm_mgpcg_solver3;

pub mod svd;


// //--------------------------------------------------------------------------------------------------
// pub mod ray2;
// pub mod ray3;

// pub mod bounding_box2;
// pub mod bounding_box3;
// pub mod transform2;
// pub mod transform3;
// pub mod surface2;
// pub mod surface3;
// pub mod sphere2;
// pub mod sphere3;
// pub mod plane2;
// pub mod box2;
// pub mod plane3;
// pub mod box3;
// pub mod cylinder3;
// pub mod triangle3;
// pub mod triangle_mesh3;
// pub mod surface_set2;
// pub mod surface_set3;
//
// pub mod intersection_query_engine2;
// pub mod intersection_query_engine3;
// pub mod nearest_neighbor_query_engine2;
// pub mod nearest_neighbor_query_engine3;
// pub mod list_query_engine2;
// pub mod list_query_engine3;
// pub mod bvh2;
// pub mod bvh3;
// pub mod quadtree;
// pub mod octree;
//
// pub mod collider2;
// pub mod collider3;
// pub mod rigid_body_collider2;
// pub mod rigid_body_collider3;
// pub mod collider_set2;
// pub mod collider_set3;
//
// pub mod point_neighbor_searcher2;
// pub mod point_neighbor_searcher3;
// pub mod point_simple_list_searcher2;
// pub mod point_simple_list_searcher3;
// pub mod point_hash_grid_searcher2;
// pub mod point_hash_grid_searcher3;
// pub mod point_parallel_hash_grid_searcher2;
// pub mod point_parallel_hash_grid_searcher3;
// pub mod kdtree2;
// pub mod kdtree3;
// pub mod point_kdtree_searcher2;
// pub mod point_kdtree_searcher3;
//
// pub mod point_generator2;
// pub mod point_generator3;
// pub mod triangle_point_generator;
// pub mod bcc_lattice_point_generator;
// pub mod fcc_lattice_point_generator;
// pub mod grid_point_generator2;
// pub mod grid_point_generator3;
//
// pub mod particle_system_data2;
// pub mod particle_system_data3;
// pub mod sph_system_data2;
// pub mod sph_system_data3;
//
// pub mod samplers;
// pub mod particle_emitter2;
// pub mod particle_emitter3;
// pub mod point_particle_emitter2;
// pub mod point_particle_emitter3;
// pub mod volume_particle_emitter2;
// pub mod volume_particle_emitter3;
// pub mod particle_emitter_set2;
// pub mod particle_emitter_set3;
//
// pub mod level_set_utils;
// pub mod implicit_surface2;
// pub mod implicit_surface3;
// pub mod surface_to_implicit2;
// pub mod surface_to_implicit3;
// pub mod custom_implicit_surface2;
// pub mod custom_implicit_surface3;
// pub mod implicit_surface_set2;
// pub mod implicit_surface_set3;
//
// pub mod field2;
// pub mod field3;
// pub mod scalar_field2;
// pub mod scalar_field3;
// pub mod vector_field2;
// pub mod vector_field3;
// pub mod constant_scalar_field2;
// pub mod constant_scalar_field3;
// pub mod constant_vector_field2;
// pub mod constant_vector_field3;
// pub mod custom_scalar_field2;
// pub mod custom_scalar_field3;
// pub mod custom_vector_field2;
// pub mod custom_vector_field3;
//
// pub mod physics_helpers;
// pub mod animation;
// pub mod physics_animation;
// pub mod particle_system_solver2;
// pub mod particle_system_solver3;
// pub mod sph_kernels2;
// pub mod sph_kernels3;
// pub mod sph_solver2;
// pub mod sph_solver3;