/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::field3::Field3;
use crate::scalar_field3::ScalarField3;
use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

/// # 3-D constant scalar field.
pub struct ConstantScalarField3 {
    _value: f64,
}

impl ConstantScalarField3 {
    /// Constructs a constant scalar field with given \p value.
    pub fn new(value: Option<f64>) -> ConstantScalarField3 {
        return ConstantScalarField3 {
            _value: value.unwrap_or(0.0)
        };
    }

    /// Returns builder fox ConstantScalarField3.
    pub fn builder() -> Builder {
        return Builder::new();
    }
}

impl Field3 for ConstantScalarField3 {}

impl ScalarField3 for ConstantScalarField3 {
    fn sample(&self, _: &Vector3D) -> f64 {
        return self._value;
    }
}

/// Shared pointer for the ConstantScalarField3 type.
pub type ConstantScalarField3Ptr = Arc<RwLock<ConstantScalarField3>>;

///
/// # Front-end to create ConstantScalarField3 objects step by step.
///
pub struct Builder {
    _value: f64,
}

impl Builder {
    /// Returns builder with value.
    pub fn with_value(&mut self, value: f64) -> &mut Self {
        self._value = value;
        return self;
    }

    /// Builds ConstantScalarField3.
    pub fn build(&mut self) -> ConstantScalarField3 {
        return ConstantScalarField3::new(Some(self._value));
    }

    /// Builds shared pointer of ConstantScalarField3 instance.
    pub fn make_shared(&mut self) -> ConstantScalarField3Ptr {
        return ConstantScalarField3Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _value: 0.0
        };
    }
}