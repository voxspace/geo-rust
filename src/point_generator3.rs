/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::bounding_box3::BoundingBox3D;
use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

///
/// # Abstract base class for 3-D point generator.
///
/// This class provides interface for 3-D point generator. For given bounding
/// box and point spacing, the inherited classes generates points with specified
/// pattern.
///
pub trait PointGenerator3 {
    /// Generates points to output array \p points inside given \p boundingBox
    /// with target point \p spacing.
    fn generate(&self, bounding_box: &BoundingBox3D,
                spacing: f64,
                points: &mut Vec<Vector3D>) {
        self.for_each_point(
            bounding_box,
            spacing,
            &mut |point: &Vector3D| {
                points.push(*point);
                return true;
            });
    }

    /// Iterates every point within the bounding box with specified
    /// point pattern and invokes the callback function.
    ///
    /// This function iterates every point within the bounding box and invokes
    /// the callback function. The position of the point is specified by the
    /// actual implementation. The suggested spacing between the points are
    /// given by \p spacing. The input parameter of the callback function is
    /// the position of the point and the return value tells whether the
    /// iteration should stop or not.
    fn for_each_point<Callback>(&self, bounding_box: &BoundingBox3D,
                                spacing: f64,
                                callback: &mut Callback) where Callback: FnMut(&Vector3D) -> bool;
}

/// Shared pointer for the PointGenerator3 type.
pub type PointGenerator3Ptr = Arc<RwLock<dyn PointGenerator3>>;