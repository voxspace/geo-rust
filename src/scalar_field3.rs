/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::field3::*;
use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

/// Abstract base class for 3-D scalar field.
pub trait ScalarField3: Field3 {
    /// Returns sampled value at given position \p x.
    fn sample(&self, x: &Vector3D) -> f64;

    /// Returns gradient vector at given position \p x.
    fn gradient(&self, _: &Vector3D) -> Vector3D {
        return Vector3D::default();
    }

    /// Returns Laplacian at given position \p x.
    fn laplacian(&self, _: &Vector3D) -> f64 {
        return 0.0;
    }
}

/// Shared pointer for the ScalarField3 type.
pub type ScalarField3Ptr = Arc<RwLock<dyn ScalarField3>>;