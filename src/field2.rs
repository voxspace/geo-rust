/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::sync::{RwLock, Arc};

/// Abstract base class for 2-D fields.
pub trait Field2 {}

pub type Field2Ptr = Arc<RwLock<dyn Field2>>;