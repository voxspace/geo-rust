/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::vector2::Vector2;
use std::ops::{AddAssign, SubAssign, MulAssign, DivAssign, Index, IndexMut, Add, Neg, Sub, Mul, Div};

///
/// # 2-D matrix class.
///
/// This class is a row-major 2-D matrix class, which means each element of
/// the matrix is stored in order of (0, 0), (0, 1), (1, 0) and (1, 1).
/// - tparam T - Type of the element.
///
#[derive(Clone, Copy)]
pub struct Matrix2x2<T: Float> {
    _elements: [T; 4],
}

/// Float-type 2x2 matrix.
pub type Matrix2x2F = Matrix2x2<f32>;

/// Double-type 2x2 matrix.
pub type Matrix2x2D = Matrix2x2<f64>;

impl<T: Float> Default for Matrix2x2<T> {
    /// Constructs identity matrix.
    fn default() -> Self {
        return Matrix2x2 {
            _elements: [T::one(), T::zero(), T::zero(), T::one()]
        };
    }
}

/// # Constructors
impl<T: Float> Matrix2x2<T> {
    /// Constructs constant value matrix.
    pub fn new_scalar(s: T) -> Matrix2x2<T> {
        return Matrix2x2 {
            _elements: [s, s, s, s]
        };
    }

    /// Constructs a matrix with input elements.
    /// \warning Ordering of the input elements is row-major.
    pub fn new(m00: T, m01: T, m10: T, m11: T) -> Matrix2x2<T> {
        return Matrix2x2 {
            _elements: [m00, m01, m10, m11]
        };
    }

    ///
    /// Constructs a matrix with given initializer list **lst**.
    ///
    /// This constructor will build a matrix with given initializer list **lst**
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::matrix2x2::Matrix2x2F;
    /// let arr = Matrix2x2F::new_slice(
    ///     &[&[1.0, 2.0],
    ///     &[9.0, 3.0]]);
    /// ```
    ///
    /// Note the initializer also has 2x2 structure.
    ///
    /// - parameter: lst Initializer list that should be copy to the new matrix.
    pub fn new_slice(lst: &[&[T]]) -> Matrix2x2<T> {
        return Matrix2x2 {
            _elements: [lst[0][0], lst[0][1], lst[1][0], lst[1][1]]
        };
    }

    /// Constructs a matrix with input array.
    /// \warning Ordering of the input elements is row-major.
    pub fn new_array(arr: &[T; 4]) -> Matrix2x2<T> {
        return Matrix2x2 {
            _elements: *arr
        };
    }
}

/// # Basic setters
impl<T: Float> Matrix2x2<T> {
    /// Sets whole matrix with input scalar.
    pub fn set_scalar(&mut self, s: T) {
        self._elements[0] = s;
        self._elements[1] = s;
        self._elements[2] = s;
        self._elements[3] = s;
    }

    /// Sets this matrix with input elements.
    /// \warning Ordering of the input elements is row-major.
    pub fn set(&mut self, m00: T, m01: T, m10: T, m11: T) {
        self._elements[0] = m00;
        self._elements[1] = m01;
        self._elements[2] = m10;
        self._elements[3] = m11;
    }

    ///
    /// \brief Sets this matrix with given initializer list **lst**.
    ///
    /// This function will fill the matrix with given initializer list **lst**
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::matrix2x2::Matrix2x2F;
    /// let mut arr =  Matrix2x2F::default();
    /// arr.set_slice(&[
    ///     &[1.0, 2.0],
    ///     &[9.0, 3.0]
    /// ]);
    /// ```
    ///
    /// Note the initializer also has 2x2 structure.
    ///
    /// - parameter: lst Initializer list that should be copy to the matrix.
    pub fn set_slice(&mut self, lst: &[&[T]]) {
        self._elements[0] = lst[0][0];
        self._elements[1] = lst[0][1];
        self._elements[2] = lst[1][0];
        self._elements[3] = lst[1][1];
    }

    /// Copies from input matrix.
    pub fn set_self(&mut self, m: &Matrix2x2<T>) {
        self._elements = m._elements;
    }

    /// Copies from input array.
    /// *warning* Ordering of the input elements is row-major.
    pub fn set_array(&mut self, arr: &[T; 4]) {
        self._elements = *arr;
    }

    /// Sets diagonal elements with input scalar.
    pub fn set_diagonal(&mut self, s: T) {
        self._elements[0] = s;
        self._elements[3] = s;
    }

    /// Sets off-diagonal elements with input scalar.
    pub fn set_off_diagonal(&mut self, s: T) {
        self._elements[1] = s;
        self._elements[2] = s;
    }

    /// Sets i-th row with input vector.
    pub fn set_row(&mut self, i: usize, row: &Vector2<T>) {
        self._elements[2 * i] = row.x;
        self._elements[2 * i + 1] = row.y;
    }

    /// Sets i-th column with input vector.
    pub fn set_column(&mut self, j: usize, col: &Vector2<T>) {
        self._elements[j] = col.x;
        self._elements[j + 2] = col.y;
    }
}

/// # Basic getters
impl<T: Float> Matrix2x2<T> {
    /// Returns true if this matrix is similar to the input matrix within the
    /// given tolerance.
    pub fn is_similar(&self, m: &Matrix2x2<T>, tol: Option<T>) -> bool {
        return (T::abs(self._elements[0] - m._elements[0]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[1] - m._elements[1]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[2] - m._elements[2]) < tol.unwrap_or(T::epsilon())) &&
            (T::abs(self._elements[3] - m._elements[3]) < tol.unwrap_or(T::epsilon()));
    }

    /// Returns true if this matrix is a square matrix.
    pub fn is_square(&self) -> bool {
        return true;
    }

    /// Returns number of rows of this matrix.
    pub fn rows(&self) -> usize {
        return 2;
    }

    /// Returns number of columns of this matrix.
    pub fn cols(&self) -> usize {
        return 2;
    }

    /// Returns data pointer of this matrix.
    pub fn data_mut(&mut self) -> &mut [T] {
        return &mut self._elements;
    }

    /// Returns constant pointer of this matrix.
    pub fn data(&self) -> &[T] {
        return &self._elements;
    }
}

/// # Binary operator methods - new instance = this instance (+) input
impl<T: Float> Matrix2x2<T> {
    /// Returns this matrix + input scalar.
    pub fn add_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0] + s, self._elements[1] + s,
                              self._elements[2] + s, self._elements[3] + s);
    }

    /// Returns this matrix + input matrix (element-wise).
    pub fn add_mat(&self, m: &Matrix2x2<T>) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0] + m._elements[0], self._elements[1] + m._elements[1],
                              self._elements[2] + m._elements[2], self._elements[3] + m._elements[3]);
    }

    /// Returns this matrix - input scalar.
    pub fn sub_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0] - s, self._elements[1] - s,
                              self._elements[2] - s, self._elements[3] - s);
    }

    /// Returns this matrix - input matrix (element-wise).
    pub fn sub_mat(&self, m: &Matrix2x2<T>) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0] - m._elements[0], self._elements[1] - m._elements[1],
                              self._elements[2] - m._elements[2], self._elements[3] - m._elements[3]);
    }

    /// Returns this matrix * input scalar.
    pub fn mul_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0] * s, self._elements[1] * s,
                              self._elements[2] * s, self._elements[3] * s);
    }

    /// Returns this matrix * input vector.
    pub fn mul_vec(&self, v: &Vector2<T>) -> Vector2<T> {
        return Vector2::new(self._elements[0] * v.x + self._elements[1] * v.y,
                            self._elements[2] * v.x + self._elements[3] * v.y);
    }

    /// Returns this matrix * input matrix.
    pub fn mul_mat(&self, m: &Matrix2x2<T>) -> Matrix2x2<T> {
        return Matrix2x2::new(
            self._elements[0] * m._elements[0] + self._elements[1] * m._elements[2],
            self._elements[0] * m._elements[1] + self._elements[1] * m._elements[3],
            self._elements[2] * m._elements[0] + self._elements[3] * m._elements[2],
            self._elements[2] * m._elements[1] + self._elements[3] * m._elements[3]);
    }

    /// Returns this matrix / input scalar.
    pub fn div_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0] / s, self._elements[1] / s,
                              self._elements[2] / s, self._elements[3] / s);
    }
}

/// # Binary operator methods - new instance = input (+) this instance
impl<T: Float> Matrix2x2<T> {
    /// Returns input scalar + this matrix.
    pub fn radd_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(s + self._elements[0], s + self._elements[1],
                              s + self._elements[2], s + self._elements[3]);
    }

    /// Returns input matrix + this matrix (element-wise).
    pub fn radd_mat(&self, m: &Matrix2x2<T>) -> Matrix2x2<T> {
        return Matrix2x2::new(m._elements[0] + self._elements[0], m._elements[1] + self._elements[1],
                              m._elements[2] + self._elements[2], m._elements[3] + self._elements[3]);
    }

    /// Returns input scalar - this matrix.
    pub fn rsub_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(s - self._elements[0], s - self._elements[1],
                              s - self._elements[2], s - self._elements[3]);
    }

    /// Returns input matrix - this matrix (element-wise).
    pub fn rsub_mat(&self, m: &Matrix2x2<T>) -> Matrix2x2<T> {
        return Matrix2x2::new(m._elements[0] - self._elements[0], m._elements[1] - self._elements[1],
                              m._elements[2] - self._elements[2], m._elements[3] - self._elements[3]);
    }

    /// Returns input scalar * this matrix.
    pub fn rmul_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(s * self._elements[0], s * self._elements[1],
                              s * self._elements[2], s * self._elements[3]);
    }

    /// Returns input matrix * this matrix.
    pub fn rmul_mat(&self, m: &Matrix2x2<T>) -> Matrix2x2<T> {
        return m.mul_mat(self);
    }

    /// Returns input scalar / this matrix.
    pub fn rdiv_scalar(&self, s: T) -> Matrix2x2<T> {
        return Matrix2x2::new(s / self._elements[0], s / self._elements[1],
                              s / self._elements[2], s / self._elements[3]);
    }
}

/// # Augmented operator methods - this instance (+)= input
impl<T: Float> Matrix2x2<T> {
    /// Adds input scalar to this matrix.
    pub fn iadd_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] + s;
        self._elements[1] = self._elements[1] + s;
        self._elements[2] = self._elements[2] + s;
        self._elements[3] = self._elements[3] + s;
    }

    /// Adds input matrix to this matrix (element-wise).
    pub fn iadd_mat(&mut self, m: &Matrix2x2<T>) {
        self._elements[0] = self._elements[0] + m._elements[0];
        self._elements[1] = self._elements[1] + m._elements[1];
        self._elements[2] = self._elements[2] + m._elements[2];
        self._elements[3] = self._elements[3] + m._elements[3];
    }

    /// Subtracts input scalar from this matrix.
    pub fn isub_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] - s;
        self._elements[1] = self._elements[1] - s;
        self._elements[2] = self._elements[2] - s;
        self._elements[3] = self._elements[3] - s;
    }

    /// Subtracts input matrix from this matrix (element-wise).
    pub fn isub_mat(&mut self, m: &Matrix2x2<T>) {
        self._elements[0] = self._elements[0] - m._elements[0];
        self._elements[1] = self._elements[1] - m._elements[1];
        self._elements[2] = self._elements[2] - m._elements[2];
        self._elements[3] = self._elements[3] - m._elements[3];
    }

    /// Multiplies input scalar to this matrix.
    pub fn imul_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] * s;
        self._elements[1] = self._elements[1] * s;
        self._elements[2] = self._elements[2] * s;
        self._elements[3] = self._elements[3] * s;
    }

    /// Multiplies input matrix to this matrix.
    pub fn imul_mat(&mut self, m: &Matrix2x2<T>) {
        self.set_self(&self.mul_mat(m));
    }

    /// Divides this matrix with input scalar.
    pub fn idiv_scalar(&mut self, s: T) {
        self._elements[0] = self._elements[0] / s;
        self._elements[1] = self._elements[1] / s;
        self._elements[2] = self._elements[2] / s;
        self._elements[3] = self._elements[3] / s;
    }
}

/// # Modifiers
impl<T: Float> Matrix2x2<T> {
    /// Transposes this matrix.
    pub fn transpose(&mut self) {
        let temp = self._elements[2];
        self._elements[2] = self._elements[1];
        self._elements[1] = temp;
    }

    /// Inverts this matrix.
    pub fn invert(&mut self) {
        let d = self.determinant();
        let mut m = Matrix2x2::default();
        m._elements[0] = self._elements[3];
        m._elements[1] = -self._elements[1];
        m._elements[2] = -self._elements[2];
        m._elements[3] = self._elements[0];
        m.idiv_scalar(d);

        self.set_self(&m);
    }
}

/// # Complex getters
impl<T: Float> Matrix2x2<T> {
    /// Returns sum of all elements.
    pub fn sum(&self) -> T {
        let mut s = T::zero();
        for i in 0..4 {
            s = s + self._elements[i];
        }
        return s;
    }

    /// Returns average of all elements.
    pub fn avg(&self) -> T {
        return self.sum() * T::from(0.25).unwrap();
    }

    /// Returns minimum among all elements.
    pub fn min(&self) -> T {
        return T::min(T::min(self._elements[0], self._elements[1]),
                      T::min(self._elements[2], self._elements[3]));
    }

    /// Returns maximum among all elements.
    pub fn max(&self) -> T {
        return T::max(T::max(self._elements[0], self._elements[1]),
                      T::max(self._elements[2], self._elements[3]));
    }

    /// Returns absolute minimum among all elements.
    pub fn absmin(&self) -> T {
        return crate::math_utils::absmin(crate::math_utils::absmin(self._elements[0], self._elements[1]),
                                         crate::math_utils::absmin(self._elements[2], self._elements[3]));
    }

    /// Returns absolute maximum among all elements.
    pub fn absmax(&self) -> T {
        return crate::math_utils::absmax(crate::math_utils::absmax(self._elements[0], self._elements[1]),
                                         crate::math_utils::absmax(self._elements[2], self._elements[3]));
    }

    /// Returns sum of all diagonal elements.
    pub fn trace(&self) -> T {
        return self._elements[0] + self._elements[3];
    }

    /// Returns determinant of this matrix.
    pub fn determinant(&self) -> T {
        return self._elements[0] * self._elements[3] - self._elements[1] * self._elements[2];
    }

    /// Returns diagonal part of this matrix.
    pub fn diagonal(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0], T::zero(),
                              T::zero(), self._elements[3]);
    }

    /// Returns off-diagonal part of this matrix.
    pub fn off_diagonal(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(T::zero(), self._elements[1],
                              self._elements[2], T::zero());
    }

    /// Returns strictly lower triangle part of this matrix.
    pub fn strict_lower_tri(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(T::zero(), T::zero(),
                              self._elements[2], T::zero());
    }

    /// Returns strictly upper triangle part of this matrix.
    pub fn strict_upper_tri(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(T::zero(), self._elements[1],
                              T::zero(), T::zero());
    }

    /// Returns lower triangle part of this matrix (including the diagonal).
    pub fn lower_tri(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0], T::zero(),
                              self._elements[2], self._elements[3]);
    }

    /// Returns upper triangle part of this matrix (including the diagonal).
    pub fn upper_tri(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0], self._elements[1],
                              T::zero(), self._elements[3]);
    }

    /// Returns transposed matrix.
    pub fn transposed(&self) -> Matrix2x2<T> {
        return Matrix2x2::new(self._elements[0], self._elements[2],
                              self._elements[1], self._elements[3]);
    }

    /// Returns inverse matrix.
    pub fn inverse(&self) -> Matrix2x2<T> {
        let mut m = *self;
        m.invert();
        return m;
    }

    /// Returns Frobenius norm.
    pub fn frobenius_norm(&self) -> T {
        return T::sqrt(self._elements[0] * self._elements[0] + self._elements[1] * self._elements[1] +
            self._elements[2] * self._elements[2] + self._elements[3] * self._elements[3]);
    }
}

/// # Setter operators

/// Addition assignment with input scalar.
impl<T: Float> AddAssign<T> for Matrix2x2<T> {
    fn add_assign(&mut self, rhs: T) {
        self.iadd_scalar(rhs);
    }
}

/// Addition assignment with input matrix (element-wise).
impl<T: Float> AddAssign for Matrix2x2<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_mat(&rhs);
    }
}

/// Subtraction assignment with input scalar.
impl<T: Float> SubAssign<T> for Matrix2x2<T> {
    fn sub_assign(&mut self, rhs: T) {
        self.isub_scalar(rhs);
    }
}

/// Subtraction assignment with input matrix (element-wise).
impl<T: Float> SubAssign for Matrix2x2<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_mat(&rhs);
    }
}

/// Multiplication assignment with input scalar.
impl<T: Float> MulAssign<T> for Matrix2x2<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.imul_scalar(rhs);
    }
}

/// Multiplication assignment with input matrix.
impl<T: Float> MulAssign for Matrix2x2<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_mat(&rhs);
    }
}

/// Division assignment with input scalar.
impl<T: Float> DivAssign<T> for Matrix2x2<T> {
    fn div_assign(&mut self, rhs: T) {
        self.idiv_scalar(rhs);
    }
}

/// # Getter operators
/// Returns constant reference of i-th element.
impl<T: Float> Index<usize> for Matrix2x2<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        return &self._elements[index];
    }
}

/// Returns reference of i-th element.
impl<T: Float> IndexMut<usize> for Matrix2x2<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._elements[index];
    }
}

/// Returns constant reference of i-th element.
impl<T: Float> Index<(usize, usize)> for Matrix2x2<T> {
    type Output = T;
    fn index(&self, index: (usize, usize)) -> &Self::Output {
        return &self._elements[2 * index.0 + index.1];
    }
}

/// Returns reference of i-th element.
impl<T: Float> IndexMut<(usize, usize)> for Matrix2x2<T> {
    fn index_mut(&mut self, index: (usize, usize)) -> &mut Self::Output {
        return &mut self._elements[2 * index.0 + index.1];
    }
}

/// Returns true if is equal to m.
impl<T: Float> PartialEq for Matrix2x2<T> {
    fn eq(&self, other: &Self) -> bool {
        return self._elements[0] == other._elements[0] && self._elements[1] == other._elements[1] &&
            self._elements[2] == other._elements[2] && self._elements[3] == other._elements[3];
    }
}

impl<T: Float> Eq for Matrix2x2<T> {}

/// # Helpers
impl<T: Float> Matrix2x2<T> {
    /// Sets all matrix entries to zero.
    pub fn make_zero() -> Matrix2x2<T> {
        return Matrix2x2::new(T::zero(), T::zero(), T::zero(), T::zero());
    }

    /// Makes all diagonal elements to 1, and other elements to 0.
    pub fn make_identity() -> Matrix2x2<T> {
        return Matrix2x2::new(T::one(), T::zero(), T::zero(), T::one());
    }

    /// Makes scale matrix.
    pub fn make_scale_matrix_scalar(sx: T, sy: T) -> Matrix2x2<T> {
        return Matrix2x2::new(sx, T::zero(), T::zero(), sy);
    }

    /// Makes scale matrix.
    pub fn make_scale_matrix_vec(s: Vector2<T>) -> Matrix2x2<T> {
        return Matrix2x2::new(s.x, T::zero(), T::zero(), s.y);
    }

    /// Makes rotation matrix.
    /// *warning* Input angle should be radian.
    pub fn make_rotation_matrix(rad: T) -> Matrix2x2<T> {
        return Matrix2x2::new(T::cos(rad), -T::sin(rad),
                              T::sin(rad), T::cos(rad));
    }
}


impl<T: Float> Neg for Matrix2x2<T> {
    type Output = Matrix2x2<T>;
    /// Negative sign operator.
    fn neg(self) -> Self::Output {
        return Matrix2x2::new(-self._elements[0], -self._elements[1],
                              -self._elements[2], -self._elements[3]);
    }
}

/// Returns a + b', where every element of matrix b' is b.
impl<T: Float> Add<T> for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn add(self, rhs: T) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Returns a + b (element-size).
impl<T: Float> Add for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn add(self, rhs: Self) -> Self::Output {
        return self.add_mat(&rhs);
    }
}

/// Returns a - b', where every element of matrix b' is b.
impl<T: Float> Sub<T> for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn sub(self, rhs: T) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Returns a - b (element-size).
impl<T: Float> Sub for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_mat(&rhs);
    }
}

/// Returns a * b', where every element of matrix b' is b.
impl<T: Float> Mul<T> for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn mul(self, rhs: T) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Returns a * b.
impl<T: Float> Mul<Vector2<T>> for Matrix2x2<T> {
    type Output = Vector2<T>;

    fn mul(self, rhs: Vector2<T>) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Returns a * b.
impl<T: Float> Mul for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_mat(&rhs);
    }
}

/// Returns a' / b, where every element of matrix a' is a.
impl<T: Float> Div<T> for Matrix2x2<T> {
    type Output = Matrix2x2<T>;

    fn div(self, rhs: T) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod matrix2x2 {
    use crate::vector2::*;
    use crate::matrix2x2::Matrix2x2D;

    #[test]
    fn constructors() {
        let mat = Matrix2x2D::default();
        assert_eq!(mat == Matrix2x2D::new(1.0, 0.0, 0.0, 1.0), true);

        let mat2 = Matrix2x2D::new_scalar(3.1);
        for i in 0..4 {
            assert_eq!(3.1, mat2[i]);
        }

        let mat3 = Matrix2x2D::new(1.0, 2.0, 3.0, 4.0);
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat3[i]);
        }

        let mat4 = Matrix2x2D::new_slice(&[&[1.0, 2.0], &[3.0, 4.0]]);
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat4[i]);
        }

        let mat5 = mat4.clone();
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat5[i]);
        }

        let arr = [1.0, 2.0, 3.0, 4.0];
        let mat6 = Matrix2x2D::new_array(&arr);
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat6[i]);
        }
    }

    #[test]
    fn set_methods() {
        let mut mat = Matrix2x2D::default();

        mat.set_scalar(3.1);
        for i in 0..4 {
            assert_eq!(3.1, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set(1.0, 2.0, 3.0, 4.0);
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_slice(&[&[1.0, 2.0], &[3.0, 4.0]]);
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_self(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        let arr = [1.0, 2.0, 3.0, 4.0];
        mat.set_array(&arr);
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_diagonal(3.1);
        for i in 0..2 {
            for j in 0..2 {
                if i == j {
                    assert_eq!(3.1, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat.set_scalar(0.0);
        mat.set_off_diagonal(4.2);
        for i in 0..2 {
            for j in 0..2 {
                if i != j {
                    assert_eq!(4.2, mat[(i, j)]);
                } else {
                    assert_eq!(0.0, mat[(i, j)]);
                }
            }
        }

        mat.set_scalar(0.0);
        mat.set_row(0, &Vector2D::new(1.0, 2.0));
        mat.set_row(1, &Vector2D::new(3.0, 4.0));
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat[i]);
        }

        mat.set_scalar(0.0);
        mat.set_column(0, &Vector2D::new(1.0, 3.0));
        mat.set_column(1, &Vector2D::new(2.0, 4.0));
        for i in 0..4 {
            assert_eq!((i + 1) as f64, mat[i]);
        }
    }

    #[test]
    fn basic_getters() {
        let mat = Matrix2x2D::new(1.0, 2.0, 3.0, 4.0);
        let mat2 = Matrix2x2D::new(1.01, 2.01, 2.99, 4.0);

        assert_eq!(mat.is_similar(&mat2, Some(0.02)), true);
        assert_eq!(mat.is_similar(&mat2, Some(0.001)), false);

        assert_eq!(mat.is_square(), true);

        assert_eq!(2, mat.rows());
        assert_eq!(2, mat.cols());
    }

    #[test]
    fn binary_operators() {
        let mat = Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0);

        let mut mat2 = mat.add_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-2.0, 5.0, 0.0, 3.0), None), true);

        mat2 = mat.add_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-3.0, 5.0, 1.0, 5.0), None), true);

        mat2 = mat.sub_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-6.0, 1.0, -4.0, -1.0), None), true);

        mat2 = mat.sub_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-5.0, 1.0, -5.0, -3.0), None), true);

        mat2 = mat.mul_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-8.0, 6.0, -4.0, 2.0), None), true);

        let vec = mat.mul_vec(&Vector2D::new(1.0, 2.0));
        assert_eq!(vec.is_similar(&Vector2D::new(2.0, 0.0), None), true);

        mat2 = mat.mul_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(5.0, 4.0, 1.0, 0.0), None), true);

        mat2 = mat.div_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-2.0, 1.5, -1.0, 0.5), None), true);

        mat2 = mat.radd_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-2.0, 5.0, 0.0, 3.0), None), true);

        mat2 = mat.radd_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-3.0, 5.0, 1.0, 5.0), None), true);

        mat2 = mat.rsub_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(6.0, -1.0, 4.0, 1.0), None), true);

        mat2 = mat.rsub_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(5.0, -1.0, 5.0, 3.0), None), true);

        mat2 = mat.rmul_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-8.0, 6.0, -4.0, 2.0), None), true);

        mat2 = mat.rmul_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-8.0, 5.0, -20.0, 13.0), None), true);

        mat2 = mat.rdiv_scalar(2.0);
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-0.5, 2.0 / 3.0, -1.0, 2.0), None), true);
    }

    #[test]
    fn augmented_operators() {
        let mut mat = Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0);

        mat.iadd_scalar(2.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-2.0, 5.0, 0.0, 3.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.iadd_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-3.0, 5.0, 1.0, 5.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.isub_scalar(2.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-6.0, 1.0, -4.0, -1.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.isub_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-5.0, 1.0, -5.0, -3.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.imul_scalar(2.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-8.0, 6.0, -4.0, 2.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.imul_mat(&Matrix2x2D::new(1.0, 2.0, 3.0, 4.0));
        assert_eq!(mat.is_similar(&Matrix2x2D::new(5.0, 4.0, 1.0, 0.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.idiv_scalar(2.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-2.0, 1.5, -1.0, 0.5), None), true);
    }

    #[test]
    fn modifiers() {
        let mut mat = Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0);

        mat.transpose();
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-4.0, -2.0, 3.0, 1.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat.invert();
        assert_eq!(mat.is_similar(&Matrix2x2D::new(0.5, -1.5, 1.0, -2.0), None), true);
    }

    #[test]
    fn complex_getters() {
        let mat = Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0);

        assert_eq!(-2.0, mat.sum());

        assert_eq!(-0.5, mat.avg());

        assert_eq!(-4.0, mat.min());

        assert_eq!(3.0, mat.max());

        assert_eq!(1.0, mat.absmin());

        assert_eq!(-4.0, mat.absmax());

        assert_eq!(-3.0, mat.trace());

        assert_eq!(2.0, mat.determinant());

        let mut mat2 = mat.diagonal();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-4.0, 0.0, 0.0, 1.0), None), true);

        mat2 = mat.off_diagonal();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(0.0, 3.0, -2.0, 0.0), None), true);

        mat2 = mat.strict_lower_tri();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(0.0, 0.0, -2.0, 0.0), None), true);

        mat2 = mat.strict_upper_tri();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(0.0, 3.0, 0.0, 0.0), None), true);

        mat2 = mat.lower_tri();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-4.0, 0.0, -2.0, 1.0), None), true);

        mat2 = mat.upper_tri();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-4.0, 3.0, 0.0, 1.0), None), true);

        mat2 = mat.transposed();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-4.0, -2.0, 3.0, 1.0), None), true);

        mat2 = mat.inverse();
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(0.5, -1.5, 1.0, -2.0), None), true);
    }

    #[test]
    fn setter_operator_overloadings() {
        let mut mat = Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0);

        let mat2 = mat;
        assert_eq!(mat2.is_similar(&Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0), None), true);

        mat += 2.0;
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-2.0, 5.0, 0.0, 3.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat += Matrix2x2D::new(1.0, 2.0, 3.0, 4.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-3.0, 5.0, 1.0, 5.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat -= 2.0;
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-6.0, 1.0, -4.0, -1.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat -= Matrix2x2D::new(1.0, 2.0, 3.0, 4.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-5.0, 1.0, -5.0, -3.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat *= 2.0;
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-8.0, 6.0, -4.0, 2.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat *= Matrix2x2D::new(1.0, 2.0, 3.0, 4.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(5.0, 4.0, 1.0, 0.0), None), true);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        mat /= 2.0;
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-2.0, 1.5, -1.0, 0.5), None), true);
    }

    #[test]
    fn getter_operator_overloadings() {
        let mut mat = Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0);

        assert_eq!(-4.0, mat[0]);
        assert_eq!(3.0, mat[1]);
        assert_eq!(-2.0, mat[2]);
        assert_eq!(1.0, mat[3]);

        mat[0] = 4.0;
        mat[1] = -3.0;
        mat[2] = 2.0;
        mat[3] = -1.0;
        assert_eq!(4.0, mat[0]);
        assert_eq!(-3.0, mat[1]);
        assert_eq!(2.0, mat[2]);
        assert_eq!(-1.0, mat[3]);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        assert_eq!(-4.0, mat[(0, 0)]);
        assert_eq!(3.0, mat[(0, 1)]);
        assert_eq!(-2.0, mat[(1, 0)]);
        assert_eq!(1.0, mat[(1, 1)]);

        mat[(0, 0)] = 4.0;
        mat[(0, 1)] = -3.0;
        mat[(1, 0)] = 2.0;
        mat[(1, 1)] = -1.0;
        assert_eq!(4.0, mat[0]);
        assert_eq!(-3.0, mat[1]);
        assert_eq!(2.0, mat[2]);
        assert_eq!(-1.0, mat[3]);

        mat.set(-4.0, 3.0, -2.0, 1.0);
        assert_eq!(mat == Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0), true);

        mat.set(4.0, 3.0, 2.0, 1.0);
        assert_eq!(mat != Matrix2x2D::new(-4.0, 3.0, -2.0, 1.0), true);
    }

    #[test]
    fn helpers() {
        let mut mat = Matrix2x2D::make_zero();
        assert_eq!(mat.is_similar(&Matrix2x2D::new(0.0, 0.0, 0.0, 0.0), None), true);

        mat = Matrix2x2D::make_identity();
        assert_eq!(mat.is_similar(&Matrix2x2D::new(1.0, 0.0, 0.0, 1.0), None), true);

        mat = Matrix2x2D::make_scale_matrix_scalar(3.0, -4.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(3.0, 0.0, 0.0, -4.0), None), true);

        mat = Matrix2x2D::make_scale_matrix_vec(Vector2D::new(-2.0, 5.0));
        assert_eq!(mat.is_similar(&Matrix2x2D::new(-2.0, 0.0, 0.0, 5.0), None), true);

        mat = Matrix2x2D::make_rotation_matrix(crate::constants::K_PI_D / 3.0);
        assert_eq!(mat.is_similar(&Matrix2x2D::new(
            0.5, -f64::sqrt(3.0) / 2.0, f64::sqrt(3.0) / 2.0, 0.5), None), true);
    }
}