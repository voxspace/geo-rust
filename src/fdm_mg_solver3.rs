/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::mg::{MgParameters, mg_vcycle};
use crate::fdm_linear_system3::*;
use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;
use crate::fdm_mg_linear_system3::{FdmMgLinearSystem3, FdmMgUtils3};
use crate::fdm_gauss_seidel_solver3::FdmGaussSeidelSolver3;
use std::sync::Arc;

/// # 3-D finite difference-type linear system solver using Multigrid.
#[derive(Default)]
pub struct FdmMgSolver3 {
    _mg_params: MgParameters<FdmBlas3>,
    _sor_factor: f64,
    _use_red_black_ordering: bool,
}

impl FdmMgSolver3 {
    /// Constructs the solver with given parameters.
    pub fn new(max_number_of_levels: usize,
               number_of_restriction_iter: Option<u32>,
               number_of_correction_iter: Option<u32>,
               number_of_coarsest_iter: Option<u32>,
               number_of_final_iter: Option<u32>,
               max_tolerance: Option<f64>, sor_factor: Option<f64>,
               use_red_black_ordering: Option<bool>) -> FdmMgSolver3 {
        let mut solver = FdmMgSolver3::default();

        solver._mg_params.max_number_of_levels = max_number_of_levels;
        solver._mg_params.number_of_restriction_iter = number_of_restriction_iter.unwrap_or(5);
        solver._mg_params.number_of_correction_iter = number_of_correction_iter.unwrap_or(5);
        solver._mg_params.number_of_coarsest_iter = number_of_coarsest_iter.unwrap_or(20);
        solver._mg_params.number_of_final_iter = number_of_final_iter.unwrap_or(20);
        solver._mg_params.max_tolerance = max_tolerance.unwrap_or(1e-9);
        if use_red_black_ordering.unwrap_or(false) {
            solver._mg_params.relax_func = Some(Arc::new(move |a: &FdmMatrix3, b: &FdmVector3, number_of_iterations: u32, _: f64,
                                                               x: &mut FdmVector3, _: &mut FdmVector3| {
                for _ in 0..number_of_iterations {
                    FdmGaussSeidelSolver3::relax_red_black(a, b, sor_factor.unwrap_or(1.5), x);
                }
            }));
        } else {
            solver._mg_params.relax_func = Some(Arc::new(move |a: &FdmMatrix3, b: &FdmVector3, number_of_iterations: u32, _: f64,
                                                               x: &mut FdmVector3, _: &mut FdmVector3| {
                for _ in 0..number_of_iterations {
                    FdmGaussSeidelSolver3::relax(a, b, sor_factor.unwrap_or(1.5), x);
                }
            }));
        }
        solver._mg_params.restrict_func = Some(Arc::new(FdmMgUtils3::restrict));
        solver._mg_params.correct_func = Some(Arc::new(FdmMgUtils3::correct));

        solver._sor_factor = sor_factor.unwrap_or(1.5);
        solver._use_red_black_ordering = use_red_black_ordering.unwrap_or(false);

        return solver;
    }

    /// Returns the Multigrid parameters.
    pub fn params(&self) -> &MgParameters<FdmBlas3> {
        return &self._mg_params;
    }

    /// Returns the SOR (Successive Over Relaxation) factor.
    pub fn sor_factor(&self) -> f64 {
        return self._sor_factor;
    }

    /// Returns true if red-black ordering is enabled.
    pub fn use_red_black_ordering(&self) -> bool {
        return self._use_red_black_ordering;
    }
}

impl<'a> FdmLinearSystemSolver3<'a> for FdmMgSolver3 {
    fn solve(&mut self, _system: &mut FdmLinearSystem3) -> bool {
        unimplemented!();
    }
}

impl FdmMgSolver3 {
    /// Solves Multigrid linear system.
    pub fn solve(&mut self, system: &mut FdmMgLinearSystem3) -> bool {
        let mut buffer = system.x.clone();
        let result = mg_vcycle(&system.a, self._mg_params.clone(), &mut system.x, &mut system.b, &mut buffer);
        return result.last_residual_norm < self._mg_params.max_tolerance;
    }
}