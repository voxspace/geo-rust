/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector_expression::*;
use crate::array_accessor1::*;
use std::ops::*;
use std::mem::swap;
use rayon::prelude::*;

///
/// # General purpose dynamically-sized N-D vector class.
///
/// This class defines N-D vector data where its size can be defined
/// dynamically.
///
/// \tparam T Type of the element.
///
#[derive(Clone)]
pub struct VectorN {
    _elements: Vec<f64>,
}

impl VectorExpression for VectorN {
    fn size(&self) -> usize {
        return self._elements.len();
    }

    fn eval(&self, i: usize) -> f64 {
        return self._elements[i];
    }
}

impl Default for VectorN {
    /// Constructs empty vector.
    fn default() -> Self {
        return VectorN {
            _elements: vec![]
        };
    }
}

/// # Constructors
impl VectorN {
    /// Constructs default vector (val, val, ... , val).
    pub fn new(n: usize, val: Option<f64>) -> VectorN {
        return VectorN {
            _elements: vec![val.unwrap_or(0.0); n]
        };
    }

    /// Constructs vector with given initializer list.
    pub fn new_slice(lst: &[f64]) -> VectorN {
        let mut vec = VectorN::default();
        vec.set_slice(lst);
        return vec;
    }

    /// Constructs vector with expression template.
    pub fn new_expression<E: VectorExpression>(other: &E) -> VectorN {
        let mut vec = VectorN::default();
        vec.set_expression(other);
        return vec;
    }
}

/// # Basic setters
impl VectorN {
    /// Resizes to \p n dimensional vector with initial value \p val.
    pub fn resize(&mut self, n: usize, val: Option<f64>) {
        self._elements.resize(n, val.unwrap_or(0.0));
    }

    /// Clears the vector and make it zero-dimensional.
    pub fn clear(&mut self) {
        self._elements.clear();
    }

    /// Sets all elements to \p s.
    pub fn set_scalar(&mut self, s: f64) {
        self._elements.fill(s);
    }

    /// Sets all elements with given initializer list.
    pub fn set_slice(&mut self, lst: &[f64]) {
        self._elements.resize(lst.len(), 0.0);
        self._elements.copy_from_slice(lst);
    }

    /// Sets vector with expression template.
    pub fn set_expression<E: VectorExpression>(&mut self, other: &E) {
        self.resize(other.size(), None);

        for i in 0..other.size() {
            self._elements[i] = other.eval(i);
        }
    }

    /// Adds an element.
    pub fn append(&mut self, val: f64) {
        self._elements.push(val);
    }

    /// Swaps the content of the vector with \p other vector.
    pub fn swap(&mut self, other: &mut VectorN) {
        swap(&mut other._elements, &mut self._elements);
    }

    /// Sets all elements to zero.
    pub fn set_zero(&mut self) {
        self.set_scalar(0.0);
    }

    /// Normalizes this vector.
    pub fn normalize(&mut self) {
        self.idiv_scalar(self.length());
    }
}

/// # Basic getters
impl VectorN {
    /// Returns the size of the vector.
    pub fn size(&self) -> usize {
        return self._elements.len();
    }

    /// Returns the raw pointer to the vector data.
    pub fn data_mut(&mut self) -> &mut [f64] {
        return &mut self._elements;
    }

    /// Returns the const raw pointer to the vector data.
    pub fn data(&self) -> &[f64] {
        return &self._elements;
    }

    /// Returns the array accessor.
    pub fn accessor(&mut self) -> ArrayAccessor1<f64> {
        return ArrayAccessor1::new(self.data_mut());
    }

    /// Returns the const array accessor.
    pub fn const_accessor(&self) -> ConstArrayAccessor1<f64> {
        return ConstArrayAccessor1::new(self.data());
    }

    /// Returns const reference to the \p i -th element of the vector.
    pub fn at(&self, i: usize) -> &f64 {
        return &self._elements[i];
    }

    /// Returns reference to the \p i -th element of the vector.
    pub fn at_mut(&mut self, i: usize) -> &mut f64 {
        return &mut self._elements[i];
    }

    /// Returns the sum of all the elements.
    pub fn sum(&self) -> f64 {
        return self._elements.par_iter().sum();
    }

    /// Returns the average of all the elements.
    pub fn avg(&self) -> f64 {
        return self.sum() / self.size() as f64;
    }

    /// Returns the minimum element.
    pub fn min(&self) -> f64 {
        return self._elements.par_iter().cloned().reduce(|| { f64::MAX }, |a, b| {
            return f64::min(a, b);
        });
    }

    /// Returns the maximum element.
    pub fn max(&self) -> f64 {
        return self._elements.par_iter().cloned().reduce(|| { f64::MIN }, |a, b| {
            return f64::max(a, b);
        });
    }

    /// Returns the absolute minimum element.
    pub fn absmin(&self) -> f64 {
        return self._elements.par_iter().cloned().reduce(|| { f64::MAX }, |a, b| {
            return crate::math_utils::absmin(a, b);
        });
    }

    /// Returns the absolute maximum element.
    pub fn absmax(&self) -> f64 {
        return self._elements.par_iter().cloned().reduce(|| { 0.0 }, |a, b| {
            return crate::math_utils::absmax(a, b);
        });
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> usize {
        let mut max_abs = 0.0;
        let mut max_index = 0;
        for i in 0..self.size() {
            let abs = self._elements[i].abs();
            if abs > max_abs {
                max_abs = abs;
                max_index = i;
            }
        }

        return max_index;
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> usize {
        let mut min_abs = f64::MAX;
        let mut min_index = self.size();
        for i in 0..self.size() {
            let abs = self._elements[i].abs();
            if abs < min_abs {
                min_abs = abs;
                min_index = i;
            }
        }

        return min_index;
    }

    /// Returns normalized vector.
    pub fn normalized(&self) -> VectorScalarDiv<VectorN> {
        let len = self.length();
        return VectorScalarDiv::new(self, len);
    }

    /// Returns the length of the vector.
    pub fn length(&self) -> f64 {
        return f64::sqrt(self.length_squared());
    }

    /// Returns the squared length of the vector.
    pub fn length_squared(&self) -> f64 {
        return self.dot(self);
    }

    /// Returns the distance to the other vector.
    pub fn distance_to<E: VectorExpression>(&self, other: &E) -> f64 {
        return f64::sqrt(self.distance_squared_to(other));
    }

    /// Returns the squared distance to the other vector.
    pub fn distance_squared_to<E: VectorExpression>(&self, other: &E) -> f64 {
        let mut result = 0.0;
        for i in 0..self.size() {
            let diff = self._elements[i] - other.eval(i);
            result += diff * diff;
        }
        return result;
    }

    /// Returns true if \p other is the same as this vector.
    pub fn is_equal<E: VectorExpression>(&self, other: &E) -> bool {
        if self.size() != other.size() {
            return false;
        }

        for i in 0..self.size() {
            if *self.at(i) != other.eval(i) {
                return false;
            }
        }
        return true;
    }

    /// Returns true if \p other is similar to this vector.
    pub fn is_similar<E: VectorExpression>(&self, other: &E, epsilon: Option<f64>) -> bool {
        if self.size() != other.size() {
            return false;
        }

        for i in 0..self.size() {
            if f64::abs(*self.at(i) - other.eval(i)) > epsilon.unwrap_or(f64::EPSILON) {
                return false;
            }
        }
        return true;
    }
}

/// # Binary operations: new instance = this (+) v
impl VectorN {
    /// Computes this + v.
    pub fn add_expression<'a, E: VectorExpression>(&'a self, v: &'a E) -> VectorAdd<VectorN, E> {
        return VectorAdd::new(self, v);
    }

    /// Computes this + (s, s, ... , s).
    pub fn add_scalar(&self, s: f64) -> VectorScalarAdd<VectorN> {
        return VectorScalarAdd::new(self, s);
    }

    /// Computes this - v.
    pub fn sub_expression<'a, E: VectorExpression>(&'a self, v: &'a E) -> VectorSub<VectorN, E> {
        return VectorSub::new(self, v);
    }

    /// Computes this - (s, s, ... , s).
    pub fn sub_scalar(&self, s: f64) -> VectorScalarSub<VectorN> {
        return VectorScalarSub::new(self, s);
    }

    /// Computes this * v.
    pub fn mul_expression<'a, E: VectorExpression>(&'a self, v: &'a E) -> VectorMul<VectorN, E> {
        return VectorMul::new(self, v);
    }

    /// Computes this * (s, s, ... , s).
    pub fn mul_scalar(&self, s: f64) -> VectorScalarMul<VectorN> {
        return VectorScalarMul::new(self, s);
    }

    /// Computes this / v.
    pub fn div_expression<'a, E: VectorExpression>(&'a self, v: &'a E) -> VectorDiv<VectorN, E> {
        return VectorDiv::new(self, v);
    }

    /// Computes this / (s, s, ... , s).
    pub fn div_scalar(&self, s: f64) -> VectorScalarDiv<VectorN> {
        return VectorScalarDiv::new(self, s);
    }

    /// Computes dot product.
    pub fn dot<E: VectorExpression>(&self, v: &E) -> f64 {
        let mut result = 0.0;
        for i in 0..self.size() {
            result += self._elements[i] * v.eval(i);
        }
        return result;
    }
}

/// # Binary operations: new instance = v (+) this
impl VectorN {
    /// Computes (s, s, ... , s) - this.
    pub fn rsub_scalar(&self, s: f64) -> VectorScalarRSub<VectorN> {
        return VectorScalarRSub::new(self, s);
    }

    /// Computes v - this.
    pub fn rsub_expression<'a, E: VectorExpression>(&'a self, v: &'a E) -> VectorSub<E, VectorN> {
        return VectorSub::new(v, self);
    }

    /// Computes (s, s, ... , s) / this.
    pub fn rdiv_scalar(&self, s: f64) -> VectorScalarRDiv<VectorN> {
        return VectorScalarRDiv::new(self, s);
    }

    /// Computes v / this.
    pub fn rdiv_expression<'a, E: VectorExpression>(&'a self, v: &'a E) -> VectorDiv<E, VectorN> {
        return VectorDiv::new(v, self);
    }
}

/// # Augmented operations: this (+)= v
impl VectorN {
    /// Computes this += (s, s, ... , s).
    pub fn iadd_scalar(&mut self, s: f64) {
        let expression = self.add_scalar(s);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this += v.
    pub fn iadd_expression<E: VectorExpression>(&mut self, v: &E) {
        let expression = self.add_expression(v);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this -= (s, s, ... , s).
    pub fn isub_scalar(&mut self, s: f64) {
        let expression = self.sub_scalar(s);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this -= v.
    pub fn isub_expression<E: VectorExpression>(&mut self, v: &E) {
        let expression = self.sub_expression(v);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this *= (s, s, ... , s).
    pub fn imul_scalar(&mut self, s: f64) {
        let expression = self.mul_scalar(s);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this *= v.
    pub fn imul_expression<E: VectorExpression>(&mut self, v: &E) {
        let expression = self.mul_expression(v);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this /= (s, s, ... , s).
    pub fn idiv_scalar(&mut self, s: f64) {
        let expression = self.div_scalar(s);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }

    /// Computes this /= v.
    pub fn idiv_expression<E: VectorExpression>(&mut self, v: &E) {
        let expression = self.div_expression(v);
        let mut element = Vec::new();
        element.resize(self.size(), 0.0);
        // Parallel evaluation of the expression
        for i in 0..self.size() {
            element[i] = expression.eval(i);
        }

        self._elements = element;
    }
}

/// # Operators
impl VectorN {
    ///
    /// \brief Iterates the vector and invoke given \p func for each element.
    ///
    /// This function iterates the vector elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be 0 to N-1 where N is the size of
    /// the vector. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// VectorN<float> vec(10, 4.f);
    /// vec.forEach([](float elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&f64)>(&self, func: Callback) {
        self.const_accessor().for_each(func);
    }

    ///
    /// \brief Iterates the vector and invoke given \p func for each index.
    ///
    /// This function iterates the vector elements and invoke the callback
    /// function \p func. The callback function takes one parameter which is the
    /// index of the vector. The order of execution will be 0 to N-1 where N is
    /// the size of the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// VectorN<float> vec(10, 4.f);
    /// vec.forEachIndex([&](size_t i) {
    ///     vec[i] = 4.f * i + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize)>(&self, func: Callback) {
        self.const_accessor().for_each_index(func);
    }
}

impl Index<usize> for VectorN {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._elements[index];
    }
}

impl IndexMut<usize> for VectorN {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._elements[index];
    }
}

impl AddAssign<f64> for VectorN {
    fn add_assign(&mut self, rhs: f64) {
        self.iadd_scalar(rhs);
    }
}

impl<E: VectorExpression> AddAssign<&E> for VectorN {
    fn add_assign(&mut self, rhs: &E) {
        self.iadd_expression(rhs);
    }
}

impl SubAssign<f64> for VectorN {
    fn sub_assign(&mut self, rhs: f64) {
        self.isub_scalar(rhs);
    }
}

impl<E: VectorExpression> SubAssign<&E> for VectorN {
    fn sub_assign(&mut self, rhs: &E) {
        self.isub_expression(rhs);
    }
}

impl MulAssign<f64> for VectorN {
    fn mul_assign(&mut self, rhs: f64) {
        self.imul_scalar(rhs);
    }
}

impl<E: VectorExpression> MulAssign<&E> for VectorN {
    fn mul_assign(&mut self, rhs: &E) {
        self.imul_expression(rhs);
    }
}

impl DivAssign<f64> for VectorN {
    fn div_assign(&mut self, rhs: f64) {
        self.idiv_scalar(rhs);
    }
}

impl<E: VectorExpression> DivAssign<&E> for VectorN {
    fn div_assign(&mut self, rhs: &E) {
        self.idiv_expression(rhs);
    }
}

impl PartialEq for VectorN {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

//--------------------------------------------------------------------------------------------------
/// Note lazy operator anymore
impl Add<f64> for VectorN {
    type Output = VectorN;

    fn add(self, rhs: f64) -> Self::Output {
        return VectorN::new_expression(&self.add_scalar(rhs));
    }
}

impl<E: VectorExpression> Add<E> for VectorN {
    type Output = VectorN;

    fn add(self, rhs: E) -> Self::Output {
        return VectorN::new_expression(&self.add_expression(&rhs));
    }
}

impl Sub<f64> for VectorN {
    type Output = VectorN;

    fn sub(self, rhs: f64) -> Self::Output {
        return VectorN::new_expression(&self.sub_scalar(rhs));
    }
}

impl<E: VectorExpression> Sub<E> for VectorN {
    type Output = VectorN;

    fn sub(self, rhs: E) -> Self::Output {
        return VectorN::new_expression(&self.sub_expression(&rhs));
    }
}

impl Mul<f64> for VectorN {
    type Output = VectorN;

    fn mul(self, rhs: f64) -> Self::Output {
        return VectorN::new_expression(&self.mul_scalar(rhs));
    }
}

impl<E: VectorExpression> Mul<E> for VectorN {
    type Output = VectorN;

    fn mul(self, rhs: E) -> Self::Output {
        return VectorN::new_expression(&self.mul_expression(&rhs));
    }
}

impl Div<f64> for VectorN {
    type Output = VectorN;

    fn div(self, rhs: f64) -> Self::Output {
        return VectorN::new_expression(&self.div_scalar(rhs));
    }
}

impl<E: VectorExpression> Div<E> for VectorN {
    type Output = VectorN;

    fn div(self, rhs: E) -> Self::Output {
        return VectorN::new_expression(&self.div_expression(&rhs));
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod vector {
    use crate::vector_expression::VectorExpression;
    use crate::vector_n::VectorN;

    #[test]
    fn constructors() {
        let empty = VectorN::default();
        assert_eq!(0, empty.size());

        let vec = VectorN::new_slice(&[1.0, 4.0, 3.0, -5.0, 2.0]);

        assert_eq!(5, vec.size());
        assert_eq!(1.0, vec[0]);
        assert_eq!(4.0, vec[1]);
        assert_eq!(3.0, vec[2]);
        assert_eq!(-5.0, vec[3]);
        assert_eq!(2.0, vec[4]);

        let vec2 = vec.clone();

        assert_eq!(5, vec2.size());
        assert_eq!(1.0, vec2[0]);
        assert_eq!(4.0, vec2[1]);
        assert_eq!(3.0, vec2[2]);
        assert_eq!(-5.0, vec2[3]);
        assert_eq!(2.0, vec2[4]);

        let vec3 = VectorN::new(7, Some(3.14));
        assert_eq!(7, vec3.size());
        for i in 0..7 {
            assert_eq!(3.14, vec3[i]);
        }
    }

    #[test]
    fn basic_setters() {
        let mut vec = VectorN::default();

        vec.resize(8, Some(3.0));
        assert_eq!(8, vec.size());
        for i in 0..8 {
            assert_eq!(3.0, vec[i]);
        }

        vec.set_slice(&[1.0, 4.0, 3.0, -5.0, 2.0]);

        assert_eq!(5, vec.size());
        assert_eq!(1.0, vec[0]);
        assert_eq!(4.0, vec[1]);
        assert_eq!(3.0, vec[2]);
        assert_eq!(-5.0, vec[3]);
        assert_eq!(2.0, vec[4]);

        let mut vec2 = VectorN::new(3, Some(4.0));

        vec2.set_expression(&vec);
        assert_eq!(5, vec2.size());
        assert_eq!(1.0, vec2[0]);
        assert_eq!(4.0, vec2[1]);
        assert_eq!(3.0, vec2[2]);
        assert_eq!(-5.0, vec2[3]);
        assert_eq!(2.0, vec2[4]);

        let mut vec3 = VectorN::new(7, Some(3.14));
        vec2.swap(&mut vec3);

        for i in 0..7 {
            assert_eq!(3.14, vec2[i]);
        }
        assert_eq!(5, vec3.size());
        assert_eq!(1.0, vec3[0]);
        assert_eq!(4.0, vec3[1]);
        assert_eq!(3.0, vec3[2]);
        assert_eq!(-5.0, vec3[3]);
        assert_eq!(2.0, vec3[4]);

        vec3.set_zero();
        for i in 0..vec3.size() {
            assert_eq!(0.0, vec3[i]);
        }

        vec3.set_expression(&vec);
        vec3.normalize();
        let denom = f64::sqrt(55.0);
        assert_eq!(1.0 / denom, vec3[0]);
        assert_eq!(4.0 / denom, vec3[1]);
        assert_eq!(3.0 / denom, vec3[2]);
        assert_eq!(-5.0 / denom, vec3[3]);
        assert_eq!(2.0 / denom, vec3[4]);
    }

    #[test]
    fn basic_getters() {
        let mut vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);

        assert_eq!(4, vec_a.size());

        let data = vec_a.data();
        assert_eq!(3.0, data[0]);
        assert_eq!(-1.0, data[1]);
        assert_eq!(2.0, data[2]);
        assert_eq!(5.0, data[3]);

        let data2 = vec_a.data_mut();
        data2[0] = 6.0;
        data2[1] = 2.5;
        data2[2] = -9.0;
        data2[3] = 8.0;
        assert_eq!(6.0, vec_a[0]);
        assert_eq!(2.5, vec_a[1]);
        assert_eq!(-9.0, vec_a[2]);
        assert_eq!(8.0, vec_a[3]);

        let iter = vec_a.data();
        assert_eq!(6.0, iter[0]);
        assert_eq!(2.5, iter[1]);
        assert_eq!(-9.0, iter[2]);
        assert_eq!(8.0, iter[3]);

        vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);
        let iter2 = vec_a.data_mut();
        iter2[0] = 6.0;
        iter2[1] = 2.5;
        iter2[2] = -9.0;
        iter2[3] = 8.0;
        assert_eq!(6.0, iter2[0]);
        assert_eq!(2.5, iter2[1]);
        assert_eq!(-9.0, iter2[2]);
        assert_eq!(8.0, iter2[3]);

        let d = vec_a.data().len();
        assert_eq!(4, d);

        let acc = vec_a.const_accessor();
        assert_eq!(4, acc.size());
        assert_eq!(6.0, acc[0]);
        assert_eq!(2.5, acc[1]);
        assert_eq!(-9.0, acc[2]);
        assert_eq!(8.0, acc[3]);

        vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);
        let mut acc2 = vec_a.accessor();
        acc2[0] = 6.0;
        acc2[1] = 2.5;
        acc2[2] = -9.0;
        acc2[3] = 8.0;
        assert_eq!(6.0, acc2[0]);
        assert_eq!(2.5, acc2[1]);
        assert_eq!(-9.0, acc2[2]);
        assert_eq!(8.0, acc2[3]);

        assert_eq!(6.0, *vec_a.at(0));
        assert_eq!(2.5, *vec_a.at(1));
        assert_eq!(-9.0, *vec_a.at(2));
        assert_eq!(8.0, *vec_a.at(3));

        vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);
        *vec_a.at_mut(0) = 6.0;
        *vec_a.at_mut(1) = 2.5;
        *vec_a.at_mut(2) = -9.0;
        *vec_a.at_mut(3) = 8.0;
        assert_eq!(6.0, vec_a[0]);
        assert_eq!(2.5, vec_a[1]);
        assert_eq!(-9.0, vec_a[2]);
        assert_eq!(8.0, vec_a[3]);

        assert_eq!(7.5, vec_a.sum());
        assert_eq!(7.5 / 4.0, vec_a.avg());
        assert_eq!(-9.0, vec_a.min());
        assert_eq!(8.0, vec_a.max());
        assert_eq!(2.5, vec_a.absmin());
        assert_eq!(-9.0, vec_a.absmax());
        assert_eq!(2, vec_a.dominant_axis());
        assert_eq!(1, vec_a.subminant_axis());

        let mut vec_b = vec_a.clone();
        let vec_c = vec_b.normalized();
        vec_a.normalize();
        for i in 0..vec_a.size() {
            assert_eq!(vec_a[i], vec_c.eval(i));
        }

        *vec_a.at_mut(0) = 6.0;
        *vec_a.at_mut(1) = 2.5;
        *vec_a.at_mut(2) = -9.0;
        *vec_a.at_mut(3) = 8.0;
        let len_sqr = vec_a.length_squared();
        assert_eq!(187.25, len_sqr);

        let len = vec_a.length();
        assert_eq!(f64::sqrt(187.25), len);

        vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);
        vec_b = VectorN::new_slice(&[6.0, 2.5, -9.0, 8.0]);
        let dist_sq = vec_a.distance_squared_to(&vec_b);
        assert_eq!(151.25, dist_sq);

        let dist = vec_a.distance_to(&vec_b);
        assert_eq!(f64::sqrt(151.25), dist);

        assert_eq!(vec_a.is_equal(&vec_b), false);
        vec_b = vec_a.clone();
        assert_eq!(vec_a.is_equal(&vec_b), true);

        vec_b[0] += 1e-8;
        vec_b[1] -= 1e-8;
        vec_b[2] += 1e-8;
        vec_b[3] -= 1e-8;
        assert_eq!(vec_a.is_equal(&vec_b), false);
        assert_eq!(vec_a.is_similar(&vec_b, Some(1e-7)), true);
    }

    #[test]
    fn binary_operator_methods() {
        let vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);
        let vec_b = VectorN::new_slice(&[6.0, 2.5, -9.0, 8.0]);
        let mut vec_c = VectorN::new_expression(&vec_a.add_expression(&vec_b));

        assert_eq!(9.0, vec_c[0]);
        assert_eq!(1.5, vec_c[1]);
        assert_eq!(-7.0, vec_c[2]);
        assert_eq!(13.0, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.add_scalar(3.0));
        assert_eq!(6.0, vec_c[0]);
        assert_eq!(2.0, vec_c[1]);
        assert_eq!(5.0, vec_c[2]);
        assert_eq!(8.0, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.sub_expression(&vec_b));
        assert_eq!(-3.0, vec_c[0]);
        assert_eq!(-3.5, vec_c[1]);
        assert_eq!(11.0, vec_c[2]);
        assert_eq!(-3.0, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.sub_scalar(4.0));
        assert_eq!(-1.0, vec_c[0]);
        assert_eq!(-5.0, vec_c[1]);
        assert_eq!(-2.0, vec_c[2]);
        assert_eq!(1.0, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.mul_expression(&vec_b));
        assert_eq!(18.0, vec_c[0]);
        assert_eq!(-2.5, vec_c[1]);
        assert_eq!(-18.0, vec_c[2]);
        assert_eq!(40.0, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.mul_scalar(2.0));
        assert_eq!(6.0, vec_c[0]);
        assert_eq!(-2.0, vec_c[1]);
        assert_eq!(4.0, vec_c[2]);
        assert_eq!(10.0, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.div_expression(&vec_b));
        assert_eq!(0.5, vec_c[0]);
        assert_eq!(-0.4, vec_c[1]);
        assert_eq!(-2.0 / 9.0, vec_c[2]);
        assert_eq!(0.625, vec_c[3]);

        vec_c = VectorN::new_expression(&vec_a.div_scalar(0.5));
        assert_eq!(6.0, vec_c[0]);
        assert_eq!(-2.0, vec_c[1]);
        assert_eq!(4.0, vec_c[2]);
        assert_eq!(10.0, vec_c[3]);

        let d = vec_a.dot(&vec_b);
        assert_eq!(37.5, d);
    }

    #[test]
    fn binary_operators() {
        let vec_a = VectorN::new_slice(&[3.0, -1.0, 2.0, 5.0]);
        let vec_b = VectorN::new_slice(&[6.0, 2.5, -9.0, 8.0]);
        let mut vec_c = vec_a.clone() + vec_b.clone();

        assert_eq!(9.0, vec_c[0]);
        assert_eq!(1.5, vec_c[1]);
        assert_eq!(-7.0, vec_c[2]);
        assert_eq!(13.0, vec_c[3]);

        vec_c = vec_a.clone() + 3.0;
        assert_eq!(6.0, vec_c[0]);
        assert_eq!(2.0, vec_c[1]);
        assert_eq!(5.0, vec_c[2]);
        assert_eq!(8.0, vec_c[3]);

        // vec_c = 2.0 + vec_a;
        // assert_eq!(5.0, vec_c[0]);
        // assert_eq!(1.0, vec_c[1]);
        // assert_eq!(4.0, vec_c[2]);
        // assert_eq!(7.0, vec_c[3]);

        vec_c = vec_a.clone() - vec_b.clone();
        assert_eq!(-3.0, vec_c[0]);
        assert_eq!(-3.5, vec_c[1]);
        assert_eq!(11.0, vec_c[2]);
        assert_eq!(-3.0, vec_c[3]);

        // vec_c = 6.0 - vec_a;
        // assert_eq!(3.0, vec_c[0]);
        // assert_eq!(7.0, vec_c[1]);
        // assert_eq!(4.0, vec_c[2]);
        // assert_eq!(1.0, vec_c[3]);

        vec_c = vec_a.clone() - 4.0;
        assert_eq!(-1.0, vec_c[0]);
        assert_eq!(-5.0, vec_c[1]);
        assert_eq!(-2.0, vec_c[2]);
        assert_eq!(1.0, vec_c[3]);

        vec_c = vec_a.clone() * vec_b.clone();
        assert_eq!(18.0, vec_c[0]);
        assert_eq!(-2.5, vec_c[1]);
        assert_eq!(-18.0, vec_c[2]);
        assert_eq!(40.0, vec_c[3]);

        vec_c = vec_a.clone() * 2.0;
        assert_eq!(6.0, vec_c[0]);
        assert_eq!(-2.0, vec_c[1]);
        assert_eq!(4.0, vec_c[2]);
        assert_eq!(10.0, vec_c[3]);

        vec_c = vec_a.clone() / vec_b.clone();
        assert_eq!(0.5, vec_c[0]);
        assert_eq!(-0.4, vec_c[1]);
        assert_eq!(-2.0 / 9.0, vec_c[2]);
        assert_eq!(0.625, vec_c[3]);

        vec_c = vec_a.clone() / 0.5;
        assert_eq!(6.0, vec_c[0]);
        assert_eq!(-2.0, vec_c[1]);
        assert_eq!(4.0, vec_c[2]);
        assert_eq!(10.0, vec_c[3]);

        // vec_c = 2.0 / vec_a;
        // assert_eq!(2.0 / 3.0, vec_c[0]);
        // assert_eq!(-2.0, vec_c[1]);
        // assert_eq!(1.0, vec_c[2]);
        // assert_eq!(0.4, vec_c[3]);
        //
        // vec_c = 3.0 / (0.5 * vec_a + 2.0 * vec_b);
        // assert_eq!(3.0 / 13.5, vec_c[0]);
        // assert_eq!(2.0 / 3.0, vec_c[1]);
        // assert_eq!(3.0 / -17.0, vec_c[2]);
        // assert_eq!(3.0 / 18.5, vec_c[3]);
    }
}