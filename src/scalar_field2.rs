/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::field2::*;
use crate::vector2::Vector2D;
use std::sync::{RwLock, Arc};

/// Abstract base class for 2-D scalar field.
pub trait ScalarField2: Field2 {
    /// Returns sampled value at given position \p x.
    fn sample(&self, x: &Vector2D) -> f64;

    /// Returns gradient vector at given position \p x.
    fn gradient(&self, _: &Vector2D) -> Vector2D {
        return Vector2D::default();
    }

    /// Returns Laplacian at given position \p x.
    fn laplacian(&self, _: &Vector2D) -> f64 {
        return 0.0;
    }
}

/// Shared pointer for the ScalarField2 type.
pub type ScalarField2Ptr = Arc<RwLock<dyn ScalarField2>>;