/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::field2::Field2;
use crate::scalar_field2::ScalarField2;
use crate::vector2::Vector2D;
use std::sync::{RwLock, Arc};

/// # 2-D constant scalar field.
pub struct ConstantScalarField2 {
    _value: f64,
}

impl ConstantScalarField2 {
    /// Constructs a constant scalar field with given \p value.
    pub fn new(value: Option<f64>) -> ConstantScalarField2 {
        return ConstantScalarField2 {
            _value: value.unwrap_or(0.0)
        };
    }

    /// Returns builder fox ConstantScalarField2.
    pub fn builder() -> Builder {
        return Builder::new();
    }
}

impl Field2 for ConstantScalarField2 {}

impl ScalarField2 for ConstantScalarField2 {
    fn sample(&self, _: &Vector2D) -> f64 {
        return self._value;
    }
}

/// Shared pointer for the ConstantScalarField2 type.
pub type ConstantScalarField2Ptr = Arc<RwLock<ConstantScalarField2>>;

///
/// # Front-end to create ConstantScalarField2 objects step by step.
///
pub struct Builder {
    _value: f64,
}

impl Builder {
    /// Returns builder with value.
    pub fn with_value(&mut self, value: f64) -> &mut Self {
        self._value = value;
        return self;
    }

    /// Builds ConstantScalarField2.
    pub fn build(&mut self) -> ConstantScalarField2 {
        return ConstantScalarField2::new(Some(self._value));
    }

    /// Builds shared pointer of ConstantScalarField2 instance.
    pub fn make_shared(&mut self) -> ConstantScalarField2Ptr {
        return ConstantScalarField2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _value: 0.0
        };
    }
}