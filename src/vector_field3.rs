/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::field3::*;
use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

/// Abstract base class for 3-D vector field.
pub trait VectorField3: Field3 {
    /// Returns sampled value at given position \p x.
    fn sample(&self, x: &Vector3D) -> Vector3D;

    /// Returns divergence at given position \p x.
    fn divergence(&self, _: &Vector3D) -> f64 {
        return 0.0;
    }

    /// Returns curl at given position \p x.
    fn curl(&self, _: &Vector3D) -> Vector3D {
        return Vector3D::default();
    }
}

/// Shared pointer for the VectorField3 type.
pub type VectorField3Ptr = Arc<RwLock<dyn VectorField3 + Send + Sync>>;