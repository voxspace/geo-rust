/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::ops::*;
use std::fmt::{Debug, Formatter, Result};

///
/// # 2-D size class.
///
/// This class defines simple 2-D size data.
///
/// - tparam T - Type of the element
///
#[derive(Clone, Copy, Default)]
pub struct ISize2 {
    /// X (or the first) component of the size.
    pub x: isize,

    /// Y (or the second) component of the size.
    pub y: isize,
}

/// # Constructors
impl ISize2 {
    /// Constructs size with given parameters \p x_ and \p y_.
    pub fn new(x_: isize, y_: isize) -> ISize2 {
        return ISize2 {
            x: x_,
            y: y_,
        };
    }

    /// Constructs size with initializer list.
    pub fn new_slice(lst: &[isize]) -> ISize2 {
        return ISize2 {
            x: lst[0],
            y: lst[1],
        };
    }
}

/// # Basic setters
impl ISize2 {
    /// Set both x and y components to **s**.
    pub fn set_scalar(&mut self, s: isize) {
        self.x = s;
        self.y = s;
    }

    /// Set x and y components with given parameters.
    pub fn set(&mut self, x: isize, y: isize) {
        self.x = x;
        self.y = y;
    }

    /// Set x and y components with given initializer list.
    pub fn set_slice(&mut self, lst: &[isize]) {
        self.x = lst[0];
        self.y = lst[1];
    }

    /// Set x and y with other size **pt**.
    pub fn set_self(&mut self, pt: &ISize2) {
        self.x = pt.x;
        self.y = pt.y;
    }

    /// Set both x and y to zero.
    pub fn set_zero(&mut self) {
        self.x = 0;
        self.y = 0;
    }
}

/// # Binary operations: new instance = this (+) v
impl ISize2 {
    /// Computes self + (v, v).
    pub fn add_scalar(&self, v: isize) -> ISize2 {
        return ISize2::new(self.x + v, self.y + v);
    }

    /// Computes self + (v.x, v.y).
    pub fn add_vec(&self, v: &ISize2) -> ISize2 {
        return ISize2::new(self.x + v.x, self.y + v.y);
    }

    /// Computes self - (v, v).
    pub fn sub_scalar(&self, v: isize) -> ISize2 {
        return ISize2::new(self.x - v, self.y - v);
    }

    /// Computes self - (v.x, v.y).
    pub fn sub_vec(&self, v: &ISize2) -> ISize2 {
        return ISize2::new(self.x - v.x, self.y - v.y);
    }

    /// Computes self * (v, v).
    pub fn mul_scalar(&self, v: isize) -> ISize2 {
        return ISize2::new(self.x * v, self.y * v);
    }

    /// Computes self * (v.x, v.y).
    pub fn mul_vec(&self, v: &ISize2) -> ISize2 {
        return ISize2::new(self.x * v.x, self.y * v.y);
    }

    /// Computes self / (v, v).
    pub fn div_scalar(&self, v: isize) -> ISize2 {
        return ISize2::new(self.x / v, self.y / v);
    }

    /// Computes self / (v.x, v.y).
    pub fn div_vec(&self, v: &ISize2) -> ISize2 {
        return ISize2::new(self.x / v.x, self.y / v.y);
    }
}

/// # Binary operations: new instance = v (+) this
impl ISize2 {
    /// Computes (v, v) - self.
    pub fn rsub_scalar(&self, v: isize) -> ISize2 {
        return ISize2::new(v - self.x, v - self.y);
    }

    /// Computes (v.x, v.y) - self.
    pub fn rsub_vec(&self, v: &ISize2) -> ISize2 {
        return ISize2::new(v.x - self.x, v.y - self.y);
    }

    /// Computes (v, v) / self.
    pub fn rdiv_scalar(&self, v: isize) -> ISize2 {
        return ISize2::new(v / self.x, v / self.y);
    }

    /// Computes (v.x, v.y) / self.
    pub fn rdiv_vec(&self, v: &ISize2) -> ISize2 {
        return ISize2::new(v.x / self.x, v.y / self.y);
    }
}

/// # Augmented operations: self (+)= v
impl ISize2 {
    /// Computes self += (v, v).
    pub fn iadd_scalar(&mut self, v: isize) {
        self.x = isize::add(self.x, v);
        self.y = isize::add(self.y, v);
    }

    /// Computes self += (v.x, v.y).
    pub fn iadd_vec(&mut self, v: &ISize2) {
        self.x = isize::add(self.x, v.x);
        self.y = isize::add(self.y, v.y);
    }

    /// Computes self -= (v, v).
    pub fn isub_scalar(&mut self, v: isize) {
        self.x = isize::sub(self.x, v);
        self.y = isize::sub(self.y, v);
    }

    /// Computes self -= (v.x, v.y).
    pub fn isub_vec(&mut self, v: &ISize2) {
        self.x = isize::sub(self.x, v.x);
        self.y = isize::sub(self.y, v.y);
    }

    /// Computes self *= (v, v).
    pub fn imul_scalar(&mut self, v: isize) {
        self.x = isize::mul(self.x, v);
        self.y = isize::mul(self.y, v);
    }

    /// Computes self *= (v.x, v.y).
    pub fn imul_vec(&mut self, v: &ISize2) {
        self.x = isize::mul(self.x, v.x);
        self.y = isize::mul(self.y, v.y);
    }

    /// Computes self /= (v, v).
    pub fn idiv_scalar(&mut self, v: isize) {
        self.x = isize::div(self.x, v);
        self.y = isize::div(self.y, v);
    }

    /// Computes self /= (v.x, v.y).
    pub fn idiv_vec(&mut self, v: &ISize2) {
        self.x = isize::div(self.x, v.x);
        self.y = isize::div(self.y, v.y);
    }
}

/// # Basic getters
impl ISize2 {
    /// Returns const reference to the **i** -th element of the size.
    pub fn at(&self, i: isize) -> &isize {
        match i {
            0 => return &self.x,
            1 => return &self.y,
            _ => { panic!() }
        }
    }

    /// Returns reference to the **i** -th element of the size.
    pub fn at_mut(&mut self, i: isize) -> &mut isize {
        match i {
            0 => return &mut self.x,
            1 => return &mut self.y,
            _ => { panic!() }
        }
    }

    /// Returns the sum of all the components (i.e. x + y).
    pub fn sum(&self) -> isize {
        return self.x + self.y;
    }

    /// Returns the minimum value among x and y.
    pub fn min(&self) -> isize {
        return self.x.min(self.y);
    }

    /// Returns the maximum value among x and y.
    pub fn max(&self) -> isize {
        return self.x.max(self.y);
    }

    /// Returns the index of the dominant axis.
    pub fn dominant_axis(&self) -> isize {
        match self.x > self.y {
            true => 0,
            false => 1
        }
    }

    /// Returns the index of the subminant axis.
    pub fn subminant_axis(&self) -> isize {
        match self.x < self.y {
            true => 0,
            false => 1
        }
    }

    /// Returns true if **other** is the same as self size.
    pub fn is_equal(&self, other: &ISize2) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

/// # Operators
/// Returns const reference to the **i** -th element of the size.
impl Index<isize> for ISize2 {
    type Output = isize;
    fn index(&self, index: isize) -> &Self::Output {
        return self.at(index);
    }
}

/// Returns reference to the **i** -th element of the size.
impl IndexMut<isize> for ISize2 {
    fn index_mut(&mut self, index: isize) -> &mut Self::Output {
        return self.at_mut(index);
    }
}

/// Computes self += (v, v)
impl AddAssign<isize> for ISize2 {
    fn add_assign(&mut self, rhs: isize) {
        self.iadd_scalar(rhs);
    }
}

/// Computes self += (v.x, v.y)
impl AddAssign for ISize2 {
    fn add_assign(&mut self, rhs: Self) {
        self.iadd_vec(&rhs);
    }
}

/// Computes self -= (v, v)
impl SubAssign<isize> for ISize2 {
    fn sub_assign(&mut self, rhs: isize) {
        self.isub_scalar(rhs);
    }
}

/// Computes self -= (v.x, v.y)
impl SubAssign for ISize2 {
    fn sub_assign(&mut self, rhs: Self) {
        self.isub_vec(&rhs);
    }
}

/// Computes self *= (v, v)
impl MulAssign<isize> for ISize2 {
    fn mul_assign(&mut self, rhs: isize) {
        self.imul_scalar(rhs);
    }
}

/// Computes self *= (v.x, v.y)
impl MulAssign for ISize2 {
    fn mul_assign(&mut self, rhs: Self) {
        self.imul_vec(&rhs);
    }
}

/// Computes self /= (v, v)
impl DivAssign<isize> for ISize2 {
    fn div_assign(&mut self, rhs: isize) {
        self.idiv_scalar(rhs);
    }
}

/// Computes self /= (v.x, v.y)
impl DivAssign for ISize2 {
    fn div_assign(&mut self, rhs: Self) {
        self.idiv_vec(&rhs);
    }
}

/// Returns true if **other** is the same as self size.
impl PartialEq for ISize2 {
    fn eq(&self, other: &Self) -> bool {
        return self.is_equal(other);
    }
}

impl Eq for ISize2 {}

/// Computes (a, a) + (b.x, b.y).
impl Add<isize> for ISize2 {
    type Output = ISize2;
    fn add(self, rhs: isize) -> Self::Output {
        return self.add_scalar(rhs);
    }
}

/// Computes (a.x, a.y) + (b.x, b.y).
impl Add for ISize2 {
    type Output = ISize2;
    fn add(self, rhs: Self) -> Self::Output {
        return self.add_vec(&rhs);
    }
}

/// Computes (a.x, a.y) - (b, b).
impl Sub<isize> for ISize2 {
    type Output = ISize2;
    fn sub(self, rhs: isize) -> Self::Output {
        return self.sub_scalar(rhs);
    }
}

/// Computes (a.x, a.y) - (b.x, b.y).
impl Sub for ISize2 {
    type Output = ISize2;
    fn sub(self, rhs: Self) -> Self::Output {
        return self.sub_vec(&rhs);
    }
}

/// Computes (a.x, a.y) * (b, b).
impl Mul<isize> for ISize2 {
    type Output = ISize2;
    fn mul(self, rhs: isize) -> Self::Output {
        return self.mul_scalar(rhs);
    }
}

/// Computes (a.x, a.y) * (b.x, b.y).
impl Mul for ISize2 {
    type Output = ISize2;
    fn mul(self, rhs: Self) -> Self::Output {
        return self.mul_vec(&rhs);
    }
}

/// Computes (a.x, a.y) / (b, b).
impl Div<isize> for ISize2 {
    type Output = ISize2;
    fn div(self, rhs: isize) -> Self::Output {
        return self.div_scalar(rhs);
    }
}

/// Computes (a.x, a.y) / (b.x, b.y).
impl Div for ISize2 {
    type Output = ISize2;
    fn div(self, rhs: Self) -> Self::Output {
        return self.div_vec(&rhs);
    }
}

impl Debug for ISize2 {
    /// # Example
    /// ```
    ///
    /// use vox_geometry_rust::isize2::ISize2;
    /// let vec = ISize2::new(10, 20);
    /// assert_eq!(format!("{:?}", vec), "(10, 20)");
    ///
    /// assert_eq!(format!("{:#?}", vec), "(
    ///     10,
    ///     20,
    /// )");
    /// ```
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.debug_tuple("")
            .field(&self.x)
            .field(&self.y)
            .finish()
    }
}

/// # utility
/// Returns element-wise min size: (min(a.x, b.x), min(a.y, b.y)).
pub fn min(a: &ISize2, b: &ISize2) -> ISize2 {
    return ISize2::new(isize::min(a.x, b.x), isize::min(a.y, b.y));
}

/// Returns element-wise max size: (max(a.x, b.x), max(a.y, b.y)).
pub fn max(a: &ISize2, b: &ISize2) -> ISize2 {
    return ISize2::new(isize::max(a.x, b.x), isize::max(a.y, b.y));
}

/// Returns element-wise clamped size.
pub fn clamp(v: &ISize2, low: &ISize2, high: &ISize2) -> ISize2 {
    return ISize2::new(isize::clamp(v.x, low.x, high.x),
                       isize::clamp(v.y, low.y, high.y));
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod isize2 {
    #[test]
    fn constructors() {}
}