/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::operators::*;

/// # VectorExpression

///
/// # Base class for vector expression.
///
/// Vector expression is a meta type that enables template expression pattern.
///
/// - tparam T - Real number type.
/// - tparam E - Subclass type.
///
pub trait VectorExpression {
    fn size(&self) -> usize;

    fn eval(&self, i: usize) -> f64;
}

/// # VectorUnaryOp

///
/// # Vector expression for unary operation.
///
/// This matrix expression represents an unary vector operation that takes
/// single input vector expression.
///
/// - tparam T - Real number type.
/// - tparam E - Input expression type.
/// - tparam Op - Unary operation.
///
pub struct VectorUnaryOp<'a, E: VectorExpression, Op: UnaryOp> {
    _u: &'a E,
    _op: Op,
}

impl<'a, E: VectorExpression, Op: UnaryOp> VectorUnaryOp<'a, E, Op> {
    /// Constructs unary operation expression for given input expression.
    pub fn new(u: &'a E) -> VectorUnaryOp<'a, E, Op> {
        return VectorUnaryOp {
            _u: u,
            _op: Op::new(),
        };
    }
}

impl<'a, E: VectorExpression, Op: UnaryOp> VectorExpression for VectorUnaryOp<'a, E, Op> {
    /// Size of the vector.
    fn size(&self) -> usize {
        return self._u.size();
    }

    /// Returns vector element at i.
    fn eval(&self, i: usize) -> f64 {
        return self._op.eval(self._u.eval(i));
    }
}

//--------------------------------------------------------------------------------------------------
/// # VectorBinaryOp

///
/// \brief Vector expression for binary operation.
///
/// This vector expression represents a binary vector operation that takes
/// two input vector expressions.
///
/// - tparam T - Real number type.
/// - tparam E1 - First input expression type.
/// - tparam E2 - Second input expression type.
/// - tparam Op - Binary operation.
///
pub struct VectorBinaryOp<'a, E1: VectorExpression, E2: VectorExpression, Op: BinaryOp> {
    _u: &'a E1,
    _v: &'a E2,
    _op: Op,
}

impl<'a, E1: VectorExpression, E2: VectorExpression, Op: BinaryOp> VectorBinaryOp<'a, E1, E2, Op> {
    /// Constructs binary operation expression for given input vector
    /// expressions.
    pub fn new(u: &'a E1, v: &'a E2) -> VectorBinaryOp<'a, E1, E2, Op> {
        return VectorBinaryOp {
            _u: u,
            _v: v,
            _op: Op::new(),
        };
    }
}

impl<'a, E1: VectorExpression, E2: VectorExpression, Op: BinaryOp> VectorExpression for VectorBinaryOp<'a, E1, E2, Op> {
    /// Size of the Vector.
    fn size(&self) -> usize {
        return self._v.size();
    }

    /// Returns vector element at i.
    fn eval(&self, i: usize) -> f64 {
        return self._op.eval(self._u.eval(i), self._v.eval(i));
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Vector expression for matrix-scalar binary operation.
///
/// This vector expression represents a binary matrix operation that takes
/// one input vector expression and one scalar.
///
/// - tparam T - Real number type.
/// - tparam E - Input expression type.
/// - tparam Op - Binary operation.
///
pub struct VectorScalarBinaryOp<'a, E: VectorExpression, Op: BinaryOp> {
    _u: &'a E,
    _v: f64,
    _op: Op,
}

impl<'a, E: VectorExpression, Op: BinaryOp> VectorScalarBinaryOp<'a, E, Op> {
    /// Constructs a binary expression for given vector and scalar.
    pub fn new(u: &'a E, v: f64) -> VectorScalarBinaryOp<'a, E, Op> {
        return VectorScalarBinaryOp {
            _u: u,
            _v: v,
            _op: Op::new(),
        };
    }
}

impl<'a, E: VectorExpression, Op: BinaryOp> VectorExpression for VectorScalarBinaryOp<'a, E, Op> {
    fn size(&self) -> usize {
        return self._u.size();
    }

    fn eval(&self, i: usize) -> f64 {
        return self._op.eval(self._u.eval(i), self._v);
    }
}

//--------------------------------------------------------------------------------------------------
/// # VectorBinaryOp Aliases
/// Vector-vector addition expression.
pub type VectorAdd<'a, E1, E2> = VectorBinaryOp<'a, E1, E2, Plus>;

/// Vector-scalar addition expression.
pub type VectorScalarAdd<'a, E> = VectorScalarBinaryOp<'a, E, Plus>;

/// Vector-vector addition expression.
pub type VectorSub<'a, E1, E2> = VectorBinaryOp<'a, E1, E2, Minus>;

/// Vector-scalar subtraction expression.
pub type VectorScalarSub<'a, E> = VectorScalarBinaryOp<'a, E, Minus>;

/// Scalar-vector subtraction expression.
pub type VectorScalarRSub<'a, E> = VectorScalarBinaryOp<'a, E, RMinus>;

/// Element-wise vector-vector multiplication expression.
pub type VectorMul<'a, E1, E2> = VectorBinaryOp<'a, E1, E2, Multiplies>;

/// Vector-scalar multiplication expression.
pub type VectorScalarMul<'a, E> = VectorScalarBinaryOp<'a, E, Multiplies>;

/// Element-wise vector-vector division expression.
pub type VectorDiv<'a, E1, E2> = VectorBinaryOp<'a, E1, E2, Divides>;

/// Vector-scalar division expression.
pub type VectorScalarDiv<'a, E> = VectorScalarBinaryOp<'a, E, Divides>;

/// Scalar-vector division expression.
pub type VectorScalarRDiv<'a, E> = VectorScalarBinaryOp<'a, E, RDivides>;

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod vector_expression_test {
    use crate::vector::Vector;
    use crate::vector_expression::*;

    #[test]
    fn constructor() {
        let a1: Vector<5> = Vector::new(&[0.0, 1.0, 2.0, 3.0, 4.0]);
        let a2: Vector<5> = Vector::new(&[0.0, 1.0, 2.0, 3.0, 4.0]);
        let a3: Vector<5> = Vector::new(&[0.0, 1.0, 2.0, 3.0, 4.0]);

        let sub_add = VectorAdd::new(&a1, &a2);
        let sub_mul = VectorMul::new(&sub_add, &a3);
        let expression = VectorSub::new(&sub_mul, &a1);

        let result = Vector::<5>::new_expression(&expression);
        assert_eq!(result.at(4), (4.0 + 4.0) * 4.0 - 4.0);
    }
}