/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::bounding_box2::BoundingBox2D;
use crate::ray2::Ray2D;
use crate::transform2::Transform2;
use std::sync::{RwLock, Arc};

/// Struct that represents ray-surface intersection point.
pub struct SurfaceRayIntersection2 {
    pub is_intersecting: bool,
    pub distance: f64,
    pub point: Vector2D,
    pub normal: Vector2D,
}

impl SurfaceRayIntersection2 {
    pub fn new() -> SurfaceRayIntersection2 {
        return SurfaceRayIntersection2 {
            is_intersecting: false,
            distance: f64::MAX,
            point: Vector2D::default(),
            normal: Vector2D::default(),
        };
    }
}

pub struct Surface2Data {
    /// Local-to-world transform.
    pub transform: Transform2,

    /// Flips normal.
    pub is_normal_flipped: bool,
}

impl Surface2Data {
    pub fn new(transform: Option<Transform2>,
               is_normal_flipped: Option<bool>) -> Surface2Data {
        return Surface2Data {
            transform: transform.unwrap_or(Transform2::default()),
            is_normal_flipped: is_normal_flipped.unwrap_or(false),
        };
    }
}

/// Abstract base class for 2-D surface.
pub trait Surface2 {
    /// Returns the closest point from the given point \p other_point to the
    /// surface in local frame.
    fn closest_point_local(&self, other_point: &Vector2D) -> Vector2D;

    /// Returns the bounding box of this surface object in local frame.
    fn bounding_box_local(&self) -> BoundingBox2D;

    /// Returns the closest intersection point for given \p ray in local frame.
    fn closest_intersection_local(&self, ray: &Ray2D) -> SurfaceRayIntersection2;

    /// Returns the normal to the closest point on the surface from the given
    /// point \p other_point in local frame.
    fn closest_normal_local(&self, other_point: &Vector2D) -> Vector2D;

    /// Returns true if the given \p ray intersects with this surface object
    /// in local frame.
    fn intersects_local(&self, ray_local: &Ray2D) -> bool {
        let result = self.closest_intersection_local(ray_local);
        return result.is_intersecting;
    }

    /// Returns the closest distance from the given point \p otherPoint to the
    /// point on the surface in local frame.
    fn closest_distance_local(&self, other_point_local: &Vector2D) -> f64 {
        return other_point_local.distance_to(self.closest_point_local(other_point_local));
    }

    /// Returns true if \p otherPoint is inside by given \p depth the volume
    /// defined by the surface in local frame.
    fn is_inside_local(&self, other_point_local: &Vector2D) -> bool {
        let cp_local = self.closest_point_local(other_point_local);
        let normal_local = self.closest_normal_local(other_point_local);
        return (*other_point_local - cp_local).dot(&normal_local) < 0.0;
    }

    //----------------------------------------------------------------------------------------------
    /// Returns the closest point from the given point \p other_point to the surface.
    fn closest_point(&self, other_point: &Vector2D) -> Vector2D {
        return self.view().transform.to_world_vec(&self.closest_point_local(
            &self.view().transform.to_local_vec(&other_point)));
    }

    /// Returns the bounding box of this surface object.
    fn bounding_box(&self) -> BoundingBox2D {
        return self.view().transform.to_world_aabb(&self.bounding_box_local());
    }

    /// Returns true if the given \p ray intersects with this surface object.
    fn intersects(&self, ray: &Ray2D) -> bool {
        return self.intersects_local(&self.view().transform.to_local_ray(&ray));
    }

    /// Returns the closest distance from the given point \p other_point to the
    /// point on the surface.
    fn closest_distance(&self, other_point: &Vector2D) -> f64 {
        return self.closest_distance_local(&self.view().transform.to_local_vec(&other_point));
    }

    /// Returns the closest intersection point for given \p ray.
    fn closest_intersection(&self, ray: &Ray2D) -> SurfaceRayIntersection2 {
        let mut result = self.closest_intersection_local(
            &self.view().transform.to_local_ray(&ray));
        result.point = self.view().transform.to_world_vec(&result.point);
        result.normal = self.view().transform.to_world_direction(&result.normal);
        match self.view().is_normal_flipped {
            true => result.normal *= -1.0,
            _ => {}
        }
        return result;
    }

    /// Returns the normal to the closest point on the surface from the given
    /// point \p other_point.
    fn closest_normal(&self, other_point: &Vector2D) -> Vector2D {
        let mut result = self.view().transform.to_world_direction(
            &self.closest_normal_local(
                &self.view().transform.to_local_vec(&other_point)));
        match self.view().is_normal_flipped {
            true => result *= -1.0,
            _ => {}
        }
        return result;
    }

    /// Updates internal spatial query engine.
    fn update_query_engine(&self) {}

    /// Returns true if bounding box can be defined.
    fn is_bounded(&self) -> bool {
        return true;
    }

    /// Returns true if the surface is a valid geometry.
    fn is_valid_geometry(&self) -> bool {
        return true;
    }

    /// Returns true if \p other_point is inside the volume defined by the surface.
    fn is_inside(&self, other_point: &Vector2D) -> bool {
        return self.view().is_normal_flipped == !self.is_inside_local(
            &self.view().transform.to_local_vec(&other_point));
    }

    //----------------------------------------------------------------------------------------------
    fn view(&self) -> &Surface2Data;
}

/// Shared pointer for the Surface2 type.
pub type Surface2Ptr = Arc<RwLock<dyn Surface2>>;

///
/// # Base class for 2-D surface builder.
///
pub trait SurfaceBuilderBase2 {
    /// Returns builder with flipped normal flag.
    fn with_is_normal_flipped(&mut self, is_normal_flipped: bool) -> &mut Self {
        self.view().is_normal_flipped = is_normal_flipped;
        return self;
    }

    /// Returns builder with translation.
    fn with_translation(&mut self, translation: Vector2D) -> &mut Self {
        self.view().transform.set_translation(translation);
        return self;
    }

    /// Returns builder with orientation.
    fn with_orientation(&mut self, orientation: f64) -> &mut Self {
        self.view().transform.set_orientation(orientation);
        return self;
    }

    /// Returns builder with transform.
    fn with_transform(&mut self, transform: Transform2) -> &mut Self {
        self.view().transform = transform;
        return self;
    }

    //----------------------------------------------------------------------------------------------
    fn view(&mut self) -> &mut Surface2Data;
}