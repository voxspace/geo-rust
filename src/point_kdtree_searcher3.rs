/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::kdtree3::KdTree3;
use crate::point_neighbor_searcher3::*;
use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

///
/// # KdTree-based 3-D point searcher.
///
/// This class implements 3-D point searcher by using KdTree for its internal
/// acceleration data structure.
///
pub struct PointKdTreeSearcher3 {
    _tree: KdTree3,
}

impl PointKdTreeSearcher3 {
    /// Constructs an empty kD-tree instance.
    pub fn new() -> PointKdTreeSearcher3 {
        return PointKdTreeSearcher3 {
            _tree: KdTree3::new()
        };
    }

    /// Returns builder fox PointSimpleListSearcher3.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    ///
    /// \brief      Creates a new instance of the object with same properties
    ///             than original.
    ///
    /// \return     Copy of this object.
    ///
    pub fn clone(&self) -> PointKdTreeSearcher3Ptr {
        let mut searcher = PointKdTreeSearcher3::new();
        searcher.set(self);
        return PointKdTreeSearcher3Ptr::new(RwLock::new(searcher));
    }

    /// Copy from the other instance.
    pub fn set(&mut self, other: &PointKdTreeSearcher3) {
        self._tree = other._tree.clone();
    }
}

impl PointNeighborSearcher3 for PointKdTreeSearcher3 {
    fn type_name() -> String {
        return "PointKdTreeSearcher3".parse().unwrap();
    }

    fn build(&mut self, points: &Vec<Vector3D>) {
        self._tree.build(points);
    }

    fn for_each_nearby_point<Callback>(&self, origin: &Vector3D, radius: f64, callback: &mut Callback)
        where Callback: ForEachNearbyPointFunc {
        self._tree.for_each_nearby_point(origin, radius, callback);
    }

    fn has_nearby_point(&self, origin: &Vector3D, radius: f64) -> bool {
        return self._tree.has_nearby_point(origin, radius);
    }
}

/// Shared pointer for the PointKdTreeSearcher3 type.
pub type PointKdTreeSearcher3Ptr = Arc<RwLock<PointKdTreeSearcher3>>;

///
/// # Front-end to create PointKdTreeSearcher3 objects step by step.
///
pub struct Builder {}

impl Builder {
    /// Builds PointKdTreeSearcher3 instance.
    pub fn build(&self) -> PointKdTreeSearcher3 {
        return PointKdTreeSearcher3::new();
    }

    /// Builds shared pointer of PointKdTreeSearcher3 instance.
    pub fn make_shared(&self) -> PointKdTreeSearcher3Ptr {
        return PointKdTreeSearcher3Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {};
    }
}