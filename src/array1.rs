/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::mem::swap;
use std::ops::{Index, IndexMut};
use crate::array_accessor1::*;
use crate::common_trait::ZeroInit;

///
/// # 1-D array accessor class.
///
/// This class represents 1-D array accessor. Array accessor provides array-like
/// data read/write functions, but does not handle memory management. Thus, it
/// is more like a random access iterator, but with multi-dimension support.
///
/// - tparam T - Array value type.
///
pub struct Array1<T: ZeroInit> {
    _data: Vec<T>,
}

impl<T: ZeroInit> Default for Array1<T> {
    /// Constructs zero-sized 1-D array.
    fn default() -> Self {
        let vec: Vec<T> = Vec::new();
        return Array1 {
            _data: vec
        };
    }
}

impl<T: ZeroInit> Array1<T> {
    /// Constructs 1-D array with given \p size and fill it with \p initVal.
    /// - parameter: size Initial size of the array.
    /// - parameter: initVal Initial value of each array element.
    pub fn new(size: usize, init_val: Option<T>) -> Array1<T> {
        let mut vec: Vec<T> = Vec::new();
        vec.resize(size, init_val.unwrap_or(T::zero_init()));
        return Array1 {
            _data: vec
        };
    }

    ///
    /// \brief Constructs 1-D array with given initializer list \p lst.
    ///
    /// This constructor will build 1-D array with given initializer list \p lst
    /// such as
    ///
    /// ```
    /// use vox_geometry_rust::array1::Array1;
    /// let arr: Array1<f32> = Array1::new_slice(&[1.0, 2.0, 4.0, 9.0, 3.0]);
    /// ```
    ///
    /// - parameter: lst Initializer list that should be copy to the new array.
    pub fn new_slice(lst: &[T]) -> Array1<T> {
        return Array1 {
            _data: Vec::from(lst)
        };
    }
}

impl<T: ZeroInit> Array1<T> {
    /// Sets entire array with given \p value.
    pub fn set_scalar(&mut self, value: T) {
        for v in &mut self._data {
            *v = value.clone();
        }
    }

    /// Copies given array \p other to this array.
    pub fn set_self(&mut self, other: &Array1<T>) {
        self._data = other._data.clone();
    }

    /// Copies given initializer list \p lst to this array.
    pub fn set_slice(&mut self, lst: Vec<T>) {
        self._data = lst.clone();
    }

    /// Clears the array and resizes to zero.
    pub fn clear(&mut self) {
        self._data.clear();
    }

    /// Resizes the array with \p size and fill the new element with \p initVal.
    pub fn resize(&mut self, size: usize, init_val: Option<T>) {
        self._data.resize(size, init_val.unwrap_or(T::zero_init()));
    }

    /// Returns the reference to the i-th element.
    pub fn at_mut(&mut self, i: usize) -> &mut T {
        return &mut self._data[i];
    }

    /// Returns the const reference to the i-th element.
    pub fn at(&self, i: usize) -> &T {
        return &self._data[i];
    }

    /// Returns size of the array.
    pub fn size(&self) -> usize {
        return self._data.len();
    }

    /// Returns the raw pointer to the array data.
    pub fn data_mut(&mut self) -> &mut [T] {
        return &mut self._data;
    }

    /// Returns the const raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return &self._data;
    }

    /// Returns the array accessor.
    pub fn accessor(&mut self) -> ArrayAccessor1<T> {
        return ArrayAccessor1::new(self.data_mut());
    }

    /// Returns the const array accessor.
    pub fn const_accessor(&self) -> ConstArrayAccessor1<T> {
        return ConstArrayAccessor1::new(self.data());
    }

    /// Swaps the content of the array with \p other array.
    pub fn swap(&mut self, other: &mut Array1<T>) {
        swap(&mut self._data, &mut other._data)
    }

    /// Appends single value \p newVal at the end of the array.
    pub fn push(&mut self, new_val: T) {
        self._data.push(new_val);
    }

    /// Appends \p other array at the end of the array.
    pub fn append(&mut self, other: &mut Array1<T>) {
        self._data.append(&mut other._data);
    }
}

impl<T: ZeroInit> Array1<T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each element.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be 0 to N-1 where N is the size of
    /// the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// Array<int, 1> array(10, 4);
    /// array.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, func: Callback) {
        self.const_accessor().for_each(func);
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes one parameter which is the
    /// index of the array. The order of execution will be 0 to N-1 where N is
    /// the size of the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// Array<int, 1> array(10, 4);
    /// array.for_each_index([&](size_t i) {
    ///     array[i] = 4.f * i + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize)>(&self, func: Callback) {
        self.const_accessor().for_each_index(func);
    }
}

/// Returns the const reference to i-th element.
impl<T: ZeroInit> Index<usize> for Array1<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data[index];
    }
}

/// Returns the reference to i-th element.
impl<T: ZeroInit> IndexMut<usize> for Array1<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._data[index];
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod array1 {
    use crate::array1::Array1;

    #[test]
    fn constructors() {
        {
            let arr: Array1<f32> = Array1::default();
            assert_eq!(0, arr.size());
        }
        {
            let arr = Array1::new(9, Some(1.5));
            assert_eq!(9, arr.size());
            for i in 0..9 {
                assert_eq!(1.5, arr[i]);
            }
        }
        {
            let arr = Array1::new_slice(&[1.0, 2.0, 3.0, 4.0]);
            assert_eq!(4, arr.size());
            for i in 0..4 {
                assert_eq!(i as f32 + 1.0, arr[i]);
            }
        }
    }

    #[test]
    fn set_methods() {
        {
            let mut arr1 = Array1::new(12, Some(-1.0));
            arr1.set_scalar(3.5);
            for a in arr1.data().iter() {
                assert_eq!(3.5, *a);
            }
        }
        {
            let mut arr1 = Array1::new(12, Some(-1.0));
            arr1.set_self(&Array1::default());
            assert_eq!(0, arr1.size());
        }
        {
            let mut arr2 = Array1::new(12, Some(-1.0));
            arr2.set_slice(vec![2.0, 5.0, 9.0, -1.0]);
            assert_eq!(4, arr2.size());
            assert_eq!(2.0, arr2[0]);
            assert_eq!(5.0, arr2[1]);
            assert_eq!(9.0, arr2[2]);
            assert_eq!(-1.0, arr2[3]);
        }
    }

    #[test]
    fn clear() {
        let mut arr1 = Array1::new_slice(&[2.0, 5.0, 9.0, -1.0]);
        arr1.clear();
        assert_eq!(0, arr1.size());
    }

    #[test]
    fn resize_method() {
        let mut arr: Array1<f32> = Array1::default();
        arr.resize(9, None);
        assert_eq!(9, arr.size());
        for i in 0..9 {
            assert_eq!(0.0, arr[i]);
        }

        arr.resize(12, Some(4.0));
        assert_eq!(12, arr.size());
        for i in 0..12 {
            if i < 9 {
                assert_eq!(0.0, arr[i]);
            } else {
                assert_eq!(4.0, arr[i]);
            }
        }
    }

    #[test]
    fn iterators() {
        let arr1: Array1<f32> = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let mut i: usize = 0;
        for elem in arr1.data().iter() {
            assert_eq!(arr1[i], *elem);
            i += 1;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let mut i = 0;
        arr1.for_each(|val| {
            assert_eq!(arr1[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let mut cnt = 0;
        arr1.for_each_index(|i| {
            assert_eq!(cnt, i);
            cnt += 1;
        });
    }
}