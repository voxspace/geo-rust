/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::field2::*;
use crate::vector2::Vector2D;
use std::sync::{RwLock, Arc};

/// Abstract base class for 2-D vector field.
pub trait VectorField2: Field2 {
    /// Returns sampled value at given position \p x.
    fn sample(&self, x: &Vector2D) -> Vector2D;

    /// Returns divergence at given position \p x.
    fn divergence(&self, _: &Vector2D) -> f64 {
        return 0.0;
    }

    /// Returns curl at given position \p x.
    fn curl(&self, _: &Vector2D) -> f64 {
        return 0.0;
    }
}

/// Shared pointer for the VectorField2 type.
pub type VectorField2Ptr = Arc<RwLock<dyn VectorField2 + Send + Sync>>;