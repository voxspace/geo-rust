/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::fdm_linear_system3::*;

/// Abstract base class for 3-D finite difference-type linear system solver.
pub trait FdmLinearSystemSolver3<'a> {
    /// Solves the given linear system.
    fn solve(&'a mut self, system: &'a mut FdmLinearSystem3) -> bool;

    /// Solves the given compressed linear system.
    fn solve_compressed(&'a mut self, _system: &'a mut FdmCompressedLinearSystem3) -> bool { return false; }
}