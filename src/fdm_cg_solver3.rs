/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector_n::VectorN;
use crate::fdm_linear_system3::*;
use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;
use crate::cg::cg;

/// # 3-D finite difference-type linear system solver using conjugate gradient.
pub struct FdmCgSolver3 {
    _max_number_of_iterations: u32,
    _last_number_of_iterations: u32,
    _tolerance: f64,
    _last_residual: f64,

    // Uncompressed vectors
    _r: FdmVector3,
    _d: FdmVector3,
    _q: FdmVector3,
    _s: FdmVector3,

    // Compressed vectors
    _r_comp: VectorN,
    _d_comp: VectorN,
    _q_comp: VectorN,
    _s_comp: VectorN,
}

impl FdmCgSolver3 {
    /// Constructs the solver with given parameters.
    pub fn new(max_number_of_iterations: u32, tolerance: f64) -> FdmCgSolver3 {
        return FdmCgSolver3 {
            _max_number_of_iterations: max_number_of_iterations,
            _last_number_of_iterations: 0,
            _tolerance: tolerance,
            _last_residual: crate::constants::K_MAX_D,
            _r: Default::default(),
            _d: Default::default(),
            _q: Default::default(),
            _s: Default::default(),
            _r_comp: Default::default(),
            _d_comp: Default::default(),
            _q_comp: Default::default(),
            _s_comp: Default::default(),
        };
    }

    /// Returns the max number of CG iterations.
    pub fn max_number_of_iterations(&self) -> u32 {
        return self._max_number_of_iterations;
    }

    /// Returns the last number of CG iterations the solver made.
    pub fn last_number_of_iterations(&self) -> u32 {
        return self._last_number_of_iterations;
    }

    /// Returns the max residual tolerance for the CG method.
    pub fn tolerance(&self) -> f64 {
        return self._tolerance;
    }

    /// Returns the last residual after the CG iterations.
    pub fn last_residual(&self) -> f64 {
        return self._last_residual;
    }
}

impl FdmCgSolver3 {
    fn clear_uncompressed_vectors(&mut self) {
        self._r.clear();
        self._d.clear();
        self._q.clear();
        self._s.clear();
    }

    fn clear_compressed_vectors(&mut self) {
        self._r_comp.clear();
        self._d_comp.clear();
        self._q_comp.clear();
        self._s_comp.clear();
    }
}

impl<'a> FdmLinearSystemSolver3<'a> for FdmCgSolver3 {
    fn solve(&mut self, system: &mut FdmLinearSystem3) -> bool {
        let matrix = &mut system.a;
        let solution = &mut system.x;
        let rhs = &mut system.b;

        debug_assert!(matrix.size() == rhs.size());
        debug_assert!(matrix.size() == solution.size());

        self.clear_compressed_vectors();

        let size = matrix.size();
        self._r.resize_with_packed_size(&size, None);
        self._d.resize_with_packed_size(&size, None);
        self._q.resize_with_packed_size(&size, None);
        self._s.resize_with_packed_size(&size, None);

        solution.set_scalar(0.0);
        self._r.set_scalar(0.0);
        self._d.set_scalar(0.0);
        self._q.set_scalar(0.0);
        self._s.set_scalar(0.0);

        cg::<FdmBlas3>(matrix, rhs, self._max_number_of_iterations, self._tolerance, solution,
                       &mut self._r, &mut self._d, &mut self._q, &mut self._s,
                       &mut self._last_number_of_iterations, &mut self._last_residual);

        return self._last_residual <= self._tolerance ||
            self._last_number_of_iterations < self._max_number_of_iterations;
    }

    fn solve_compressed(&mut self, system: &mut FdmCompressedLinearSystem3) -> bool {
        let matrix = &mut system.a;
        let solution = &mut system.x;
        let rhs = &mut system.b;

        self.clear_uncompressed_vectors();

        let size = solution.size();
        self._r_comp.resize(size, None);
        self._d_comp.resize(size, None);
        self._q_comp.resize(size, None);
        self._s_comp.resize(size, None);

        solution.set_scalar(0.0);
        self._r_comp.set_scalar(0.0);
        self._d_comp.set_scalar(0.0);
        self._q_comp.set_scalar(0.0);
        self._s_comp.set_scalar(0.0);

        cg::<FdmCompressedBlas3>(matrix, rhs, self._max_number_of_iterations, self._tolerance, solution,
                                 &mut self._r_comp, &mut self._d_comp, &mut self._q_comp, &mut self._s_comp,
                                 &mut self._last_number_of_iterations, &mut self._last_residual);

        return self._last_residual <= self._tolerance ||
            self._last_number_of_iterations < self._max_number_of_iterations;
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod fdm_cg_solver3 {
    use crate::fdm_linear_system3::{FdmLinearSystem3, FdmCompressedLinearSystem3};
    use crate::usize3::USize3;
    use crate::fdm_cg_solver3::FdmCgSolver3;
    use crate::fdm_linear_system_solver3::FdmLinearSystemSolver3;

    #[test]
    fn solve() {
        let mut system = FdmLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_linear_system(
            &mut system, &USize3::new(3, 3, 3));

        let mut solver = FdmCgSolver3::new(100, 1e-9);
        solver.solve(&mut system);

        assert_eq!(solver.tolerance() > solver.last_residual(), true);
    }

    #[test]
    fn solve_compressed() {
        let mut system = FdmCompressedLinearSystem3::default();
        crate::unit_tests_utils::FdmLinearSystemSolverTestHelper3::build_test_compressed_linear_system(
            &mut system, &USize3::new(3, 3, 3));

        let mut solver = FdmCgSolver3::new(100, 1e-9);
        solver.solve_compressed(&mut system);

        assert_eq!(solver.tolerance() > solver.last_residual(), true);
    }
}