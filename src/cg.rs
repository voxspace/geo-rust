/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::blas::Blas;
use std::marker::PhantomData;

pub trait PrecondTypeProtocol<BlasType: Blas> {
    fn solve(&mut self, b: &BlasType::VectorType, x: &mut BlasType::VectorType);
}

///
/// # No-op preconditioner for conjugate gradient.
///
/// This preconditioner does nothing but simply copies the input vector to the
/// output vector. Thus, it can be considered as an identity matrix.
///
pub struct NullCgPreconditioner<BlasType: Blas> {
    _blas: PhantomData<BlasType>,
}

impl<BlasType: Blas> NullCgPreconditioner<BlasType> {
    pub fn build(_: &BlasType::MatrixType) {}
}

impl<BlasType: Blas> PrecondTypeProtocol<BlasType> for NullCgPreconditioner<BlasType> {
    fn solve(&mut self, b: &BlasType::VectorType, x: &mut BlasType::VectorType) {
        BlasType::set_vv(b, x);
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # Solves conjugate gradient.
///
pub fn cg<BlasType: Blas>(
    a: &BlasType::MatrixType, b: &BlasType::VectorType,
    max_number_of_iterations: u32, tolerance: f64,
    x: &mut BlasType::VectorType, r: &mut BlasType::VectorType,
    d: &mut BlasType::VectorType, q: &mut BlasType::VectorType, s: &mut BlasType::VectorType,
    last_number_of_iterations: &mut u32, last_residual_norm: &mut f64) {
    let mut precond = NullCgPreconditioner {
        _blas: Default::default()
    };
    pcg::<BlasType, NullCgPreconditioner<BlasType>>(
        a,
        b,
        max_number_of_iterations,
        tolerance,
        &mut precond,
        x,
        r,
        d,
        q,
        s,
        last_number_of_iterations,
        last_residual_norm);
}

///
/// # Solves pre-conditioned conjugate gradient.
///
pub fn pcg<BlasType: Blas, PrecondType: PrecondTypeProtocol<BlasType>>(
    a: &BlasType::MatrixType, b: &BlasType::VectorType, max_number_of_iterations: u32, tolerance: f64, m: &mut PrecondType,
    x: &mut BlasType::VectorType, r: &mut BlasType::VectorType, d: &mut BlasType::VectorType,
    q: &mut BlasType::VectorType, s: &mut BlasType::VectorType,
    last_number_of_iterations: &mut u32, last_residual_norm: &mut f64) {
    // Clear
    BlasType::set_sv(0.0, r);
    BlasType::set_sv(0.0, d);
    BlasType::set_sv(0.0, q);
    BlasType::set_sv(0.0, s);

    // r = b - Ax
    BlasType::residual(a, x, b, r);

    // d = m^-1r
    m.solve(r, d);

    // sigma_new = r.d
    let mut sigma_new = BlasType::dot(r, d);

    let mut iter = 0;
    let mut trigger = false;

    while sigma_new > tolerance * tolerance && iter < max_number_of_iterations {
        // q = Ad
        BlasType::mvm(a, d, q);

        // alpha = sigma_new/d.q
        let alpha = sigma_new / BlasType::dot(d, q);

        // x = x + alpha*d
        let mut result = x.clone();
        BlasType::axpy(alpha, d, x, &mut result);
        *x = result;

        // if i is divisible by 50...
        if trigger || (iter % 50 == 0 && iter > 0) {
            // r = b - Ax
            BlasType::residual(a, x, b, r);
            trigger = false;
        } else {
            // r = r - alpha*q
            let mut result = r.clone();
            BlasType::axpy(-alpha, q, r, &mut result);
            *r = result;
        }

        // s = m^-1r
        m.solve(r, s);

        // sigma_old = sigma_new
        let sigma_old = sigma_new;

        // sigma_new = r.s
        sigma_new = BlasType::dot(r, s);

        if sigma_new > sigma_old {
            trigger = true;
        }

        // beta = sigma_new/sigma_old
        let beta = sigma_new / sigma_old;

        // d = s + beta*d
        let mut result = d.clone();
        BlasType::axpy(beta, d, s, &mut result);
        *d = result;

        iter += 1;
    }

    *last_number_of_iterations = iter;

    // std::fabs(sigma_new) - Workaround for negative zero
    *last_residual_norm = f64::sqrt(f64::abs(sigma_new));
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------#[cfg(test)]
mod cg_test {
    use crate::vector2::Vector2D;
    use crate::matrix2x2::Matrix2x2D;
    use crate::blas::BlasVector2;
    use crate::cg::*;
    use crate::assert_delta;

    #[test]
    fn cg_solver() {
        // Solve:
        // | 4 1 | |x|   |1|
        // | 1 3 | |y| = |2|

        let matrix = Matrix2x2D::new(4.0, 1.0, 1.0, 3.0);
        let rhs = Vector2D::new(1.0, 2.0);
        {
            // Zero iteration should give proper residual from iteration data.
            let mut x = Vector2D::default();
            let mut r = Vector2D::default();
            let mut d = Vector2D::default();
            let mut q = Vector2D::default();
            let mut s = Vector2D::default();
            let mut last_num_iter = 0;
            let mut last_residual_norm = 0.0;

            cg::<BlasVector2>(&matrix, &rhs, 0, 0.0,
                              &mut x, &mut r, &mut d, &mut q, &mut s,
                              &mut last_num_iter,
                              &mut last_residual_norm);

            assert_eq!(0.0, x.x);
            assert_eq!(0.0, x.y);

            assert_eq!(f64::sqrt(5.0), last_residual_norm);
            assert_eq!(0, last_num_iter);
        }
        {
            let mut x = Vector2D::default();
            let mut r = Vector2D::default();
            let mut d = Vector2D::default();
            let mut q = Vector2D::default();
            let mut s = Vector2D::default();
            let mut last_num_iter = 0;
            let mut last_residual_norm = 0.0;

            cg::<BlasVector2>(&matrix, &rhs, 10, 0.0,
                              &mut x, &mut r, &mut d, &mut q, &mut s,
                              &mut last_num_iter,
                              &mut last_residual_norm);

            assert_eq!(1.0 / 11.0, x.x);
            assert_eq!(7.0 / 11.0, x.y);

            assert_eq!(last_residual_norm < crate::constants::K_EPSILON_D, true);
            assert_eq!(last_num_iter <= 2, true);
        }
    }

    #[test]
    fn pcg_solver() {
        // Solve:
        // | 4 1 | |x|   |1|
        // | 1 3 | |y| = |2|

        let matrix = Matrix2x2D::new(4.0, 1.0, 1.0, 3.0);
        let rhs = Vector2D::new(1.0, 2.0);

        struct DiagonalPreconditioner {
            precond: Vector2D,
        }

        impl DiagonalPreconditioner {
            fn build(&mut self, matrix: &Matrix2x2D) {
                self.precond.x = matrix[(0, 0)];
                self.precond.y = matrix[(1, 1)];
            }
        }

        impl PrecondTypeProtocol<BlasVector2> for DiagonalPreconditioner {
            fn solve(&mut self, b: &Vector2D, x: &mut Vector2D) {
                *x = *b / self.precond;
            }
        }

        {
            let mut x = Vector2D::default();
            let mut r = Vector2D::default();
            let mut d = Vector2D::default();
            let mut q = Vector2D::default();
            let mut s = Vector2D::default();
            let mut last_num_iter = 0;
            let mut last_residual_norm = 0.0;

            let mut precond = DiagonalPreconditioner {
                precond: Default::default()
            };

            precond.build(&matrix);

            pcg::<BlasVector2, DiagonalPreconditioner>(&matrix, &rhs, 10, 0.0,
                                                       &mut precond, &mut x, &mut r, &mut d, &mut q, &mut s,
                                                       &mut last_num_iter, &mut last_residual_norm);

            assert_delta!(1.0 / 11.0, x.x, f64::EPSILON);
            assert_eq!(7.0 / 11.0, x.y);

            assert_eq!(last_residual_norm < crate::constants::K_EPSILON_D, true);
            assert_eq!(last_num_iter <= 2, true);
        }
    }
}