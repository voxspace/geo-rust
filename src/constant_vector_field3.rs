/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector3::Vector3D;
use crate::field3::Field3;
use crate::vector_field3::VectorField3;
use std::sync::{RwLock, Arc};

/// # 3-D constant vector field.
pub struct ConstantVectorField3 {
    _value: Vector3D,
}

impl ConstantVectorField3 {
    /// Constructs a constant vector field with given \p value.
    pub fn new(value: Option<Vector3D>) -> ConstantVectorField3 {
        return ConstantVectorField3 {
            _value: value.unwrap_or(Vector3D::default())
        };
    }

    /// Returns builder fox ConstantVectorField3.
    pub fn builder() -> Builder {
        return Builder::new();
    }
}

impl Field3 for ConstantVectorField3 {}

impl VectorField3 for ConstantVectorField3 {
    fn sample(&self, _: &Vector3D) -> Vector3D {
        return self._value;
    }
}

/// Shared pointer for the ConstantVectorField3 type.
pub type ConstantVectorField3Ptr = Arc<RwLock<ConstantVectorField3>>;

///
/// # Front-end to create ConstantVectorField3 objects step by step.
///
pub struct Builder {
    _value: Vector3D,
}

impl Builder {
    /// Returns builder with value.
    pub fn with_value(&mut self, value: Vector3D) -> &mut Self {
        self._value = value;
        return self;
    }

    /// Builds ConstantVectorField3.
    pub fn build(&mut self) -> ConstantVectorField3 {
        return ConstantVectorField3::new(Some(self._value));
    }

    /// Builds shared pointer of ConstantVectorField3 instance.
    pub fn make_shared(&mut self) -> ConstantVectorField3Ptr {
        return ConstantVectorField3Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _value: Vector3D::default()
        };
    }
}