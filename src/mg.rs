/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::blas::Blas;
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};
use std::sync::Arc;

// Multigrid matrix wrapper.
#[derive(Default)]
pub struct MgMatrix<BlasType: Blas> {
    _blas: PhantomData<BlasType>,

    pub levels: Vec<BlasType::MatrixType>,
}

impl<BlasType: Blas> MgMatrix<BlasType> {
    pub fn finest(&self) -> &BlasType::MatrixType {
        return self.levels.first().unwrap();
    }

    pub fn finest_mut(&mut self) -> &mut BlasType::MatrixType {
        return self.levels.first_mut().unwrap();
    }
}

impl<BlasType: Blas> Index<usize> for MgMatrix<BlasType> {
    type Output = BlasType::MatrixType;

    fn index(&self, index: usize) -> &Self::Output {
        return &self.levels[index];
    }
}

impl<BlasType: Blas> IndexMut<usize> for MgMatrix<BlasType> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self.levels[index];
    }
}

//--------------------------------------------------------------------------------------------------
/// Multigrid vector wrapper.
#[derive(Default, Clone)]
pub struct MgVector<BlasType: Blas> {
    _blas: PhantomData<BlasType>,

    pub levels: Vec<BlasType::VectorType>,
}

impl<BlasType: Blas> MgVector<BlasType> {
    pub fn finest(&self) -> &BlasType::VectorType {
        return self.levels.first().unwrap();
    }

    pub fn finest_mut(&mut self) -> &mut BlasType::VectorType {
        return self.levels.first_mut().unwrap();
    }
}

impl<BlasType: Blas> Index<usize> for MgVector<BlasType> {
    type Output = BlasType::VectorType;

    fn index(&self, index: usize) -> &Self::Output {
        return &self.levels[index];
    }
}

impl<BlasType: Blas> IndexMut<usize> for MgVector<BlasType> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self.levels[index];
    }
}

//--------------------------------------------------------------------------------------------------
/// Multigrid relax function type.
pub trait MgRelaxFunc<BlasType: Blas>: Fn(&BlasType::MatrixType, &BlasType::VectorType,
    u32, f64, &mut BlasType::VectorType, &mut BlasType::VectorType) {}

impl<BlasType: Blas, Super: Fn(&BlasType::MatrixType, &BlasType::VectorType,
    u32, f64, &mut BlasType::VectorType, &mut BlasType::VectorType)>
MgRelaxFunc<BlasType> for Super {}

/// Multigrid restriction function type.
pub trait MgRestrictFunc<BlasType: Blas>: Fn(&BlasType::VectorType, &mut BlasType::VectorType) {}

impl<BlasType: Blas, Super: Fn(&BlasType::VectorType, &mut BlasType::VectorType)>
MgRestrictFunc<BlasType> for Super {}

/// Multigrid correction function type.
pub trait MgCorrectFunc<BlasType: Blas>: Fn(&BlasType::VectorType, &mut BlasType::VectorType) {}

impl<BlasType: Blas, Super: Fn(&BlasType::VectorType, &mut BlasType::VectorType)>
MgCorrectFunc<BlasType> for Super {}

/// Multigrid input parameter set.
#[derive(Clone)]
pub struct MgParameters<BlasType: Blas> {
    /// Max number of multigrid levels.
    pub max_number_of_levels: usize,

    /// Number of iteration at restriction step.
    pub number_of_restriction_iter: u32,

    /// Number of iteration at correction step.
    pub number_of_correction_iter: u32,

    /// Number of iteration at coarsest step.
    pub number_of_coarsest_iter: u32,

    /// Number of iteration at final step.
    pub number_of_final_iter: u32,

    /// Relaxation function such as Jacobi or Gauss-Seidel.
    pub relax_func: Option<Arc<dyn MgRelaxFunc<BlasType>>>,

    /// Restrict function that maps finer to coarser grid.
    pub restrict_func: Option<Arc<dyn MgRestrictFunc<BlasType>>>,

    /// Correction function that maps coarser to finer grid.
    pub correct_func: Option<Arc<dyn MgCorrectFunc<BlasType>>>,

    /// Max error tolerance.
    pub max_tolerance: f64,
}

impl<BlasType: Blas>
Default for MgParameters<BlasType> {
    fn default() -> Self {
        return MgParameters {
            max_number_of_levels: 1,
            number_of_restriction_iter: 5,
            number_of_correction_iter: 5,
            number_of_coarsest_iter: 20,
            number_of_final_iter: 20,
            relax_func: None,
            restrict_func: None,
            correct_func: None,
            max_tolerance: 1e-9,
        };
    }
}

/// Multigrid result type.
#[derive(Default)]
pub struct MgResult {
    /// Lastly measured norm of residual.
    pub last_residual_norm: f64,
}

//--------------------------------------------------------------------------------------------------
///
/// # Performs Multigrid with V-cycle.
///
/// For given linear system matrix \p A and RHS vector \p b, this function
/// computes the solution \p x using Multigrid method with V-cycle.
///
pub fn mg_vcycle<BlasType: Blas>(
    a: &MgMatrix<BlasType>, params: MgParameters<BlasType>,
    x: &mut MgVector<BlasType>, b: &mut MgVector<BlasType>, buffer: &mut MgVector<BlasType>) -> MgResult {
    return mg_vcycle_internal(a, params, 0, x, b, buffer);
}

fn mg_vcycle_internal<BlasType: Blas>(
    a: &MgMatrix<BlasType>, mut params: MgParameters<BlasType>,
    current_level: usize, x: &mut MgVector<BlasType>,
    b: &mut MgVector<BlasType>, buffer: &mut MgVector<BlasType>) -> MgResult {
    // 1) Relax a few times on Ax = b, with arbitrary x
    (params.relax_func.as_ref().unwrap())(&a[current_level], &b[current_level],
                                          params.number_of_restriction_iter, params.max_tolerance,
                                          &mut x[current_level], &mut buffer[current_level]);

    // 2) if current_level is the coarsest grid, goto 5)
    if current_level < a.levels.len() - 1 {
        BlasType::residual(&a[current_level], &x[current_level],
                           &b[current_level], &mut buffer[current_level]);
        (params.restrict_func.as_ref().unwrap())(&buffer[current_level], &mut b[current_level + 1]);

        BlasType::set_sv(0.0, &mut x[current_level + 1]);

        params.max_tolerance *= 0.5;
        // Solve Ae = r
        mg_vcycle_internal(a, params.clone(), current_level + 1, x, b, buffer);
        params.max_tolerance *= 2.0;

        // 3) correct
        (params.correct_func.as_ref().unwrap())(&x[current_level + 1].clone(), &mut x[current_level]);

        // 4) relax nItr times on Ax = b, with initial guess x
        if current_level > 0 {
            (params.relax_func.as_ref().unwrap())(&a[current_level], &b[current_level],
                                                  params.number_of_correction_iter, params.max_tolerance,
                                                  &mut x[current_level], &mut buffer[current_level]);
        } else {
            (params.relax_func.as_ref().unwrap())(&a[current_level], &b[current_level],
                                                  params.number_of_final_iter, params.max_tolerance,
                                                  &mut x[current_level], &mut buffer[current_level]);
        }
    } else {
        // 5) solve directly with initial guess x
        (params.relax_func.as_ref().unwrap())(&a[current_level], &b[current_level],
                                              params.number_of_coarsest_iter, params.max_tolerance,
                                              &mut x[current_level], &mut buffer[current_level]);

        BlasType::residual(&a[current_level], &x[current_level],
                           &b[current_level], &mut buffer[current_level]);
    }

    BlasType::residual(&a[current_level], &x[current_level], &b[current_level],
                       &mut buffer[current_level]);

    let mut result = MgResult::default();
    result.last_residual_norm = BlasType::l2norm(&buffer[current_level]);
    return result;
}