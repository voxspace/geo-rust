/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

/// Callback function for nearby search query. The first parameter is the
/// index of the nearby point, and the second is the position of the point.
pub trait ForEachNearbyPointFunc: FnMut(usize, &Vector3D) {}

impl<Super: FnMut(usize, &Vector3D)> ForEachNearbyPointFunc for Super {}

///
/// # Abstract base class for 3-D neighbor point searcher.
///
/// This class provides interface for 3-D neighbor point searcher. For given
/// list of points, the class builds internal cache to accelerate the search.
/// Once built, the data structure is used to search nearby points for given
/// origin point.
///
pub trait PointNeighborSearcher3 {
    /// Returns the type name of the derived class.
    fn type_name() -> String;

    /// Builds internal acceleration structure for given points list.
    fn build(&mut self, points: &Vec<Vector3D>);

    /// Invokes the callback function for each nearby point around the origin
    /// within given radius.
    /// - Parameters:
    ///   - origin: The origin position.
    ///   - radius: The search radius.
    ///   - callback: The callback function.
    fn for_each_nearby_point<Callback>(&self, origin: &Vector3D, radius: f64,
                                       callback: &mut Callback) where Callback: ForEachNearbyPointFunc;


    /// Returns true if there are any nearby points for given origin within radius.
    /// - Parameters:
    ///   - origin: The origin.
    ///   - radius: The radius.
    fn has_nearby_point(&self, origin: &Vector3D, radius: f64) -> bool;
}

/// Shared pointer for the PointNeighborSearcher3 type.
pub type PointNeighborSearcher3Ptr = Arc<RwLock<dyn PointNeighborSearcher3>>;

/// Abstract base class for 3-D point neighbor searcher builders.
pub trait PointNeighborSearcherBuilder3 {}

/// Shared pointer for the PointNeighborSearcherBuilder3 type.
pub type PointNeighborSearcherBuilder3Ptr = Arc<RwLock<dyn PointNeighborSearcherBuilder3>>;