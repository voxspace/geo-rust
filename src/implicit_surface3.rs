/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::surface3::Surface3;
use crate::vector3::Vector3D;
use std::sync::{RwLock, Arc};

/// Abstract base class for 3-D implicit surface.
pub trait ImplicitSurface3: Surface3 {
    /// Returns signed distance from the given point \p otherPoint.
    fn signed_distance(&self, other_point: &Vector3D) -> f64 {
        let sd = self.signed_distance_local(&self.view().transform.to_local_vec(&other_point));
        return match self.view().is_normal_flipped {
            true => -sd,
            false => sd
        };
    }

    /// Returns signed distance from the given point \p otherPoint in local space.
    fn signed_distance_local(&self, other_point: &Vector3D) -> f64;

    fn closest_distance_local(&self, other_point: &Vector3D) -> f64 {
        return f64::abs(self.signed_distance_local(other_point));
    }

    fn is_inside_local(&self, other_point: &Vector3D) -> bool {
        return crate::level_set_utils::is_inside_sdf(self.signed_distance_local(other_point));
    }
}

/// Shared pointer type for the ImplicitSurface3.
pub type ImplicitSurface3Ptr = Arc<RwLock<dyn ImplicitSurface3>>;