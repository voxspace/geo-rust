/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::bounding_box2::BoundingBox2D;
use crate::ray2::Ray2D;
use crate::nearest_neighbor_query_engine2::*;
use crate::intersection_query_engine2::*;
use crate::vector2::Vector2D;

/// # list-based 2-D intersection/nearest-neighbor query engine.
pub struct ListQueryEngine2<T: Clone> {
    _items: Vec<T>,
}

impl<T: Clone> ListQueryEngine2<T> {
    pub fn new() -> ListQueryEngine2<T> {
        return ListQueryEngine2 {
            _items: vec![]
        };
    }

    /// Adds an item to the container.
    pub fn add(&mut self, item: T) {
        self._items.push(item);
    }

    /// Adds items to the container.
    pub fn add_vec(&mut self, items: Vec<T>) {
        self._items.reserve(items.len());
        for val in items {
            self._items.push(val);
        }
    }
}

impl<T: Clone> IntersectionQueryEngine2<T> for ListQueryEngine2<T> {
    fn intersects_aabb<Callback>(&self, aabb: &BoundingBox2D, test_func: &mut Callback) -> bool
        where Callback: BoxIntersectionTestFunc2<T> {
        for item in &self._items {
            if test_func(item, aabb) {
                return true;
            }
        }

        return false;
    }

    fn intersects_ray<Callback>(&self, ray: &Ray2D, test_func: &mut Callback) -> bool
        where Callback: RayIntersectionTestFunc2<T> {
        for item in &self._items {
            if test_func(item, ray) {
                return true;
            }
        }

        return false;
    }

    fn for_each_intersecting_item_aabb<Callback, Visitor>(&self, aabb: &BoundingBox2D,
                                                          test_func: &mut Callback, visitor_func: &mut Visitor)
        where Callback: BoxIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T> {
        for item in &self._items {
            if test_func(item, aabb) {
                visitor_func(item);
            }
        }
    }

    fn for_each_intersecting_item_ray<Callback, Visitor>(&self, ray: &Ray2D,
                                                         test_func: &mut Callback, visitor_func: &mut Visitor)
        where Callback: RayIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T> {
        for item in &self._items {
            if test_func(item, ray) {
                visitor_func(item);
            }
        }
    }

    fn closest_intersection<Callback>(&self, ray: &Ray2D, test_func: &mut Callback) -> ClosestIntersectionQueryResult2<T>
        where Callback: GetRayIntersectionFunc2<T> {
        let mut best: ClosestIntersectionQueryResult2<T> = ClosestIntersectionQueryResult2::new();
        for item in &self._items {
            let dist = test_func(item, ray);
            if dist < best.distance {
                best.distance = dist;
                best.item = Some(item.clone());
            }
        }

        return best;
    }
}

impl<T: Clone> NearestNeighborQueryEngine2<T> for ListQueryEngine2<T> {
    fn nearest<Callback>(&self, pt: &Vector2D, distance_func: &mut Callback) -> NearestNeighborQueryResult2<T>
        where Callback: NearestNeighborDistanceFunc2<T> {
        let mut best = NearestNeighborQueryResult2::new();
        for item in &self._items {
            let dist = distance_func(item, pt);
            if dist < best.distance {
                best.item = Some(item.clone());
                best.distance = dist;
            }
        }

        return best;
    }
}