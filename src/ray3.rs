/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::vector3::Vector3;

///
/// #      Class for 3-D ray.
///
/// - tparam     T     The value type.
///
pub struct Ray3<T: Float> {
    /// The origin of the ray.
    pub origin: Vector3<T>,

    /// The direction of the ray.
    pub direction: Vector3<T>,
}

/// Float-type 3-D ray.
pub type Ray3F = Ray3<f32>;

/// Double-type 2-D ray.
pub type Ray3D = Ray3<f64>;

impl<T: Float> Ray3<T> {
    /// Constructs an empty ray that points (1, 0) from (0, 0).
    pub fn default() -> Ray3<T> {
        return Ray3 {
            origin: Vector3::default(),
            direction: Vector3::new(T::one(), T::zero(), T::zero()),
        };
    }

    /// Constructs a ray with given origin and direction.
    pub fn new(new_origin: Vector3<T>, new_direction: Vector3<T>) -> Ray3<T> {
        return Ray3 {
            origin: new_origin,
            direction: new_direction.normalized(),
        };
    }

    /// Returns a point on the ray at distance **t**.
    pub fn point_at(&self, t: T) -> Vector3<T> {
        return self.origin + self.direction * t;
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod ray3 {
    #[test]
    fn constructors() {
        todo!()
    }

    #[test]
    fn point_at() {
        todo!()
    }
}