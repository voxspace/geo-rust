/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::point_generator3::PointGenerator3;
use crate::vector3::Vector3D;
use crate::bounding_box3::BoundingBox3D;
use std::sync::{RwLock, Arc};

///
/// # Face-centered lattice points generator.
///
/// \see http://en.wikipedia.org/wiki/Cubic_crystal_system
///      http://mathworld.wolfram.com/CubicClosePacking.html
///
pub struct FccLatticePointGenerator {}

impl PointGenerator3 for FccLatticePointGenerator {
    ///
    /// \brief Invokes \p callback function for each FCC-lattice points inside
    /// \p bounding_box.
    ///
    /// This function iterates every FCC-lattice points inside \p bounding_box
    /// where \p spacing is the size of the unit cell of FCC structure.
    ///
    fn for_each_point<Callback>(&self, bounding_box: &BoundingBox3D, spacing: f64, callback: &mut Callback)
        where Callback: FnMut(&Vector3D) -> bool {
        let half_spacing = spacing / 2.0;
        let box_width = bounding_box.width();
        let box_height = bounding_box.height();
        let box_depth = bounding_box.depth();

        let mut position = Vector3D::default();
        let mut should_quit = false;
        let mut k = 0;
        while k as f64 * half_spacing <= box_depth && !should_quit {
            position.z = k as f64 * half_spacing + bounding_box.lower_corner.z;

            let mut has_offset = k % 2 == 1;

            let mut j = 0;
            while j as f64 * half_spacing <= box_height && !should_quit {
                position.y = j as f64 * half_spacing + bounding_box.lower_corner.y;

                let offset = match has_offset {
                    true => half_spacing,
                    false => 0.0
                };

                let mut i = 0;
                while i as f64 * spacing + offset <= box_width {
                    position.x = i as f64 * spacing + offset + bounding_box.lower_corner.x;
                    if !callback(&position) {
                        should_quit = true;
                        break;
                    }
                    i += 1;
                }

                has_offset = !has_offset;
                j += 1;
            }
            k += 1;
        }
    }
}

/// Shared pointer type for the FccLatticePointGenerator.
pub type FccLatticePointGeneratorPtr = Arc<RwLock<FccLatticePointGenerator>>;