/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

pub trait UnaryOp {
    fn new() -> Self;

    fn eval(&self, x: f64) -> f64;
}

pub trait BinaryOp {
    fn new() -> Self;

    fn eval(&self, x: f64, y: f64) -> f64;
}

//--------------------------------------------------------------------------------------------------
pub struct Plus {}

impl BinaryOp for Plus {
    fn new() -> Self {
        return Plus {};
    }

    fn eval(&self, x: f64, y: f64) -> f64 {
        return x + y;
    }
}

//--------------------------------------------------------------------------------------------------
pub struct Minus {}

impl BinaryOp for Minus {
    fn new() -> Self {
        return Minus {};
    }

    fn eval(&self, x: f64, y: f64) -> f64 {
        return x - y;
    }
}

//--------------------------------------------------------------------------------------------------
pub struct RMinus {}

impl BinaryOp for RMinus {
    fn new() -> Self {
        return RMinus {};
    }

    fn eval(&self, x: f64, y: f64) -> f64 {
        return y - x;
    }
}

//--------------------------------------------------------------------------------------------------
pub struct Multiplies {}

impl BinaryOp for Multiplies {
    fn new() -> Self {
        return Multiplies {};
    }

    fn eval(&self, x: f64, y: f64) -> f64 {
        return x * y;
    }
}

//--------------------------------------------------------------------------------------------------
pub struct Divides {}

impl BinaryOp for Divides {
    fn new() -> Self {
        return Divides {};
    }

    fn eval(&self, x: f64, y: f64) -> f64 {
        return x / y;
    }
}

//--------------------------------------------------------------------------------------------------
pub struct RDivides {}

impl BinaryOp for RDivides {
    fn new() -> Self {
        return RDivides {};
    }

    fn eval(&self, x: f64, y: f64) -> f64 {
        return y / x;
    }
}