/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::kdtree2::KdTree2;
use crate::point_neighbor_searcher2::*;
use crate::vector2::Vector2D;
use std::sync::{RwLock, Arc};

///
/// # KdTree-based 2-D point searcher.
///
/// This class implements 2-D point searcher by using KdTree for its internal
/// acceleration data structure.
///
pub struct PointKdTreeSearcher2 {
    _tree: KdTree2,
}

impl PointKdTreeSearcher2 {
    /// Constructs an empty kD-tree instance.
    pub fn new() -> PointKdTreeSearcher2 {
        return PointKdTreeSearcher2 {
            _tree: KdTree2::new()
        };
    }

    /// Returns builder fox PointSimpleListSearcher2.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    ///
    /// \brief      Creates a new instance of the object with same properties
    ///             than original.
    ///
    /// \return     Copy of this object.
    ///
    pub fn clone(&self) -> PointKdTreeSearcher2Ptr {
        let mut searcher = PointKdTreeSearcher2::new();
        searcher.set(self);
        return PointKdTreeSearcher2Ptr::new(RwLock::new(searcher));
    }

    /// Copy from the other instance.
    pub fn set(&mut self, other: &PointKdTreeSearcher2) {
        self._tree = other._tree.clone();
    }
}

impl PointNeighborSearcher2 for PointKdTreeSearcher2 {
    fn type_name() -> String {
        return "PointKdTreeSearcher2".parse().unwrap();
    }

    fn build(&mut self, points: &Vec<Vector2D>) {
        self._tree.build(points);
    }

    fn for_each_nearby_point<Callback>(&self, origin: &Vector2D, radius: f64, callback: &mut Callback)
        where Callback: ForEachNearbyPointFunc {
        self._tree.for_each_nearby_point(origin, radius, callback);
    }

    fn has_nearby_point(&self, origin: &Vector2D, radius: f64) -> bool {
        return self._tree.has_nearby_point(origin, radius);
    }
}

/// Shared pointer for the PointKdTreeSearcher2 type.
pub type PointKdTreeSearcher2Ptr = Arc<RwLock<PointKdTreeSearcher2>>;

///
/// # Front-end to create PointKdTreeSearcher2 objects step by step.
///
pub struct Builder {}

impl Builder {
    /// Builds PointKdTreeSearcher2 instance.
    pub fn build(&self) -> PointKdTreeSearcher2 {
        return PointKdTreeSearcher2::new();
    }

    /// Builds shared pointer of PointKdTreeSearcher2 instance.
    pub fn make_shared(&self) -> PointKdTreeSearcher2Ptr {
        return PointKdTreeSearcher2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {};
    }
}