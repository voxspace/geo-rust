/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::surface2::*;
use crate::bvh2::Bvh2;
use crate::transform2::Transform2;
use crate::vector2::Vector2D;
use crate::bounding_box2::BoundingBox2D;
use crate::ray2::Ray2D;
use crate::nearest_neighbor_query_engine2::NearestNeighborQueryEngine2;
use crate::intersection_query_engine2::IntersectionQueryEngine2;
use std::sync::{RwLock, Arc};

///
/// # 2-D surface set.
///
/// This class represents 2-D surface set which extends Surface2 by overriding
/// surface-related queries. This is class can hold a collection of other
/// surface instances.
///
pub struct SurfaceSet2 {
    _surfaces: Vec<Surface2Ptr>,
    _unbounded_surfaces: Vec<Surface2Ptr>,
    _bvh: RwLock<Bvh2<Surface2Ptr>>,
    _bvh_invalidated: RwLock<bool>,

    /// data from surface2
    pub surface_data: Surface2Data,
}

impl SurfaceSet2 {
    /// Constructs an empty surface set.
    /// ```
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// let sset1 = SurfaceSet2::default();
    /// assert_eq!(0, sset1.number_of_surfaces());
    /// ```
    pub fn default() -> SurfaceSet2 {
        return SurfaceSet2 {
            _surfaces: vec![],
            _unbounded_surfaces: vec![],
            _bvh: RwLock::new(Bvh2::new()),
            _bvh_invalidated: RwLock::new(true),
            surface_data: Surface2Data::new(None, None),
        };
    }

    /// Constructs with a list of other surfaces.
    /// ```
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::transform2::Transform2;;
    /// let sph1 = Sphere2::builder().with_radius(1.0).with_center(Vector2D::default()).make_shared();
    /// let sph2 = Sphere2::builder().with_radius(0.5).with_center(Vector2D::new(0.0, 3.0)).make_shared();
    /// let sph3 = Sphere2::builder().with_radius(0.25).with_center(Vector2D::new(-2.0, 0.0)).make_shared();
    /// let sset2 = SurfaceSet2::new(vec![sph1, sph2, sph3], Some(Transform2::default()), Some(false));
    ///
    /// assert_eq!(3, sset2.number_of_surfaces());
    /// assert_eq!(Vector2D::default(), sset2.transform.translation());
    /// assert_eq!(0.0, sset2.transform.orientation());
    /// ```
    pub fn new(others: Vec<Surface2Ptr>,
               transform: Option<Transform2>,
               is_normal_flipped: Option<bool>) -> SurfaceSet2 {
        let mut unbounded_surfaces: Vec<Surface2Ptr> = vec![];
        for surface in &others {
            if !surface.read().unwrap().is_bounded() {
                unbounded_surfaces.push(surface.clone());
            }
        }

        return SurfaceSet2 {
            _surfaces: others.clone(),
            _unbounded_surfaces: unbounded_surfaces,
            _bvh: RwLock::new(Bvh2::new()),
            _bvh_invalidated: RwLock::new(true),
            surface_data: Surface2Data::new(transform, is_normal_flipped),
        };
    }

    /// Returns builder fox SurfaceSet2.
    pub fn builder() -> Builder {
        return Builder::new();
    }

    /// Returns the number of surfaces.
    pub fn number_of_surfaces(&self) -> usize {
        return self._surfaces.len();
    }

    /// Returns the i-th surface.
    pub fn surface_at(&self, i: usize) -> Surface2Ptr {
        return self._surfaces[i].clone();
    }

    /// Adds a surface instance.
    /// ```
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// let mut sset1 = SurfaceSet2::default();
    /// assert_eq!(0, sset1.number_of_surfaces());
    ///
    /// let sph1 = Sphere2::builder().with_radius(1.0).with_center(Vector2D::default()).make_shared();
    /// let sph2 = Sphere2::builder().with_radius(0.5).with_center(Vector2D::new(0.0, 3.0)).make_shared();
    /// let sph3 = Sphere2::builder().with_radius(0.25).with_center(Vector2D::new(-2.0, 0.0)).make_shared();
    ///
    /// sset1.add_surface(sph1);
    /// sset1.add_surface(sph2);
    /// sset1.add_surface(sph3);
    ///
    /// assert_eq!(3, sset1.number_of_surfaces());
    /// ```
    pub fn add_surface(&mut self, surface: Surface2Ptr) {
        self._surfaces.push(surface.clone());
        if !surface.read().unwrap().is_bounded() {
            self._unbounded_surfaces.push(surface.clone());
        }
        self.invalidate_bvh();
    }

    /// must call it manually in Rust!
    pub fn build_bvh(&self) {
        if *self._bvh_invalidated.read().unwrap() {
            let mut surfs: Vec<Surface2Ptr> = Vec::new();
            let mut bounds: Vec<BoundingBox2D> = Vec::new();
            for i in 0..self._surfaces.len() {
                if self._surfaces[i].read().unwrap().is_bounded() {
                    surfs.push(self._surfaces[i].clone());
                    bounds.push(self._surfaces[i].read().unwrap().bounding_box());
                }
            }
            self._bvh.write().unwrap().build(&surfs, &bounds);
            *(self._bvh_invalidated.write().unwrap()) = false;
        }
    }

    fn invalidate_bvh(&self) {
        *(self._bvh_invalidated.write().unwrap()) = true;
    }
}

impl Surface2 for SurfaceSet2 {
    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::Vector2D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    /// // Test empty set
    /// let empty_point = sset1.closest_point(&Vector2D::new(1.0, 2.0));
    /// assert_eq!(f64::MAX, empty_point.x);
    /// assert_eq!(f64::MAX, empty_point.y);
    ///
    /// let num_samples = get_number_of_sample_points2();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    ///
    /// let brute_force_search = |pt:&Vector2D| {
    ///     let mut min_dist2 = f64::MAX;
    ///     let mut result = Vector2D::default();
    ///     for i in 0..num_samples/2 {
    ///         let local_result = sset1.surface_at(i).read().unwrap().closest_point(pt);
    ///         let local_dist2 = pt.distance_squared_to(local_result);
    ///         if local_dist2 < min_dist2 {
    ///             min_dist2 = local_dist2;
    ///             result = local_result;
    ///         }
    ///     }
    ///     return result;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset1.closest_point(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     let expected = brute_force_search(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset2 = SurfaceSet2::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::default())
    ///                        .with_translation(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset2.add_surface(sph);
    /// }
    ///
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset2.closest_point(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     let expected = brute_force_search(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn closest_point_local(&self, other_point: &Vector2D) -> Vector2D {
        self.build_bvh();

        let mut distance_func = |surface: &Surface2Ptr, pt: &Vector2D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let mut result = Vector2D::new(f64::MAX, f64::MAX);
        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);
        match query_result.item {
            None => {}
            Some(ptr) => {
                result = ptr.read().unwrap().closest_point(other_point);
            }
        }

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
                result = surface.read().unwrap().closest_point(other_point);
            }
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::bounding_box2::BoundingBox2D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    ///
    /// let num_samples = get_number_of_sample_points2();
    ///
    ///     // Use first half of the samples as the centers of the spheres
    /// let mut answer = BoundingBox2D::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph.clone());
    ///
    ///     answer.merge_box(&sph.read().unwrap().bounding_box());
    /// }
    /// assert_eq!(answer.lower_corner.is_similar(&sset1.bounding_box().lower_corner, Some(1e-9)), true);
    /// assert_eq!(answer.upper_corner.is_similar(&sset1.bounding_box().upper_corner, Some(1e-9)), true);
    ///
    /// // Now with translation instead of center
    /// let mut sset2 = SurfaceSet2::default();
    /// let mut debug = BoundingBox2D::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::default())
    ///                        .with_translation(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset2.add_surface(sph.clone());
    ///
    ///     debug.merge_box(&sph.read().unwrap().bounding_box());
    /// }
    /// assert_eq!(answer.lower_corner.is_similar(&debug.lower_corner, Some(1e-9)), true);
    /// assert_eq!(answer.upper_corner.is_similar(&debug.upper_corner, Some(1e-9)), true);
    ///
    /// assert_eq!(answer.lower_corner.is_similar(&sset2.bounding_box().lower_corner, Some(1e-9)), true);
    /// assert_eq!(answer.upper_corner.is_similar(&sset2.bounding_box().upper_corner, Some(1e-9)), true);
    /// ```
    fn bounding_box_local(&self) -> BoundingBox2D {
        self.build_bvh();

        return self._bvh.read().unwrap().bounding_box();
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::ray2::Ray2D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    ///
    /// let num_samples = get_number_of_sample_points2();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    /// let brute_force_test = |ray:&Ray2D| {
    ///     let mut result = SurfaceRayIntersection2::new();
    ///     for i in 0..num_samples/2 {
    ///         let local_result = sset1.surface_at(i).read().unwrap().closest_intersection(ray);
    ///         if local_result.distance < result.distance {
    ///             result = local_result;
    ///         }
    ///     }
    ///     return result;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray2D::new(Vector2D::new_slice(get_sample_points2()[i]), Vector2D::new_slice(get_sample_dirs2()[i]));
    ///     let actual = sset1.closest_intersection(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected.distance, actual.distance);
    ///     assert_eq!(expected.point, actual.point);
    ///     assert_eq!(expected.normal, actual.normal);
    ///     assert_eq!(expected.is_intersecting, actual.is_intersecting);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset2 = SurfaceSet2::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::default())
    ///                        .with_translation(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///         sset2.add_surface(sph);
    /// }
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray2D::new(Vector2D::new_slice(get_sample_points2()[i]), Vector2D::new_slice(get_sample_dirs2()[i]));
    ///     let actual = sset2.closest_intersection(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected.distance, actual.distance);
    ///     assert_eq!(expected.point, actual.point);
    ///     assert_eq!(expected.normal, actual.normal);
    ///     assert_eq!(expected.is_intersecting, actual.is_intersecting);
    /// }
    /// ```
    fn closest_intersection_local(&self, ray: &Ray2D) -> SurfaceRayIntersection2 {
        self.build_bvh();

        let mut test_func = |surface: &Surface2Ptr, ray: &Ray2D| {
            let result = surface.read().unwrap().closest_intersection(ray);
            return result.distance;
        };

        let query_result = self._bvh.read().unwrap().closest_intersection(ray, &mut test_func);
        let mut result = SurfaceRayIntersection2::new();
        result.distance = query_result.distance;
        match query_result.item {
            None => {
                result.is_intersecting = false;
            }
            Some(ptr) => {
                result.is_intersecting = true;
                result.point = ray.point_at(query_result.distance);
                result.normal = ptr.read().unwrap().closest_normal(&result.point);
            }
        }

        for surface in &self._unbounded_surfaces {
            let local_result = surface.read().unwrap().closest_intersection(ray);
            if local_result.distance < result.distance {
                result = local_result;
            }
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    ///
    /// let num_samples = get_number_of_sample_points2();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    ///
    /// let brute_force_search = |pt:&Vector2D| {
    ///     let mut min_dist2 = f64::MAX;
    ///     let mut result = Vector2D::default();
    ///     for i in 0..num_samples/2 {
    ///         let local_result = sset1.surface_at(i).read().unwrap().closest_normal(pt);
    ///         let closest_pt = sset1.surface_at(i).read().unwrap().closest_point(pt);
    ///         let local_dist2 = pt.distance_squared_to(closest_pt);
    ///         if local_dist2 < min_dist2 {
    ///             min_dist2 = local_dist2;
    ///             result = local_result;
    ///         }
    ///     }
    ///     return result;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset1.closest_normal(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     let expected = brute_force_search(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset2 = SurfaceSet2::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::default())
    ///                        .with_translation(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset2.add_surface(sph);
    /// }
    ///
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset2.closest_normal(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     let expected = brute_force_search(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn closest_normal_local(&self, other_point: &Vector2D) -> Vector2D {
        self.build_bvh();

        let mut distance_func = |surface: &Surface2Ptr, pt: &Vector2D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let mut result = Vector2D::new(1.0, 0.0);
        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);
        match query_result.item {
            None => {}
            Some(ptr) => {
                result = ptr.read().unwrap().closest_normal(other_point);
            }
        }

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
                result = surface.read().unwrap().closest_normal(other_point);
            }
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::ray2::Ray2D;
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    ///
    /// let num_samples = get_number_of_sample_points2();
    ///
    ///  // Use first half of the samples as the centers of the spheres
    ///  for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    /// let brute_force_test = |ray:&Ray2D| {
    ///     for i in 0..num_samples/2 {
    ///         if sset1.surface_at(i).read().unwrap().intersects(ray) {
    ///             return true;
    ///         }
    ///     }
    ///     return false;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray2D::new(Vector2D::new_slice(get_sample_points2()[i]), Vector2D::new_slice(get_sample_dirs2()[i]));
    ///     let actual = sset1.intersects(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset2 = SurfaceSet2::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::default())
    ///                        .with_translation(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset2.add_surface(sph);
    /// }
    /// for i in num_samples/2..num_samples {
    ///     let ray = Ray2D::new(Vector2D::new_slice(get_sample_points2()[i]), Vector2D::new_slice(get_sample_dirs2()[i]));
    ///     let actual = sset2.intersects(&ray);
    ///     let expected = brute_force_test(&ray);
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn intersects_local(&self, ray_local: &Ray2D) -> bool {
        self.build_bvh();

        let mut test_func = |surface: &Surface2Ptr, ray: &Ray2D| {
            return surface.read().unwrap().intersects(ray);
        };

        let mut result = self._bvh.read().unwrap().intersects_ray(ray_local, &mut test_func);
        for surface in &self._unbounded_surfaces {
            result |= surface.read().unwrap().intersects(ray_local);
        }

        return result;
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    ///
    /// let num_samples = get_number_of_sample_points2();
    ///
    /// // Use first half of the samples as the centers of the spheres
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///     sset1.add_surface(sph);
    /// }
    /// let brute_force_search = |pt:&Vector2D| {
    ///     let mut min_dist = f64::MAX;
    ///     for i in 0..num_samples/2 {
    ///         let local_dist = sset1.surface_at(i).read().unwrap().closest_distance(pt);
    ///         if local_dist < min_dist {
    ///             min_dist = local_dist;
    ///         }
    ///     }
    ///     return min_dist;
    /// };
    ///
    /// // Use second half of the samples as the query points
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset1.closest_distance(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     let expected = brute_force_search(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    ///
    /// // Now with translation instead of center
    /// let mut sset2 = SurfaceSet2::default();
    /// for i in 0..num_samples/2 {
    ///     let sph = Sphere2::builder()
    ///                        .with_radius(0.01)
    ///                        .with_center(Vector2D::default())
    ///                        .with_translation(Vector2D::new_slice(get_sample_points2()[i]))
    ///                        .make_shared();
    ///         sset2.add_surface(sph);
    /// }
    /// for i in num_samples/2..num_samples {
    ///     let actual = sset2.closest_distance(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     let expected = brute_force_search(&Vector2D::new_slice(get_sample_points2()[i]));
    ///     assert_eq!(expected, actual);
    /// }
    /// ```
    fn closest_distance_local(&self, other_point: &Vector2D) -> f64 {
        self.build_bvh();

        let mut distance_func = |surface: &Surface2Ptr, pt: &Vector2D| {
            return surface.read().unwrap().closest_distance(pt);
        };

        let query_result = self._bvh.read().unwrap().nearest(other_point, &mut distance_func);

        let mut min_dist = query_result.distance;
        for surface in &self._unbounded_surfaces {
            let pt = surface.read().unwrap().closest_point(other_point);
            let dist = pt.distance_to(*other_point);
            if dist < min_dist {
                min_dist = dist;
            }
        }

        return min_dist;
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::plane2::*;
    /// use vox_geometry_rust::bounding_box2::*;
    /// use vox_geometry_rust::transform2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    ///
    /// let domain = BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 2.0));
    /// let offset = Vector2D::new(1.0, 2.0);
    ///
    /// let plane = Plane2::builder()
    ///                      .with_normal(Vector2D::new(0.0, 1.0))
    ///                      .with_point(Vector2D::new(0.0, 0.25 * domain.height()))
    ///                      .make_shared();
    ///
    /// let sphere = Sphere2::builder()
    ///                       .with_center(domain.mid_point())
    ///                       .with_radius(0.15 * domain.width())
    ///                       .make_shared();
    ///
    /// let surface_set = SurfaceSet2::builder()
    ///                           .with_surfaces(vec![plane, sphere])
    ///                           .with_transform(Transform2::new(offset, 0.0))
    ///                           .make_shared();
    ///
    /// assert_eq!(surface_set.read().unwrap().is_inside(&(Vector2D::new(0.5, 0.25) + offset)), true);
    /// assert_eq!(surface_set.read().unwrap().is_inside(&(Vector2D::new(0.5, 1.0) + offset)), true);
    /// assert_eq!(surface_set.read().unwrap().is_inside(&(Vector2D::new(0.5, 1.5) + offset)), false);
    /// ```
    fn is_inside_local(&self, other_point_local: &Vector2D) -> bool {
        for surface in &self._surfaces {
            if surface.read().unwrap().is_inside(other_point_local) {
                return true;
            }
        }

        return false;
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::plane2::*;
    /// use vox_geometry_rust::bounding_box2::*;
    /// use vox_geometry_rust::transform2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    ///
    /// let sphere =
    ///         Sphere2::builder().with_center(Vector2D::new(-1.0, 1.0)).with_radius(0.5).make_shared();
    ///
    /// let surface_set = SurfaceSet2::builder()
    ///                           .with_surfaces(vec![sphere.clone()])
    ///                           .with_transform(Transform2::new(Vector2D::new(1.0, 2.0), 0.0))
    ///                           .make_shared();
    ///
    /// let bbox1 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(-0.5, 2.5), Vector2D::new(0.5, 3.5)).upper_corner, bbox1.upper_corner);
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(-0.5, 2.5), Vector2D::new(0.5, 3.5)).lower_corner, bbox1.lower_corner);
    ///
    /// surface_set.write().unwrap().transform = Transform2::new(Vector2D::new(3.0, -4.0), 0.0);
    /// surface_set.write().unwrap().update_query_engine();
    /// let bbox2 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(1.5, -3.5), Vector2D::new(2.5, -2.5)).upper_corner, bbox2.upper_corner);
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(1.5, -3.5), Vector2D::new(2.5, -2.5)).lower_corner, bbox2.lower_corner);
    ///
    /// sphere.write().unwrap().transform = Transform2::new(Vector2D::new(-6.0, 9.0), 0.0);
    /// surface_set.write().unwrap().update_query_engine();
    /// let bbox3 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(-4.5, 5.5), Vector2D::new(-3.5, 6.5)).upper_corner, bbox3.upper_corner);
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(-4.5, 5.5), Vector2D::new(-3.5, 6.5)).lower_corner, bbox3.lower_corner);
    ///
    /// // Plane is unbounded. Total bbox should ignore it.
    /// let plane = Plane2::builder().with_normal(Vector2D::new(1.0, 0.0)).make_shared();
    /// surface_set.write().unwrap().add_surface(plane);
    /// surface_set.write().unwrap().update_query_engine();
    /// let bbox4 = surface_set.read().unwrap().bounding_box();
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(-4.5, 5.5), Vector2D::new(-3.5, 6.5)).upper_corner, bbox4.upper_corner);
    /// assert_eq!(BoundingBox2D::new(Vector2D::new(-4.5, 5.5), Vector2D::new(-3.5, 6.5)).lower_corner, bbox4.lower_corner);
    /// ```
    fn update_query_engine(&self) {
        self.invalidate_bvh();
        self.build_bvh();
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::plane2::*;
    /// use vox_geometry_rust::bounding_box2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let mut sset1 = SurfaceSet2::default();
    ///
    /// let domain = BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 2.0));
    ///
    /// let plane = Plane2::builder()
    ///                      .with_normal(Vector2D::new(0.0, 1.0))
    ///                      .with_point(Vector2D::new(0.0, 0.25 * domain.height()))
    ///                      .make_shared();
    ///
    /// let sphere = Sphere2::builder()
    ///                       .with_center(domain.mid_point())
    ///                       .with_radius(0.15 * domain.width())
    ///                       .make_shared();
    ///
    /// let surface_set = SurfaceSet2::builder().with_surfaces(vec![plane, sphere]).make_shared();
    /// assert_eq!(surface_set.read().unwrap().is_bounded(), false);
    ///
    /// let cp = surface_set.read().unwrap().closest_point(&Vector2D::new(0.5, 0.4));
    /// let answer = Vector2D::new(0.5, 0.5);
    ///
    /// assert_eq!(answer.is_similar(&cp, Some(1.0e-9)), true);
    /// ```
    fn is_bounded(&self) -> bool {
        // All surfaces should be bounded.
        for surface in &self._surfaces {
            if !surface.read().unwrap().is_bounded() {
                return false;
            }
        }

        // Empty set is not bounded.
        return !self._surfaces.is_empty();
    }

    /// ```
    /// use vox_geometry_rust::surface2::*;
    /// use vox_geometry_rust::surface_set2::SurfaceSet2;
    /// use vox_geometry_rust::sphere2::*;
    /// use vox_geometry_rust::plane2::*;
    /// use vox_geometry_rust::bounding_box2::*;
    /// use vox_geometry_rust::vector2::{Vector2D, Vector2};
    /// use vox_geometry_rust::unit_tests_utils::*;
    /// let surfaceSet = SurfaceSet2::builder().make_shared();
    ///
    /// assert_eq!(surfaceSet.read().unwrap().is_valid_geometry(), false);
    ///
    /// let domain = BoundingBox2D::new(Vector2D::default(), Vector2D::new(1.0, 2.0));
    ///
    /// let plane = Plane2::builder()
    ///                      .with_normal(Vector2D::new(0.0, 1.0))
    ///                      .with_point(Vector2D::new(0.0, 0.25 * domain.height()))
    ///                      .make_shared();
    ///
    /// let sphere = Sphere2::builder()
    ///                       .with_center(domain.mid_point())
    ///                       .with_radius(0.15 * domain.width())
    ///                       .make_shared();
    ///
    /// let surfaceSet2 =
    ///         SurfaceSet2::builder().with_surfaces(vec![plane, sphere]).make_shared();
    ///
    /// assert_eq!(surfaceSet2.read().unwrap().is_valid_geometry(), true);
    ///
    /// surfaceSet2.write().unwrap().add_surface(surfaceSet);
    ///
    /// assert_eq!(surfaceSet2.read().unwrap().is_valid_geometry(), false);
    /// ```
    fn is_valid_geometry(&self) -> bool {
        // All surfaces should be valid.
        for surface in &self._surfaces {
            if !surface.read().unwrap().is_valid_geometry() {
                return false;
            }
        }

        // Empty set is not valid.
        return !self._surfaces.is_empty();
    }

    fn view(&self) -> &Surface2Data {
        return &self.surface_data;
    }
}

/// Shared pointer for the SurfaceSet2 type.
pub type SurfaceSet2Ptr = Arc<RwLock<SurfaceSet2>>;

///
/// # Front-end to create SurfaceSet2 objects step by step.
///
pub struct Builder {
    _surfaces: Vec<Surface2Ptr>,

    _surface_data: Surface2Data,
}

impl Builder {
    /// Returns builder with other surfaces.
    pub fn with_surfaces(&mut self, others: Vec<Surface2Ptr>) -> &mut Self {
        self._surfaces = others;
        return self;
    }

    /// Builds SurfaceSet2.
    pub fn build(&mut self) -> SurfaceSet2 {
        return SurfaceSet2::new(self._surfaces.clone(),
                                Some(self._surface_data.transform.clone()),
                                Some(self._surface_data.is_normal_flipped));
    }

    /// Builds shared pointer of SurfaceSet2 instance.
    pub fn make_shared(&mut self) -> SurfaceSet2Ptr {
        return SurfaceSet2Ptr::new(RwLock::new(self.build()));
    }

    /// constructor
    pub fn new() -> Builder {
        return Builder {
            _surfaces: vec![],
            _surface_data: Surface2Data::new(None, None),
        };
    }
}

impl SurfaceBuilderBase2 for Builder {
    fn view(&mut self) -> &mut Surface2Data {
        return &mut self._surface_data;
    }
}