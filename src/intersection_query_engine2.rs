/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::vector2::Vector2D;
use crate::ray2::Ray2D;
use crate::bounding_box2::BoundingBox2D;

/// Closest intersection query result.
pub struct ClosestIntersectionQueryResult2<T> {
    pub item: Option<T>,
    pub distance: f64,
}

impl<T> ClosestIntersectionQueryResult2<T> {
    pub fn new() -> ClosestIntersectionQueryResult2<T> {
        return ClosestIntersectionQueryResult2 {
            item: None,
            distance: f64::MAX,
        };
    }
}

///Closest intersection distance measure function.
pub trait ClosestIntersectionDistanceFunc2<T>: FnMut(&T, &Vector2D) -> f64 {}

impl<T, Super: FnMut(&T, &Vector2D) -> f64> ClosestIntersectionDistanceFunc2<T> for Super {}

/// Box-item intersection test function.
pub trait BoxIntersectionTestFunc2<T>: FnMut(&T, &BoundingBox2D) -> bool {}

impl<T, Super: FnMut(&T, &BoundingBox2D) -> bool> BoxIntersectionTestFunc2<T> for Super {}

/// Ray-item intersection test function.
pub trait RayIntersectionTestFunc2<T>: FnMut(&T, &Ray2D) -> bool {}

impl<T, Super: FnMut(&T, &Ray2D) -> bool> RayIntersectionTestFunc2<T> for Super {}

/// Ray-item closest intersection evaluation function.
pub trait GetRayIntersectionFunc2<T>: FnMut(&T, &Ray2D) -> f64 {}

impl<T, Super: FnMut(&T, &Ray2D) -> f64> GetRayIntersectionFunc2<T> for Super {}

/// Visitor function which is invoked for each intersecting item.
pub trait IntersectionVisitorFunc2<T>: FnMut(&T) {}

impl<T, Super: FnMut(&T)> IntersectionVisitorFunc2<T> for Super {}

/// Abstract base class for 2-D intersection test query engine.
pub trait IntersectionQueryEngine2<T> {
    /// Returns true if given \p box intersects with any of the stored items.
    fn intersects_aabb<Callback>(&self, aabb: &BoundingBox2D,
                                 test_func: &mut Callback) -> bool
        where Callback: BoxIntersectionTestFunc2<T>;

    /// Returns true if given \p ray intersects with any of the stored items.
    fn intersects_ray<Callback>(&self, ray: &Ray2D,
                                test_func: &mut Callback) -> bool
        where Callback: RayIntersectionTestFunc2<T>;

    /// Invokes \p visitor_func for every intersecting items.
    fn for_each_intersecting_item_aabb<Callback, Visitor>(&self, aabb: &BoundingBox2D,
                                                          test_func: &mut Callback,
                                                          visitor_func: &mut Visitor)
        where Callback: BoxIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T>;

    /// Invokes \p visitor_func for every intersecting items.
    fn for_each_intersecting_item_ray<Callback, Visitor>(&self, ray: &Ray2D,
                                                         test_func: &mut Callback,
                                                         visitor_func: &mut Visitor)
        where Callback: RayIntersectionTestFunc2<T>,
              Visitor: IntersectionVisitorFunc2<T>;

    /// Returns the closest intersection for given \p ray.
    fn closest_intersection<Callback>(&self, ray: &Ray2D,
                                      test_func: &mut Callback) -> ClosestIntersectionQueryResult2<T>
        where Callback: GetRayIntersectionFunc2<T>;
}