/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::common_trait::ZeroInit;
use std::ops::{Index, IndexMut};
use std::mem::swap;

///
/// \brief 1-D array accessor class.
///
/// This class represents 1-D array accessor. Array accessor provides array-like
/// data read/write functions, but does not handle memory management. Thus, it
/// is more like a random access iterator, but with multi-dimension support.
///
/// \see Array1<T, 2>
///
/// \tparam T - Array value type.
///
pub struct ArrayAccessor1<'a, T: ZeroInit> {
    _data: Option<&'a mut [T]>,
}

impl<'a, T: ZeroInit> Default for ArrayAccessor1<'a, T> {
    /// Constructs empty 1-D array accessor.
    fn default() -> Self {
        return ArrayAccessor1 {
            _data: None
        };
    }
}

impl<'a, T: ZeroInit> ArrayAccessor1<'a, T> {
    /// Constructs an array accessor that wraps given array.
    pub fn new(data: &'a mut [T]) -> ArrayAccessor1<'a, T> {
        return ArrayAccessor1 {
            _data: Some(data)
        };
    }

    /// Resets the array.
    pub fn reset(&mut self, data: &'a mut [T]) {
        self._data = Some(data);
    }

    /// Returns the reference to the i-th element.
    pub fn at_mut(&mut self, i: usize) -> &mut T {
        debug_assert!(i < self._data.as_ref().unwrap().len());
        return &mut self._data.as_mut().unwrap()[i];
    }

    /// Returns the const reference to the i-th element.
    pub fn at(&self, i: usize) -> &T {
        debug_assert!(i < self._data.as_ref().unwrap().len());
        return &self._data.as_ref().unwrap()[i];
    }

    /// Returns size of the array.
    pub fn size(&self) -> usize {
        return self._data.as_ref().unwrap().len();
    }

    /// Returns the raw pointer to the array data.
    pub fn data(&mut self) -> &mut [T] {
        return self._data.as_mut().unwrap();
    }

    /// Swaps the content of with \p other array accessor.
    pub fn swap(&mut self, other: &mut ArrayAccessor1<'a, T>) {
        swap(&mut other._data, &mut self._data);
    }
}

impl<'a, T: ZeroInit> ArrayAccessor1<'a, T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each element.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be 0 to N-1 where N is the size of
    /// the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ArrayAccessor<int, 1> acc(6, data);
    /// acc.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, mut func: Callback) {
        for i in 0..self.size() {
            func(self.at(i));
        }
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes one parameter which is the
    /// index of the array. The order of execution will be 0 to N-1 where N is
    /// the size of the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ArrayAccessor<int, 1> acc(6, data);
    /// acc.for_each_index([&](size_t i) {
    ///     acc[i] = 4.f * i + 1.5f;
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize)>(&self, mut func: Callback) {
        for i in 0..self.size() {
            func(i);
        }
    }
}

impl<'a, T: ZeroInit> Index<usize> for ArrayAccessor1<'a, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data.as_ref().unwrap()[index];
    }
}

impl<'a, T: ZeroInit> IndexMut<usize> for ArrayAccessor1<'a, T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self._data.as_mut().unwrap()[index];
    }
}

//--------------------------------------------------------------------------------------------------
///
/// # 1-D read-only array accessor class.
///
/// This class represents 1-D read-only array accessor. Array accessor provides
/// array-like data read/write functions, but does not handle memory management.
/// Thus, it is more like a random access iterator, but with multi-dimension
/// support.
///
pub struct ConstArrayAccessor1<'a, T: ZeroInit> {
    _data: Option<&'a [T]>,
}

impl<'a, T: ZeroInit> Default for ConstArrayAccessor1<'a, T> {
    /// Constructs empty 1-D array accessor.
    fn default() -> Self {
        return ConstArrayAccessor1 {
            _data: None
        };
    }
}

impl<'a, T: ZeroInit> ConstArrayAccessor1<'a, T> {
    /// Constructs an read-only array accessor that wraps given array.
    pub fn new(data: &'a [T]) -> ConstArrayAccessor1<'a, T> {
        return ConstArrayAccessor1 {
            _data: Some(data)
        };
    }

    /// Returns the const reference to the i-th element.
    pub fn at(&self, i: usize) -> &T {
        debug_assert!(i < self._data.as_ref().unwrap().len());
        return &self._data.as_ref().unwrap()[i];
    }

    /// Returns size of the array.
    pub fn size(&self) -> usize {
        return self._data.as_ref().unwrap().len();
    }

    /// Returns the raw pointer to the array data.
    pub fn data(&self) -> &[T] {
        return self._data.as_ref().unwrap();
    }
}

impl<'a, T: ZeroInit> ConstArrayAccessor1<'a, T> {
    ///
    /// \brief Iterates the array and invoke given \p func for each element.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes array's element as its
    /// input. The order of execution will be 0 to N-1 where N is the size of
    /// the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ConstArrayAccessor<int, 1> acc(6, data);
    /// acc.for_each([](int elem) {
    ///     printf("%d\n", elem);
    /// });
    /// \endcode
    ///
    pub fn for_each<Callback: FnMut(&T)>(&self, mut func: Callback) {
        for i in 0..self.size() {
            func(self.at(i));
        }
    }

    ///
    /// \brief Iterates the array and invoke given \p func for each index.
    ///
    /// This function iterates the array elements and invoke the callback
    /// function \p func. The callback function takes one parameter which is the
    /// index of the array. The order of execution will be 0 to N-1 where N is
    /// the size of the array. Below is the sample usage:
    ///
    /// \code{.cpp}
    /// int data = {1, 2, 3, 4, 5, 6};
    /// ConstArrayAccessor<int, 1> acc(6, data);
    /// acc.for_each_index([&](size_t i) {
    ///     data[i] = acc[i] * acc[i];
    /// });
    /// \endcode
    ///
    pub fn for_each_index<Callback: FnMut(usize)>(&self, mut func: Callback) {
        for i in 0..self.size() {
            func(i);
        }
    }
}

impl<'a, T: ZeroInit> Index<usize> for ConstArrayAccessor1<'a, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        return &self._data.as_ref().unwrap()[index];
    }
}

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod array_accessor1 {
    use crate::array_accessor1::ArrayAccessor1;
    use crate::array1::Array1;

    #[test]
    fn constructors() {
        let mut data = [0.0; 5];
        for i in 0..5 {
            data[i] = i as f64;
        }

        let acc = ArrayAccessor1::new(&mut data);

        assert_eq!(5, acc.size());
    }

    #[test]
    fn iterators() {
        // let mut arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        // let mut acc = arr1.accessor();
        //
        // let mut i = 0;
        // for elem in acc.data() {
        //     assert_eq!(acc[i], *elem);
        //     i += 1;
        // }
    }

    #[test]
    fn for_each() {
        // let mut arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        // let acc = arr1.accessor();
        //
        // let mut i = 0;
        // acc.for_each(|val| {
        //     assert_eq!(arr1[i], *val);
        //     i += 1;
        // });
    }

    #[test]
    fn for_each_index() {
        let mut arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let acc = arr1.accessor();

        let mut cnt = 0;
        acc.for_each_index(|i| {
            assert_eq!(cnt, i);
            cnt += 1;
        });
    }
}

//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod const_array_accessor1 {
    use crate::array_accessor1::ConstArrayAccessor1;
    use crate::array1::Array1;

    #[test]
    fn constructors() {
        let mut data = [0.0; 5];
        for i in 0..5 {
            data[i] = i as f64;
        }

        // Construct with ArrayAccessor1
        let acc = ConstArrayAccessor1::new(&data);

        assert_eq!(5, acc.size());
    }

    #[test]
    fn iterators() {
        let arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let acc = arr1.const_accessor();

        let mut i = 0;
        for elem in acc.data() {
            assert_eq!(acc[i], *elem);
            i += 1;
        }
    }

    #[test]
    fn for_each() {
        let arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let acc = arr1.const_accessor();

        let mut i = 0;
        acc.for_each(|val| {
            assert_eq!(arr1[i], *val);
            i += 1;
        });
    }

    #[test]
    fn for_each_index() {
        let arr1 = Array1::new_slice(&[6.0, 4.0, 1.0, -5.0]);
        let acc = arr1.const_accessor();

        let mut cnt = 0;
        acc.for_each_index(|i| {
            assert_eq!(cnt, i);
            cnt += 1;
        });
    }
}